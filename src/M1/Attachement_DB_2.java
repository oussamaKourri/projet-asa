/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attachement DB 2</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Attachement_DB_2#getRole_requie_2 <em>Role requie 2</em>}</li>
 *   <li>{@link M1.Attachement_DB_2#getPort_fournie_DB_2 <em>Port fournie DB 2</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getAttachement_DB_2()
 * @model
 * @generated
 */
public interface Attachement_DB_2 extends EObject {
	/**
	 * Returns the value of the '<em><b>Role requie 2</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Role_Requie_DB_2#getAttachement_DB_2 <em>Attachement DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role requie 2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role requie 2</em>' reference.
	 * @see #setRole_requie_2(Role_Requie_DB_2)
	 * @see M1.M1Package#getAttachement_DB_2_Role_requie_2()
	 * @see M1.Role_Requie_DB_2#getAttachement_DB_2
	 * @model opposite="attachement_DB_2"
	 * @generated
	 */
	Role_Requie_DB_2 getRole_requie_2();

	/**
	 * Sets the value of the '{@link M1.Attachement_DB_2#getRole_requie_2 <em>Role requie 2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role requie 2</em>' reference.
	 * @see #getRole_requie_2()
	 * @generated
	 */
	void setRole_requie_2(Role_Requie_DB_2 value);

	/**
	 * Returns the value of the '<em><b>Port fournie DB 2</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Port_fournie_DB_2#getAttachement_DB_2 <em>Attachement DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port fournie DB 2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port fournie DB 2</em>' reference.
	 * @see #setPort_fournie_DB_2(Port_fournie_DB_2)
	 * @see M1.M1Package#getAttachement_DB_2_Port_fournie_DB_2()
	 * @see M1.Port_fournie_DB_2#getAttachement_DB_2
	 * @model opposite="attachement_DB_2"
	 * @generated
	 */
	Port_fournie_DB_2 getPort_fournie_DB_2();

	/**
	 * Sets the value of the '{@link M1.Attachement_DB_2#getPort_fournie_DB_2 <em>Port fournie DB 2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port fournie DB 2</em>' reference.
	 * @see #getPort_fournie_DB_2()
	 * @generated
	 */
	void setPort_fournie_DB_2(Port_fournie_DB_2 value);

} // Attachement_DB_2
