/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role fournie DB 3</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Role_fournie_DB_3#getAttachement_DB_3 <em>Attachement DB 3</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getRole_fournie_DB_3()
 * @model
 * @generated
 */
public interface Role_fournie_DB_3 extends EObject {
	/**
	 * Returns the value of the '<em><b>Attachement DB 3</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Attachement_DB_3#getRole_fourni_DB_3 <em>Role fourni DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attachement DB 3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attachement DB 3</em>' reference.
	 * @see #setAttachement_DB_3(Attachement_DB_3)
	 * @see M1.M1Package#getRole_fournie_DB_3_Attachement_DB_3()
	 * @see M1.Attachement_DB_3#getRole_fourni_DB_3
	 * @model opposite="role_fourni_DB_3"
	 * @generated
	 */
	Attachement_DB_3 getAttachement_DB_3();

	/**
	 * Sets the value of the '{@link M1.Role_fournie_DB_3#getAttachement_DB_3 <em>Attachement DB 3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attachement DB 3</em>' reference.
	 * @see #getAttachement_DB_3()
	 * @generated
	 */
	void setAttachement_DB_3(Attachement_DB_3 value);

} // Role_fournie_DB_3
