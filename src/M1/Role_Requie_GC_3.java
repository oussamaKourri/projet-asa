/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role Requie GC 3</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Role_Requie_GC_3#getEReference0 <em>EReference0</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getRole_Requie_GC_3()
 * @model
 * @generated
 */
public interface Role_Requie_GC_3 extends EObject {
	/**
	 * Returns the value of the '<em><b>EReference0</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Attachement_GC_3#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference0</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference0</em>' reference.
	 * @see #setEReference0(Attachement_GC_3)
	 * @see M1.M1Package#getRole_Requie_GC_3_EReference0()
	 * @see M1.Attachement_GC_3#getEReference0
	 * @model opposite="EReference0"
	 * @generated
	 */
	Attachement_GC_3 getEReference0();

	/**
	 * Sets the value of the '{@link M1.Role_Requie_GC_3#getEReference0 <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference0</em>' reference.
	 * @see #getEReference0()
	 * @generated
	 */
	void setEReference0(Attachement_GC_3 value);

} // Role_Requie_GC_3
