/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connecteur GC GS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Connecteur_GC_GS#getIconnecteur_GC_GS <em>Iconnecteur GC GS</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getConnecteur_GC_GS()
 * @model
 * @generated
 */
public interface Connecteur_GC_GS extends EObject {
	/**
	 * Returns the value of the '<em><b>Iconnecteur GC GS</b></em>' containment reference list.
	 * The list contents are of type {@link M1.IConnecteur_GC_GS}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iconnecteur GC GS</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iconnecteur GC GS</em>' containment reference list.
	 * @see M1.M1Package#getConnecteur_GC_GS_Iconnecteur_GC_GS()
	 * @model type="M1.IConnecteur_GC_GS" containment="true"
	 * @generated
	 */
	EList getIconnecteur_GC_GS();

} // Connecteur_GC_GS
