/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connecteur RPC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.ConnecteurRPC#getInterfaceConnecteur <em>Interface Connecteur</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getConnecteurRPC()
 * @model
 * @generated
 */
public interface ConnecteurRPC extends EObject {
	/**
	 * Returns the value of the '<em><b>Interface Connecteur</b></em>' containment reference list.
	 * The list contents are of type {@link M1.IConnecteurRPC}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface Connecteur</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface Connecteur</em>' containment reference list.
	 * @see M1.M1Package#getConnecteurRPC_InterfaceConnecteur()
	 * @model type="M1.IConnecteurRPC" containment="true"
	 * @generated
	 */
	EList getInterfaceConnecteur();

} // ConnecteurRPC
