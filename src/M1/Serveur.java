/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Serveur</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Serveur#getInterfaceServeur <em>Interface Serveur</em>}</li>
 *   <li>{@link M1.Serveur#getServeur_Config <em>Serveur Config</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getServeur()
 * @model
 * @generated
 */
public interface Serveur extends EObject {
	/**
	 * Returns the value of the '<em><b>Interface Serveur</b></em>' containment reference list.
	 * The list contents are of type {@link M1.IServeur}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface Serveur</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface Serveur</em>' containment reference list.
	 * @see M1.M1Package#getServeur_InterfaceServeur()
	 * @model type="M1.IServeur" containment="true"
	 * @generated
	 */
	EList getInterfaceServeur();

	/**
	 * Returns the value of the '<em><b>Serveur Config</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Server_Configuration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Serveur Config</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Serveur Config</em>' containment reference list.
	 * @see M1.M1Package#getServeur_Serveur_Config()
	 * @model type="M1.Server_Configuration" containment="true"
	 * @generated
	 */
	EList getServeur_Config();

} // Serveur
