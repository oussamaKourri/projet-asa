/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attachement Serveur RPC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.AttachementServeurRPC#getEReference0 <em>EReference0</em>}</li>
 *   <li>{@link M1.AttachementServeurRPC#getEReference1 <em>EReference1</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getAttachementServeurRPC()
 * @model
 * @generated
 */
public interface AttachementServeurRPC extends EObject {
	/**
	 * Returns the value of the '<em><b>EReference0</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.RoleFournie_SR#getAttachement_SR <em>Attachement SR</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference0</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference0</em>' reference.
	 * @see #setEReference0(RoleFournie_SR)
	 * @see M1.M1Package#getAttachementServeurRPC_EReference0()
	 * @see M1.RoleFournie_SR#getAttachement_SR
	 * @model opposite="attachement_SR"
	 * @generated
	 */
	RoleFournie_SR getEReference0();

	/**
	 * Sets the value of the '{@link M1.AttachementServeurRPC#getEReference0 <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference0</em>' reference.
	 * @see #getEReference0()
	 * @generated
	 */
	void setEReference0(RoleFournie_SR value);

	/**
	 * Returns the value of the '<em><b>EReference1</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.PortRequie_SR#getAttachement_SR <em>Attachement SR</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference1</em>' reference.
	 * @see #setEReference1(PortRequie_SR)
	 * @see M1.M1Package#getAttachementServeurRPC_EReference1()
	 * @see M1.PortRequie_SR#getAttachement_SR
	 * @model opposite="attachement_SR"
	 * @generated
	 */
	PortRequie_SR getEReference1();

	/**
	 * Sets the value of the '{@link M1.AttachementServeurRPC#getEReference1 <em>EReference1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference1</em>' reference.
	 * @see #getEReference1()
	 * @generated
	 */
	void setEReference1(PortRequie_SR value);

} // AttachementServeurRPC
