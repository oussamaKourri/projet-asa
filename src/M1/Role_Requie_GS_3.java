/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role Requie GS 3</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Role_Requie_GS_3#getAttachement_GS_3 <em>Attachement GS 3</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getRole_Requie_GS_3()
 * @model
 * @generated
 */
public interface Role_Requie_GS_3 extends EObject {
	/**
	 * Returns the value of the '<em><b>Attachement GS 3</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Attachement_GS_3#getRole_requie_GS_3 <em>Role requie GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attachement GS 3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attachement GS 3</em>' reference.
	 * @see #setAttachement_GS_3(Attachement_GS_3)
	 * @see M1.M1Package#getRole_Requie_GS_3_Attachement_GS_3()
	 * @see M1.Attachement_GS_3#getRole_requie_GS_3
	 * @model opposite="role_requie_GS_3"
	 * @generated
	 */
	Attachement_GS_3 getAttachement_GS_3();

	/**
	 * Sets the value of the '{@link M1.Role_Requie_GS_3#getAttachement_GS_3 <em>Attachement GS 3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attachement GS 3</em>' reference.
	 * @see #getAttachement_GS_3()
	 * @generated
	 */
	void setAttachement_GS_3(Attachement_GS_3 value);

} // Role_Requie_GS_3
