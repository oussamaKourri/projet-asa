/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Requie GS 2</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Port_Requie_GS_2#getAttachement_GS_2 <em>Attachement GS 2</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getPort_Requie_GS_2()
 * @model
 * @generated
 */
public interface Port_Requie_GS_2 extends EObject {
	/**
	 * Returns the value of the '<em><b>Attachement GS 2</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Attachement_GS_2#getPort_requie_GS_2 <em>Port requie GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attachement GS 2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attachement GS 2</em>' reference.
	 * @see #setAttachement_GS_2(Attachement_GS_2)
	 * @see M1.M1Package#getPort_Requie_GS_2_Attachement_GS_2()
	 * @see M1.Attachement_GS_2#getPort_requie_GS_2
	 * @model opposite="port_requie_GS_2"
	 * @generated
	 */
	Attachement_GS_2 getAttachement_GS_2();

	/**
	 * Sets the value of the '{@link M1.Port_Requie_GS_2#getAttachement_GS_2 <em>Attachement GS 2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attachement GS 2</em>' reference.
	 * @see #getAttachement_GS_2()
	 * @generated
	 */
	void setAttachement_GS_2(Attachement_GS_2 value);

} // Port_Requie_GS_2
