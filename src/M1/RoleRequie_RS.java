/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role Requie RS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.RoleRequie_RS#getAttachement_RS <em>Attachement RS</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getRoleRequie_RS()
 * @model
 * @generated
 */
public interface RoleRequie_RS extends EObject {
	/**
	 * Returns the value of the '<em><b>Attachement RS</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.AttachementRPCServeur#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attachement RS</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attachement RS</em>' reference.
	 * @see #setAttachement_RS(AttachementRPCServeur)
	 * @see M1.M1Package#getRoleRequie_RS_Attachement_RS()
	 * @see M1.AttachementRPCServeur#getEReference0
	 * @model opposite="EReference0"
	 * @generated
	 */
	AttachementRPCServeur getAttachement_RS();

	/**
	 * Sets the value of the '{@link M1.RoleRequie_RS#getAttachement_RS <em>Attachement RS</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attachement RS</em>' reference.
	 * @see #getAttachement_RS()
	 * @generated
	 */
	void setAttachement_RS(AttachementRPCServeur value);

} // RoleRequie_RS
