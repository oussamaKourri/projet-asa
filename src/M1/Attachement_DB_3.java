/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attachement DB 3</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Attachement_DB_3#getRole_fourni_DB_3 <em>Role fourni DB 3</em>}</li>
 *   <li>{@link M1.Attachement_DB_3#getPort_requie_DB_3 <em>Port requie DB 3</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getAttachement_DB_3()
 * @model
 * @generated
 */
public interface Attachement_DB_3 extends EObject {
	/**
	 * Returns the value of the '<em><b>Role fourni DB 3</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Role_fournie_DB_3#getAttachement_DB_3 <em>Attachement DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role fourni DB 3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role fourni DB 3</em>' reference.
	 * @see #setRole_fourni_DB_3(Role_fournie_DB_3)
	 * @see M1.M1Package#getAttachement_DB_3_Role_fourni_DB_3()
	 * @see M1.Role_fournie_DB_3#getAttachement_DB_3
	 * @model opposite="attachement_DB_3"
	 * @generated
	 */
	Role_fournie_DB_3 getRole_fourni_DB_3();

	/**
	 * Sets the value of the '{@link M1.Attachement_DB_3#getRole_fourni_DB_3 <em>Role fourni DB 3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role fourni DB 3</em>' reference.
	 * @see #getRole_fourni_DB_3()
	 * @generated
	 */
	void setRole_fourni_DB_3(Role_fournie_DB_3 value);

	/**
	 * Returns the value of the '<em><b>Port requie DB 3</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Port_requie_DB_3#getAttachement_DB_3 <em>Attachement DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port requie DB 3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port requie DB 3</em>' reference.
	 * @see #setPort_requie_DB_3(Port_requie_DB_3)
	 * @see M1.M1Package#getAttachement_DB_3_Port_requie_DB_3()
	 * @see M1.Port_requie_DB_3#getAttachement_DB_3
	 * @model opposite="attachement_DB_3"
	 * @generated
	 */
	Port_requie_DB_3 getPort_requie_DB_3();

	/**
	 * Sets the value of the '{@link M1.Attachement_DB_3#getPort_requie_DB_3 <em>Port requie DB 3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port requie DB 3</em>' reference.
	 * @see #getPort_requie_DB_3()
	 * @generated
	 */
	void setPort_requie_DB_3(Port_requie_DB_3 value);

} // Attachement_DB_3
