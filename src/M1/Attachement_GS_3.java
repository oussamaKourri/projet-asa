/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attachement GS 3</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Attachement_GS_3#getPort_fournie_GS_3 <em>Port fournie GS 3</em>}</li>
 *   <li>{@link M1.Attachement_GS_3#getRole_requie_GS_3 <em>Role requie GS 3</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getAttachement_GS_3()
 * @model
 * @generated
 */
public interface Attachement_GS_3 extends EObject {
	/**
	 * Returns the value of the '<em><b>Port fournie GS 3</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Port_Fournie_GS_3#getAttachement_GS_3 <em>Attachement GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port fournie GS 3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port fournie GS 3</em>' reference.
	 * @see #setPort_fournie_GS_3(Port_Fournie_GS_3)
	 * @see M1.M1Package#getAttachement_GS_3_Port_fournie_GS_3()
	 * @see M1.Port_Fournie_GS_3#getAttachement_GS_3
	 * @model opposite="attachement_GS_3"
	 * @generated
	 */
	Port_Fournie_GS_3 getPort_fournie_GS_3();

	/**
	 * Sets the value of the '{@link M1.Attachement_GS_3#getPort_fournie_GS_3 <em>Port fournie GS 3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port fournie GS 3</em>' reference.
	 * @see #getPort_fournie_GS_3()
	 * @generated
	 */
	void setPort_fournie_GS_3(Port_Fournie_GS_3 value);

	/**
	 * Returns the value of the '<em><b>Role requie GS 3</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Role_Requie_GS_3#getAttachement_GS_3 <em>Attachement GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role requie GS 3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role requie GS 3</em>' reference.
	 * @see #setRole_requie_GS_3(Role_Requie_GS_3)
	 * @see M1.M1Package#getAttachement_GS_3_Role_requie_GS_3()
	 * @see M1.Role_Requie_GS_3#getAttachement_GS_3
	 * @model opposite="attachement_GS_3"
	 * @generated
	 */
	Role_Requie_GS_3 getRole_requie_GS_3();

	/**
	 * Sets the value of the '{@link M1.Attachement_GS_3#getRole_requie_GS_3 <em>Role requie GS 3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role requie GS 3</em>' reference.
	 * @see #getRole_requie_GS_3()
	 * @generated
	 */
	void setRole_requie_GS_3(Role_Requie_GS_3 value);

} // Attachement_GS_3
