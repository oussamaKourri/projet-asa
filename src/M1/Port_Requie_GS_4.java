/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Requie GS 4</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Port_Requie_GS_4#getAttachement_GS_4 <em>Attachement GS 4</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getPort_Requie_GS_4()
 * @model
 * @generated
 */
public interface Port_Requie_GS_4 extends EObject {
	/**
	 * Returns the value of the '<em><b>Attachement GS 4</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Attachement_GS_4#getPort_requie_GS_4 <em>Port requie GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attachement GS 4</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attachement GS 4</em>' reference.
	 * @see #setAttachement_GS_4(Attachement_GS_4)
	 * @see M1.M1Package#getPort_Requie_GS_4_Attachement_GS_4()
	 * @see M1.Attachement_GS_4#getPort_requie_GS_4
	 * @model opposite="port_requie_GS_4"
	 * @generated
	 */
	Attachement_GS_4 getAttachement_GS_4();

	/**
	 * Sets the value of the '{@link M1.Port_Requie_GS_4#getAttachement_GS_4 <em>Attachement GS 4</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attachement GS 4</em>' reference.
	 * @see #getAttachement_GS_4()
	 * @generated
	 */
	void setAttachement_GS_4(Attachement_GS_4 value);

} // Port_Requie_GS_4
