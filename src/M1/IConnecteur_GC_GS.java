/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IConnecteur GC GS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.IConnecteur_GC_GS#getRole_requie_GC_3 <em>Role requie GC 3</em>}</li>
 *   <li>{@link M1.IConnecteur_GC_GS#getRole_fournie_GC_4 <em>Role fournie GC 4</em>}</li>
 *   <li>{@link M1.IConnecteur_GC_GS#getRole_requie_GS_1 <em>Role requie GS 1</em>}</li>
 *   <li>{@link M1.IConnecteur_GC_GS#getRole_fournie_GS_2 <em>Role fournie GS 2</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getIConnecteur_GC_GS()
 * @model
 * @generated
 */
public interface IConnecteur_GC_GS extends EObject {
	/**
	 * Returns the value of the '<em><b>Role requie GC 3</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Role_Requie_GC_3}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role requie GC 3</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role requie GC 3</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteur_GC_GS_Role_requie_GC_3()
	 * @model type="M1.Role_Requie_GC_3" containment="true"
	 * @generated
	 */
	EList getRole_requie_GC_3();

	/**
	 * Returns the value of the '<em><b>Role fournie GC 4</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Role_Fournie_GC_4}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role fournie GC 4</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role fournie GC 4</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteur_GC_GS_Role_fournie_GC_4()
	 * @model type="M1.Role_Fournie_GC_4" containment="true"
	 * @generated
	 */
	EList getRole_fournie_GC_4();

	/**
	 * Returns the value of the '<em><b>Role requie GS 1</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Role_Requie_GS_1}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role requie GS 1</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role requie GS 1</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteur_GC_GS_Role_requie_GS_1()
	 * @model type="M1.Role_Requie_GS_1" containment="true"
	 * @generated
	 */
	EList getRole_requie_GS_1();

	/**
	 * Returns the value of the '<em><b>Role fournie GS 2</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Role_Fournie_GS_2}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role fournie GS 2</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role fournie GS 2</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteur_GC_GS_Role_fournie_GS_2()
	 * @model type="M1.Role_Fournie_GS_2" containment="true"
	 * @generated
	 */
	EList getRole_fournie_GS_2();

} // IConnecteur_GC_GS
