/**
 */
package M1.util;

import M1.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see M1.M1Package
 * @generated
 */
public class M1AdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static M1Package modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public M1AdapterFactory() {
		if (modelPackage == null) {
			modelPackage = M1Package.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected M1Switch modelSwitch =
		new M1Switch() {
			public Object caseServer_Configuration(Server_Configuration object) {
				return createServer_ConfigurationAdapter();
			}
			public Object caseDataBase(DataBase object) {
				return createDataBaseAdapter();
			}
			public Object caseIDataBase(IDataBase object) {
				return createIDataBaseAdapter();
			}
			public Object caseAtachement_DB_1(Atachement_DB_1 object) {
				return createAtachement_DB_1Adapter();
			}
			public Object caseAttachement_DB_2(Attachement_DB_2 object) {
				return createAttachement_DB_2Adapter();
			}
			public Object caseAttachement_DB_3(Attachement_DB_3 object) {
				return createAttachement_DB_3Adapter();
			}
			public Object caseAttachement_DB_4(Attachement_DB_4 object) {
				return createAttachement_DB_4Adapter();
			}
			public Object caseRole_Requie_DB_4(Role_Requie_DB_4 object) {
				return createRole_Requie_DB_4Adapter();
			}
			public Object casePort_fornie_DB_4(Port_fornie_DB_4 object) {
				return createPort_fornie_DB_4Adapter();
			}
			public Object caseRole_fournie_DB_3(Role_fournie_DB_3 object) {
				return createRole_fournie_DB_3Adapter();
			}
			public Object casePort_requie_DB_3(Port_requie_DB_3 object) {
				return createPort_requie_DB_3Adapter();
			}
			public Object caseRole_Requie_DB_2(Role_Requie_DB_2 object) {
				return createRole_Requie_DB_2Adapter();
			}
			public Object caseRole_fournie_DB_1(Role_fournie_DB_1 object) {
				return createRole_fournie_DB_1Adapter();
			}
			public Object casePort_Requie_DB_1(Port_Requie_DB_1 object) {
				return createPort_Requie_DB_1Adapter();
			}
			public Object casePort_fournie_DB_2(Port_fournie_DB_2 object) {
				return createPort_fournie_DB_2Adapter();
			}
			public Object caseGestionnaire_Connexion(Gestionnaire_Connexion object) {
				return createGestionnaire_ConnexionAdapter();
			}
			public Object caseIGestionnaire_Connexion(IGestionnaire_Connexion object) {
				return createIGestionnaire_ConnexionAdapter();
			}
			public Object caseAttachement_GC_2(Attachement_GC_2 object) {
				return createAttachement_GC_2Adapter();
			}
			public Object caseAttachement_GC_3(Attachement_GC_3 object) {
				return createAttachement_GC_3Adapter();
			}
			public Object caseAttachement_GC_4(Attachement_GC_4 object) {
				return createAttachement_GC_4Adapter();
			}
			public Object caseAttachement_GC_1(Attachement_GC_1 object) {
				return createAttachement_GC_1Adapter();
			}
			public Object caseRole_Requie_GC_3(Role_Requie_GC_3 object) {
				return createRole_Requie_GC_3Adapter();
			}
			public Object casePort_Requie_GC_4(Port_Requie_GC_4 object) {
				return createPort_Requie_GC_4Adapter();
			}
			public Object casePort_Fournie_GC_3(Port_Fournie_GC_3 object) {
				return createPort_Fournie_GC_3Adapter();
			}
			public Object caseRole_Fournie_GC_4(Role_Fournie_GC_4 object) {
				return createRole_Fournie_GC_4Adapter();
			}
			public Object caseRole_Fournie_GC_2(Role_Fournie_GC_2 object) {
				return createRole_Fournie_GC_2Adapter();
			}
			public Object casePort_Requie_GC_2(Port_Requie_GC_2 object) {
				return createPort_Requie_GC_2Adapter();
			}
			public Object caseRole_Requie_GC_1(Role_Requie_GC_1 object) {
				return createRole_Requie_GC_1Adapter();
			}
			public Object casePort_Fournie_GC_1(Port_Fournie_GC_1 object) {
				return createPort_Fournie_GC_1Adapter();
			}
			public Object caseIConnecteur_GC_DB(IConnecteur_GC_DB object) {
				return createIConnecteur_GC_DBAdapter();
			}
			public Object caseConnecteur_GC_DB(Connecteur_GC_DB object) {
				return createConnecteur_GC_DBAdapter();
			}
			public Object caseGestion_Securiterity(Gestion_Securiterity object) {
				return createGestion_SecuriterityAdapter();
			}
			public Object caseAttachement_GS_1(Attachement_GS_1 object) {
				return createAttachement_GS_1Adapter();
			}
			public Object casePort_Requie_GS_2(Port_Requie_GS_2 object) {
				return createPort_Requie_GS_2Adapter();
			}
			public Object caseAttachement_GS_2(Attachement_GS_2 object) {
				return createAttachement_GS_2Adapter();
			}
			public Object caseAttachement_GS_3(Attachement_GS_3 object) {
				return createAttachement_GS_3Adapter();
			}
			public Object casePort_Fournie_GS_1(Port_Fournie_GS_1 object) {
				return createPort_Fournie_GS_1Adapter();
			}
			public Object caseRole_Requie_GS_1(Role_Requie_GS_1 object) {
				return createRole_Requie_GS_1Adapter();
			}
			public Object caseRole_Fournie_GS_2(Role_Fournie_GS_2 object) {
				return createRole_Fournie_GS_2Adapter();
			}
			public Object casePort_Fournie_GS_3(Port_Fournie_GS_3 object) {
				return createPort_Fournie_GS_3Adapter();
			}
			public Object caseRole_Requie_GS_3(Role_Requie_GS_3 object) {
				return createRole_Requie_GS_3Adapter();
			}
			public Object caseAttachement_GS_4(Attachement_GS_4 object) {
				return createAttachement_GS_4Adapter();
			}
			public Object casePort_Requie_GS_4(Port_Requie_GS_4 object) {
				return createPort_Requie_GS_4Adapter();
			}
			public Object caseRole_Fournie_GS_4(Role_Fournie_GS_4 object) {
				return createRole_Fournie_GS_4Adapter();
			}
			public Object caseIGestion_Securite(IGestion_Securite object) {
				return createIGestion_SecuriteAdapter();
			}
			public Object caseConnecteur_GC_GS(Connecteur_GC_GS object) {
				return createConnecteur_GC_GSAdapter();
			}
			public Object caseIConnecteur_GC_GS(IConnecteur_GC_GS object) {
				return createIConnecteur_GC_GSAdapter();
			}
			public Object caseIConnecteur_GS_DB(IConnecteur_GS_DB object) {
				return createIConnecteur_GS_DBAdapter();
			}
			public Object caseConnecteur_GS_DB(Connecteur_GS_DB object) {
				return createConnecteur_GS_DBAdapter();
			}
			public Object caseClientServeur(ClientServeur object) {
				return createClientServeurAdapter();
			}
			public Object caseClient(Client object) {
				return createClientAdapter();
			}
			public Object caseConnecteurRPC(ConnecteurRPC object) {
				return createConnecteurRPCAdapter();
			}
			public Object caseServeur(Serveur object) {
				return createServeurAdapter();
			}
			public Object caseIClient(IClient object) {
				return createIClientAdapter();
			}
			public Object caseIConnecteurRPC(IConnecteurRPC object) {
				return createIConnecteurRPCAdapter();
			}
			public Object caseIServeur(IServeur object) {
				return createIServeurAdapter();
			}
			public Object caseSendRequest(SendRequest object) {
				return createSendRequestAdapter();
			}
			public Object caseAttachementClientRPC(AttachementClientRPC object) {
				return createAttachementClientRPCAdapter();
			}
			public Object caseRoleRequie_CR(RoleRequie_CR object) {
				return createRoleRequie_CRAdapter();
			}
			public Object caseAttachementRPCClient(AttachementRPCClient object) {
				return createAttachementRPCClientAdapter();
			}
			public Object caseRoleFournie_RC(RoleFournie_RC object) {
				return createRoleFournie_RCAdapter();
			}
			public Object caseReceveResponse(ReceveResponse object) {
				return createReceveResponseAdapter();
			}
			public Object caseAttachementRPCServeur(AttachementRPCServeur object) {
				return createAttachementRPCServeurAdapter();
			}
			public Object caseRoleRequie_RS(RoleRequie_RS object) {
				return createRoleRequie_RSAdapter();
			}
			public Object casePortFournie_RS(PortFournie_RS object) {
				return createPortFournie_RSAdapter();
			}
			public Object caseAttachementServeurRPC(AttachementServeurRPC object) {
				return createAttachementServeurRPCAdapter();
			}
			public Object caseRoleFournie_SR(RoleFournie_SR object) {
				return createRoleFournie_SRAdapter();
			}
			public Object casePortRequie_SR(PortRequie_SR object) {
				return createPortRequie_SRAdapter();
			}
			public Object caseBinding_S_RPC(Binding_S_RPC object) {
				return createBinding_S_RPCAdapter();
			}
			public Object casePort_RPC_S(Port_RPC_S object) {
				return createPort_RPC_SAdapter();
			}
			public Object casePort_S_RPC(Port_S_RPC object) {
				return createPort_S_RPCAdapter();
			}
			public Object caseBinding_CM_S(Binding_CM_S object) {
				return createBinding_CM_SAdapter();
			}
			public Object casePort_S_CM(Port_S_CM object) {
				return createPort_S_CMAdapter();
			}
			public Object casePort_CM_S(Port_CM_S object) {
				return createPort_CM_SAdapter();
			}
			public Object defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter)modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link M1.Server_Configuration <em>Server Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Server_Configuration
	 * @generated
	 */
	public Adapter createServer_ConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.DataBase <em>Data Base</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.DataBase
	 * @generated
	 */
	public Adapter createDataBaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.IDataBase <em>IData Base</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.IDataBase
	 * @generated
	 */
	public Adapter createIDataBaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Atachement_DB_1 <em>Atachement DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Atachement_DB_1
	 * @generated
	 */
	public Adapter createAtachement_DB_1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Attachement_DB_2 <em>Attachement DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Attachement_DB_2
	 * @generated
	 */
	public Adapter createAttachement_DB_2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Attachement_DB_3 <em>Attachement DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Attachement_DB_3
	 * @generated
	 */
	public Adapter createAttachement_DB_3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Attachement_DB_4 <em>Attachement DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Attachement_DB_4
	 * @generated
	 */
	public Adapter createAttachement_DB_4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_Requie_DB_4 <em>Role Requie DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_Requie_DB_4
	 * @generated
	 */
	public Adapter createRole_Requie_DB_4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_fornie_DB_4 <em>Port fornie DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_fornie_DB_4
	 * @generated
	 */
	public Adapter createPort_fornie_DB_4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_fournie_DB_3 <em>Role fournie DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_fournie_DB_3
	 * @generated
	 */
	public Adapter createRole_fournie_DB_3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_requie_DB_3 <em>Port requie DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_requie_DB_3
	 * @generated
	 */
	public Adapter createPort_requie_DB_3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_Requie_DB_2 <em>Role Requie DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_Requie_DB_2
	 * @generated
	 */
	public Adapter createRole_Requie_DB_2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_fournie_DB_1 <em>Role fournie DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_fournie_DB_1
	 * @generated
	 */
	public Adapter createRole_fournie_DB_1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_Requie_DB_1 <em>Port Requie DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_Requie_DB_1
	 * @generated
	 */
	public Adapter createPort_Requie_DB_1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_fournie_DB_2 <em>Port fournie DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_fournie_DB_2
	 * @generated
	 */
	public Adapter createPort_fournie_DB_2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Gestionnaire_Connexion <em>Gestionnaire Connexion</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Gestionnaire_Connexion
	 * @generated
	 */
	public Adapter createGestionnaire_ConnexionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.IGestionnaire_Connexion <em>IGestionnaire Connexion</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.IGestionnaire_Connexion
	 * @generated
	 */
	public Adapter createIGestionnaire_ConnexionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Attachement_GC_2 <em>Attachement GC 2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Attachement_GC_2
	 * @generated
	 */
	public Adapter createAttachement_GC_2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Attachement_GC_3 <em>Attachement GC 3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Attachement_GC_3
	 * @generated
	 */
	public Adapter createAttachement_GC_3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Attachement_GC_4 <em>Attachement GC 4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Attachement_GC_4
	 * @generated
	 */
	public Adapter createAttachement_GC_4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Attachement_GC_1 <em>Attachement GC 1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Attachement_GC_1
	 * @generated
	 */
	public Adapter createAttachement_GC_1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_Requie_GC_3 <em>Role Requie GC 3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_Requie_GC_3
	 * @generated
	 */
	public Adapter createRole_Requie_GC_3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_Requie_GC_4 <em>Port Requie GC 4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_Requie_GC_4
	 * @generated
	 */
	public Adapter createPort_Requie_GC_4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_Fournie_GC_3 <em>Port Fournie GC 3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_Fournie_GC_3
	 * @generated
	 */
	public Adapter createPort_Fournie_GC_3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_Fournie_GC_4 <em>Role Fournie GC 4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_Fournie_GC_4
	 * @generated
	 */
	public Adapter createRole_Fournie_GC_4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_Fournie_GC_2 <em>Role Fournie GC 2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_Fournie_GC_2
	 * @generated
	 */
	public Adapter createRole_Fournie_GC_2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_Requie_GC_2 <em>Port Requie GC 2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_Requie_GC_2
	 * @generated
	 */
	public Adapter createPort_Requie_GC_2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_Requie_GC_1 <em>Role Requie GC 1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_Requie_GC_1
	 * @generated
	 */
	public Adapter createRole_Requie_GC_1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_Fournie_GC_1 <em>Port Fournie GC 1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_Fournie_GC_1
	 * @generated
	 */
	public Adapter createPort_Fournie_GC_1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.IConnecteur_GC_DB <em>IConnecteur GC DB</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.IConnecteur_GC_DB
	 * @generated
	 */
	public Adapter createIConnecteur_GC_DBAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Connecteur_GC_DB <em>Connecteur GC DB</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Connecteur_GC_DB
	 * @generated
	 */
	public Adapter createConnecteur_GC_DBAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Gestion_Securiterity <em>Gestion Securiterity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Gestion_Securiterity
	 * @generated
	 */
	public Adapter createGestion_SecuriterityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Attachement_GS_1 <em>Attachement GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Attachement_GS_1
	 * @generated
	 */
	public Adapter createAttachement_GS_1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_Requie_GS_2 <em>Port Requie GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_Requie_GS_2
	 * @generated
	 */
	public Adapter createPort_Requie_GS_2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Attachement_GS_2 <em>Attachement GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Attachement_GS_2
	 * @generated
	 */
	public Adapter createAttachement_GS_2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Attachement_GS_3 <em>Attachement GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Attachement_GS_3
	 * @generated
	 */
	public Adapter createAttachement_GS_3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_Fournie_GS_1 <em>Port Fournie GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_Fournie_GS_1
	 * @generated
	 */
	public Adapter createPort_Fournie_GS_1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_Requie_GS_1 <em>Role Requie GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_Requie_GS_1
	 * @generated
	 */
	public Adapter createRole_Requie_GS_1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_Fournie_GS_2 <em>Role Fournie GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_Fournie_GS_2
	 * @generated
	 */
	public Adapter createRole_Fournie_GS_2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_Fournie_GS_3 <em>Port Fournie GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_Fournie_GS_3
	 * @generated
	 */
	public Adapter createPort_Fournie_GS_3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_Requie_GS_3 <em>Role Requie GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_Requie_GS_3
	 * @generated
	 */
	public Adapter createRole_Requie_GS_3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Attachement_GS_4 <em>Attachement GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Attachement_GS_4
	 * @generated
	 */
	public Adapter createAttachement_GS_4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_Requie_GS_4 <em>Port Requie GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_Requie_GS_4
	 * @generated
	 */
	public Adapter createPort_Requie_GS_4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Role_Fournie_GS_4 <em>Role Fournie GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Role_Fournie_GS_4
	 * @generated
	 */
	public Adapter createRole_Fournie_GS_4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.IGestion_Securite <em>IGestion Securite</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.IGestion_Securite
	 * @generated
	 */
	public Adapter createIGestion_SecuriteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Connecteur_GC_GS <em>Connecteur GC GS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Connecteur_GC_GS
	 * @generated
	 */
	public Adapter createConnecteur_GC_GSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.IConnecteur_GC_GS <em>IConnecteur GC GS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.IConnecteur_GC_GS
	 * @generated
	 */
	public Adapter createIConnecteur_GC_GSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.IConnecteur_GS_DB <em>IConnecteur GS DB</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.IConnecteur_GS_DB
	 * @generated
	 */
	public Adapter createIConnecteur_GS_DBAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Connecteur_GS_DB <em>Connecteur GS DB</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Connecteur_GS_DB
	 * @generated
	 */
	public Adapter createConnecteur_GS_DBAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.ClientServeur <em>Client Serveur</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.ClientServeur
	 * @generated
	 */
	public Adapter createClientServeurAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Client <em>Client</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Client
	 * @generated
	 */
	public Adapter createClientAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.ConnecteurRPC <em>Connecteur RPC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.ConnecteurRPC
	 * @generated
	 */
	public Adapter createConnecteurRPCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Serveur <em>Serveur</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Serveur
	 * @generated
	 */
	public Adapter createServeurAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.IClient <em>IClient</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.IClient
	 * @generated
	 */
	public Adapter createIClientAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.IConnecteurRPC <em>IConnecteur RPC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.IConnecteurRPC
	 * @generated
	 */
	public Adapter createIConnecteurRPCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.IServeur <em>IServeur</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.IServeur
	 * @generated
	 */
	public Adapter createIServeurAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.SendRequest <em>Send Request</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.SendRequest
	 * @generated
	 */
	public Adapter createSendRequestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.AttachementClientRPC <em>Attachement Client RPC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.AttachementClientRPC
	 * @generated
	 */
	public Adapter createAttachementClientRPCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.RoleRequie_CR <em>Role Requie CR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.RoleRequie_CR
	 * @generated
	 */
	public Adapter createRoleRequie_CRAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.AttachementRPCClient <em>Attachement RPC Client</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.AttachementRPCClient
	 * @generated
	 */
	public Adapter createAttachementRPCClientAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.RoleFournie_RC <em>Role Fournie RC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.RoleFournie_RC
	 * @generated
	 */
	public Adapter createRoleFournie_RCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.ReceveResponse <em>Receve Response</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.ReceveResponse
	 * @generated
	 */
	public Adapter createReceveResponseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.AttachementRPCServeur <em>Attachement RPC Serveur</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.AttachementRPCServeur
	 * @generated
	 */
	public Adapter createAttachementRPCServeurAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.RoleRequie_RS <em>Role Requie RS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.RoleRequie_RS
	 * @generated
	 */
	public Adapter createRoleRequie_RSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.PortFournie_RS <em>Port Fournie RS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.PortFournie_RS
	 * @generated
	 */
	public Adapter createPortFournie_RSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.AttachementServeurRPC <em>Attachement Serveur RPC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.AttachementServeurRPC
	 * @generated
	 */
	public Adapter createAttachementServeurRPCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.RoleFournie_SR <em>Role Fournie SR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.RoleFournie_SR
	 * @generated
	 */
	public Adapter createRoleFournie_SRAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.PortRequie_SR <em>Port Requie SR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.PortRequie_SR
	 * @generated
	 */
	public Adapter createPortRequie_SRAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Binding_S_RPC <em>Binding SRPC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Binding_S_RPC
	 * @generated
	 */
	public Adapter createBinding_S_RPCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_RPC_S <em>Port RPC S</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_RPC_S
	 * @generated
	 */
	public Adapter createPort_RPC_SAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_S_RPC <em>Port SRPC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_S_RPC
	 * @generated
	 */
	public Adapter createPort_S_RPCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Binding_CM_S <em>Binding CM S</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Binding_CM_S
	 * @generated
	 */
	public Adapter createBinding_CM_SAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_S_CM <em>Port SCM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_S_CM
	 * @generated
	 */
	public Adapter createPort_S_CMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link M1.Port_CM_S <em>Port CM S</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see M1.Port_CM_S
	 * @generated
	 */
	public Adapter createPort_CM_SAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //M1AdapterFactory
