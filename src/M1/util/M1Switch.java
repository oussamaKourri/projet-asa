/**
 */
package M1.util;

import M1.*;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see M1.M1Package
 * @generated
 */
public class M1Switch {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static M1Package modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public M1Switch() {
		if (modelPackage == null) {
			modelPackage = M1Package.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public Object doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch((EClass)eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case M1Package.SERVER_CONFIGURATION: {
				Server_Configuration server_Configuration = (Server_Configuration)theEObject;
				Object result = caseServer_Configuration(server_Configuration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.DATA_BASE: {
				DataBase dataBase = (DataBase)theEObject;
				Object result = caseDataBase(dataBase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.IDATA_BASE: {
				IDataBase iDataBase = (IDataBase)theEObject;
				Object result = caseIDataBase(iDataBase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATACHEMENT_DB_1: {
				Atachement_DB_1 atachement_DB_1 = (Atachement_DB_1)theEObject;
				Object result = caseAtachement_DB_1(atachement_DB_1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_DB_2: {
				Attachement_DB_2 attachement_DB_2 = (Attachement_DB_2)theEObject;
				Object result = caseAttachement_DB_2(attachement_DB_2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_DB_3: {
				Attachement_DB_3 attachement_DB_3 = (Attachement_DB_3)theEObject;
				Object result = caseAttachement_DB_3(attachement_DB_3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_DB_4: {
				Attachement_DB_4 attachement_DB_4 = (Attachement_DB_4)theEObject;
				Object result = caseAttachement_DB_4(attachement_DB_4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_REQUIE_DB_4: {
				Role_Requie_DB_4 role_Requie_DB_4 = (Role_Requie_DB_4)theEObject;
				Object result = caseRole_Requie_DB_4(role_Requie_DB_4);
				if (result == null) result = caseAttachement_DB_4(role_Requie_DB_4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_FORNIE_DB_4: {
				Port_fornie_DB_4 port_fornie_DB_4 = (Port_fornie_DB_4)theEObject;
				Object result = casePort_fornie_DB_4(port_fornie_DB_4);
				if (result == null) result = caseAttachement_DB_4(port_fornie_DB_4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_FOURNIE_DB_3: {
				Role_fournie_DB_3 role_fournie_DB_3 = (Role_fournie_DB_3)theEObject;
				Object result = caseRole_fournie_DB_3(role_fournie_DB_3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_REQUIE_DB_3: {
				Port_requie_DB_3 port_requie_DB_3 = (Port_requie_DB_3)theEObject;
				Object result = casePort_requie_DB_3(port_requie_DB_3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_REQUIE_DB_2: {
				Role_Requie_DB_2 role_Requie_DB_2 = (Role_Requie_DB_2)theEObject;
				Object result = caseRole_Requie_DB_2(role_Requie_DB_2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_FOURNIE_DB_1: {
				Role_fournie_DB_1 role_fournie_DB_1 = (Role_fournie_DB_1)theEObject;
				Object result = caseRole_fournie_DB_1(role_fournie_DB_1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_REQUIE_DB_1: {
				Port_Requie_DB_1 port_Requie_DB_1 = (Port_Requie_DB_1)theEObject;
				Object result = casePort_Requie_DB_1(port_Requie_DB_1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_FOURNIE_DB_2: {
				Port_fournie_DB_2 port_fournie_DB_2 = (Port_fournie_DB_2)theEObject;
				Object result = casePort_fournie_DB_2(port_fournie_DB_2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.GESTIONNAIRE_CONNEXION: {
				Gestionnaire_Connexion gestionnaire_Connexion = (Gestionnaire_Connexion)theEObject;
				Object result = caseGestionnaire_Connexion(gestionnaire_Connexion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.IGESTIONNAIRE_CONNEXION: {
				IGestionnaire_Connexion iGestionnaire_Connexion = (IGestionnaire_Connexion)theEObject;
				Object result = caseIGestionnaire_Connexion(iGestionnaire_Connexion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_GC_2: {
				Attachement_GC_2 attachement_GC_2 = (Attachement_GC_2)theEObject;
				Object result = caseAttachement_GC_2(attachement_GC_2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_GC_3: {
				Attachement_GC_3 attachement_GC_3 = (Attachement_GC_3)theEObject;
				Object result = caseAttachement_GC_3(attachement_GC_3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_GC_4: {
				Attachement_GC_4 attachement_GC_4 = (Attachement_GC_4)theEObject;
				Object result = caseAttachement_GC_4(attachement_GC_4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_GC_1: {
				Attachement_GC_1 attachement_GC_1 = (Attachement_GC_1)theEObject;
				Object result = caseAttachement_GC_1(attachement_GC_1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_REQUIE_GC_3: {
				Role_Requie_GC_3 role_Requie_GC_3 = (Role_Requie_GC_3)theEObject;
				Object result = caseRole_Requie_GC_3(role_Requie_GC_3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_REQUIE_GC_4: {
				Port_Requie_GC_4 port_Requie_GC_4 = (Port_Requie_GC_4)theEObject;
				Object result = casePort_Requie_GC_4(port_Requie_GC_4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_FOURNIE_GC_3: {
				Port_Fournie_GC_3 port_Fournie_GC_3 = (Port_Fournie_GC_3)theEObject;
				Object result = casePort_Fournie_GC_3(port_Fournie_GC_3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_FOURNIE_GC_4: {
				Role_Fournie_GC_4 role_Fournie_GC_4 = (Role_Fournie_GC_4)theEObject;
				Object result = caseRole_Fournie_GC_4(role_Fournie_GC_4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_FOURNIE_GC_2: {
				Role_Fournie_GC_2 role_Fournie_GC_2 = (Role_Fournie_GC_2)theEObject;
				Object result = caseRole_Fournie_GC_2(role_Fournie_GC_2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_REQUIE_GC_2: {
				Port_Requie_GC_2 port_Requie_GC_2 = (Port_Requie_GC_2)theEObject;
				Object result = casePort_Requie_GC_2(port_Requie_GC_2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_REQUIE_GC_1: {
				Role_Requie_GC_1 role_Requie_GC_1 = (Role_Requie_GC_1)theEObject;
				Object result = caseRole_Requie_GC_1(role_Requie_GC_1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_FOURNIE_GC_1: {
				Port_Fournie_GC_1 port_Fournie_GC_1 = (Port_Fournie_GC_1)theEObject;
				Object result = casePort_Fournie_GC_1(port_Fournie_GC_1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ICONNECTEUR_GC_DB: {
				IConnecteur_GC_DB iConnecteur_GC_DB = (IConnecteur_GC_DB)theEObject;
				Object result = caseIConnecteur_GC_DB(iConnecteur_GC_DB);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.CONNECTEUR_GC_DB: {
				Connecteur_GC_DB connecteur_GC_DB = (Connecteur_GC_DB)theEObject;
				Object result = caseConnecteur_GC_DB(connecteur_GC_DB);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.GESTION_SECURITERITY: {
				Gestion_Securiterity gestion_Securiterity = (Gestion_Securiterity)theEObject;
				Object result = caseGestion_Securiterity(gestion_Securiterity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_GS_1: {
				Attachement_GS_1 attachement_GS_1 = (Attachement_GS_1)theEObject;
				Object result = caseAttachement_GS_1(attachement_GS_1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_REQUIE_GS_2: {
				Port_Requie_GS_2 port_Requie_GS_2 = (Port_Requie_GS_2)theEObject;
				Object result = casePort_Requie_GS_2(port_Requie_GS_2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_GS_2: {
				Attachement_GS_2 attachement_GS_2 = (Attachement_GS_2)theEObject;
				Object result = caseAttachement_GS_2(attachement_GS_2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_GS_3: {
				Attachement_GS_3 attachement_GS_3 = (Attachement_GS_3)theEObject;
				Object result = caseAttachement_GS_3(attachement_GS_3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_FOURNIE_GS_1: {
				Port_Fournie_GS_1 port_Fournie_GS_1 = (Port_Fournie_GS_1)theEObject;
				Object result = casePort_Fournie_GS_1(port_Fournie_GS_1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_REQUIE_GS_1: {
				Role_Requie_GS_1 role_Requie_GS_1 = (Role_Requie_GS_1)theEObject;
				Object result = caseRole_Requie_GS_1(role_Requie_GS_1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_FOURNIE_GS_2: {
				Role_Fournie_GS_2 role_Fournie_GS_2 = (Role_Fournie_GS_2)theEObject;
				Object result = caseRole_Fournie_GS_2(role_Fournie_GS_2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_FOURNIE_GS_3: {
				Port_Fournie_GS_3 port_Fournie_GS_3 = (Port_Fournie_GS_3)theEObject;
				Object result = casePort_Fournie_GS_3(port_Fournie_GS_3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_REQUIE_GS_3: {
				Role_Requie_GS_3 role_Requie_GS_3 = (Role_Requie_GS_3)theEObject;
				Object result = caseRole_Requie_GS_3(role_Requie_GS_3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_GS_4: {
				Attachement_GS_4 attachement_GS_4 = (Attachement_GS_4)theEObject;
				Object result = caseAttachement_GS_4(attachement_GS_4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_REQUIE_GS_4: {
				Port_Requie_GS_4 port_Requie_GS_4 = (Port_Requie_GS_4)theEObject;
				Object result = casePort_Requie_GS_4(port_Requie_GS_4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_FOURNIE_GS_4: {
				Role_Fournie_GS_4 role_Fournie_GS_4 = (Role_Fournie_GS_4)theEObject;
				Object result = caseRole_Fournie_GS_4(role_Fournie_GS_4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.IGESTION_SECURITE: {
				IGestion_Securite iGestion_Securite = (IGestion_Securite)theEObject;
				Object result = caseIGestion_Securite(iGestion_Securite);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.CONNECTEUR_GC_GS: {
				Connecteur_GC_GS connecteur_GC_GS = (Connecteur_GC_GS)theEObject;
				Object result = caseConnecteur_GC_GS(connecteur_GC_GS);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ICONNECTEUR_GC_GS: {
				IConnecteur_GC_GS iConnecteur_GC_GS = (IConnecteur_GC_GS)theEObject;
				Object result = caseIConnecteur_GC_GS(iConnecteur_GC_GS);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ICONNECTEUR_GS_DB: {
				IConnecteur_GS_DB iConnecteur_GS_DB = (IConnecteur_GS_DB)theEObject;
				Object result = caseIConnecteur_GS_DB(iConnecteur_GS_DB);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.CONNECTEUR_GS_DB: {
				Connecteur_GS_DB connecteur_GS_DB = (Connecteur_GS_DB)theEObject;
				Object result = caseConnecteur_GS_DB(connecteur_GS_DB);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.CLIENT_SERVEUR: {
				ClientServeur clientServeur = (ClientServeur)theEObject;
				Object result = caseClientServeur(clientServeur);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.CLIENT: {
				Client client = (Client)theEObject;
				Object result = caseClient(client);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.CONNECTEUR_RPC: {
				ConnecteurRPC connecteurRPC = (ConnecteurRPC)theEObject;
				Object result = caseConnecteurRPC(connecteurRPC);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.SERVEUR: {
				Serveur serveur = (Serveur)theEObject;
				Object result = caseServeur(serveur);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ICLIENT: {
				IClient iClient = (IClient)theEObject;
				Object result = caseIClient(iClient);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ICONNECTEUR_RPC: {
				IConnecteurRPC iConnecteurRPC = (IConnecteurRPC)theEObject;
				Object result = caseIConnecteurRPC(iConnecteurRPC);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ISERVEUR: {
				IServeur iServeur = (IServeur)theEObject;
				Object result = caseIServeur(iServeur);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.SEND_REQUEST: {
				SendRequest sendRequest = (SendRequest)theEObject;
				Object result = caseSendRequest(sendRequest);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_CLIENT_RPC: {
				AttachementClientRPC attachementClientRPC = (AttachementClientRPC)theEObject;
				Object result = caseAttachementClientRPC(attachementClientRPC);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_REQUIE_CR: {
				RoleRequie_CR roleRequie_CR = (RoleRequie_CR)theEObject;
				Object result = caseRoleRequie_CR(roleRequie_CR);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_RPC_CLIENT: {
				AttachementRPCClient attachementRPCClient = (AttachementRPCClient)theEObject;
				Object result = caseAttachementRPCClient(attachementRPCClient);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_FOURNIE_RC: {
				RoleFournie_RC roleFournie_RC = (RoleFournie_RC)theEObject;
				Object result = caseRoleFournie_RC(roleFournie_RC);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.RECEVE_RESPONSE: {
				ReceveResponse receveResponse = (ReceveResponse)theEObject;
				Object result = caseReceveResponse(receveResponse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_RPC_SERVEUR: {
				AttachementRPCServeur attachementRPCServeur = (AttachementRPCServeur)theEObject;
				Object result = caseAttachementRPCServeur(attachementRPCServeur);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_REQUIE_RS: {
				RoleRequie_RS roleRequie_RS = (RoleRequie_RS)theEObject;
				Object result = caseRoleRequie_RS(roleRequie_RS);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_FOURNIE_RS: {
				PortFournie_RS portFournie_RS = (PortFournie_RS)theEObject;
				Object result = casePortFournie_RS(portFournie_RS);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ATTACHEMENT_SERVEUR_RPC: {
				AttachementServeurRPC attachementServeurRPC = (AttachementServeurRPC)theEObject;
				Object result = caseAttachementServeurRPC(attachementServeurRPC);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.ROLE_FOURNIE_SR: {
				RoleFournie_SR roleFournie_SR = (RoleFournie_SR)theEObject;
				Object result = caseRoleFournie_SR(roleFournie_SR);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_REQUIE_SR: {
				PortRequie_SR portRequie_SR = (PortRequie_SR)theEObject;
				Object result = casePortRequie_SR(portRequie_SR);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.BINDING_SRPC: {
				Binding_S_RPC binding_S_RPC = (Binding_S_RPC)theEObject;
				Object result = caseBinding_S_RPC(binding_S_RPC);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_RPC_S: {
				Port_RPC_S port_RPC_S = (Port_RPC_S)theEObject;
				Object result = casePort_RPC_S(port_RPC_S);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_SRPC: {
				Port_S_RPC port_S_RPC = (Port_S_RPC)theEObject;
				Object result = casePort_S_RPC(port_S_RPC);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.BINDING_CM_S: {
				Binding_CM_S binding_CM_S = (Binding_CM_S)theEObject;
				Object result = caseBinding_CM_S(binding_CM_S);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_SCM: {
				Port_S_CM port_S_CM = (Port_S_CM)theEObject;
				Object result = casePort_S_CM(port_S_CM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case M1Package.PORT_CM_S: {
				Port_CM_S port_CM_S = (Port_CM_S)theEObject;
				Object result = casePort_CM_S(port_CM_S);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseServer_Configuration(Server_Configuration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Base</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Base</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDataBase(DataBase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IData Base</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IData Base</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIDataBase(IDataBase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atachement DB 1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atachement DB 1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAtachement_DB_1(Atachement_DB_1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement DB 2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement DB 2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachement_DB_2(Attachement_DB_2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement DB 3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement DB 3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachement_DB_3(Attachement_DB_3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement DB 4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement DB 4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachement_DB_4(Attachement_DB_4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Requie DB 4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Requie DB 4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_Requie_DB_4(Role_Requie_DB_4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port fornie DB 4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port fornie DB 4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_fornie_DB_4(Port_fornie_DB_4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role fournie DB 3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role fournie DB 3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_fournie_DB_3(Role_fournie_DB_3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port requie DB 3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port requie DB 3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_requie_DB_3(Port_requie_DB_3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Requie DB 2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Requie DB 2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_Requie_DB_2(Role_Requie_DB_2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role fournie DB 1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role fournie DB 1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_fournie_DB_1(Role_fournie_DB_1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Requie DB 1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Requie DB 1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_Requie_DB_1(Port_Requie_DB_1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port fournie DB 2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port fournie DB 2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_fournie_DB_2(Port_fournie_DB_2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gestionnaire Connexion</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gestionnaire Connexion</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseGestionnaire_Connexion(Gestionnaire_Connexion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IGestionnaire Connexion</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IGestionnaire Connexion</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIGestionnaire_Connexion(IGestionnaire_Connexion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement GC 2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement GC 2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachement_GC_2(Attachement_GC_2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement GC 3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement GC 3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachement_GC_3(Attachement_GC_3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement GC 4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement GC 4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachement_GC_4(Attachement_GC_4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement GC 1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement GC 1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachement_GC_1(Attachement_GC_1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Requie GC 3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Requie GC 3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_Requie_GC_3(Role_Requie_GC_3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Requie GC 4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Requie GC 4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_Requie_GC_4(Port_Requie_GC_4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Fournie GC 3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Fournie GC 3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_Fournie_GC_3(Port_Fournie_GC_3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Fournie GC 4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Fournie GC 4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_Fournie_GC_4(Role_Fournie_GC_4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Fournie GC 2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Fournie GC 2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_Fournie_GC_2(Role_Fournie_GC_2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Requie GC 2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Requie GC 2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_Requie_GC_2(Port_Requie_GC_2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Requie GC 1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Requie GC 1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_Requie_GC_1(Role_Requie_GC_1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Fournie GC 1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Fournie GC 1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_Fournie_GC_1(Port_Fournie_GC_1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConnecteur GC DB</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConnecteur GC DB</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIConnecteur_GC_DB(IConnecteur_GC_DB object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connecteur GC DB</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connecteur GC DB</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseConnecteur_GC_DB(Connecteur_GC_DB object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gestion Securiterity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gestion Securiterity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseGestion_Securiterity(Gestion_Securiterity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement GS 1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement GS 1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachement_GS_1(Attachement_GS_1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Requie GS 2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Requie GS 2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_Requie_GS_2(Port_Requie_GS_2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement GS 2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement GS 2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachement_GS_2(Attachement_GS_2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement GS 3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement GS 3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachement_GS_3(Attachement_GS_3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Fournie GS 1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Fournie GS 1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_Fournie_GS_1(Port_Fournie_GS_1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Requie GS 1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Requie GS 1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_Requie_GS_1(Role_Requie_GS_1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Fournie GS 2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Fournie GS 2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_Fournie_GS_2(Role_Fournie_GS_2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Fournie GS 3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Fournie GS 3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_Fournie_GS_3(Port_Fournie_GS_3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Requie GS 3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Requie GS 3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_Requie_GS_3(Role_Requie_GS_3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement GS 4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement GS 4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachement_GS_4(Attachement_GS_4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Requie GS 4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Requie GS 4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_Requie_GS_4(Port_Requie_GS_4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Fournie GS 4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Fournie GS 4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRole_Fournie_GS_4(Role_Fournie_GS_4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IGestion Securite</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IGestion Securite</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIGestion_Securite(IGestion_Securite object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connecteur GC GS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connecteur GC GS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseConnecteur_GC_GS(Connecteur_GC_GS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConnecteur GC GS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConnecteur GC GS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIConnecteur_GC_GS(IConnecteur_GC_GS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConnecteur GS DB</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConnecteur GS DB</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIConnecteur_GS_DB(IConnecteur_GS_DB object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connecteur GS DB</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connecteur GS DB</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseConnecteur_GS_DB(Connecteur_GS_DB object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Client Serveur</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Client Serveur</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseClientServeur(ClientServeur object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Client</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Client</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseClient(Client object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connecteur RPC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connecteur RPC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseConnecteurRPC(ConnecteurRPC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Serveur</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Serveur</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseServeur(Serveur object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IClient</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IClient</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIClient(IClient object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IConnecteur RPC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IConnecteur RPC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIConnecteurRPC(IConnecteurRPC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IServeur</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IServeur</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIServeur(IServeur object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Send Request</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Send Request</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSendRequest(SendRequest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement Client RPC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement Client RPC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachementClientRPC(AttachementClientRPC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Requie CR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Requie CR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRoleRequie_CR(RoleRequie_CR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement RPC Client</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement RPC Client</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachementRPCClient(AttachementRPCClient object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Fournie RC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Fournie RC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRoleFournie_RC(RoleFournie_RC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Receve Response</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Receve Response</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseReceveResponse(ReceveResponse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement RPC Serveur</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement RPC Serveur</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachementRPCServeur(AttachementRPCServeur object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Requie RS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Requie RS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRoleRequie_RS(RoleRequie_RS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Fournie RS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Fournie RS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePortFournie_RS(PortFournie_RS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attachement Serveur RPC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attachement Serveur RPC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAttachementServeurRPC(AttachementServeurRPC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Fournie SR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Fournie SR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseRoleFournie_SR(RoleFournie_SR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Requie SR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Requie SR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePortRequie_SR(PortRequie_SR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binding SRPC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binding SRPC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseBinding_S_RPC(Binding_S_RPC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port RPC S</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port RPC S</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_RPC_S(Port_RPC_S object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port SRPC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port SRPC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_S_RPC(Port_S_RPC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binding CM S</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binding CM S</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseBinding_CM_S(Binding_CM_S object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port SCM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port SCM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_S_CM(Port_S_CM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port CM S</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port CM S</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePort_CM_S(Port_CM_S object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public Object defaultCase(EObject object) {
		return null;
	}

} //M1Switch
