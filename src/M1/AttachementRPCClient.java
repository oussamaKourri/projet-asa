/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attachement RPC Client</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.AttachementRPCClient#getReceveResponce <em>Receve Responce</em>}</li>
 *   <li>{@link M1.AttachementRPCClient#getRoleFourie_RC <em>Role Fourie RC</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getAttachementRPCClient()
 * @model
 * @generated
 */
public interface AttachementRPCClient extends EObject {
	/**
	 * Returns the value of the '<em><b>Receve Responce</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.ReceveResponse#getAttachementRC <em>Attachement RC</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receve Responce</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receve Responce</em>' reference.
	 * @see #setReceveResponce(ReceveResponse)
	 * @see M1.M1Package#getAttachementRPCClient_ReceveResponce()
	 * @see M1.ReceveResponse#getAttachementRC
	 * @model opposite="attachementRC"
	 * @generated
	 */
	ReceveResponse getReceveResponce();

	/**
	 * Sets the value of the '{@link M1.AttachementRPCClient#getReceveResponce <em>Receve Responce</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Receve Responce</em>' reference.
	 * @see #getReceveResponce()
	 * @generated
	 */
	void setReceveResponce(ReceveResponse value);

	/**
	 * Returns the value of the '<em><b>Role Fourie RC</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.RoleFournie_RC#getAttachementRC <em>Attachement RC</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Fourie RC</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Fourie RC</em>' reference.
	 * @see #setRoleFourie_RC(RoleFournie_RC)
	 * @see M1.M1Package#getAttachementRPCClient_RoleFourie_RC()
	 * @see M1.RoleFournie_RC#getAttachementRC
	 * @model opposite="attachementRC"
	 * @generated
	 */
	RoleFournie_RC getRoleFourie_RC();

	/**
	 * Sets the value of the '{@link M1.AttachementRPCClient#getRoleFourie_RC <em>Role Fourie RC</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role Fourie RC</em>' reference.
	 * @see #getRoleFourie_RC()
	 * @generated
	 */
	void setRoleFourie_RC(RoleFournie_RC value);

} // AttachementRPCClient
