/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IServeur</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.IServeur#getPortFournie_RS <em>Port Fournie RS</em>}</li>
 *   <li>{@link M1.IServeur#getPortRequie_SR <em>Port Requie SR</em>}</li>
 *   <li>{@link M1.IServeur#getPert_S_CM <em>Pert SCM</em>}</li>
 *   <li>{@link M1.IServeur#getPort_S_RPC <em>Port SRPC</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getIServeur()
 * @model
 * @generated
 */
public interface IServeur extends EObject {
	/**
	 * Returns the value of the '<em><b>Port Fournie RS</b></em>' containment reference list.
	 * The list contents are of type {@link M1.PortFournie_RS}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Fournie RS</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Fournie RS</em>' containment reference list.
	 * @see M1.M1Package#getIServeur_PortFournie_RS()
	 * @model type="M1.PortFournie_RS" containment="true"
	 * @generated
	 */
	EList getPortFournie_RS();

	/**
	 * Returns the value of the '<em><b>Port Requie SR</b></em>' containment reference list.
	 * The list contents are of type {@link M1.PortRequie_SR}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Requie SR</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Requie SR</em>' containment reference list.
	 * @see M1.M1Package#getIServeur_PortRequie_SR()
	 * @model type="M1.PortRequie_SR" containment="true"
	 * @generated
	 */
	EList getPortRequie_SR();

	/**
	 * Returns the value of the '<em><b>Pert SCM</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Port_S_CM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pert SCM</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pert SCM</em>' containment reference list.
	 * @see M1.M1Package#getIServeur_Pert_S_CM()
	 * @model type="M1.Port_S_CM" containment="true"
	 * @generated
	 */
	EList getPert_S_CM();

	/**
	 * Returns the value of the '<em><b>Port SRPC</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Port_S_RPC}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port SRPC</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port SRPC</em>' containment reference list.
	 * @see M1.M1Package#getIServeur_Port_S_RPC()
	 * @model type="M1.Port_S_RPC" containment="true"
	 * @generated
	 */
	EList getPort_S_RPC();

} // IServeur
