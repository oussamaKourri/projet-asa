/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Client</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Client#getInterfaceClient <em>Interface Client</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getClient()
 * @model
 * @generated
 */
public interface Client extends EObject {
	/**
	 * Returns the value of the '<em><b>Interface Client</b></em>' containment reference list.
	 * The list contents are of type {@link M1.IClient}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface Client</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface Client</em>' containment reference list.
	 * @see M1.M1Package#getClient_InterfaceClient()
	 * @model type="M1.IClient" containment="true"
	 * @generated
	 */
	EList getInterfaceClient();

} // Client
