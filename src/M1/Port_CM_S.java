/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port CM S</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Port_CM_S#getEReference0 <em>EReference0</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getPort_CM_S()
 * @model
 * @generated
 */
public interface Port_CM_S extends EObject {
	/**
	 * Returns the value of the '<em><b>EReference0</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Binding_CM_S#getEReference1 <em>EReference1</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference0</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference0</em>' reference.
	 * @see #setEReference0(Binding_CM_S)
	 * @see M1.M1Package#getPort_CM_S_EReference0()
	 * @see M1.Binding_CM_S#getEReference1
	 * @model opposite="EReference1"
	 * @generated
	 */
	Binding_CM_S getEReference0();

	/**
	 * Sets the value of the '{@link M1.Port_CM_S#getEReference0 <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference0</em>' reference.
	 * @see #getEReference0()
	 * @generated
	 */
	void setEReference0(Binding_CM_S value);

} // Port_CM_S
