/**
 */
package M1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port fornie DB 4</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Port_fornie_DB_4#getAttachement_DB_4 <em>Attachement DB 4</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getPort_fornie_DB_4()
 * @model
 * @generated
 */
public interface Port_fornie_DB_4 extends Attachement_DB_4 {
	/**
	 * Returns the value of the '<em><b>Attachement DB 4</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Attachement_DB_4#getPort_fourne_4 <em>Port fourne 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attachement DB 4</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attachement DB 4</em>' reference.
	 * @see #setAttachement_DB_4(Attachement_DB_4)
	 * @see M1.M1Package#getPort_fornie_DB_4_Attachement_DB_4()
	 * @see M1.Attachement_DB_4#getPort_fourne_4
	 * @model opposite="port_fourne_4"
	 * @generated
	 */
	Attachement_DB_4 getAttachement_DB_4();

	/**
	 * Sets the value of the '{@link M1.Port_fornie_DB_4#getAttachement_DB_4 <em>Attachement DB 4</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attachement DB 4</em>' reference.
	 * @see #getAttachement_DB_4()
	 * @generated
	 */
	void setAttachement_DB_4(Attachement_DB_4 value);

} // Port_fornie_DB_4
