/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role Fournie RC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.RoleFournie_RC#getAttachementRC <em>Attachement RC</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getRoleFournie_RC()
 * @model
 * @generated
 */
public interface RoleFournie_RC extends EObject {
	/**
	 * Returns the value of the '<em><b>Attachement RC</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.AttachementRPCClient#getRoleFourie_RC <em>Role Fourie RC</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attachement RC</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attachement RC</em>' reference.
	 * @see #setAttachementRC(AttachementRPCClient)
	 * @see M1.M1Package#getRoleFournie_RC_AttachementRC()
	 * @see M1.AttachementRPCClient#getRoleFourie_RC
	 * @model opposite="roleFourie_RC"
	 * @generated
	 */
	AttachementRPCClient getAttachementRC();

	/**
	 * Sets the value of the '{@link M1.RoleFournie_RC#getAttachementRC <em>Attachement RC</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attachement RC</em>' reference.
	 * @see #getAttachementRC()
	 * @generated
	 */
	void setAttachementRC(AttachementRPCClient value);

} // RoleFournie_RC
