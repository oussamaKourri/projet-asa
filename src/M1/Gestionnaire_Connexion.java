/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gestionnaire Connexion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Gestionnaire_Connexion#getIgestionnaireConnexion <em>Igestionnaire Connexion</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getGestionnaire_Connexion()
 * @model
 * @generated
 */
public interface Gestionnaire_Connexion extends EObject {
	/**
	 * Returns the value of the '<em><b>Igestionnaire Connexion</b></em>' containment reference list.
	 * The list contents are of type {@link M1.IGestionnaire_Connexion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Igestionnaire Connexion</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Igestionnaire Connexion</em>' containment reference list.
	 * @see M1.M1Package#getGestionnaire_Connexion_IgestionnaireConnexion()
	 * @model type="M1.IGestionnaire_Connexion" containment="true"
	 * @generated
	 */
	EList getIgestionnaireConnexion();

} // Gestionnaire_Connexion
