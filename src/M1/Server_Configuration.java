/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Server Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Server_Configuration#getEReference0 <em>EReference0</em>}</li>
 *   <li>{@link M1.Server_Configuration#getDataBase <em>Data Base</em>}</li>
 *   <li>{@link M1.Server_Configuration#getGestionnaire_Connexion <em>Gestionnaire Connexion</em>}</li>
 *   <li>{@link M1.Server_Configuration#getConnecteur_GC_DB <em>Connecteur GC DB</em>}</li>
 *   <li>{@link M1.Server_Configuration#getGestion_securite <em>Gestion securite</em>}</li>
 *   <li>{@link M1.Server_Configuration#getConnecteur_GC_GS <em>Connecteur GC GS</em>}</li>
 *   <li>{@link M1.Server_Configuration#getConnecteur_GS_DB <em>Connecteur GS DB</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getServer_Configuration()
 * @model
 * @generated
 */
public interface Server_Configuration extends EObject {
	/**
	 * Returns the value of the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference0</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference0</em>' reference.
	 * @see #setEReference0(DataBase)
	 * @see M1.M1Package#getServer_Configuration_EReference0()
	 * @model
	 * @generated
	 */
	DataBase getEReference0();

	/**
	 * Sets the value of the '{@link M1.Server_Configuration#getEReference0 <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference0</em>' reference.
	 * @see #getEReference0()
	 * @generated
	 */
	void setEReference0(DataBase value);

	/**
	 * Returns the value of the '<em><b>Data Base</b></em>' containment reference list.
	 * The list contents are of type {@link M1.DataBase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Base</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Base</em>' containment reference list.
	 * @see M1.M1Package#getServer_Configuration_DataBase()
	 * @model type="M1.DataBase" containment="true"
	 * @generated
	 */
	EList getDataBase();

	/**
	 * Returns the value of the '<em><b>Gestionnaire Connexion</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Gestionnaire_Connexion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gestionnaire Connexion</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gestionnaire Connexion</em>' containment reference list.
	 * @see M1.M1Package#getServer_Configuration_Gestionnaire_Connexion()
	 * @model type="M1.Gestionnaire_Connexion" containment="true"
	 * @generated
	 */
	EList getGestionnaire_Connexion();

	/**
	 * Returns the value of the '<em><b>Connecteur GC DB</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Connecteur_GC_DB}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connecteur GC DB</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connecteur GC DB</em>' containment reference list.
	 * @see M1.M1Package#getServer_Configuration_Connecteur_GC_DB()
	 * @model type="M1.Connecteur_GC_DB" containment="true"
	 * @generated
	 */
	EList getConnecteur_GC_DB();

	/**
	 * Returns the value of the '<em><b>Gestion securite</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Gestion_Securiterity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gestion securite</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gestion securite</em>' containment reference list.
	 * @see M1.M1Package#getServer_Configuration_Gestion_securite()
	 * @model type="M1.Gestion_Securiterity" containment="true"
	 * @generated
	 */
	EList getGestion_securite();

	/**
	 * Returns the value of the '<em><b>Connecteur GC GS</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Connecteur_GC_GS}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connecteur GC GS</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connecteur GC GS</em>' containment reference list.
	 * @see M1.M1Package#getServer_Configuration_Connecteur_GC_GS()
	 * @model type="M1.Connecteur_GC_GS" containment="true"
	 * @generated
	 */
	EList getConnecteur_GC_GS();

	/**
	 * Returns the value of the '<em><b>Connecteur GS DB</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Connecteur_GS_DB}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connecteur GS DB</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connecteur GS DB</em>' containment reference list.
	 * @see M1.M1Package#getServer_Configuration_Connecteur_GS_DB()
	 * @model type="M1.Connecteur_GS_DB" containment="true"
	 * @generated
	 */
	EList getConnecteur_GS_DB();

} // Server_Configuration
