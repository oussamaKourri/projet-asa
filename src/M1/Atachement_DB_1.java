/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Atachement DB 1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Atachement_DB_1#getRole_fournie_DB_1 <em>Role fournie DB 1</em>}</li>
 *   <li>{@link M1.Atachement_DB_1#getPort_requie_DB_1 <em>Port requie DB 1</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getAtachement_DB_1()
 * @model
 * @generated
 */
public interface Atachement_DB_1 extends EObject {
	/**
	 * Returns the value of the '<em><b>Role fournie DB 1</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Role_fournie_DB_1#getAttachement_DB_1 <em>Attachement DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role fournie DB 1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role fournie DB 1</em>' reference.
	 * @see #setRole_fournie_DB_1(Role_fournie_DB_1)
	 * @see M1.M1Package#getAtachement_DB_1_Role_fournie_DB_1()
	 * @see M1.Role_fournie_DB_1#getAttachement_DB_1
	 * @model opposite="attachement_DB_1"
	 * @generated
	 */
	Role_fournie_DB_1 getRole_fournie_DB_1();

	/**
	 * Sets the value of the '{@link M1.Atachement_DB_1#getRole_fournie_DB_1 <em>Role fournie DB 1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role fournie DB 1</em>' reference.
	 * @see #getRole_fournie_DB_1()
	 * @generated
	 */
	void setRole_fournie_DB_1(Role_fournie_DB_1 value);

	/**
	 * Returns the value of the '<em><b>Port requie DB 1</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Port_Requie_DB_1#getAttachement_DB_1 <em>Attachement DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port requie DB 1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port requie DB 1</em>' reference.
	 * @see #setPort_requie_DB_1(Port_Requie_DB_1)
	 * @see M1.M1Package#getAtachement_DB_1_Port_requie_DB_1()
	 * @see M1.Port_Requie_DB_1#getAttachement_DB_1
	 * @model opposite="attachement_DB_1"
	 * @generated
	 */
	Port_Requie_DB_1 getPort_requie_DB_1();

	/**
	 * Sets the value of the '{@link M1.Atachement_DB_1#getPort_requie_DB_1 <em>Port requie DB 1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port requie DB 1</em>' reference.
	 * @see #getPort_requie_DB_1()
	 * @generated
	 */
	void setPort_requie_DB_1(Port_Requie_DB_1 value);

} // Atachement_DB_1
