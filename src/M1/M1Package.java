/**
 */
package M1;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see M1.M1Factory
 * @model kind="package"
 * @generated
 */
public interface M1Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "M1";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/cs";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "client_serveur";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	M1Package eINSTANCE = M1.impl.M1PackageImpl.init();

	/**
	 * The meta object id for the '{@link M1.impl.Server_ConfigurationImpl <em>Server Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Server_ConfigurationImpl
	 * @see M1.impl.M1PackageImpl#getServer_Configuration()
	 * @generated
	 */
	int SERVER_CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION__EREFERENCE0 = 0;

	/**
	 * The feature id for the '<em><b>Data Base</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION__DATA_BASE = 1;

	/**
	 * The feature id for the '<em><b>Gestionnaire Connexion</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION__GESTIONNAIRE_CONNEXION = 2;

	/**
	 * The feature id for the '<em><b>Connecteur GC DB</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION__CONNECTEUR_GC_DB = 3;

	/**
	 * The feature id for the '<em><b>Gestion securite</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION__GESTION_SECURITE = 4;

	/**
	 * The feature id for the '<em><b>Connecteur GC GS</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION__CONNECTEUR_GC_GS = 5;

	/**
	 * The feature id for the '<em><b>Connecteur GS DB</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION__CONNECTEUR_GS_DB = 6;

	/**
	 * The number of structural features of the '<em>Server Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link M1.impl.DataBaseImpl <em>Data Base</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.DataBaseImpl
	 * @see M1.impl.M1PackageImpl#getDataBase()
	 * @generated
	 */
	int DATA_BASE = 1;

	/**
	 * The feature id for the '<em><b>Idata Base</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_BASE__IDATA_BASE = 0;

	/**
	 * The number of structural features of the '<em>Data Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_BASE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.IDataBaseImpl <em>IData Base</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.IDataBaseImpl
	 * @see M1.impl.M1PackageImpl#getIDataBase()
	 * @generated
	 */
	int IDATA_BASE = 2;

	/**
	 * The feature id for the '<em><b>Port requie DB 1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDATA_BASE__PORT_REQUIE_DB_1 = 0;

	/**
	 * The feature id for the '<em><b>Port fournie DB 2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDATA_BASE__PORT_FOURNIE_DB_2 = 1;

	/**
	 * The feature id for the '<em><b>Port requie DB 3</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDATA_BASE__PORT_REQUIE_DB_3 = 2;

	/**
	 * The feature id for the '<em><b>Port fournie DB 4</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDATA_BASE__PORT_FOURNIE_DB_4 = 3;

	/**
	 * The number of structural features of the '<em>IData Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDATA_BASE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link M1.impl.Atachement_DB_1Impl <em>Atachement DB 1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Atachement_DB_1Impl
	 * @see M1.impl.M1PackageImpl#getAtachement_DB_1()
	 * @generated
	 */
	int ATACHEMENT_DB_1 = 3;

	/**
	 * The feature id for the '<em><b>Role fournie DB 1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1 = 0;

	/**
	 * The feature id for the '<em><b>Port requie DB 1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATACHEMENT_DB_1__PORT_REQUIE_DB_1 = 1;

	/**
	 * The number of structural features of the '<em>Atachement DB 1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATACHEMENT_DB_1_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Attachement_DB_2Impl <em>Attachement DB 2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Attachement_DB_2Impl
	 * @see M1.impl.M1PackageImpl#getAttachement_DB_2()
	 * @generated
	 */
	int ATTACHEMENT_DB_2 = 4;

	/**
	 * The feature id for the '<em><b>Role requie 2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_DB_2__ROLE_REQUIE_2 = 0;

	/**
	 * The feature id for the '<em><b>Port fournie DB 2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2 = 1;

	/**
	 * The number of structural features of the '<em>Attachement DB 2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_DB_2_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Attachement_DB_3Impl <em>Attachement DB 3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Attachement_DB_3Impl
	 * @see M1.impl.M1PackageImpl#getAttachement_DB_3()
	 * @generated
	 */
	int ATTACHEMENT_DB_3 = 5;

	/**
	 * The feature id for the '<em><b>Role fourni DB 3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3 = 0;

	/**
	 * The feature id for the '<em><b>Port requie DB 3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_DB_3__PORT_REQUIE_DB_3 = 1;

	/**
	 * The number of structural features of the '<em>Attachement DB 3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_DB_3_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Attachement_DB_4Impl <em>Attachement DB 4</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Attachement_DB_4Impl
	 * @see M1.impl.M1PackageImpl#getAttachement_DB_4()
	 * @generated
	 */
	int ATTACHEMENT_DB_4 = 6;

	/**
	 * The feature id for the '<em><b>Role Requie 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_DB_4__ROLE_REQUIE_4 = 0;

	/**
	 * The feature id for the '<em><b>Port fourne 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_DB_4__PORT_FOURNE_4 = 1;

	/**
	 * The number of structural features of the '<em>Attachement DB 4</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_DB_4_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Role_Requie_DB_4Impl <em>Role Requie DB 4</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_Requie_DB_4Impl
	 * @see M1.impl.M1PackageImpl#getRole_Requie_DB_4()
	 * @generated
	 */
	int ROLE_REQUIE_DB_4 = 7;

	/**
	 * The feature id for the '<em><b>Role Requie 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_DB_4__ROLE_REQUIE_4 = ATTACHEMENT_DB_4__ROLE_REQUIE_4;

	/**
	 * The feature id for the '<em><b>Port fourne 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_DB_4__PORT_FOURNE_4 = ATTACHEMENT_DB_4__PORT_FOURNE_4;

	/**
	 * The feature id for the '<em><b>Attachement DB 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4 = ATTACHEMENT_DB_4_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Role Requie DB 4</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_DB_4_FEATURE_COUNT = ATTACHEMENT_DB_4_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link M1.impl.Port_fornie_DB_4Impl <em>Port fornie DB 4</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_fornie_DB_4Impl
	 * @see M1.impl.M1PackageImpl#getPort_fornie_DB_4()
	 * @generated
	 */
	int PORT_FORNIE_DB_4 = 8;

	/**
	 * The feature id for the '<em><b>Role Requie 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FORNIE_DB_4__ROLE_REQUIE_4 = ATTACHEMENT_DB_4__ROLE_REQUIE_4;

	/**
	 * The feature id for the '<em><b>Port fourne 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FORNIE_DB_4__PORT_FOURNE_4 = ATTACHEMENT_DB_4__PORT_FOURNE_4;

	/**
	 * The feature id for the '<em><b>Attachement DB 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FORNIE_DB_4__ATTACHEMENT_DB_4 = ATTACHEMENT_DB_4_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Port fornie DB 4</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FORNIE_DB_4_FEATURE_COUNT = ATTACHEMENT_DB_4_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link M1.impl.Role_fournie_DB_3Impl <em>Role fournie DB 3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_fournie_DB_3Impl
	 * @see M1.impl.M1PackageImpl#getRole_fournie_DB_3()
	 * @generated
	 */
	int ROLE_FOURNIE_DB_3 = 9;

	/**
	 * The feature id for the '<em><b>Attachement DB 3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3 = 0;

	/**
	 * The number of structural features of the '<em>Role fournie DB 3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_DB_3_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Port_requie_DB_3Impl <em>Port requie DB 3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_requie_DB_3Impl
	 * @see M1.impl.M1PackageImpl#getPort_requie_DB_3()
	 * @generated
	 */
	int PORT_REQUIE_DB_3 = 10;

	/**
	 * The feature id for the '<em><b>Attachement DB 3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_DB_3__ATTACHEMENT_DB_3 = 0;

	/**
	 * The number of structural features of the '<em>Port requie DB 3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_DB_3_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Role_Requie_DB_2Impl <em>Role Requie DB 2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_Requie_DB_2Impl
	 * @see M1.impl.M1PackageImpl#getRole_Requie_DB_2()
	 * @generated
	 */
	int ROLE_REQUIE_DB_2 = 11;

	/**
	 * The feature id for the '<em><b>Attachement DB 2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2 = 0;

	/**
	 * The number of structural features of the '<em>Role Requie DB 2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_DB_2_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Role_fournie_DB_1Impl <em>Role fournie DB 1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_fournie_DB_1Impl
	 * @see M1.impl.M1PackageImpl#getRole_fournie_DB_1()
	 * @generated
	 */
	int ROLE_FOURNIE_DB_1 = 12;

	/**
	 * The feature id for the '<em><b>Attachement DB 1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_DB_1__ATTACHEMENT_DB_1 = 0;

	/**
	 * The number of structural features of the '<em>Role fournie DB 1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_DB_1_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Port_Requie_DB_1Impl <em>Port Requie DB 1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_Requie_DB_1Impl
	 * @see M1.impl.M1PackageImpl#getPort_Requie_DB_1()
	 * @generated
	 */
	int PORT_REQUIE_DB_1 = 13;

	/**
	 * The feature id for the '<em><b>Attachement DB 1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_DB_1__ATTACHEMENT_DB_1 = 0;

	/**
	 * The number of structural features of the '<em>Port Requie DB 1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_DB_1_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Port_fournie_DB_2Impl <em>Port fournie DB 2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_fournie_DB_2Impl
	 * @see M1.impl.M1PackageImpl#getPort_fournie_DB_2()
	 * @generated
	 */
	int PORT_FOURNIE_DB_2 = 14;

	/**
	 * The feature id for the '<em><b>Attachement DB 2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_DB_2__ATTACHEMENT_DB_2 = 0;

	/**
	 * The number of structural features of the '<em>Port fournie DB 2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_DB_2_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Gestionnaire_ConnexionImpl <em>Gestionnaire Connexion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Gestionnaire_ConnexionImpl
	 * @see M1.impl.M1PackageImpl#getGestionnaire_Connexion()
	 * @generated
	 */
	int GESTIONNAIRE_CONNEXION = 15;

	/**
	 * The feature id for the '<em><b>Igestionnaire Connexion</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GESTIONNAIRE_CONNEXION__IGESTIONNAIRE_CONNEXION = 0;

	/**
	 * The number of structural features of the '<em>Gestionnaire Connexion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GESTIONNAIRE_CONNEXION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.IGestionnaire_ConnexionImpl <em>IGestionnaire Connexion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.IGestionnaire_ConnexionImpl
	 * @see M1.impl.M1PackageImpl#getIGestionnaire_Connexion()
	 * @generated
	 */
	int IGESTIONNAIRE_CONNEXION = 16;

	/**
	 * The feature id for the '<em><b>Port Fournie Gc 1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1 = 0;

	/**
	 * The feature id for the '<em><b>Port Requie Gc 2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2 = 1;

	/**
	 * The feature id for the '<em><b>Port Fournie Gc 3</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3 = 2;

	/**
	 * The feature id for the '<em><b>Port Requie Gc 4</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4 = 3;

	/**
	 * The feature id for the '<em><b>Port CM S</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGESTIONNAIRE_CONNEXION__PORT_CM_S = 4;

	/**
	 * The number of structural features of the '<em>IGestionnaire Connexion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGESTIONNAIRE_CONNEXION_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link M1.impl.Attachement_GC_2Impl <em>Attachement GC 2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Attachement_GC_2Impl
	 * @see M1.impl.M1PackageImpl#getAttachement_GC_2()
	 * @generated
	 */
	int ATTACHEMENT_GC_2 = 17;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_2__EREFERENCE0 = 0;

	/**
	 * The feature id for the '<em><b>EReference1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_2__EREFERENCE1 = 1;

	/**
	 * The number of structural features of the '<em>Attachement GC 2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_2_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Attachement_GC_3Impl <em>Attachement GC 3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Attachement_GC_3Impl
	 * @see M1.impl.M1PackageImpl#getAttachement_GC_3()
	 * @generated
	 */
	int ATTACHEMENT_GC_3 = 18;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_3__EREFERENCE0 = 0;

	/**
	 * The feature id for the '<em><b>EReference1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_3__EREFERENCE1 = 1;

	/**
	 * The number of structural features of the '<em>Attachement GC 3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_3_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Attachement_GC_4Impl <em>Attachement GC 4</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Attachement_GC_4Impl
	 * @see M1.impl.M1PackageImpl#getAttachement_GC_4()
	 * @generated
	 */
	int ATTACHEMENT_GC_4 = 19;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_4__EREFERENCE0 = 0;

	/**
	 * The feature id for the '<em><b>EReference1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_4__EREFERENCE1 = 1;

	/**
	 * The number of structural features of the '<em>Attachement GC 4</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_4_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Attachement_GC_1Impl <em>Attachement GC 1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Attachement_GC_1Impl
	 * @see M1.impl.M1PackageImpl#getAttachement_GC_1()
	 * @generated
	 */
	int ATTACHEMENT_GC_1 = 20;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_1__EREFERENCE0 = 0;

	/**
	 * The feature id for the '<em><b>EReference1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_1__EREFERENCE1 = 1;

	/**
	 * The number of structural features of the '<em>Attachement GC 1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GC_1_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Role_Requie_GC_3Impl <em>Role Requie GC 3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_Requie_GC_3Impl
	 * @see M1.impl.M1PackageImpl#getRole_Requie_GC_3()
	 * @generated
	 */
	int ROLE_REQUIE_GC_3 = 21;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_GC_3__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Role Requie GC 3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_GC_3_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Port_Requie_GC_4Impl <em>Port Requie GC 4</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_Requie_GC_4Impl
	 * @see M1.impl.M1PackageImpl#getPort_Requie_GC_4()
	 * @generated
	 */
	int PORT_REQUIE_GC_4 = 22;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_GC_4__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Port Requie GC 4</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_GC_4_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Port_Fournie_GC_3Impl <em>Port Fournie GC 3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_Fournie_GC_3Impl
	 * @see M1.impl.M1PackageImpl#getPort_Fournie_GC_3()
	 * @generated
	 */
	int PORT_FOURNIE_GC_3 = 23;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_GC_3__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Port Fournie GC 3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_GC_3_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Role_Fournie_GC_4Impl <em>Role Fournie GC 4</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_Fournie_GC_4Impl
	 * @see M1.impl.M1PackageImpl#getRole_Fournie_GC_4()
	 * @generated
	 */
	int ROLE_FOURNIE_GC_4 = 24;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_GC_4__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Role Fournie GC 4</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_GC_4_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Role_Fournie_GC_2Impl <em>Role Fournie GC 2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_Fournie_GC_2Impl
	 * @see M1.impl.M1PackageImpl#getRole_Fournie_GC_2()
	 * @generated
	 */
	int ROLE_FOURNIE_GC_2 = 25;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_GC_2__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Role Fournie GC 2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_GC_2_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Port_Requie_GC_2Impl <em>Port Requie GC 2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_Requie_GC_2Impl
	 * @see M1.impl.M1PackageImpl#getPort_Requie_GC_2()
	 * @generated
	 */
	int PORT_REQUIE_GC_2 = 26;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_GC_2__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Port Requie GC 2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_GC_2_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Role_Requie_GC_1Impl <em>Role Requie GC 1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_Requie_GC_1Impl
	 * @see M1.impl.M1PackageImpl#getRole_Requie_GC_1()
	 * @generated
	 */
	int ROLE_REQUIE_GC_1 = 27;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_GC_1__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Role Requie GC 1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_GC_1_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Port_Fournie_GC_1Impl <em>Port Fournie GC 1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_Fournie_GC_1Impl
	 * @see M1.impl.M1PackageImpl#getPort_Fournie_GC_1()
	 * @generated
	 */
	int PORT_FOURNIE_GC_1 = 28;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_GC_1__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Port Fournie GC 1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_GC_1_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.IConnecteur_GC_DBImpl <em>IConnecteur GC DB</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.IConnecteur_GC_DBImpl
	 * @see M1.impl.M1PackageImpl#getIConnecteur_GC_DB()
	 * @generated
	 */
	int ICONNECTEUR_GC_DB = 29;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GC_DB__EREFERENCE0 = 0;

	/**
	 * The feature id for the '<em><b>EReference1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GC_DB__EREFERENCE1 = 1;

	/**
	 * The feature id for the '<em><b>EReference2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GC_DB__EREFERENCE2 = 2;

	/**
	 * The feature id for the '<em><b>EReference3</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GC_DB__EREFERENCE3 = 3;

	/**
	 * The number of structural features of the '<em>IConnecteur GC DB</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GC_DB_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link M1.impl.Connecteur_GC_DBImpl <em>Connecteur GC DB</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Connecteur_GC_DBImpl
	 * @see M1.impl.M1PackageImpl#getConnecteur_GC_DB()
	 * @generated
	 */
	int CONNECTEUR_GC_DB = 30;

	/**
	 * The feature id for the '<em><b>Iconnecteur BCDB</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_GC_DB__ICONNECTEUR_BCDB = 0;

	/**
	 * The number of structural features of the '<em>Connecteur GC DB</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_GC_DB_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Gestion_SecuriterityImpl <em>Gestion Securiterity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Gestion_SecuriterityImpl
	 * @see M1.impl.M1PackageImpl#getGestion_Securiterity()
	 * @generated
	 */
	int GESTION_SECURITERITY = 31;

	/**
	 * The feature id for the '<em><b>Igestion Securite</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GESTION_SECURITERITY__IGESTION_SECURITE = 0;

	/**
	 * The number of structural features of the '<em>Gestion Securiterity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GESTION_SECURITERITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Attachement_GS_1Impl <em>Attachement GS 1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Attachement_GS_1Impl
	 * @see M1.impl.M1PackageImpl#getAttachement_GS_1()
	 * @generated
	 */
	int ATTACHEMENT_GS_1 = 32;

	/**
	 * The feature id for the '<em><b>Port fournie GS 1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1 = 0;

	/**
	 * The feature id for the '<em><b>Role requie GS 1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1 = 1;

	/**
	 * The number of structural features of the '<em>Attachement GS 1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_1_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Port_Requie_GS_2Impl <em>Port Requie GS 2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_Requie_GS_2Impl
	 * @see M1.impl.M1PackageImpl#getPort_Requie_GS_2()
	 * @generated
	 */
	int PORT_REQUIE_GS_2 = 33;

	/**
	 * The feature id for the '<em><b>Attachement GS 2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_GS_2__ATTACHEMENT_GS_2 = 0;

	/**
	 * The number of structural features of the '<em>Port Requie GS 2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_GS_2_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Attachement_GS_2Impl <em>Attachement GS 2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Attachement_GS_2Impl
	 * @see M1.impl.M1PackageImpl#getAttachement_GS_2()
	 * @generated
	 */
	int ATTACHEMENT_GS_2 = 34;

	/**
	 * The feature id for the '<em><b>Port requie GS 2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_2__PORT_REQUIE_GS_2 = 0;

	/**
	 * The feature id for the '<em><b>Role fournie GS 2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2 = 1;

	/**
	 * The number of structural features of the '<em>Attachement GS 2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_2_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Attachement_GS_3Impl <em>Attachement GS 3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Attachement_GS_3Impl
	 * @see M1.impl.M1PackageImpl#getAttachement_GS_3()
	 * @generated
	 */
	int ATTACHEMENT_GS_3 = 35;

	/**
	 * The feature id for the '<em><b>Port fournie GS 3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_3__PORT_FOURNIE_GS_3 = 0;

	/**
	 * The feature id for the '<em><b>Role requie GS 3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_3__ROLE_REQUIE_GS_3 = 1;

	/**
	 * The number of structural features of the '<em>Attachement GS 3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_3_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Port_Fournie_GS_1Impl <em>Port Fournie GS 1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_Fournie_GS_1Impl
	 * @see M1.impl.M1PackageImpl#getPort_Fournie_GS_1()
	 * @generated
	 */
	int PORT_FOURNIE_GS_1 = 36;

	/**
	 * The feature id for the '<em><b>Attachement GS 1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1 = 0;

	/**
	 * The number of structural features of the '<em>Port Fournie GS 1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_GS_1_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Role_Requie_GS_1Impl <em>Role Requie GS 1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_Requie_GS_1Impl
	 * @see M1.impl.M1PackageImpl#getRole_Requie_GS_1()
	 * @generated
	 */
	int ROLE_REQUIE_GS_1 = 37;

	/**
	 * The feature id for the '<em><b>Attachement GS 1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_GS_1__ATTACHEMENT_GS_1 = 0;

	/**
	 * The number of structural features of the '<em>Role Requie GS 1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_GS_1_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Role_Fournie_GS_2Impl <em>Role Fournie GS 2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_Fournie_GS_2Impl
	 * @see M1.impl.M1PackageImpl#getRole_Fournie_GS_2()
	 * @generated
	 */
	int ROLE_FOURNIE_GS_2 = 38;

	/**
	 * The feature id for the '<em><b>Attachement GS 2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_GS_2__ATTACHEMENT_GS_2 = 0;

	/**
	 * The number of structural features of the '<em>Role Fournie GS 2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_GS_2_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Port_Fournie_GS_3Impl <em>Port Fournie GS 3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_Fournie_GS_3Impl
	 * @see M1.impl.M1PackageImpl#getPort_Fournie_GS_3()
	 * @generated
	 */
	int PORT_FOURNIE_GS_3 = 39;

	/**
	 * The feature id for the '<em><b>Attachement GS 3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_GS_3__ATTACHEMENT_GS_3 = 0;

	/**
	 * The number of structural features of the '<em>Port Fournie GS 3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_GS_3_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Role_Requie_GS_3Impl <em>Role Requie GS 3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_Requie_GS_3Impl
	 * @see M1.impl.M1PackageImpl#getRole_Requie_GS_3()
	 * @generated
	 */
	int ROLE_REQUIE_GS_3 = 40;

	/**
	 * The feature id for the '<em><b>Attachement GS 3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_GS_3__ATTACHEMENT_GS_3 = 0;

	/**
	 * The number of structural features of the '<em>Role Requie GS 3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_GS_3_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Attachement_GS_4Impl <em>Attachement GS 4</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Attachement_GS_4Impl
	 * @see M1.impl.M1PackageImpl#getAttachement_GS_4()
	 * @generated
	 */
	int ATTACHEMENT_GS_4 = 41;

	/**
	 * The feature id for the '<em><b>Port requie GS 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_4__PORT_REQUIE_GS_4 = 0;

	/**
	 * The feature id for the '<em><b>Role fournie GS 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4 = 1;

	/**
	 * The number of structural features of the '<em>Attachement GS 4</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_GS_4_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Port_Requie_GS_4Impl <em>Port Requie GS 4</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_Requie_GS_4Impl
	 * @see M1.impl.M1PackageImpl#getPort_Requie_GS_4()
	 * @generated
	 */
	int PORT_REQUIE_GS_4 = 42;

	/**
	 * The feature id for the '<em><b>Attachement GS 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_GS_4__ATTACHEMENT_GS_4 = 0;

	/**
	 * The number of structural features of the '<em>Port Requie GS 4</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_GS_4_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Role_Fournie_GS_4Impl <em>Role Fournie GS 4</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Role_Fournie_GS_4Impl
	 * @see M1.impl.M1PackageImpl#getRole_Fournie_GS_4()
	 * @generated
	 */
	int ROLE_FOURNIE_GS_4 = 43;

	/**
	 * The feature id for the '<em><b>Attachement GS 4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_GS_4__ATTACHEMENT_GS_4 = 0;

	/**
	 * The number of structural features of the '<em>Role Fournie GS 4</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_GS_4_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.IGestion_SecuriteImpl <em>IGestion Securite</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.IGestion_SecuriteImpl
	 * @see M1.impl.M1PackageImpl#getIGestion_Securite()
	 * @generated
	 */
	int IGESTION_SECURITE = 44;

	/**
	 * The feature id for the '<em><b>Port fournie GS 1</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGESTION_SECURITE__PORT_FOURNIE_GS_1 = 0;

	/**
	 * The feature id for the '<em><b>Port requie GS 2</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGESTION_SECURITE__PORT_REQUIE_GS_2 = 1;

	/**
	 * The feature id for the '<em><b>Port fournie GS 3</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGESTION_SECURITE__PORT_FOURNIE_GS_3 = 2;

	/**
	 * The feature id for the '<em><b>Port requie GS 4</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGESTION_SECURITE__PORT_REQUIE_GS_4 = 3;

	/**
	 * The number of structural features of the '<em>IGestion Securite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGESTION_SECURITE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link M1.impl.Connecteur_GC_GSImpl <em>Connecteur GC GS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Connecteur_GC_GSImpl
	 * @see M1.impl.M1PackageImpl#getConnecteur_GC_GS()
	 * @generated
	 */
	int CONNECTEUR_GC_GS = 45;

	/**
	 * The feature id for the '<em><b>Iconnecteur GC GS</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_GC_GS__ICONNECTEUR_GC_GS = 0;

	/**
	 * The number of structural features of the '<em>Connecteur GC GS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_GC_GS_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.IConnecteur_GC_GSImpl <em>IConnecteur GC GS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.IConnecteur_GC_GSImpl
	 * @see M1.impl.M1PackageImpl#getIConnecteur_GC_GS()
	 * @generated
	 */
	int ICONNECTEUR_GC_GS = 46;

	/**
	 * The feature id for the '<em><b>Role requie GC 3</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GC_GS__ROLE_REQUIE_GC_3 = 0;

	/**
	 * The feature id for the '<em><b>Role fournie GC 4</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GC_GS__ROLE_FOURNIE_GC_4 = 1;

	/**
	 * The feature id for the '<em><b>Role requie GS 1</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GC_GS__ROLE_REQUIE_GS_1 = 2;

	/**
	 * The feature id for the '<em><b>Role fournie GS 2</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GC_GS__ROLE_FOURNIE_GS_2 = 3;

	/**
	 * The number of structural features of the '<em>IConnecteur GC GS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GC_GS_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link M1.impl.IConnecteur_GS_DBImpl <em>IConnecteur GS DB</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.IConnecteur_GS_DBImpl
	 * @see M1.impl.M1PackageImpl#getIConnecteur_GS_DB()
	 * @generated
	 */
	int ICONNECTEUR_GS_DB = 47;

	/**
	 * The feature id for the '<em><b>Role fournie DB 3</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GS_DB__ROLE_FOURNIE_DB_3 = 0;

	/**
	 * The feature id for the '<em><b>Role requie DB 4</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GS_DB__ROLE_REQUIE_DB_4 = 1;

	/**
	 * The feature id for the '<em><b>Role fournie GS 4</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GS_DB__ROLE_FOURNIE_GS_4 = 2;

	/**
	 * The feature id for the '<em><b>Role requie GS 3</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GS_DB__ROLE_REQUIE_GS_3 = 3;

	/**
	 * The number of structural features of the '<em>IConnecteur GS DB</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_GS_DB_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link M1.impl.Connecteur_GS_DBImpl <em>Connecteur GS DB</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Connecteur_GS_DBImpl
	 * @see M1.impl.M1PackageImpl#getConnecteur_GS_DB()
	 * @generated
	 */
	int CONNECTEUR_GS_DB = 48;

	/**
	 * The feature id for the '<em><b>Iconnecteur GS DB</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_GS_DB__ICONNECTEUR_GS_DB = 0;

	/**
	 * The number of structural features of the '<em>Connecteur GS DB</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_GS_DB_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.ClientServeurImpl <em>Client Serveur</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.ClientServeurImpl
	 * @see M1.impl.M1PackageImpl#getClientServeur()
	 * @generated
	 */
	int CLIENT_SERVEUR = 49;

	/**
	 * The feature id for the '<em><b>Client</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_SERVEUR__CLIENT = 0;

	/**
	 * The feature id for the '<em><b>Connecteur RPC</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_SERVEUR__CONNECTEUR_RPC = 1;

	/**
	 * The feature id for the '<em><b>Serveur</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_SERVEUR__SERVEUR = 2;

	/**
	 * The number of structural features of the '<em>Client Serveur</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_SERVEUR_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link M1.impl.ClientImpl <em>Client</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.ClientImpl
	 * @see M1.impl.M1PackageImpl#getClient()
	 * @generated
	 */
	int CLIENT = 50;

	/**
	 * The feature id for the '<em><b>Interface Client</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT__INTERFACE_CLIENT = 0;

	/**
	 * The number of structural features of the '<em>Client</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.ConnecteurRPCImpl <em>Connecteur RPC</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.ConnecteurRPCImpl
	 * @see M1.impl.M1PackageImpl#getConnecteurRPC()
	 * @generated
	 */
	int CONNECTEUR_RPC = 51;

	/**
	 * The feature id for the '<em><b>Interface Connecteur</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_RPC__INTERFACE_CONNECTEUR = 0;

	/**
	 * The number of structural features of the '<em>Connecteur RPC</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTEUR_RPC_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.ServeurImpl <em>Serveur</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.ServeurImpl
	 * @see M1.impl.M1PackageImpl#getServeur()
	 * @generated
	 */
	int SERVEUR = 52;

	/**
	 * The feature id for the '<em><b>Interface Serveur</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVEUR__INTERFACE_SERVEUR = 0;

	/**
	 * The feature id for the '<em><b>Serveur Config</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVEUR__SERVEUR_CONFIG = 1;

	/**
	 * The number of structural features of the '<em>Serveur</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVEUR_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.IClientImpl <em>IClient</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.IClientImpl
	 * @see M1.impl.M1PackageImpl#getIClient()
	 * @generated
	 */
	int ICLIENT = 53;

	/**
	 * The feature id for the '<em><b>Send Request</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICLIENT__SEND_REQUEST = 0;

	/**
	 * The feature id for the '<em><b>Receve Request</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICLIENT__RECEVE_REQUEST = 1;

	/**
	 * The number of structural features of the '<em>IClient</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICLIENT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.IConnecteurRPCImpl <em>IConnecteur RPC</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.IConnecteurRPCImpl
	 * @see M1.impl.M1PackageImpl#getIConnecteurRPC()
	 * @generated
	 */
	int ICONNECTEUR_RPC = 54;

	/**
	 * The feature id for the '<em><b>Role Requie CR</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_RPC__ROLE_REQUIE_CR = 0;

	/**
	 * The feature id for the '<em><b>Role Fournie RC</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_RPC__ROLE_FOURNIE_RC = 1;

	/**
	 * The feature id for the '<em><b>Role Requie RS</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_RPC__ROLE_REQUIE_RS = 2;

	/**
	 * The feature id for the '<em><b>Role Fournie SR</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_RPC__ROLE_FOURNIE_SR = 3;

	/**
	 * The feature id for the '<em><b>Port RPC S</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_RPC__PORT_RPC_S = 4;

	/**
	 * The number of structural features of the '<em>IConnecteur RPC</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONNECTEUR_RPC_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link M1.impl.IServeurImpl <em>IServeur</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.IServeurImpl
	 * @see M1.impl.M1PackageImpl#getIServeur()
	 * @generated
	 */
	int ISERVEUR = 55;

	/**
	 * The feature id for the '<em><b>Port Fournie RS</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISERVEUR__PORT_FOURNIE_RS = 0;

	/**
	 * The feature id for the '<em><b>Port Requie SR</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISERVEUR__PORT_REQUIE_SR = 1;

	/**
	 * The feature id for the '<em><b>Pert SCM</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISERVEUR__PERT_SCM = 2;

	/**
	 * The feature id for the '<em><b>Port SRPC</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISERVEUR__PORT_SRPC = 3;

	/**
	 * The number of structural features of the '<em>IServeur</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISERVEUR_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link M1.impl.SendRequestImpl <em>Send Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.SendRequestImpl
	 * @see M1.impl.M1PackageImpl#getSendRequest()
	 * @generated
	 */
	int SEND_REQUEST = 56;

	/**
	 * The feature id for the '<em><b>Attachement CR</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_REQUEST__ATTACHEMENT_CR = 0;

	/**
	 * The number of structural features of the '<em>Send Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_REQUEST_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.AttachementClientRPCImpl <em>Attachement Client RPC</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.AttachementClientRPCImpl
	 * @see M1.impl.M1PackageImpl#getAttachementClientRPC()
	 * @generated
	 */
	int ATTACHEMENT_CLIENT_RPC = 57;

	/**
	 * The feature id for the '<em><b>Send Request</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_CLIENT_RPC__SEND_REQUEST = 0;

	/**
	 * The feature id for the '<em><b>Role Requie CR</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_CLIENT_RPC__ROLE_REQUIE_CR = 1;

	/**
	 * The number of structural features of the '<em>Attachement Client RPC</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_CLIENT_RPC_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.RoleRequie_CRImpl <em>Role Requie CR</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.RoleRequie_CRImpl
	 * @see M1.impl.M1PackageImpl#getRoleRequie_CR()
	 * @generated
	 */
	int ROLE_REQUIE_CR = 58;

	/**
	 * The feature id for the '<em><b>Attachement CR</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_CR__ATTACHEMENT_CR = 0;

	/**
	 * The number of structural features of the '<em>Role Requie CR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_CR_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.AttachementRPCClientImpl <em>Attachement RPC Client</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.AttachementRPCClientImpl
	 * @see M1.impl.M1PackageImpl#getAttachementRPCClient()
	 * @generated
	 */
	int ATTACHEMENT_RPC_CLIENT = 59;

	/**
	 * The feature id for the '<em><b>Receve Responce</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE = 0;

	/**
	 * The feature id for the '<em><b>Role Fourie RC</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC = 1;

	/**
	 * The number of structural features of the '<em>Attachement RPC Client</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_RPC_CLIENT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.RoleFournie_RCImpl <em>Role Fournie RC</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.RoleFournie_RCImpl
	 * @see M1.impl.M1PackageImpl#getRoleFournie_RC()
	 * @generated
	 */
	int ROLE_FOURNIE_RC = 60;

	/**
	 * The feature id for the '<em><b>Attachement RC</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_RC__ATTACHEMENT_RC = 0;

	/**
	 * The number of structural features of the '<em>Role Fournie RC</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_RC_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.ReceveResponseImpl <em>Receve Response</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.ReceveResponseImpl
	 * @see M1.impl.M1PackageImpl#getReceveResponse()
	 * @generated
	 */
	int RECEVE_RESPONSE = 61;

	/**
	 * The feature id for the '<em><b>Attachement RC</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEVE_RESPONSE__ATTACHEMENT_RC = 0;

	/**
	 * The number of structural features of the '<em>Receve Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEVE_RESPONSE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.AttachementRPCServeurImpl <em>Attachement RPC Serveur</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.AttachementRPCServeurImpl
	 * @see M1.impl.M1PackageImpl#getAttachementRPCServeur()
	 * @generated
	 */
	int ATTACHEMENT_RPC_SERVEUR = 62;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_RPC_SERVEUR__EREFERENCE0 = 0;

	/**
	 * The feature id for the '<em><b>EReference1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_RPC_SERVEUR__EREFERENCE1 = 1;

	/**
	 * The number of structural features of the '<em>Attachement RPC Serveur</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_RPC_SERVEUR_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.RoleRequie_RSImpl <em>Role Requie RS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.RoleRequie_RSImpl
	 * @see M1.impl.M1PackageImpl#getRoleRequie_RS()
	 * @generated
	 */
	int ROLE_REQUIE_RS = 63;

	/**
	 * The feature id for the '<em><b>Attachement RS</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_RS__ATTACHEMENT_RS = 0;

	/**
	 * The number of structural features of the '<em>Role Requie RS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_REQUIE_RS_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.PortFournie_RSImpl <em>Port Fournie RS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.PortFournie_RSImpl
	 * @see M1.impl.M1PackageImpl#getPortFournie_RS()
	 * @generated
	 */
	int PORT_FOURNIE_RS = 64;

	/**
	 * The feature id for the '<em><b>Attachement RS</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_RS__ATTACHEMENT_RS = 0;

	/**
	 * The number of structural features of the '<em>Port Fournie RS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FOURNIE_RS_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.AttachementServeurRPCImpl <em>Attachement Serveur RPC</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.AttachementServeurRPCImpl
	 * @see M1.impl.M1PackageImpl#getAttachementServeurRPC()
	 * @generated
	 */
	int ATTACHEMENT_SERVEUR_RPC = 65;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_SERVEUR_RPC__EREFERENCE0 = 0;

	/**
	 * The feature id for the '<em><b>EReference1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_SERVEUR_RPC__EREFERENCE1 = 1;

	/**
	 * The number of structural features of the '<em>Attachement Serveur RPC</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHEMENT_SERVEUR_RPC_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.RoleFournie_SRImpl <em>Role Fournie SR</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.RoleFournie_SRImpl
	 * @see M1.impl.M1PackageImpl#getRoleFournie_SR()
	 * @generated
	 */
	int ROLE_FOURNIE_SR = 66;

	/**
	 * The feature id for the '<em><b>Attachement SR</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_SR__ATTACHEMENT_SR = 0;

	/**
	 * The number of structural features of the '<em>Role Fournie SR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FOURNIE_SR_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.PortRequie_SRImpl <em>Port Requie SR</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.PortRequie_SRImpl
	 * @see M1.impl.M1PackageImpl#getPortRequie_SR()
	 * @generated
	 */
	int PORT_REQUIE_SR = 67;

	/**
	 * The feature id for the '<em><b>Attachement SR</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_SR__ATTACHEMENT_SR = 0;

	/**
	 * The number of structural features of the '<em>Port Requie SR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REQUIE_SR_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Binding_S_RPCImpl <em>Binding SRPC</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Binding_S_RPCImpl
	 * @see M1.impl.M1PackageImpl#getBinding_S_RPC()
	 * @generated
	 */
	int BINDING_SRPC = 68;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_SRPC__EREFERENCE0 = 0;

	/**
	 * The feature id for the '<em><b>EReference1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_SRPC__EREFERENCE1 = 1;

	/**
	 * The number of structural features of the '<em>Binding SRPC</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_SRPC_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Port_RPC_SImpl <em>Port RPC S</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_RPC_SImpl
	 * @see M1.impl.M1PackageImpl#getPort_RPC_S()
	 * @generated
	 */
	int PORT_RPC_S = 69;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_RPC_S__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Port RPC S</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_RPC_S_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Port_S_RPCImpl <em>Port SRPC</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_S_RPCImpl
	 * @see M1.impl.M1PackageImpl#getPort_S_RPC()
	 * @generated
	 */
	int PORT_SRPC = 70;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SRPC__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Port SRPC</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SRPC_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Binding_CM_SImpl <em>Binding CM S</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Binding_CM_SImpl
	 * @see M1.impl.M1PackageImpl#getBinding_CM_S()
	 * @generated
	 */
	int BINDING_CM_S = 71;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_CM_S__EREFERENCE0 = 0;

	/**
	 * The feature id for the '<em><b>EReference1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_CM_S__EREFERENCE1 = 1;

	/**
	 * The number of structural features of the '<em>Binding CM S</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_CM_S_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link M1.impl.Port_S_CMImpl <em>Port SCM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_S_CMImpl
	 * @see M1.impl.M1PackageImpl#getPort_S_CM()
	 * @generated
	 */
	int PORT_SCM = 72;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SCM__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Port SCM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_SCM_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link M1.impl.Port_CM_SImpl <em>Port CM S</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see M1.impl.Port_CM_SImpl
	 * @see M1.impl.M1PackageImpl#getPort_CM_S()
	 * @generated
	 */
	int PORT_CM_S = 73;

	/**
	 * The feature id for the '<em><b>EReference0</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CM_S__EREFERENCE0 = 0;

	/**
	 * The number of structural features of the '<em>Port CM S</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CM_S_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link M1.Server_Configuration <em>Server Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Configuration</em>'.
	 * @see M1.Server_Configuration
	 * @generated
	 */
	EClass getServer_Configuration();

	/**
	 * Returns the meta object for the reference '{@link M1.Server_Configuration#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Server_Configuration#getEReference0()
	 * @see #getServer_Configuration()
	 * @generated
	 */
	EReference getServer_Configuration_EReference0();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Server_Configuration#getDataBase <em>Data Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Base</em>'.
	 * @see M1.Server_Configuration#getDataBase()
	 * @see #getServer_Configuration()
	 * @generated
	 */
	EReference getServer_Configuration_DataBase();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Server_Configuration#getGestionnaire_Connexion <em>Gestionnaire Connexion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Gestionnaire Connexion</em>'.
	 * @see M1.Server_Configuration#getGestionnaire_Connexion()
	 * @see #getServer_Configuration()
	 * @generated
	 */
	EReference getServer_Configuration_Gestionnaire_Connexion();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Server_Configuration#getConnecteur_GC_DB <em>Connecteur GC DB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connecteur GC DB</em>'.
	 * @see M1.Server_Configuration#getConnecteur_GC_DB()
	 * @see #getServer_Configuration()
	 * @generated
	 */
	EReference getServer_Configuration_Connecteur_GC_DB();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Server_Configuration#getGestion_securite <em>Gestion securite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Gestion securite</em>'.
	 * @see M1.Server_Configuration#getGestion_securite()
	 * @see #getServer_Configuration()
	 * @generated
	 */
	EReference getServer_Configuration_Gestion_securite();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Server_Configuration#getConnecteur_GC_GS <em>Connecteur GC GS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connecteur GC GS</em>'.
	 * @see M1.Server_Configuration#getConnecteur_GC_GS()
	 * @see #getServer_Configuration()
	 * @generated
	 */
	EReference getServer_Configuration_Connecteur_GC_GS();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Server_Configuration#getConnecteur_GS_DB <em>Connecteur GS DB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connecteur GS DB</em>'.
	 * @see M1.Server_Configuration#getConnecteur_GS_DB()
	 * @see #getServer_Configuration()
	 * @generated
	 */
	EReference getServer_Configuration_Connecteur_GS_DB();

	/**
	 * Returns the meta object for class '{@link M1.DataBase <em>Data Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Base</em>'.
	 * @see M1.DataBase
	 * @generated
	 */
	EClass getDataBase();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.DataBase#getIdataBase <em>Idata Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Idata Base</em>'.
	 * @see M1.DataBase#getIdataBase()
	 * @see #getDataBase()
	 * @generated
	 */
	EReference getDataBase_IdataBase();

	/**
	 * Returns the meta object for class '{@link M1.IDataBase <em>IData Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IData Base</em>'.
	 * @see M1.IDataBase
	 * @generated
	 */
	EClass getIDataBase();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IDataBase#getPort_requie_DB_1 <em>Port requie DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port requie DB 1</em>'.
	 * @see M1.IDataBase#getPort_requie_DB_1()
	 * @see #getIDataBase()
	 * @generated
	 */
	EReference getIDataBase_Port_requie_DB_1();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IDataBase#getPort_fournie_DB_2 <em>Port fournie DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port fournie DB 2</em>'.
	 * @see M1.IDataBase#getPort_fournie_DB_2()
	 * @see #getIDataBase()
	 * @generated
	 */
	EReference getIDataBase_Port_fournie_DB_2();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IDataBase#getPort_requie_DB_3 <em>Port requie DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port requie DB 3</em>'.
	 * @see M1.IDataBase#getPort_requie_DB_3()
	 * @see #getIDataBase()
	 * @generated
	 */
	EReference getIDataBase_Port_requie_DB_3();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IDataBase#getPort_fournie_DB_4 <em>Port fournie DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port fournie DB 4</em>'.
	 * @see M1.IDataBase#getPort_fournie_DB_4()
	 * @see #getIDataBase()
	 * @generated
	 */
	EReference getIDataBase_Port_fournie_DB_4();

	/**
	 * Returns the meta object for class '{@link M1.Atachement_DB_1 <em>Atachement DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atachement DB 1</em>'.
	 * @see M1.Atachement_DB_1
	 * @generated
	 */
	EClass getAtachement_DB_1();

	/**
	 * Returns the meta object for the reference '{@link M1.Atachement_DB_1#getRole_fournie_DB_1 <em>Role fournie DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role fournie DB 1</em>'.
	 * @see M1.Atachement_DB_1#getRole_fournie_DB_1()
	 * @see #getAtachement_DB_1()
	 * @generated
	 */
	EReference getAtachement_DB_1_Role_fournie_DB_1();

	/**
	 * Returns the meta object for the reference '{@link M1.Atachement_DB_1#getPort_requie_DB_1 <em>Port requie DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port requie DB 1</em>'.
	 * @see M1.Atachement_DB_1#getPort_requie_DB_1()
	 * @see #getAtachement_DB_1()
	 * @generated
	 */
	EReference getAtachement_DB_1_Port_requie_DB_1();

	/**
	 * Returns the meta object for class '{@link M1.Attachement_DB_2 <em>Attachement DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement DB 2</em>'.
	 * @see M1.Attachement_DB_2
	 * @generated
	 */
	EClass getAttachement_DB_2();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_DB_2#getRole_requie_2 <em>Role requie 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role requie 2</em>'.
	 * @see M1.Attachement_DB_2#getRole_requie_2()
	 * @see #getAttachement_DB_2()
	 * @generated
	 */
	EReference getAttachement_DB_2_Role_requie_2();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_DB_2#getPort_fournie_DB_2 <em>Port fournie DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port fournie DB 2</em>'.
	 * @see M1.Attachement_DB_2#getPort_fournie_DB_2()
	 * @see #getAttachement_DB_2()
	 * @generated
	 */
	EReference getAttachement_DB_2_Port_fournie_DB_2();

	/**
	 * Returns the meta object for class '{@link M1.Attachement_DB_3 <em>Attachement DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement DB 3</em>'.
	 * @see M1.Attachement_DB_3
	 * @generated
	 */
	EClass getAttachement_DB_3();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_DB_3#getRole_fourni_DB_3 <em>Role fourni DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role fourni DB 3</em>'.
	 * @see M1.Attachement_DB_3#getRole_fourni_DB_3()
	 * @see #getAttachement_DB_3()
	 * @generated
	 */
	EReference getAttachement_DB_3_Role_fourni_DB_3();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_DB_3#getPort_requie_DB_3 <em>Port requie DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port requie DB 3</em>'.
	 * @see M1.Attachement_DB_3#getPort_requie_DB_3()
	 * @see #getAttachement_DB_3()
	 * @generated
	 */
	EReference getAttachement_DB_3_Port_requie_DB_3();

	/**
	 * Returns the meta object for class '{@link M1.Attachement_DB_4 <em>Attachement DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement DB 4</em>'.
	 * @see M1.Attachement_DB_4
	 * @generated
	 */
	EClass getAttachement_DB_4();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_DB_4#getRole_Requie_4 <em>Role Requie 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role Requie 4</em>'.
	 * @see M1.Attachement_DB_4#getRole_Requie_4()
	 * @see #getAttachement_DB_4()
	 * @generated
	 */
	EReference getAttachement_DB_4_Role_Requie_4();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_DB_4#getPort_fourne_4 <em>Port fourne 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port fourne 4</em>'.
	 * @see M1.Attachement_DB_4#getPort_fourne_4()
	 * @see #getAttachement_DB_4()
	 * @generated
	 */
	EReference getAttachement_DB_4_Port_fourne_4();

	/**
	 * Returns the meta object for class '{@link M1.Role_Requie_DB_4 <em>Role Requie DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Requie DB 4</em>'.
	 * @see M1.Role_Requie_DB_4
	 * @generated
	 */
	EClass getRole_Requie_DB_4();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_Requie_DB_4#getAttachement_DB_4 <em>Attachement DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement DB 4</em>'.
	 * @see M1.Role_Requie_DB_4#getAttachement_DB_4()
	 * @see #getRole_Requie_DB_4()
	 * @generated
	 */
	EReference getRole_Requie_DB_4_Attachement_DB_4();

	/**
	 * Returns the meta object for class '{@link M1.Port_fornie_DB_4 <em>Port fornie DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port fornie DB 4</em>'.
	 * @see M1.Port_fornie_DB_4
	 * @generated
	 */
	EClass getPort_fornie_DB_4();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_fornie_DB_4#getAttachement_DB_4 <em>Attachement DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement DB 4</em>'.
	 * @see M1.Port_fornie_DB_4#getAttachement_DB_4()
	 * @see #getPort_fornie_DB_4()
	 * @generated
	 */
	EReference getPort_fornie_DB_4_Attachement_DB_4();

	/**
	 * Returns the meta object for class '{@link M1.Role_fournie_DB_3 <em>Role fournie DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role fournie DB 3</em>'.
	 * @see M1.Role_fournie_DB_3
	 * @generated
	 */
	EClass getRole_fournie_DB_3();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_fournie_DB_3#getAttachement_DB_3 <em>Attachement DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement DB 3</em>'.
	 * @see M1.Role_fournie_DB_3#getAttachement_DB_3()
	 * @see #getRole_fournie_DB_3()
	 * @generated
	 */
	EReference getRole_fournie_DB_3_Attachement_DB_3();

	/**
	 * Returns the meta object for class '{@link M1.Port_requie_DB_3 <em>Port requie DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port requie DB 3</em>'.
	 * @see M1.Port_requie_DB_3
	 * @generated
	 */
	EClass getPort_requie_DB_3();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_requie_DB_3#getAttachement_DB_3 <em>Attachement DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement DB 3</em>'.
	 * @see M1.Port_requie_DB_3#getAttachement_DB_3()
	 * @see #getPort_requie_DB_3()
	 * @generated
	 */
	EReference getPort_requie_DB_3_Attachement_DB_3();

	/**
	 * Returns the meta object for class '{@link M1.Role_Requie_DB_2 <em>Role Requie DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Requie DB 2</em>'.
	 * @see M1.Role_Requie_DB_2
	 * @generated
	 */
	EClass getRole_Requie_DB_2();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_Requie_DB_2#getAttachement_DB_2 <em>Attachement DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement DB 2</em>'.
	 * @see M1.Role_Requie_DB_2#getAttachement_DB_2()
	 * @see #getRole_Requie_DB_2()
	 * @generated
	 */
	EReference getRole_Requie_DB_2_Attachement_DB_2();

	/**
	 * Returns the meta object for class '{@link M1.Role_fournie_DB_1 <em>Role fournie DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role fournie DB 1</em>'.
	 * @see M1.Role_fournie_DB_1
	 * @generated
	 */
	EClass getRole_fournie_DB_1();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_fournie_DB_1#getAttachement_DB_1 <em>Attachement DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement DB 1</em>'.
	 * @see M1.Role_fournie_DB_1#getAttachement_DB_1()
	 * @see #getRole_fournie_DB_1()
	 * @generated
	 */
	EReference getRole_fournie_DB_1_Attachement_DB_1();

	/**
	 * Returns the meta object for class '{@link M1.Port_Requie_DB_1 <em>Port Requie DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Requie DB 1</em>'.
	 * @see M1.Port_Requie_DB_1
	 * @generated
	 */
	EClass getPort_Requie_DB_1();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_Requie_DB_1#getAttachement_DB_1 <em>Attachement DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement DB 1</em>'.
	 * @see M1.Port_Requie_DB_1#getAttachement_DB_1()
	 * @see #getPort_Requie_DB_1()
	 * @generated
	 */
	EReference getPort_Requie_DB_1_Attachement_DB_1();

	/**
	 * Returns the meta object for class '{@link M1.Port_fournie_DB_2 <em>Port fournie DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port fournie DB 2</em>'.
	 * @see M1.Port_fournie_DB_2
	 * @generated
	 */
	EClass getPort_fournie_DB_2();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_fournie_DB_2#getAttachement_DB_2 <em>Attachement DB 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement DB 2</em>'.
	 * @see M1.Port_fournie_DB_2#getAttachement_DB_2()
	 * @see #getPort_fournie_DB_2()
	 * @generated
	 */
	EReference getPort_fournie_DB_2_Attachement_DB_2();

	/**
	 * Returns the meta object for class '{@link M1.Gestionnaire_Connexion <em>Gestionnaire Connexion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gestionnaire Connexion</em>'.
	 * @see M1.Gestionnaire_Connexion
	 * @generated
	 */
	EClass getGestionnaire_Connexion();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Gestionnaire_Connexion#getIgestionnaireConnexion <em>Igestionnaire Connexion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Igestionnaire Connexion</em>'.
	 * @see M1.Gestionnaire_Connexion#getIgestionnaireConnexion()
	 * @see #getGestionnaire_Connexion()
	 * @generated
	 */
	EReference getGestionnaire_Connexion_IgestionnaireConnexion();

	/**
	 * Returns the meta object for class '{@link M1.IGestionnaire_Connexion <em>IGestionnaire Connexion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IGestionnaire Connexion</em>'.
	 * @see M1.IGestionnaire_Connexion
	 * @generated
	 */
	EClass getIGestionnaire_Connexion();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IGestionnaire_Connexion#getPort_Fournie_Gc_1 <em>Port Fournie Gc 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port Fournie Gc 1</em>'.
	 * @see M1.IGestionnaire_Connexion#getPort_Fournie_Gc_1()
	 * @see #getIGestionnaire_Connexion()
	 * @generated
	 */
	EReference getIGestionnaire_Connexion_Port_Fournie_Gc_1();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IGestionnaire_Connexion#getPort_Requie_Gc_2 <em>Port Requie Gc 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port Requie Gc 2</em>'.
	 * @see M1.IGestionnaire_Connexion#getPort_Requie_Gc_2()
	 * @see #getIGestionnaire_Connexion()
	 * @generated
	 */
	EReference getIGestionnaire_Connexion_Port_Requie_Gc_2();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IGestionnaire_Connexion#getPort_Fournie_Gc_3 <em>Port Fournie Gc 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port Fournie Gc 3</em>'.
	 * @see M1.IGestionnaire_Connexion#getPort_Fournie_Gc_3()
	 * @see #getIGestionnaire_Connexion()
	 * @generated
	 */
	EReference getIGestionnaire_Connexion_Port_Fournie_Gc_3();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IGestionnaire_Connexion#getPort_Requie_Gc_4 <em>Port Requie Gc 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port Requie Gc 4</em>'.
	 * @see M1.IGestionnaire_Connexion#getPort_Requie_Gc_4()
	 * @see #getIGestionnaire_Connexion()
	 * @generated
	 */
	EReference getIGestionnaire_Connexion_Port_Requie_Gc_4();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IGestionnaire_Connexion#getPort_CM_S <em>Port CM S</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port CM S</em>'.
	 * @see M1.IGestionnaire_Connexion#getPort_CM_S()
	 * @see #getIGestionnaire_Connexion()
	 * @generated
	 */
	EReference getIGestionnaire_Connexion_Port_CM_S();

	/**
	 * Returns the meta object for class '{@link M1.Attachement_GC_2 <em>Attachement GC 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement GC 2</em>'.
	 * @see M1.Attachement_GC_2
	 * @generated
	 */
	EClass getAttachement_GC_2();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GC_2#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Attachement_GC_2#getEReference0()
	 * @see #getAttachement_GC_2()
	 * @generated
	 */
	EReference getAttachement_GC_2_EReference0();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GC_2#getEReference1 <em>EReference1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference1</em>'.
	 * @see M1.Attachement_GC_2#getEReference1()
	 * @see #getAttachement_GC_2()
	 * @generated
	 */
	EReference getAttachement_GC_2_EReference1();

	/**
	 * Returns the meta object for class '{@link M1.Attachement_GC_3 <em>Attachement GC 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement GC 3</em>'.
	 * @see M1.Attachement_GC_3
	 * @generated
	 */
	EClass getAttachement_GC_3();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GC_3#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Attachement_GC_3#getEReference0()
	 * @see #getAttachement_GC_3()
	 * @generated
	 */
	EReference getAttachement_GC_3_EReference0();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GC_3#getEReference1 <em>EReference1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference1</em>'.
	 * @see M1.Attachement_GC_3#getEReference1()
	 * @see #getAttachement_GC_3()
	 * @generated
	 */
	EReference getAttachement_GC_3_EReference1();

	/**
	 * Returns the meta object for class '{@link M1.Attachement_GC_4 <em>Attachement GC 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement GC 4</em>'.
	 * @see M1.Attachement_GC_4
	 * @generated
	 */
	EClass getAttachement_GC_4();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GC_4#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Attachement_GC_4#getEReference0()
	 * @see #getAttachement_GC_4()
	 * @generated
	 */
	EReference getAttachement_GC_4_EReference0();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GC_4#getEReference1 <em>EReference1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference1</em>'.
	 * @see M1.Attachement_GC_4#getEReference1()
	 * @see #getAttachement_GC_4()
	 * @generated
	 */
	EReference getAttachement_GC_4_EReference1();

	/**
	 * Returns the meta object for class '{@link M1.Attachement_GC_1 <em>Attachement GC 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement GC 1</em>'.
	 * @see M1.Attachement_GC_1
	 * @generated
	 */
	EClass getAttachement_GC_1();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GC_1#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Attachement_GC_1#getEReference0()
	 * @see #getAttachement_GC_1()
	 * @generated
	 */
	EReference getAttachement_GC_1_EReference0();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GC_1#getEReference1 <em>EReference1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference1</em>'.
	 * @see M1.Attachement_GC_1#getEReference1()
	 * @see #getAttachement_GC_1()
	 * @generated
	 */
	EReference getAttachement_GC_1_EReference1();

	/**
	 * Returns the meta object for class '{@link M1.Role_Requie_GC_3 <em>Role Requie GC 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Requie GC 3</em>'.
	 * @see M1.Role_Requie_GC_3
	 * @generated
	 */
	EClass getRole_Requie_GC_3();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_Requie_GC_3#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Role_Requie_GC_3#getEReference0()
	 * @see #getRole_Requie_GC_3()
	 * @generated
	 */
	EReference getRole_Requie_GC_3_EReference0();

	/**
	 * Returns the meta object for class '{@link M1.Port_Requie_GC_4 <em>Port Requie GC 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Requie GC 4</em>'.
	 * @see M1.Port_Requie_GC_4
	 * @generated
	 */
	EClass getPort_Requie_GC_4();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_Requie_GC_4#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Port_Requie_GC_4#getEReference0()
	 * @see #getPort_Requie_GC_4()
	 * @generated
	 */
	EReference getPort_Requie_GC_4_EReference0();

	/**
	 * Returns the meta object for class '{@link M1.Port_Fournie_GC_3 <em>Port Fournie GC 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Fournie GC 3</em>'.
	 * @see M1.Port_Fournie_GC_3
	 * @generated
	 */
	EClass getPort_Fournie_GC_3();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_Fournie_GC_3#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Port_Fournie_GC_3#getEReference0()
	 * @see #getPort_Fournie_GC_3()
	 * @generated
	 */
	EReference getPort_Fournie_GC_3_EReference0();

	/**
	 * Returns the meta object for class '{@link M1.Role_Fournie_GC_4 <em>Role Fournie GC 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Fournie GC 4</em>'.
	 * @see M1.Role_Fournie_GC_4
	 * @generated
	 */
	EClass getRole_Fournie_GC_4();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_Fournie_GC_4#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Role_Fournie_GC_4#getEReference0()
	 * @see #getRole_Fournie_GC_4()
	 * @generated
	 */
	EReference getRole_Fournie_GC_4_EReference0();

	/**
	 * Returns the meta object for class '{@link M1.Role_Fournie_GC_2 <em>Role Fournie GC 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Fournie GC 2</em>'.
	 * @see M1.Role_Fournie_GC_2
	 * @generated
	 */
	EClass getRole_Fournie_GC_2();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_Fournie_GC_2#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Role_Fournie_GC_2#getEReference0()
	 * @see #getRole_Fournie_GC_2()
	 * @generated
	 */
	EReference getRole_Fournie_GC_2_EReference0();

	/**
	 * Returns the meta object for class '{@link M1.Port_Requie_GC_2 <em>Port Requie GC 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Requie GC 2</em>'.
	 * @see M1.Port_Requie_GC_2
	 * @generated
	 */
	EClass getPort_Requie_GC_2();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_Requie_GC_2#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Port_Requie_GC_2#getEReference0()
	 * @see #getPort_Requie_GC_2()
	 * @generated
	 */
	EReference getPort_Requie_GC_2_EReference0();

	/**
	 * Returns the meta object for class '{@link M1.Role_Requie_GC_1 <em>Role Requie GC 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Requie GC 1</em>'.
	 * @see M1.Role_Requie_GC_1
	 * @generated
	 */
	EClass getRole_Requie_GC_1();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_Requie_GC_1#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Role_Requie_GC_1#getEReference0()
	 * @see #getRole_Requie_GC_1()
	 * @generated
	 */
	EReference getRole_Requie_GC_1_EReference0();

	/**
	 * Returns the meta object for class '{@link M1.Port_Fournie_GC_1 <em>Port Fournie GC 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Fournie GC 1</em>'.
	 * @see M1.Port_Fournie_GC_1
	 * @generated
	 */
	EClass getPort_Fournie_GC_1();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_Fournie_GC_1#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Port_Fournie_GC_1#getEReference0()
	 * @see #getPort_Fournie_GC_1()
	 * @generated
	 */
	EReference getPort_Fournie_GC_1_EReference0();

	/**
	 * Returns the meta object for class '{@link M1.IConnecteur_GC_DB <em>IConnecteur GC DB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IConnecteur GC DB</em>'.
	 * @see M1.IConnecteur_GC_DB
	 * @generated
	 */
	EClass getIConnecteur_GC_DB();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IConnecteur_GC_DB#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>EReference0</em>'.
	 * @see M1.IConnecteur_GC_DB#getEReference0()
	 * @see #getIConnecteur_GC_DB()
	 * @generated
	 */
	EReference getIConnecteur_GC_DB_EReference0();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IConnecteur_GC_DB#getEReference1 <em>EReference1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>EReference1</em>'.
	 * @see M1.IConnecteur_GC_DB#getEReference1()
	 * @see #getIConnecteur_GC_DB()
	 * @generated
	 */
	EReference getIConnecteur_GC_DB_EReference1();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IConnecteur_GC_DB#getEReference2 <em>EReference2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>EReference2</em>'.
	 * @see M1.IConnecteur_GC_DB#getEReference2()
	 * @see #getIConnecteur_GC_DB()
	 * @generated
	 */
	EReference getIConnecteur_GC_DB_EReference2();

	/**
	 * Returns the meta object for the containment reference '{@link M1.IConnecteur_GC_DB#getEReference3 <em>EReference3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>EReference3</em>'.
	 * @see M1.IConnecteur_GC_DB#getEReference3()
	 * @see #getIConnecteur_GC_DB()
	 * @generated
	 */
	EReference getIConnecteur_GC_DB_EReference3();

	/**
	 * Returns the meta object for class '{@link M1.Connecteur_GC_DB <em>Connecteur GC DB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connecteur GC DB</em>'.
	 * @see M1.Connecteur_GC_DB
	 * @generated
	 */
	EClass getConnecteur_GC_DB();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Connecteur_GC_DB#getIconnecteurBCDB <em>Iconnecteur BCDB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Iconnecteur BCDB</em>'.
	 * @see M1.Connecteur_GC_DB#getIconnecteurBCDB()
	 * @see #getConnecteur_GC_DB()
	 * @generated
	 */
	EReference getConnecteur_GC_DB_IconnecteurBCDB();

	/**
	 * Returns the meta object for class '{@link M1.Gestion_Securiterity <em>Gestion Securiterity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gestion Securiterity</em>'.
	 * @see M1.Gestion_Securiterity
	 * @generated
	 */
	EClass getGestion_Securiterity();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Gestion_Securiterity#getIgestionSecurite <em>Igestion Securite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Igestion Securite</em>'.
	 * @see M1.Gestion_Securiterity#getIgestionSecurite()
	 * @see #getGestion_Securiterity()
	 * @generated
	 */
	EReference getGestion_Securiterity_IgestionSecurite();

	/**
	 * Returns the meta object for class '{@link M1.Attachement_GS_1 <em>Attachement GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement GS 1</em>'.
	 * @see M1.Attachement_GS_1
	 * @generated
	 */
	EClass getAttachement_GS_1();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GS_1#getPort_fournie_GS_1 <em>Port fournie GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port fournie GS 1</em>'.
	 * @see M1.Attachement_GS_1#getPort_fournie_GS_1()
	 * @see #getAttachement_GS_1()
	 * @generated
	 */
	EReference getAttachement_GS_1_Port_fournie_GS_1();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GS_1#getRole_requie_GS_1 <em>Role requie GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role requie GS 1</em>'.
	 * @see M1.Attachement_GS_1#getRole_requie_GS_1()
	 * @see #getAttachement_GS_1()
	 * @generated
	 */
	EReference getAttachement_GS_1_Role_requie_GS_1();

	/**
	 * Returns the meta object for class '{@link M1.Port_Requie_GS_2 <em>Port Requie GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Requie GS 2</em>'.
	 * @see M1.Port_Requie_GS_2
	 * @generated
	 */
	EClass getPort_Requie_GS_2();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_Requie_GS_2#getAttachement_GS_2 <em>Attachement GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement GS 2</em>'.
	 * @see M1.Port_Requie_GS_2#getAttachement_GS_2()
	 * @see #getPort_Requie_GS_2()
	 * @generated
	 */
	EReference getPort_Requie_GS_2_Attachement_GS_2();

	/**
	 * Returns the meta object for class '{@link M1.Attachement_GS_2 <em>Attachement GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement GS 2</em>'.
	 * @see M1.Attachement_GS_2
	 * @generated
	 */
	EClass getAttachement_GS_2();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GS_2#getPort_requie_GS_2 <em>Port requie GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port requie GS 2</em>'.
	 * @see M1.Attachement_GS_2#getPort_requie_GS_2()
	 * @see #getAttachement_GS_2()
	 * @generated
	 */
	EReference getAttachement_GS_2_Port_requie_GS_2();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GS_2#getRole_fournie_GS_2 <em>Role fournie GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role fournie GS 2</em>'.
	 * @see M1.Attachement_GS_2#getRole_fournie_GS_2()
	 * @see #getAttachement_GS_2()
	 * @generated
	 */
	EReference getAttachement_GS_2_Role_fournie_GS_2();

	/**
	 * Returns the meta object for class '{@link M1.Attachement_GS_3 <em>Attachement GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement GS 3</em>'.
	 * @see M1.Attachement_GS_3
	 * @generated
	 */
	EClass getAttachement_GS_3();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GS_3#getPort_fournie_GS_3 <em>Port fournie GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port fournie GS 3</em>'.
	 * @see M1.Attachement_GS_3#getPort_fournie_GS_3()
	 * @see #getAttachement_GS_3()
	 * @generated
	 */
	EReference getAttachement_GS_3_Port_fournie_GS_3();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GS_3#getRole_requie_GS_3 <em>Role requie GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role requie GS 3</em>'.
	 * @see M1.Attachement_GS_3#getRole_requie_GS_3()
	 * @see #getAttachement_GS_3()
	 * @generated
	 */
	EReference getAttachement_GS_3_Role_requie_GS_3();

	/**
	 * Returns the meta object for class '{@link M1.Port_Fournie_GS_1 <em>Port Fournie GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Fournie GS 1</em>'.
	 * @see M1.Port_Fournie_GS_1
	 * @generated
	 */
	EClass getPort_Fournie_GS_1();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_Fournie_GS_1#getAttachement_GS_1 <em>Attachement GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement GS 1</em>'.
	 * @see M1.Port_Fournie_GS_1#getAttachement_GS_1()
	 * @see #getPort_Fournie_GS_1()
	 * @generated
	 */
	EReference getPort_Fournie_GS_1_Attachement_GS_1();

	/**
	 * Returns the meta object for class '{@link M1.Role_Requie_GS_1 <em>Role Requie GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Requie GS 1</em>'.
	 * @see M1.Role_Requie_GS_1
	 * @generated
	 */
	EClass getRole_Requie_GS_1();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_Requie_GS_1#getAttachement_GS_1 <em>Attachement GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement GS 1</em>'.
	 * @see M1.Role_Requie_GS_1#getAttachement_GS_1()
	 * @see #getRole_Requie_GS_1()
	 * @generated
	 */
	EReference getRole_Requie_GS_1_Attachement_GS_1();

	/**
	 * Returns the meta object for class '{@link M1.Role_Fournie_GS_2 <em>Role Fournie GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Fournie GS 2</em>'.
	 * @see M1.Role_Fournie_GS_2
	 * @generated
	 */
	EClass getRole_Fournie_GS_2();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_Fournie_GS_2#getAttachement_GS_2 <em>Attachement GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement GS 2</em>'.
	 * @see M1.Role_Fournie_GS_2#getAttachement_GS_2()
	 * @see #getRole_Fournie_GS_2()
	 * @generated
	 */
	EReference getRole_Fournie_GS_2_Attachement_GS_2();

	/**
	 * Returns the meta object for class '{@link M1.Port_Fournie_GS_3 <em>Port Fournie GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Fournie GS 3</em>'.
	 * @see M1.Port_Fournie_GS_3
	 * @generated
	 */
	EClass getPort_Fournie_GS_3();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_Fournie_GS_3#getAttachement_GS_3 <em>Attachement GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement GS 3</em>'.
	 * @see M1.Port_Fournie_GS_3#getAttachement_GS_3()
	 * @see #getPort_Fournie_GS_3()
	 * @generated
	 */
	EReference getPort_Fournie_GS_3_Attachement_GS_3();

	/**
	 * Returns the meta object for class '{@link M1.Role_Requie_GS_3 <em>Role Requie GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Requie GS 3</em>'.
	 * @see M1.Role_Requie_GS_3
	 * @generated
	 */
	EClass getRole_Requie_GS_3();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_Requie_GS_3#getAttachement_GS_3 <em>Attachement GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement GS 3</em>'.
	 * @see M1.Role_Requie_GS_3#getAttachement_GS_3()
	 * @see #getRole_Requie_GS_3()
	 * @generated
	 */
	EReference getRole_Requie_GS_3_Attachement_GS_3();

	/**
	 * Returns the meta object for class '{@link M1.Attachement_GS_4 <em>Attachement GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement GS 4</em>'.
	 * @see M1.Attachement_GS_4
	 * @generated
	 */
	EClass getAttachement_GS_4();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GS_4#getPort_requie_GS_4 <em>Port requie GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port requie GS 4</em>'.
	 * @see M1.Attachement_GS_4#getPort_requie_GS_4()
	 * @see #getAttachement_GS_4()
	 * @generated
	 */
	EReference getAttachement_GS_4_Port_requie_GS_4();

	/**
	 * Returns the meta object for the reference '{@link M1.Attachement_GS_4#getRole_fournie_GS_4 <em>Role fournie GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role fournie GS 4</em>'.
	 * @see M1.Attachement_GS_4#getRole_fournie_GS_4()
	 * @see #getAttachement_GS_4()
	 * @generated
	 */
	EReference getAttachement_GS_4_Role_fournie_GS_4();

	/**
	 * Returns the meta object for class '{@link M1.Port_Requie_GS_4 <em>Port Requie GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Requie GS 4</em>'.
	 * @see M1.Port_Requie_GS_4
	 * @generated
	 */
	EClass getPort_Requie_GS_4();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_Requie_GS_4#getAttachement_GS_4 <em>Attachement GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement GS 4</em>'.
	 * @see M1.Port_Requie_GS_4#getAttachement_GS_4()
	 * @see #getPort_Requie_GS_4()
	 * @generated
	 */
	EReference getPort_Requie_GS_4_Attachement_GS_4();

	/**
	 * Returns the meta object for class '{@link M1.Role_Fournie_GS_4 <em>Role Fournie GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Fournie GS 4</em>'.
	 * @see M1.Role_Fournie_GS_4
	 * @generated
	 */
	EClass getRole_Fournie_GS_4();

	/**
	 * Returns the meta object for the reference '{@link M1.Role_Fournie_GS_4#getAttachement_GS_4 <em>Attachement GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement GS 4</em>'.
	 * @see M1.Role_Fournie_GS_4#getAttachement_GS_4()
	 * @see #getRole_Fournie_GS_4()
	 * @generated
	 */
	EReference getRole_Fournie_GS_4_Attachement_GS_4();

	/**
	 * Returns the meta object for class '{@link M1.IGestion_Securite <em>IGestion Securite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IGestion Securite</em>'.
	 * @see M1.IGestion_Securite
	 * @generated
	 */
	EClass getIGestion_Securite();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IGestion_Securite#getPort_fournie_GS_1 <em>Port fournie GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port fournie GS 1</em>'.
	 * @see M1.IGestion_Securite#getPort_fournie_GS_1()
	 * @see #getIGestion_Securite()
	 * @generated
	 */
	EReference getIGestion_Securite_Port_fournie_GS_1();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IGestion_Securite#getPort_requie_GS_2 <em>Port requie GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port requie GS 2</em>'.
	 * @see M1.IGestion_Securite#getPort_requie_GS_2()
	 * @see #getIGestion_Securite()
	 * @generated
	 */
	EReference getIGestion_Securite_Port_requie_GS_2();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IGestion_Securite#getPort_fournie_GS_3 <em>Port fournie GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port fournie GS 3</em>'.
	 * @see M1.IGestion_Securite#getPort_fournie_GS_3()
	 * @see #getIGestion_Securite()
	 * @generated
	 */
	EReference getIGestion_Securite_Port_fournie_GS_3();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IGestion_Securite#getPort_requie_GS_4 <em>Port requie GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port requie GS 4</em>'.
	 * @see M1.IGestion_Securite#getPort_requie_GS_4()
	 * @see #getIGestion_Securite()
	 * @generated
	 */
	EReference getIGestion_Securite_Port_requie_GS_4();

	/**
	 * Returns the meta object for class '{@link M1.Connecteur_GC_GS <em>Connecteur GC GS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connecteur GC GS</em>'.
	 * @see M1.Connecteur_GC_GS
	 * @generated
	 */
	EClass getConnecteur_GC_GS();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Connecteur_GC_GS#getIconnecteur_GC_GS <em>Iconnecteur GC GS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Iconnecteur GC GS</em>'.
	 * @see M1.Connecteur_GC_GS#getIconnecteur_GC_GS()
	 * @see #getConnecteur_GC_GS()
	 * @generated
	 */
	EReference getConnecteur_GC_GS_Iconnecteur_GC_GS();

	/**
	 * Returns the meta object for class '{@link M1.IConnecteur_GC_GS <em>IConnecteur GC GS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IConnecteur GC GS</em>'.
	 * @see M1.IConnecteur_GC_GS
	 * @generated
	 */
	EClass getIConnecteur_GC_GS();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteur_GC_GS#getRole_requie_GC_3 <em>Role requie GC 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role requie GC 3</em>'.
	 * @see M1.IConnecteur_GC_GS#getRole_requie_GC_3()
	 * @see #getIConnecteur_GC_GS()
	 * @generated
	 */
	EReference getIConnecteur_GC_GS_Role_requie_GC_3();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteur_GC_GS#getRole_fournie_GC_4 <em>Role fournie GC 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role fournie GC 4</em>'.
	 * @see M1.IConnecteur_GC_GS#getRole_fournie_GC_4()
	 * @see #getIConnecteur_GC_GS()
	 * @generated
	 */
	EReference getIConnecteur_GC_GS_Role_fournie_GC_4();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteur_GC_GS#getRole_requie_GS_1 <em>Role requie GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role requie GS 1</em>'.
	 * @see M1.IConnecteur_GC_GS#getRole_requie_GS_1()
	 * @see #getIConnecteur_GC_GS()
	 * @generated
	 */
	EReference getIConnecteur_GC_GS_Role_requie_GS_1();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteur_GC_GS#getRole_fournie_GS_2 <em>Role fournie GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role fournie GS 2</em>'.
	 * @see M1.IConnecteur_GC_GS#getRole_fournie_GS_2()
	 * @see #getIConnecteur_GC_GS()
	 * @generated
	 */
	EReference getIConnecteur_GC_GS_Role_fournie_GS_2();

	/**
	 * Returns the meta object for class '{@link M1.IConnecteur_GS_DB <em>IConnecteur GS DB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IConnecteur GS DB</em>'.
	 * @see M1.IConnecteur_GS_DB
	 * @generated
	 */
	EClass getIConnecteur_GS_DB();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteur_GS_DB#getRole_fournie_DB_3 <em>Role fournie DB 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role fournie DB 3</em>'.
	 * @see M1.IConnecteur_GS_DB#getRole_fournie_DB_3()
	 * @see #getIConnecteur_GS_DB()
	 * @generated
	 */
	EReference getIConnecteur_GS_DB_Role_fournie_DB_3();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteur_GS_DB#getRole_requie_DB_4 <em>Role requie DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role requie DB 4</em>'.
	 * @see M1.IConnecteur_GS_DB#getRole_requie_DB_4()
	 * @see #getIConnecteur_GS_DB()
	 * @generated
	 */
	EReference getIConnecteur_GS_DB_Role_requie_DB_4();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteur_GS_DB#getRole_fournie_GS_4 <em>Role fournie GS 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role fournie GS 4</em>'.
	 * @see M1.IConnecteur_GS_DB#getRole_fournie_GS_4()
	 * @see #getIConnecteur_GS_DB()
	 * @generated
	 */
	EReference getIConnecteur_GS_DB_Role_fournie_GS_4();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteur_GS_DB#getRole_requie_GS_3 <em>Role requie GS 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role requie GS 3</em>'.
	 * @see M1.IConnecteur_GS_DB#getRole_requie_GS_3()
	 * @see #getIConnecteur_GS_DB()
	 * @generated
	 */
	EReference getIConnecteur_GS_DB_Role_requie_GS_3();

	/**
	 * Returns the meta object for class '{@link M1.Connecteur_GS_DB <em>Connecteur GS DB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connecteur GS DB</em>'.
	 * @see M1.Connecteur_GS_DB
	 * @generated
	 */
	EClass getConnecteur_GS_DB();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Connecteur_GS_DB#getIconnecteur_GS_DB <em>Iconnecteur GS DB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Iconnecteur GS DB</em>'.
	 * @see M1.Connecteur_GS_DB#getIconnecteur_GS_DB()
	 * @see #getConnecteur_GS_DB()
	 * @generated
	 */
	EReference getConnecteur_GS_DB_Iconnecteur_GS_DB();

	/**
	 * Returns the meta object for class '{@link M1.ClientServeur <em>Client Serveur</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Client Serveur</em>'.
	 * @see M1.ClientServeur
	 * @generated
	 */
	EClass getClientServeur();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.ClientServeur#getClient <em>Client</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Client</em>'.
	 * @see M1.ClientServeur#getClient()
	 * @see #getClientServeur()
	 * @generated
	 */
	EReference getClientServeur_Client();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.ClientServeur#getConnecteurRPC <em>Connecteur RPC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connecteur RPC</em>'.
	 * @see M1.ClientServeur#getConnecteurRPC()
	 * @see #getClientServeur()
	 * @generated
	 */
	EReference getClientServeur_ConnecteurRPC();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.ClientServeur#getServeur <em>Serveur</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Serveur</em>'.
	 * @see M1.ClientServeur#getServeur()
	 * @see #getClientServeur()
	 * @generated
	 */
	EReference getClientServeur_Serveur();

	/**
	 * Returns the meta object for class '{@link M1.Client <em>Client</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Client</em>'.
	 * @see M1.Client
	 * @generated
	 */
	EClass getClient();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Client#getInterfaceClient <em>Interface Client</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interface Client</em>'.
	 * @see M1.Client#getInterfaceClient()
	 * @see #getClient()
	 * @generated
	 */
	EReference getClient_InterfaceClient();

	/**
	 * Returns the meta object for class '{@link M1.ConnecteurRPC <em>Connecteur RPC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connecteur RPC</em>'.
	 * @see M1.ConnecteurRPC
	 * @generated
	 */
	EClass getConnecteurRPC();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.ConnecteurRPC#getInterfaceConnecteur <em>Interface Connecteur</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interface Connecteur</em>'.
	 * @see M1.ConnecteurRPC#getInterfaceConnecteur()
	 * @see #getConnecteurRPC()
	 * @generated
	 */
	EReference getConnecteurRPC_InterfaceConnecteur();

	/**
	 * Returns the meta object for class '{@link M1.Serveur <em>Serveur</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Serveur</em>'.
	 * @see M1.Serveur
	 * @generated
	 */
	EClass getServeur();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Serveur#getInterfaceServeur <em>Interface Serveur</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interface Serveur</em>'.
	 * @see M1.Serveur#getInterfaceServeur()
	 * @see #getServeur()
	 * @generated
	 */
	EReference getServeur_InterfaceServeur();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.Serveur#getServeur_Config <em>Serveur Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Serveur Config</em>'.
	 * @see M1.Serveur#getServeur_Config()
	 * @see #getServeur()
	 * @generated
	 */
	EReference getServeur_Serveur_Config();

	/**
	 * Returns the meta object for class '{@link M1.IClient <em>IClient</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IClient</em>'.
	 * @see M1.IClient
	 * @generated
	 */
	EClass getIClient();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IClient#getSendRequest <em>Send Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Send Request</em>'.
	 * @see M1.IClient#getSendRequest()
	 * @see #getIClient()
	 * @generated
	 */
	EReference getIClient_SendRequest();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IClient#getReceveRequest <em>Receve Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Receve Request</em>'.
	 * @see M1.IClient#getReceveRequest()
	 * @see #getIClient()
	 * @generated
	 */
	EReference getIClient_ReceveRequest();

	/**
	 * Returns the meta object for class '{@link M1.IConnecteurRPC <em>IConnecteur RPC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IConnecteur RPC</em>'.
	 * @see M1.IConnecteurRPC
	 * @generated
	 */
	EClass getIConnecteurRPC();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteurRPC#getRoleRequie_CR <em>Role Requie CR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role Requie CR</em>'.
	 * @see M1.IConnecteurRPC#getRoleRequie_CR()
	 * @see #getIConnecteurRPC()
	 * @generated
	 */
	EReference getIConnecteurRPC_RoleRequie_CR();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteurRPC#getRoleFournie_RC <em>Role Fournie RC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role Fournie RC</em>'.
	 * @see M1.IConnecteurRPC#getRoleFournie_RC()
	 * @see #getIConnecteurRPC()
	 * @generated
	 */
	EReference getIConnecteurRPC_RoleFournie_RC();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteurRPC#getRoleRequie_RS <em>Role Requie RS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role Requie RS</em>'.
	 * @see M1.IConnecteurRPC#getRoleRequie_RS()
	 * @see #getIConnecteurRPC()
	 * @generated
	 */
	EReference getIConnecteurRPC_RoleRequie_RS();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteurRPC#getRoleFournie_SR <em>Role Fournie SR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role Fournie SR</em>'.
	 * @see M1.IConnecteurRPC#getRoleFournie_SR()
	 * @see #getIConnecteurRPC()
	 * @generated
	 */
	EReference getIConnecteurRPC_RoleFournie_SR();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IConnecteurRPC#getPort_RPC_S <em>Port RPC S</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port RPC S</em>'.
	 * @see M1.IConnecteurRPC#getPort_RPC_S()
	 * @see #getIConnecteurRPC()
	 * @generated
	 */
	EReference getIConnecteurRPC_Port_RPC_S();

	/**
	 * Returns the meta object for class '{@link M1.IServeur <em>IServeur</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IServeur</em>'.
	 * @see M1.IServeur
	 * @generated
	 */
	EClass getIServeur();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IServeur#getPortFournie_RS <em>Port Fournie RS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port Fournie RS</em>'.
	 * @see M1.IServeur#getPortFournie_RS()
	 * @see #getIServeur()
	 * @generated
	 */
	EReference getIServeur_PortFournie_RS();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IServeur#getPortRequie_SR <em>Port Requie SR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port Requie SR</em>'.
	 * @see M1.IServeur#getPortRequie_SR()
	 * @see #getIServeur()
	 * @generated
	 */
	EReference getIServeur_PortRequie_SR();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IServeur#getPert_S_CM <em>Pert SCM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pert SCM</em>'.
	 * @see M1.IServeur#getPert_S_CM()
	 * @see #getIServeur()
	 * @generated
	 */
	EReference getIServeur_Pert_S_CM();

	/**
	 * Returns the meta object for the containment reference list '{@link M1.IServeur#getPort_S_RPC <em>Port SRPC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port SRPC</em>'.
	 * @see M1.IServeur#getPort_S_RPC()
	 * @see #getIServeur()
	 * @generated
	 */
	EReference getIServeur_Port_S_RPC();

	/**
	 * Returns the meta object for class '{@link M1.SendRequest <em>Send Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Send Request</em>'.
	 * @see M1.SendRequest
	 * @generated
	 */
	EClass getSendRequest();

	/**
	 * Returns the meta object for the reference '{@link M1.SendRequest#getAttachement_CR <em>Attachement CR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement CR</em>'.
	 * @see M1.SendRequest#getAttachement_CR()
	 * @see #getSendRequest()
	 * @generated
	 */
	EReference getSendRequest_Attachement_CR();

	/**
	 * Returns the meta object for class '{@link M1.AttachementClientRPC <em>Attachement Client RPC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement Client RPC</em>'.
	 * @see M1.AttachementClientRPC
	 * @generated
	 */
	EClass getAttachementClientRPC();

	/**
	 * Returns the meta object for the reference '{@link M1.AttachementClientRPC#getSendRequest <em>Send Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Send Request</em>'.
	 * @see M1.AttachementClientRPC#getSendRequest()
	 * @see #getAttachementClientRPC()
	 * @generated
	 */
	EReference getAttachementClientRPC_SendRequest();

	/**
	 * Returns the meta object for the reference '{@link M1.AttachementClientRPC#getRoleRequie_CR <em>Role Requie CR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role Requie CR</em>'.
	 * @see M1.AttachementClientRPC#getRoleRequie_CR()
	 * @see #getAttachementClientRPC()
	 * @generated
	 */
	EReference getAttachementClientRPC_RoleRequie_CR();

	/**
	 * Returns the meta object for class '{@link M1.RoleRequie_CR <em>Role Requie CR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Requie CR</em>'.
	 * @see M1.RoleRequie_CR
	 * @generated
	 */
	EClass getRoleRequie_CR();

	/**
	 * Returns the meta object for the reference '{@link M1.RoleRequie_CR#getAttachement_CR <em>Attachement CR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement CR</em>'.
	 * @see M1.RoleRequie_CR#getAttachement_CR()
	 * @see #getRoleRequie_CR()
	 * @generated
	 */
	EReference getRoleRequie_CR_Attachement_CR();

	/**
	 * Returns the meta object for class '{@link M1.AttachementRPCClient <em>Attachement RPC Client</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement RPC Client</em>'.
	 * @see M1.AttachementRPCClient
	 * @generated
	 */
	EClass getAttachementRPCClient();

	/**
	 * Returns the meta object for the reference '{@link M1.AttachementRPCClient#getReceveResponce <em>Receve Responce</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Receve Responce</em>'.
	 * @see M1.AttachementRPCClient#getReceveResponce()
	 * @see #getAttachementRPCClient()
	 * @generated
	 */
	EReference getAttachementRPCClient_ReceveResponce();

	/**
	 * Returns the meta object for the reference '{@link M1.AttachementRPCClient#getRoleFourie_RC <em>Role Fourie RC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role Fourie RC</em>'.
	 * @see M1.AttachementRPCClient#getRoleFourie_RC()
	 * @see #getAttachementRPCClient()
	 * @generated
	 */
	EReference getAttachementRPCClient_RoleFourie_RC();

	/**
	 * Returns the meta object for class '{@link M1.RoleFournie_RC <em>Role Fournie RC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Fournie RC</em>'.
	 * @see M1.RoleFournie_RC
	 * @generated
	 */
	EClass getRoleFournie_RC();

	/**
	 * Returns the meta object for the reference '{@link M1.RoleFournie_RC#getAttachementRC <em>Attachement RC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement RC</em>'.
	 * @see M1.RoleFournie_RC#getAttachementRC()
	 * @see #getRoleFournie_RC()
	 * @generated
	 */
	EReference getRoleFournie_RC_AttachementRC();

	/**
	 * Returns the meta object for class '{@link M1.ReceveResponse <em>Receve Response</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Receve Response</em>'.
	 * @see M1.ReceveResponse
	 * @generated
	 */
	EClass getReceveResponse();

	/**
	 * Returns the meta object for the reference '{@link M1.ReceveResponse#getAttachementRC <em>Attachement RC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement RC</em>'.
	 * @see M1.ReceveResponse#getAttachementRC()
	 * @see #getReceveResponse()
	 * @generated
	 */
	EReference getReceveResponse_AttachementRC();

	/**
	 * Returns the meta object for class '{@link M1.AttachementRPCServeur <em>Attachement RPC Serveur</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement RPC Serveur</em>'.
	 * @see M1.AttachementRPCServeur
	 * @generated
	 */
	EClass getAttachementRPCServeur();

	/**
	 * Returns the meta object for the reference '{@link M1.AttachementRPCServeur#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.AttachementRPCServeur#getEReference0()
	 * @see #getAttachementRPCServeur()
	 * @generated
	 */
	EReference getAttachementRPCServeur_EReference0();

	/**
	 * Returns the meta object for the reference '{@link M1.AttachementRPCServeur#getEReference1 <em>EReference1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference1</em>'.
	 * @see M1.AttachementRPCServeur#getEReference1()
	 * @see #getAttachementRPCServeur()
	 * @generated
	 */
	EReference getAttachementRPCServeur_EReference1();

	/**
	 * Returns the meta object for class '{@link M1.RoleRequie_RS <em>Role Requie RS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Requie RS</em>'.
	 * @see M1.RoleRequie_RS
	 * @generated
	 */
	EClass getRoleRequie_RS();

	/**
	 * Returns the meta object for the reference '{@link M1.RoleRequie_RS#getAttachement_RS <em>Attachement RS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement RS</em>'.
	 * @see M1.RoleRequie_RS#getAttachement_RS()
	 * @see #getRoleRequie_RS()
	 * @generated
	 */
	EReference getRoleRequie_RS_Attachement_RS();

	/**
	 * Returns the meta object for class '{@link M1.PortFournie_RS <em>Port Fournie RS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Fournie RS</em>'.
	 * @see M1.PortFournie_RS
	 * @generated
	 */
	EClass getPortFournie_RS();

	/**
	 * Returns the meta object for the reference '{@link M1.PortFournie_RS#getAttachement_RS <em>Attachement RS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement RS</em>'.
	 * @see M1.PortFournie_RS#getAttachement_RS()
	 * @see #getPortFournie_RS()
	 * @generated
	 */
	EReference getPortFournie_RS_Attachement_RS();

	/**
	 * Returns the meta object for class '{@link M1.AttachementServeurRPC <em>Attachement Serveur RPC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attachement Serveur RPC</em>'.
	 * @see M1.AttachementServeurRPC
	 * @generated
	 */
	EClass getAttachementServeurRPC();

	/**
	 * Returns the meta object for the reference '{@link M1.AttachementServeurRPC#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.AttachementServeurRPC#getEReference0()
	 * @see #getAttachementServeurRPC()
	 * @generated
	 */
	EReference getAttachementServeurRPC_EReference0();

	/**
	 * Returns the meta object for the reference '{@link M1.AttachementServeurRPC#getEReference1 <em>EReference1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference1</em>'.
	 * @see M1.AttachementServeurRPC#getEReference1()
	 * @see #getAttachementServeurRPC()
	 * @generated
	 */
	EReference getAttachementServeurRPC_EReference1();

	/**
	 * Returns the meta object for class '{@link M1.RoleFournie_SR <em>Role Fournie SR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Fournie SR</em>'.
	 * @see M1.RoleFournie_SR
	 * @generated
	 */
	EClass getRoleFournie_SR();

	/**
	 * Returns the meta object for the reference '{@link M1.RoleFournie_SR#getAttachement_SR <em>Attachement SR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement SR</em>'.
	 * @see M1.RoleFournie_SR#getAttachement_SR()
	 * @see #getRoleFournie_SR()
	 * @generated
	 */
	EReference getRoleFournie_SR_Attachement_SR();

	/**
	 * Returns the meta object for class '{@link M1.PortRequie_SR <em>Port Requie SR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Requie SR</em>'.
	 * @see M1.PortRequie_SR
	 * @generated
	 */
	EClass getPortRequie_SR();

	/**
	 * Returns the meta object for the reference '{@link M1.PortRequie_SR#getAttachement_SR <em>Attachement SR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attachement SR</em>'.
	 * @see M1.PortRequie_SR#getAttachement_SR()
	 * @see #getPortRequie_SR()
	 * @generated
	 */
	EReference getPortRequie_SR_Attachement_SR();

	/**
	 * Returns the meta object for class '{@link M1.Binding_S_RPC <em>Binding SRPC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binding SRPC</em>'.
	 * @see M1.Binding_S_RPC
	 * @generated
	 */
	EClass getBinding_S_RPC();

	/**
	 * Returns the meta object for the reference '{@link M1.Binding_S_RPC#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Binding_S_RPC#getEReference0()
	 * @see #getBinding_S_RPC()
	 * @generated
	 */
	EReference getBinding_S_RPC_EReference0();

	/**
	 * Returns the meta object for the reference '{@link M1.Binding_S_RPC#getEReference1 <em>EReference1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference1</em>'.
	 * @see M1.Binding_S_RPC#getEReference1()
	 * @see #getBinding_S_RPC()
	 * @generated
	 */
	EReference getBinding_S_RPC_EReference1();

	/**
	 * Returns the meta object for class '{@link M1.Port_RPC_S <em>Port RPC S</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port RPC S</em>'.
	 * @see M1.Port_RPC_S
	 * @generated
	 */
	EClass getPort_RPC_S();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_RPC_S#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Port_RPC_S#getEReference0()
	 * @see #getPort_RPC_S()
	 * @generated
	 */
	EReference getPort_RPC_S_EReference0();

	/**
	 * Returns the meta object for class '{@link M1.Port_S_RPC <em>Port SRPC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port SRPC</em>'.
	 * @see M1.Port_S_RPC
	 * @generated
	 */
	EClass getPort_S_RPC();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_S_RPC#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Port_S_RPC#getEReference0()
	 * @see #getPort_S_RPC()
	 * @generated
	 */
	EReference getPort_S_RPC_EReference0();

	/**
	 * Returns the meta object for class '{@link M1.Binding_CM_S <em>Binding CM S</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binding CM S</em>'.
	 * @see M1.Binding_CM_S
	 * @generated
	 */
	EClass getBinding_CM_S();

	/**
	 * Returns the meta object for the reference '{@link M1.Binding_CM_S#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Binding_CM_S#getEReference0()
	 * @see #getBinding_CM_S()
	 * @generated
	 */
	EReference getBinding_CM_S_EReference0();

	/**
	 * Returns the meta object for the reference '{@link M1.Binding_CM_S#getEReference1 <em>EReference1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference1</em>'.
	 * @see M1.Binding_CM_S#getEReference1()
	 * @see #getBinding_CM_S()
	 * @generated
	 */
	EReference getBinding_CM_S_EReference1();

	/**
	 * Returns the meta object for class '{@link M1.Port_S_CM <em>Port SCM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port SCM</em>'.
	 * @see M1.Port_S_CM
	 * @generated
	 */
	EClass getPort_S_CM();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_S_CM#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Port_S_CM#getEReference0()
	 * @see #getPort_S_CM()
	 * @generated
	 */
	EReference getPort_S_CM_EReference0();

	/**
	 * Returns the meta object for class '{@link M1.Port_CM_S <em>Port CM S</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port CM S</em>'.
	 * @see M1.Port_CM_S
	 * @generated
	 */
	EClass getPort_CM_S();

	/**
	 * Returns the meta object for the reference '{@link M1.Port_CM_S#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EReference0</em>'.
	 * @see M1.Port_CM_S#getEReference0()
	 * @see #getPort_CM_S()
	 * @generated
	 */
	EReference getPort_CM_S_EReference0();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	M1Factory getM1Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link M1.impl.Server_ConfigurationImpl <em>Server Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Server_ConfigurationImpl
		 * @see M1.impl.M1PackageImpl#getServer_Configuration()
		 * @generated
		 */
		EClass SERVER_CONFIGURATION = eINSTANCE.getServer_Configuration();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVER_CONFIGURATION__EREFERENCE0 = eINSTANCE.getServer_Configuration_EReference0();

		/**
		 * The meta object literal for the '<em><b>Data Base</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVER_CONFIGURATION__DATA_BASE = eINSTANCE.getServer_Configuration_DataBase();

		/**
		 * The meta object literal for the '<em><b>Gestionnaire Connexion</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVER_CONFIGURATION__GESTIONNAIRE_CONNEXION = eINSTANCE.getServer_Configuration_Gestionnaire_Connexion();

		/**
		 * The meta object literal for the '<em><b>Connecteur GC DB</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVER_CONFIGURATION__CONNECTEUR_GC_DB = eINSTANCE.getServer_Configuration_Connecteur_GC_DB();

		/**
		 * The meta object literal for the '<em><b>Gestion securite</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVER_CONFIGURATION__GESTION_SECURITE = eINSTANCE.getServer_Configuration_Gestion_securite();

		/**
		 * The meta object literal for the '<em><b>Connecteur GC GS</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVER_CONFIGURATION__CONNECTEUR_GC_GS = eINSTANCE.getServer_Configuration_Connecteur_GC_GS();

		/**
		 * The meta object literal for the '<em><b>Connecteur GS DB</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVER_CONFIGURATION__CONNECTEUR_GS_DB = eINSTANCE.getServer_Configuration_Connecteur_GS_DB();

		/**
		 * The meta object literal for the '{@link M1.impl.DataBaseImpl <em>Data Base</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.DataBaseImpl
		 * @see M1.impl.M1PackageImpl#getDataBase()
		 * @generated
		 */
		EClass DATA_BASE = eINSTANCE.getDataBase();

		/**
		 * The meta object literal for the '<em><b>Idata Base</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_BASE__IDATA_BASE = eINSTANCE.getDataBase_IdataBase();

		/**
		 * The meta object literal for the '{@link M1.impl.IDataBaseImpl <em>IData Base</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.IDataBaseImpl
		 * @see M1.impl.M1PackageImpl#getIDataBase()
		 * @generated
		 */
		EClass IDATA_BASE = eINSTANCE.getIDataBase();

		/**
		 * The meta object literal for the '<em><b>Port requie DB 1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDATA_BASE__PORT_REQUIE_DB_1 = eINSTANCE.getIDataBase_Port_requie_DB_1();

		/**
		 * The meta object literal for the '<em><b>Port fournie DB 2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDATA_BASE__PORT_FOURNIE_DB_2 = eINSTANCE.getIDataBase_Port_fournie_DB_2();

		/**
		 * The meta object literal for the '<em><b>Port requie DB 3</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDATA_BASE__PORT_REQUIE_DB_3 = eINSTANCE.getIDataBase_Port_requie_DB_3();

		/**
		 * The meta object literal for the '<em><b>Port fournie DB 4</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDATA_BASE__PORT_FOURNIE_DB_4 = eINSTANCE.getIDataBase_Port_fournie_DB_4();

		/**
		 * The meta object literal for the '{@link M1.impl.Atachement_DB_1Impl <em>Atachement DB 1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Atachement_DB_1Impl
		 * @see M1.impl.M1PackageImpl#getAtachement_DB_1()
		 * @generated
		 */
		EClass ATACHEMENT_DB_1 = eINSTANCE.getAtachement_DB_1();

		/**
		 * The meta object literal for the '<em><b>Role fournie DB 1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1 = eINSTANCE.getAtachement_DB_1_Role_fournie_DB_1();

		/**
		 * The meta object literal for the '<em><b>Port requie DB 1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATACHEMENT_DB_1__PORT_REQUIE_DB_1 = eINSTANCE.getAtachement_DB_1_Port_requie_DB_1();

		/**
		 * The meta object literal for the '{@link M1.impl.Attachement_DB_2Impl <em>Attachement DB 2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Attachement_DB_2Impl
		 * @see M1.impl.M1PackageImpl#getAttachement_DB_2()
		 * @generated
		 */
		EClass ATTACHEMENT_DB_2 = eINSTANCE.getAttachement_DB_2();

		/**
		 * The meta object literal for the '<em><b>Role requie 2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_DB_2__ROLE_REQUIE_2 = eINSTANCE.getAttachement_DB_2_Role_requie_2();

		/**
		 * The meta object literal for the '<em><b>Port fournie DB 2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2 = eINSTANCE.getAttachement_DB_2_Port_fournie_DB_2();

		/**
		 * The meta object literal for the '{@link M1.impl.Attachement_DB_3Impl <em>Attachement DB 3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Attachement_DB_3Impl
		 * @see M1.impl.M1PackageImpl#getAttachement_DB_3()
		 * @generated
		 */
		EClass ATTACHEMENT_DB_3 = eINSTANCE.getAttachement_DB_3();

		/**
		 * The meta object literal for the '<em><b>Role fourni DB 3</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3 = eINSTANCE.getAttachement_DB_3_Role_fourni_DB_3();

		/**
		 * The meta object literal for the '<em><b>Port requie DB 3</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_DB_3__PORT_REQUIE_DB_3 = eINSTANCE.getAttachement_DB_3_Port_requie_DB_3();

		/**
		 * The meta object literal for the '{@link M1.impl.Attachement_DB_4Impl <em>Attachement DB 4</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Attachement_DB_4Impl
		 * @see M1.impl.M1PackageImpl#getAttachement_DB_4()
		 * @generated
		 */
		EClass ATTACHEMENT_DB_4 = eINSTANCE.getAttachement_DB_4();

		/**
		 * The meta object literal for the '<em><b>Role Requie 4</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_DB_4__ROLE_REQUIE_4 = eINSTANCE.getAttachement_DB_4_Role_Requie_4();

		/**
		 * The meta object literal for the '<em><b>Port fourne 4</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_DB_4__PORT_FOURNE_4 = eINSTANCE.getAttachement_DB_4_Port_fourne_4();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_Requie_DB_4Impl <em>Role Requie DB 4</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_Requie_DB_4Impl
		 * @see M1.impl.M1PackageImpl#getRole_Requie_DB_4()
		 * @generated
		 */
		EClass ROLE_REQUIE_DB_4 = eINSTANCE.getRole_Requie_DB_4();

		/**
		 * The meta object literal for the '<em><b>Attachement DB 4</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4 = eINSTANCE.getRole_Requie_DB_4_Attachement_DB_4();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_fornie_DB_4Impl <em>Port fornie DB 4</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_fornie_DB_4Impl
		 * @see M1.impl.M1PackageImpl#getPort_fornie_DB_4()
		 * @generated
		 */
		EClass PORT_FORNIE_DB_4 = eINSTANCE.getPort_fornie_DB_4();

		/**
		 * The meta object literal for the '<em><b>Attachement DB 4</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_FORNIE_DB_4__ATTACHEMENT_DB_4 = eINSTANCE.getPort_fornie_DB_4_Attachement_DB_4();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_fournie_DB_3Impl <em>Role fournie DB 3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_fournie_DB_3Impl
		 * @see M1.impl.M1PackageImpl#getRole_fournie_DB_3()
		 * @generated
		 */
		EClass ROLE_FOURNIE_DB_3 = eINSTANCE.getRole_fournie_DB_3();

		/**
		 * The meta object literal for the '<em><b>Attachement DB 3</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3 = eINSTANCE.getRole_fournie_DB_3_Attachement_DB_3();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_requie_DB_3Impl <em>Port requie DB 3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_requie_DB_3Impl
		 * @see M1.impl.M1PackageImpl#getPort_requie_DB_3()
		 * @generated
		 */
		EClass PORT_REQUIE_DB_3 = eINSTANCE.getPort_requie_DB_3();

		/**
		 * The meta object literal for the '<em><b>Attachement DB 3</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_REQUIE_DB_3__ATTACHEMENT_DB_3 = eINSTANCE.getPort_requie_DB_3_Attachement_DB_3();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_Requie_DB_2Impl <em>Role Requie DB 2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_Requie_DB_2Impl
		 * @see M1.impl.M1PackageImpl#getRole_Requie_DB_2()
		 * @generated
		 */
		EClass ROLE_REQUIE_DB_2 = eINSTANCE.getRole_Requie_DB_2();

		/**
		 * The meta object literal for the '<em><b>Attachement DB 2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2 = eINSTANCE.getRole_Requie_DB_2_Attachement_DB_2();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_fournie_DB_1Impl <em>Role fournie DB 1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_fournie_DB_1Impl
		 * @see M1.impl.M1PackageImpl#getRole_fournie_DB_1()
		 * @generated
		 */
		EClass ROLE_FOURNIE_DB_1 = eINSTANCE.getRole_fournie_DB_1();

		/**
		 * The meta object literal for the '<em><b>Attachement DB 1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_FOURNIE_DB_1__ATTACHEMENT_DB_1 = eINSTANCE.getRole_fournie_DB_1_Attachement_DB_1();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_Requie_DB_1Impl <em>Port Requie DB 1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_Requie_DB_1Impl
		 * @see M1.impl.M1PackageImpl#getPort_Requie_DB_1()
		 * @generated
		 */
		EClass PORT_REQUIE_DB_1 = eINSTANCE.getPort_Requie_DB_1();

		/**
		 * The meta object literal for the '<em><b>Attachement DB 1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_REQUIE_DB_1__ATTACHEMENT_DB_1 = eINSTANCE.getPort_Requie_DB_1_Attachement_DB_1();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_fournie_DB_2Impl <em>Port fournie DB 2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_fournie_DB_2Impl
		 * @see M1.impl.M1PackageImpl#getPort_fournie_DB_2()
		 * @generated
		 */
		EClass PORT_FOURNIE_DB_2 = eINSTANCE.getPort_fournie_DB_2();

		/**
		 * The meta object literal for the '<em><b>Attachement DB 2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_FOURNIE_DB_2__ATTACHEMENT_DB_2 = eINSTANCE.getPort_fournie_DB_2_Attachement_DB_2();

		/**
		 * The meta object literal for the '{@link M1.impl.Gestionnaire_ConnexionImpl <em>Gestionnaire Connexion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Gestionnaire_ConnexionImpl
		 * @see M1.impl.M1PackageImpl#getGestionnaire_Connexion()
		 * @generated
		 */
		EClass GESTIONNAIRE_CONNEXION = eINSTANCE.getGestionnaire_Connexion();

		/**
		 * The meta object literal for the '<em><b>Igestionnaire Connexion</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GESTIONNAIRE_CONNEXION__IGESTIONNAIRE_CONNEXION = eINSTANCE.getGestionnaire_Connexion_IgestionnaireConnexion();

		/**
		 * The meta object literal for the '{@link M1.impl.IGestionnaire_ConnexionImpl <em>IGestionnaire Connexion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.IGestionnaire_ConnexionImpl
		 * @see M1.impl.M1PackageImpl#getIGestionnaire_Connexion()
		 * @generated
		 */
		EClass IGESTIONNAIRE_CONNEXION = eINSTANCE.getIGestionnaire_Connexion();

		/**
		 * The meta object literal for the '<em><b>Port Fournie Gc 1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1 = eINSTANCE.getIGestionnaire_Connexion_Port_Fournie_Gc_1();

		/**
		 * The meta object literal for the '<em><b>Port Requie Gc 2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2 = eINSTANCE.getIGestionnaire_Connexion_Port_Requie_Gc_2();

		/**
		 * The meta object literal for the '<em><b>Port Fournie Gc 3</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3 = eINSTANCE.getIGestionnaire_Connexion_Port_Fournie_Gc_3();

		/**
		 * The meta object literal for the '<em><b>Port Requie Gc 4</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4 = eINSTANCE.getIGestionnaire_Connexion_Port_Requie_Gc_4();

		/**
		 * The meta object literal for the '<em><b>Port CM S</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGESTIONNAIRE_CONNEXION__PORT_CM_S = eINSTANCE.getIGestionnaire_Connexion_Port_CM_S();

		/**
		 * The meta object literal for the '{@link M1.impl.Attachement_GC_2Impl <em>Attachement GC 2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Attachement_GC_2Impl
		 * @see M1.impl.M1PackageImpl#getAttachement_GC_2()
		 * @generated
		 */
		EClass ATTACHEMENT_GC_2 = eINSTANCE.getAttachement_GC_2();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GC_2__EREFERENCE0 = eINSTANCE.getAttachement_GC_2_EReference0();

		/**
		 * The meta object literal for the '<em><b>EReference1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GC_2__EREFERENCE1 = eINSTANCE.getAttachement_GC_2_EReference1();

		/**
		 * The meta object literal for the '{@link M1.impl.Attachement_GC_3Impl <em>Attachement GC 3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Attachement_GC_3Impl
		 * @see M1.impl.M1PackageImpl#getAttachement_GC_3()
		 * @generated
		 */
		EClass ATTACHEMENT_GC_3 = eINSTANCE.getAttachement_GC_3();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GC_3__EREFERENCE0 = eINSTANCE.getAttachement_GC_3_EReference0();

		/**
		 * The meta object literal for the '<em><b>EReference1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GC_3__EREFERENCE1 = eINSTANCE.getAttachement_GC_3_EReference1();

		/**
		 * The meta object literal for the '{@link M1.impl.Attachement_GC_4Impl <em>Attachement GC 4</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Attachement_GC_4Impl
		 * @see M1.impl.M1PackageImpl#getAttachement_GC_4()
		 * @generated
		 */
		EClass ATTACHEMENT_GC_4 = eINSTANCE.getAttachement_GC_4();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GC_4__EREFERENCE0 = eINSTANCE.getAttachement_GC_4_EReference0();

		/**
		 * The meta object literal for the '<em><b>EReference1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GC_4__EREFERENCE1 = eINSTANCE.getAttachement_GC_4_EReference1();

		/**
		 * The meta object literal for the '{@link M1.impl.Attachement_GC_1Impl <em>Attachement GC 1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Attachement_GC_1Impl
		 * @see M1.impl.M1PackageImpl#getAttachement_GC_1()
		 * @generated
		 */
		EClass ATTACHEMENT_GC_1 = eINSTANCE.getAttachement_GC_1();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GC_1__EREFERENCE0 = eINSTANCE.getAttachement_GC_1_EReference0();

		/**
		 * The meta object literal for the '<em><b>EReference1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GC_1__EREFERENCE1 = eINSTANCE.getAttachement_GC_1_EReference1();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_Requie_GC_3Impl <em>Role Requie GC 3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_Requie_GC_3Impl
		 * @see M1.impl.M1PackageImpl#getRole_Requie_GC_3()
		 * @generated
		 */
		EClass ROLE_REQUIE_GC_3 = eINSTANCE.getRole_Requie_GC_3();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_REQUIE_GC_3__EREFERENCE0 = eINSTANCE.getRole_Requie_GC_3_EReference0();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_Requie_GC_4Impl <em>Port Requie GC 4</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_Requie_GC_4Impl
		 * @see M1.impl.M1PackageImpl#getPort_Requie_GC_4()
		 * @generated
		 */
		EClass PORT_REQUIE_GC_4 = eINSTANCE.getPort_Requie_GC_4();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_REQUIE_GC_4__EREFERENCE0 = eINSTANCE.getPort_Requie_GC_4_EReference0();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_Fournie_GC_3Impl <em>Port Fournie GC 3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_Fournie_GC_3Impl
		 * @see M1.impl.M1PackageImpl#getPort_Fournie_GC_3()
		 * @generated
		 */
		EClass PORT_FOURNIE_GC_3 = eINSTANCE.getPort_Fournie_GC_3();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_FOURNIE_GC_3__EREFERENCE0 = eINSTANCE.getPort_Fournie_GC_3_EReference0();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_Fournie_GC_4Impl <em>Role Fournie GC 4</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_Fournie_GC_4Impl
		 * @see M1.impl.M1PackageImpl#getRole_Fournie_GC_4()
		 * @generated
		 */
		EClass ROLE_FOURNIE_GC_4 = eINSTANCE.getRole_Fournie_GC_4();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_FOURNIE_GC_4__EREFERENCE0 = eINSTANCE.getRole_Fournie_GC_4_EReference0();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_Fournie_GC_2Impl <em>Role Fournie GC 2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_Fournie_GC_2Impl
		 * @see M1.impl.M1PackageImpl#getRole_Fournie_GC_2()
		 * @generated
		 */
		EClass ROLE_FOURNIE_GC_2 = eINSTANCE.getRole_Fournie_GC_2();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_FOURNIE_GC_2__EREFERENCE0 = eINSTANCE.getRole_Fournie_GC_2_EReference0();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_Requie_GC_2Impl <em>Port Requie GC 2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_Requie_GC_2Impl
		 * @see M1.impl.M1PackageImpl#getPort_Requie_GC_2()
		 * @generated
		 */
		EClass PORT_REQUIE_GC_2 = eINSTANCE.getPort_Requie_GC_2();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_REQUIE_GC_2__EREFERENCE0 = eINSTANCE.getPort_Requie_GC_2_EReference0();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_Requie_GC_1Impl <em>Role Requie GC 1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_Requie_GC_1Impl
		 * @see M1.impl.M1PackageImpl#getRole_Requie_GC_1()
		 * @generated
		 */
		EClass ROLE_REQUIE_GC_1 = eINSTANCE.getRole_Requie_GC_1();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_REQUIE_GC_1__EREFERENCE0 = eINSTANCE.getRole_Requie_GC_1_EReference0();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_Fournie_GC_1Impl <em>Port Fournie GC 1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_Fournie_GC_1Impl
		 * @see M1.impl.M1PackageImpl#getPort_Fournie_GC_1()
		 * @generated
		 */
		EClass PORT_FOURNIE_GC_1 = eINSTANCE.getPort_Fournie_GC_1();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_FOURNIE_GC_1__EREFERENCE0 = eINSTANCE.getPort_Fournie_GC_1_EReference0();

		/**
		 * The meta object literal for the '{@link M1.impl.IConnecteur_GC_DBImpl <em>IConnecteur GC DB</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.IConnecteur_GC_DBImpl
		 * @see M1.impl.M1PackageImpl#getIConnecteur_GC_DB()
		 * @generated
		 */
		EClass ICONNECTEUR_GC_DB = eINSTANCE.getIConnecteur_GC_DB();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GC_DB__EREFERENCE0 = eINSTANCE.getIConnecteur_GC_DB_EReference0();

		/**
		 * The meta object literal for the '<em><b>EReference1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GC_DB__EREFERENCE1 = eINSTANCE.getIConnecteur_GC_DB_EReference1();

		/**
		 * The meta object literal for the '<em><b>EReference2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GC_DB__EREFERENCE2 = eINSTANCE.getIConnecteur_GC_DB_EReference2();

		/**
		 * The meta object literal for the '<em><b>EReference3</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GC_DB__EREFERENCE3 = eINSTANCE.getIConnecteur_GC_DB_EReference3();

		/**
		 * The meta object literal for the '{@link M1.impl.Connecteur_GC_DBImpl <em>Connecteur GC DB</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Connecteur_GC_DBImpl
		 * @see M1.impl.M1PackageImpl#getConnecteur_GC_DB()
		 * @generated
		 */
		EClass CONNECTEUR_GC_DB = eINSTANCE.getConnecteur_GC_DB();

		/**
		 * The meta object literal for the '<em><b>Iconnecteur BCDB</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTEUR_GC_DB__ICONNECTEUR_BCDB = eINSTANCE.getConnecteur_GC_DB_IconnecteurBCDB();

		/**
		 * The meta object literal for the '{@link M1.impl.Gestion_SecuriterityImpl <em>Gestion Securiterity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Gestion_SecuriterityImpl
		 * @see M1.impl.M1PackageImpl#getGestion_Securiterity()
		 * @generated
		 */
		EClass GESTION_SECURITERITY = eINSTANCE.getGestion_Securiterity();

		/**
		 * The meta object literal for the '<em><b>Igestion Securite</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GESTION_SECURITERITY__IGESTION_SECURITE = eINSTANCE.getGestion_Securiterity_IgestionSecurite();

		/**
		 * The meta object literal for the '{@link M1.impl.Attachement_GS_1Impl <em>Attachement GS 1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Attachement_GS_1Impl
		 * @see M1.impl.M1PackageImpl#getAttachement_GS_1()
		 * @generated
		 */
		EClass ATTACHEMENT_GS_1 = eINSTANCE.getAttachement_GS_1();

		/**
		 * The meta object literal for the '<em><b>Port fournie GS 1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1 = eINSTANCE.getAttachement_GS_1_Port_fournie_GS_1();

		/**
		 * The meta object literal for the '<em><b>Role requie GS 1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1 = eINSTANCE.getAttachement_GS_1_Role_requie_GS_1();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_Requie_GS_2Impl <em>Port Requie GS 2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_Requie_GS_2Impl
		 * @see M1.impl.M1PackageImpl#getPort_Requie_GS_2()
		 * @generated
		 */
		EClass PORT_REQUIE_GS_2 = eINSTANCE.getPort_Requie_GS_2();

		/**
		 * The meta object literal for the '<em><b>Attachement GS 2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_REQUIE_GS_2__ATTACHEMENT_GS_2 = eINSTANCE.getPort_Requie_GS_2_Attachement_GS_2();

		/**
		 * The meta object literal for the '{@link M1.impl.Attachement_GS_2Impl <em>Attachement GS 2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Attachement_GS_2Impl
		 * @see M1.impl.M1PackageImpl#getAttachement_GS_2()
		 * @generated
		 */
		EClass ATTACHEMENT_GS_2 = eINSTANCE.getAttachement_GS_2();

		/**
		 * The meta object literal for the '<em><b>Port requie GS 2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GS_2__PORT_REQUIE_GS_2 = eINSTANCE.getAttachement_GS_2_Port_requie_GS_2();

		/**
		 * The meta object literal for the '<em><b>Role fournie GS 2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2 = eINSTANCE.getAttachement_GS_2_Role_fournie_GS_2();

		/**
		 * The meta object literal for the '{@link M1.impl.Attachement_GS_3Impl <em>Attachement GS 3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Attachement_GS_3Impl
		 * @see M1.impl.M1PackageImpl#getAttachement_GS_3()
		 * @generated
		 */
		EClass ATTACHEMENT_GS_3 = eINSTANCE.getAttachement_GS_3();

		/**
		 * The meta object literal for the '<em><b>Port fournie GS 3</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GS_3__PORT_FOURNIE_GS_3 = eINSTANCE.getAttachement_GS_3_Port_fournie_GS_3();

		/**
		 * The meta object literal for the '<em><b>Role requie GS 3</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GS_3__ROLE_REQUIE_GS_3 = eINSTANCE.getAttachement_GS_3_Role_requie_GS_3();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_Fournie_GS_1Impl <em>Port Fournie GS 1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_Fournie_GS_1Impl
		 * @see M1.impl.M1PackageImpl#getPort_Fournie_GS_1()
		 * @generated
		 */
		EClass PORT_FOURNIE_GS_1 = eINSTANCE.getPort_Fournie_GS_1();

		/**
		 * The meta object literal for the '<em><b>Attachement GS 1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1 = eINSTANCE.getPort_Fournie_GS_1_Attachement_GS_1();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_Requie_GS_1Impl <em>Role Requie GS 1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_Requie_GS_1Impl
		 * @see M1.impl.M1PackageImpl#getRole_Requie_GS_1()
		 * @generated
		 */
		EClass ROLE_REQUIE_GS_1 = eINSTANCE.getRole_Requie_GS_1();

		/**
		 * The meta object literal for the '<em><b>Attachement GS 1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_REQUIE_GS_1__ATTACHEMENT_GS_1 = eINSTANCE.getRole_Requie_GS_1_Attachement_GS_1();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_Fournie_GS_2Impl <em>Role Fournie GS 2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_Fournie_GS_2Impl
		 * @see M1.impl.M1PackageImpl#getRole_Fournie_GS_2()
		 * @generated
		 */
		EClass ROLE_FOURNIE_GS_2 = eINSTANCE.getRole_Fournie_GS_2();

		/**
		 * The meta object literal for the '<em><b>Attachement GS 2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_FOURNIE_GS_2__ATTACHEMENT_GS_2 = eINSTANCE.getRole_Fournie_GS_2_Attachement_GS_2();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_Fournie_GS_3Impl <em>Port Fournie GS 3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_Fournie_GS_3Impl
		 * @see M1.impl.M1PackageImpl#getPort_Fournie_GS_3()
		 * @generated
		 */
		EClass PORT_FOURNIE_GS_3 = eINSTANCE.getPort_Fournie_GS_3();

		/**
		 * The meta object literal for the '<em><b>Attachement GS 3</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_FOURNIE_GS_3__ATTACHEMENT_GS_3 = eINSTANCE.getPort_Fournie_GS_3_Attachement_GS_3();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_Requie_GS_3Impl <em>Role Requie GS 3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_Requie_GS_3Impl
		 * @see M1.impl.M1PackageImpl#getRole_Requie_GS_3()
		 * @generated
		 */
		EClass ROLE_REQUIE_GS_3 = eINSTANCE.getRole_Requie_GS_3();

		/**
		 * The meta object literal for the '<em><b>Attachement GS 3</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_REQUIE_GS_3__ATTACHEMENT_GS_3 = eINSTANCE.getRole_Requie_GS_3_Attachement_GS_3();

		/**
		 * The meta object literal for the '{@link M1.impl.Attachement_GS_4Impl <em>Attachement GS 4</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Attachement_GS_4Impl
		 * @see M1.impl.M1PackageImpl#getAttachement_GS_4()
		 * @generated
		 */
		EClass ATTACHEMENT_GS_4 = eINSTANCE.getAttachement_GS_4();

		/**
		 * The meta object literal for the '<em><b>Port requie GS 4</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GS_4__PORT_REQUIE_GS_4 = eINSTANCE.getAttachement_GS_4_Port_requie_GS_4();

		/**
		 * The meta object literal for the '<em><b>Role fournie GS 4</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4 = eINSTANCE.getAttachement_GS_4_Role_fournie_GS_4();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_Requie_GS_4Impl <em>Port Requie GS 4</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_Requie_GS_4Impl
		 * @see M1.impl.M1PackageImpl#getPort_Requie_GS_4()
		 * @generated
		 */
		EClass PORT_REQUIE_GS_4 = eINSTANCE.getPort_Requie_GS_4();

		/**
		 * The meta object literal for the '<em><b>Attachement GS 4</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_REQUIE_GS_4__ATTACHEMENT_GS_4 = eINSTANCE.getPort_Requie_GS_4_Attachement_GS_4();

		/**
		 * The meta object literal for the '{@link M1.impl.Role_Fournie_GS_4Impl <em>Role Fournie GS 4</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Role_Fournie_GS_4Impl
		 * @see M1.impl.M1PackageImpl#getRole_Fournie_GS_4()
		 * @generated
		 */
		EClass ROLE_FOURNIE_GS_4 = eINSTANCE.getRole_Fournie_GS_4();

		/**
		 * The meta object literal for the '<em><b>Attachement GS 4</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_FOURNIE_GS_4__ATTACHEMENT_GS_4 = eINSTANCE.getRole_Fournie_GS_4_Attachement_GS_4();

		/**
		 * The meta object literal for the '{@link M1.impl.IGestion_SecuriteImpl <em>IGestion Securite</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.IGestion_SecuriteImpl
		 * @see M1.impl.M1PackageImpl#getIGestion_Securite()
		 * @generated
		 */
		EClass IGESTION_SECURITE = eINSTANCE.getIGestion_Securite();

		/**
		 * The meta object literal for the '<em><b>Port fournie GS 1</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGESTION_SECURITE__PORT_FOURNIE_GS_1 = eINSTANCE.getIGestion_Securite_Port_fournie_GS_1();

		/**
		 * The meta object literal for the '<em><b>Port requie GS 2</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGESTION_SECURITE__PORT_REQUIE_GS_2 = eINSTANCE.getIGestion_Securite_Port_requie_GS_2();

		/**
		 * The meta object literal for the '<em><b>Port fournie GS 3</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGESTION_SECURITE__PORT_FOURNIE_GS_3 = eINSTANCE.getIGestion_Securite_Port_fournie_GS_3();

		/**
		 * The meta object literal for the '<em><b>Port requie GS 4</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IGESTION_SECURITE__PORT_REQUIE_GS_4 = eINSTANCE.getIGestion_Securite_Port_requie_GS_4();

		/**
		 * The meta object literal for the '{@link M1.impl.Connecteur_GC_GSImpl <em>Connecteur GC GS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Connecteur_GC_GSImpl
		 * @see M1.impl.M1PackageImpl#getConnecteur_GC_GS()
		 * @generated
		 */
		EClass CONNECTEUR_GC_GS = eINSTANCE.getConnecteur_GC_GS();

		/**
		 * The meta object literal for the '<em><b>Iconnecteur GC GS</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTEUR_GC_GS__ICONNECTEUR_GC_GS = eINSTANCE.getConnecteur_GC_GS_Iconnecteur_GC_GS();

		/**
		 * The meta object literal for the '{@link M1.impl.IConnecteur_GC_GSImpl <em>IConnecteur GC GS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.IConnecteur_GC_GSImpl
		 * @see M1.impl.M1PackageImpl#getIConnecteur_GC_GS()
		 * @generated
		 */
		EClass ICONNECTEUR_GC_GS = eINSTANCE.getIConnecteur_GC_GS();

		/**
		 * The meta object literal for the '<em><b>Role requie GC 3</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GC_GS__ROLE_REQUIE_GC_3 = eINSTANCE.getIConnecteur_GC_GS_Role_requie_GC_3();

		/**
		 * The meta object literal for the '<em><b>Role fournie GC 4</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GC_GS__ROLE_FOURNIE_GC_4 = eINSTANCE.getIConnecteur_GC_GS_Role_fournie_GC_4();

		/**
		 * The meta object literal for the '<em><b>Role requie GS 1</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GC_GS__ROLE_REQUIE_GS_1 = eINSTANCE.getIConnecteur_GC_GS_Role_requie_GS_1();

		/**
		 * The meta object literal for the '<em><b>Role fournie GS 2</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GC_GS__ROLE_FOURNIE_GS_2 = eINSTANCE.getIConnecteur_GC_GS_Role_fournie_GS_2();

		/**
		 * The meta object literal for the '{@link M1.impl.IConnecteur_GS_DBImpl <em>IConnecteur GS DB</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.IConnecteur_GS_DBImpl
		 * @see M1.impl.M1PackageImpl#getIConnecteur_GS_DB()
		 * @generated
		 */
		EClass ICONNECTEUR_GS_DB = eINSTANCE.getIConnecteur_GS_DB();

		/**
		 * The meta object literal for the '<em><b>Role fournie DB 3</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GS_DB__ROLE_FOURNIE_DB_3 = eINSTANCE.getIConnecteur_GS_DB_Role_fournie_DB_3();

		/**
		 * The meta object literal for the '<em><b>Role requie DB 4</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GS_DB__ROLE_REQUIE_DB_4 = eINSTANCE.getIConnecteur_GS_DB_Role_requie_DB_4();

		/**
		 * The meta object literal for the '<em><b>Role fournie GS 4</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GS_DB__ROLE_FOURNIE_GS_4 = eINSTANCE.getIConnecteur_GS_DB_Role_fournie_GS_4();

		/**
		 * The meta object literal for the '<em><b>Role requie GS 3</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_GS_DB__ROLE_REQUIE_GS_3 = eINSTANCE.getIConnecteur_GS_DB_Role_requie_GS_3();

		/**
		 * The meta object literal for the '{@link M1.impl.Connecteur_GS_DBImpl <em>Connecteur GS DB</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Connecteur_GS_DBImpl
		 * @see M1.impl.M1PackageImpl#getConnecteur_GS_DB()
		 * @generated
		 */
		EClass CONNECTEUR_GS_DB = eINSTANCE.getConnecteur_GS_DB();

		/**
		 * The meta object literal for the '<em><b>Iconnecteur GS DB</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTEUR_GS_DB__ICONNECTEUR_GS_DB = eINSTANCE.getConnecteur_GS_DB_Iconnecteur_GS_DB();

		/**
		 * The meta object literal for the '{@link M1.impl.ClientServeurImpl <em>Client Serveur</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.ClientServeurImpl
		 * @see M1.impl.M1PackageImpl#getClientServeur()
		 * @generated
		 */
		EClass CLIENT_SERVEUR = eINSTANCE.getClientServeur();

		/**
		 * The meta object literal for the '<em><b>Client</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLIENT_SERVEUR__CLIENT = eINSTANCE.getClientServeur_Client();

		/**
		 * The meta object literal for the '<em><b>Connecteur RPC</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLIENT_SERVEUR__CONNECTEUR_RPC = eINSTANCE.getClientServeur_ConnecteurRPC();

		/**
		 * The meta object literal for the '<em><b>Serveur</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLIENT_SERVEUR__SERVEUR = eINSTANCE.getClientServeur_Serveur();

		/**
		 * The meta object literal for the '{@link M1.impl.ClientImpl <em>Client</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.ClientImpl
		 * @see M1.impl.M1PackageImpl#getClient()
		 * @generated
		 */
		EClass CLIENT = eINSTANCE.getClient();

		/**
		 * The meta object literal for the '<em><b>Interface Client</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLIENT__INTERFACE_CLIENT = eINSTANCE.getClient_InterfaceClient();

		/**
		 * The meta object literal for the '{@link M1.impl.ConnecteurRPCImpl <em>Connecteur RPC</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.ConnecteurRPCImpl
		 * @see M1.impl.M1PackageImpl#getConnecteurRPC()
		 * @generated
		 */
		EClass CONNECTEUR_RPC = eINSTANCE.getConnecteurRPC();

		/**
		 * The meta object literal for the '<em><b>Interface Connecteur</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTEUR_RPC__INTERFACE_CONNECTEUR = eINSTANCE.getConnecteurRPC_InterfaceConnecteur();

		/**
		 * The meta object literal for the '{@link M1.impl.ServeurImpl <em>Serveur</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.ServeurImpl
		 * @see M1.impl.M1PackageImpl#getServeur()
		 * @generated
		 */
		EClass SERVEUR = eINSTANCE.getServeur();

		/**
		 * The meta object literal for the '<em><b>Interface Serveur</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVEUR__INTERFACE_SERVEUR = eINSTANCE.getServeur_InterfaceServeur();

		/**
		 * The meta object literal for the '<em><b>Serveur Config</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVEUR__SERVEUR_CONFIG = eINSTANCE.getServeur_Serveur_Config();

		/**
		 * The meta object literal for the '{@link M1.impl.IClientImpl <em>IClient</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.IClientImpl
		 * @see M1.impl.M1PackageImpl#getIClient()
		 * @generated
		 */
		EClass ICLIENT = eINSTANCE.getIClient();

		/**
		 * The meta object literal for the '<em><b>Send Request</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICLIENT__SEND_REQUEST = eINSTANCE.getIClient_SendRequest();

		/**
		 * The meta object literal for the '<em><b>Receve Request</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICLIENT__RECEVE_REQUEST = eINSTANCE.getIClient_ReceveRequest();

		/**
		 * The meta object literal for the '{@link M1.impl.IConnecteurRPCImpl <em>IConnecteur RPC</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.IConnecteurRPCImpl
		 * @see M1.impl.M1PackageImpl#getIConnecteurRPC()
		 * @generated
		 */
		EClass ICONNECTEUR_RPC = eINSTANCE.getIConnecteurRPC();

		/**
		 * The meta object literal for the '<em><b>Role Requie CR</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_RPC__ROLE_REQUIE_CR = eINSTANCE.getIConnecteurRPC_RoleRequie_CR();

		/**
		 * The meta object literal for the '<em><b>Role Fournie RC</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_RPC__ROLE_FOURNIE_RC = eINSTANCE.getIConnecteurRPC_RoleFournie_RC();

		/**
		 * The meta object literal for the '<em><b>Role Requie RS</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_RPC__ROLE_REQUIE_RS = eINSTANCE.getIConnecteurRPC_RoleRequie_RS();

		/**
		 * The meta object literal for the '<em><b>Role Fournie SR</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_RPC__ROLE_FOURNIE_SR = eINSTANCE.getIConnecteurRPC_RoleFournie_SR();

		/**
		 * The meta object literal for the '<em><b>Port RPC S</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ICONNECTEUR_RPC__PORT_RPC_S = eINSTANCE.getIConnecteurRPC_Port_RPC_S();

		/**
		 * The meta object literal for the '{@link M1.impl.IServeurImpl <em>IServeur</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.IServeurImpl
		 * @see M1.impl.M1PackageImpl#getIServeur()
		 * @generated
		 */
		EClass ISERVEUR = eINSTANCE.getIServeur();

		/**
		 * The meta object literal for the '<em><b>Port Fournie RS</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISERVEUR__PORT_FOURNIE_RS = eINSTANCE.getIServeur_PortFournie_RS();

		/**
		 * The meta object literal for the '<em><b>Port Requie SR</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISERVEUR__PORT_REQUIE_SR = eINSTANCE.getIServeur_PortRequie_SR();

		/**
		 * The meta object literal for the '<em><b>Pert SCM</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISERVEUR__PERT_SCM = eINSTANCE.getIServeur_Pert_S_CM();

		/**
		 * The meta object literal for the '<em><b>Port SRPC</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISERVEUR__PORT_SRPC = eINSTANCE.getIServeur_Port_S_RPC();

		/**
		 * The meta object literal for the '{@link M1.impl.SendRequestImpl <em>Send Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.SendRequestImpl
		 * @see M1.impl.M1PackageImpl#getSendRequest()
		 * @generated
		 */
		EClass SEND_REQUEST = eINSTANCE.getSendRequest();

		/**
		 * The meta object literal for the '<em><b>Attachement CR</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEND_REQUEST__ATTACHEMENT_CR = eINSTANCE.getSendRequest_Attachement_CR();

		/**
		 * The meta object literal for the '{@link M1.impl.AttachementClientRPCImpl <em>Attachement Client RPC</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.AttachementClientRPCImpl
		 * @see M1.impl.M1PackageImpl#getAttachementClientRPC()
		 * @generated
		 */
		EClass ATTACHEMENT_CLIENT_RPC = eINSTANCE.getAttachementClientRPC();

		/**
		 * The meta object literal for the '<em><b>Send Request</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_CLIENT_RPC__SEND_REQUEST = eINSTANCE.getAttachementClientRPC_SendRequest();

		/**
		 * The meta object literal for the '<em><b>Role Requie CR</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_CLIENT_RPC__ROLE_REQUIE_CR = eINSTANCE.getAttachementClientRPC_RoleRequie_CR();

		/**
		 * The meta object literal for the '{@link M1.impl.RoleRequie_CRImpl <em>Role Requie CR</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.RoleRequie_CRImpl
		 * @see M1.impl.M1PackageImpl#getRoleRequie_CR()
		 * @generated
		 */
		EClass ROLE_REQUIE_CR = eINSTANCE.getRoleRequie_CR();

		/**
		 * The meta object literal for the '<em><b>Attachement CR</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_REQUIE_CR__ATTACHEMENT_CR = eINSTANCE.getRoleRequie_CR_Attachement_CR();

		/**
		 * The meta object literal for the '{@link M1.impl.AttachementRPCClientImpl <em>Attachement RPC Client</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.AttachementRPCClientImpl
		 * @see M1.impl.M1PackageImpl#getAttachementRPCClient()
		 * @generated
		 */
		EClass ATTACHEMENT_RPC_CLIENT = eINSTANCE.getAttachementRPCClient();

		/**
		 * The meta object literal for the '<em><b>Receve Responce</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE = eINSTANCE.getAttachementRPCClient_ReceveResponce();

		/**
		 * The meta object literal for the '<em><b>Role Fourie RC</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC = eINSTANCE.getAttachementRPCClient_RoleFourie_RC();

		/**
		 * The meta object literal for the '{@link M1.impl.RoleFournie_RCImpl <em>Role Fournie RC</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.RoleFournie_RCImpl
		 * @see M1.impl.M1PackageImpl#getRoleFournie_RC()
		 * @generated
		 */
		EClass ROLE_FOURNIE_RC = eINSTANCE.getRoleFournie_RC();

		/**
		 * The meta object literal for the '<em><b>Attachement RC</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_FOURNIE_RC__ATTACHEMENT_RC = eINSTANCE.getRoleFournie_RC_AttachementRC();

		/**
		 * The meta object literal for the '{@link M1.impl.ReceveResponseImpl <em>Receve Response</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.ReceveResponseImpl
		 * @see M1.impl.M1PackageImpl#getReceveResponse()
		 * @generated
		 */
		EClass RECEVE_RESPONSE = eINSTANCE.getReceveResponse();

		/**
		 * The meta object literal for the '<em><b>Attachement RC</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEVE_RESPONSE__ATTACHEMENT_RC = eINSTANCE.getReceveResponse_AttachementRC();

		/**
		 * The meta object literal for the '{@link M1.impl.AttachementRPCServeurImpl <em>Attachement RPC Serveur</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.AttachementRPCServeurImpl
		 * @see M1.impl.M1PackageImpl#getAttachementRPCServeur()
		 * @generated
		 */
		EClass ATTACHEMENT_RPC_SERVEUR = eINSTANCE.getAttachementRPCServeur();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_RPC_SERVEUR__EREFERENCE0 = eINSTANCE.getAttachementRPCServeur_EReference0();

		/**
		 * The meta object literal for the '<em><b>EReference1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_RPC_SERVEUR__EREFERENCE1 = eINSTANCE.getAttachementRPCServeur_EReference1();

		/**
		 * The meta object literal for the '{@link M1.impl.RoleRequie_RSImpl <em>Role Requie RS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.RoleRequie_RSImpl
		 * @see M1.impl.M1PackageImpl#getRoleRequie_RS()
		 * @generated
		 */
		EClass ROLE_REQUIE_RS = eINSTANCE.getRoleRequie_RS();

		/**
		 * The meta object literal for the '<em><b>Attachement RS</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_REQUIE_RS__ATTACHEMENT_RS = eINSTANCE.getRoleRequie_RS_Attachement_RS();

		/**
		 * The meta object literal for the '{@link M1.impl.PortFournie_RSImpl <em>Port Fournie RS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.PortFournie_RSImpl
		 * @see M1.impl.M1PackageImpl#getPortFournie_RS()
		 * @generated
		 */
		EClass PORT_FOURNIE_RS = eINSTANCE.getPortFournie_RS();

		/**
		 * The meta object literal for the '<em><b>Attachement RS</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_FOURNIE_RS__ATTACHEMENT_RS = eINSTANCE.getPortFournie_RS_Attachement_RS();

		/**
		 * The meta object literal for the '{@link M1.impl.AttachementServeurRPCImpl <em>Attachement Serveur RPC</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.AttachementServeurRPCImpl
		 * @see M1.impl.M1PackageImpl#getAttachementServeurRPC()
		 * @generated
		 */
		EClass ATTACHEMENT_SERVEUR_RPC = eINSTANCE.getAttachementServeurRPC();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_SERVEUR_RPC__EREFERENCE0 = eINSTANCE.getAttachementServeurRPC_EReference0();

		/**
		 * The meta object literal for the '<em><b>EReference1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACHEMENT_SERVEUR_RPC__EREFERENCE1 = eINSTANCE.getAttachementServeurRPC_EReference1();

		/**
		 * The meta object literal for the '{@link M1.impl.RoleFournie_SRImpl <em>Role Fournie SR</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.RoleFournie_SRImpl
		 * @see M1.impl.M1PackageImpl#getRoleFournie_SR()
		 * @generated
		 */
		EClass ROLE_FOURNIE_SR = eINSTANCE.getRoleFournie_SR();

		/**
		 * The meta object literal for the '<em><b>Attachement SR</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_FOURNIE_SR__ATTACHEMENT_SR = eINSTANCE.getRoleFournie_SR_Attachement_SR();

		/**
		 * The meta object literal for the '{@link M1.impl.PortRequie_SRImpl <em>Port Requie SR</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.PortRequie_SRImpl
		 * @see M1.impl.M1PackageImpl#getPortRequie_SR()
		 * @generated
		 */
		EClass PORT_REQUIE_SR = eINSTANCE.getPortRequie_SR();

		/**
		 * The meta object literal for the '<em><b>Attachement SR</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_REQUIE_SR__ATTACHEMENT_SR = eINSTANCE.getPortRequie_SR_Attachement_SR();

		/**
		 * The meta object literal for the '{@link M1.impl.Binding_S_RPCImpl <em>Binding SRPC</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Binding_S_RPCImpl
		 * @see M1.impl.M1PackageImpl#getBinding_S_RPC()
		 * @generated
		 */
		EClass BINDING_SRPC = eINSTANCE.getBinding_S_RPC();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING_SRPC__EREFERENCE0 = eINSTANCE.getBinding_S_RPC_EReference0();

		/**
		 * The meta object literal for the '<em><b>EReference1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING_SRPC__EREFERENCE1 = eINSTANCE.getBinding_S_RPC_EReference1();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_RPC_SImpl <em>Port RPC S</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_RPC_SImpl
		 * @see M1.impl.M1PackageImpl#getPort_RPC_S()
		 * @generated
		 */
		EClass PORT_RPC_S = eINSTANCE.getPort_RPC_S();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_RPC_S__EREFERENCE0 = eINSTANCE.getPort_RPC_S_EReference0();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_S_RPCImpl <em>Port SRPC</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_S_RPCImpl
		 * @see M1.impl.M1PackageImpl#getPort_S_RPC()
		 * @generated
		 */
		EClass PORT_SRPC = eINSTANCE.getPort_S_RPC();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_SRPC__EREFERENCE0 = eINSTANCE.getPort_S_RPC_EReference0();

		/**
		 * The meta object literal for the '{@link M1.impl.Binding_CM_SImpl <em>Binding CM S</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Binding_CM_SImpl
		 * @see M1.impl.M1PackageImpl#getBinding_CM_S()
		 * @generated
		 */
		EClass BINDING_CM_S = eINSTANCE.getBinding_CM_S();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING_CM_S__EREFERENCE0 = eINSTANCE.getBinding_CM_S_EReference0();

		/**
		 * The meta object literal for the '<em><b>EReference1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING_CM_S__EREFERENCE1 = eINSTANCE.getBinding_CM_S_EReference1();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_S_CMImpl <em>Port SCM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_S_CMImpl
		 * @see M1.impl.M1PackageImpl#getPort_S_CM()
		 * @generated
		 */
		EClass PORT_SCM = eINSTANCE.getPort_S_CM();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_SCM__EREFERENCE0 = eINSTANCE.getPort_S_CM_EReference0();

		/**
		 * The meta object literal for the '{@link M1.impl.Port_CM_SImpl <em>Port CM S</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see M1.impl.Port_CM_SImpl
		 * @see M1.impl.M1PackageImpl#getPort_CM_S()
		 * @generated
		 */
		EClass PORT_CM_S = eINSTANCE.getPort_CM_S();

		/**
		 * The meta object literal for the '<em><b>EReference0</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_CM_S__EREFERENCE0 = eINSTANCE.getPort_CM_S_EReference0();

	}

} //M1Package
