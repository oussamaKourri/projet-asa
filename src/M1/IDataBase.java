/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IData Base</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.IDataBase#getPort_requie_DB_1 <em>Port requie DB 1</em>}</li>
 *   <li>{@link M1.IDataBase#getPort_fournie_DB_2 <em>Port fournie DB 2</em>}</li>
 *   <li>{@link M1.IDataBase#getPort_requie_DB_3 <em>Port requie DB 3</em>}</li>
 *   <li>{@link M1.IDataBase#getPort_fournie_DB_4 <em>Port fournie DB 4</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getIDataBase()
 * @model
 * @generated
 */
public interface IDataBase extends EObject {
	/**
	 * Returns the value of the '<em><b>Port requie DB 1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port requie DB 1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port requie DB 1</em>' containment reference.
	 * @see #setPort_requie_DB_1(Port_Requie_DB_1)
	 * @see M1.M1Package#getIDataBase_Port_requie_DB_1()
	 * @model containment="true"
	 * @generated
	 */
	Port_Requie_DB_1 getPort_requie_DB_1();

	/**
	 * Sets the value of the '{@link M1.IDataBase#getPort_requie_DB_1 <em>Port requie DB 1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port requie DB 1</em>' containment reference.
	 * @see #getPort_requie_DB_1()
	 * @generated
	 */
	void setPort_requie_DB_1(Port_Requie_DB_1 value);

	/**
	 * Returns the value of the '<em><b>Port fournie DB 2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port fournie DB 2</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port fournie DB 2</em>' containment reference.
	 * @see #setPort_fournie_DB_2(Port_fournie_DB_2)
	 * @see M1.M1Package#getIDataBase_Port_fournie_DB_2()
	 * @model containment="true"
	 * @generated
	 */
	Port_fournie_DB_2 getPort_fournie_DB_2();

	/**
	 * Sets the value of the '{@link M1.IDataBase#getPort_fournie_DB_2 <em>Port fournie DB 2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port fournie DB 2</em>' containment reference.
	 * @see #getPort_fournie_DB_2()
	 * @generated
	 */
	void setPort_fournie_DB_2(Port_fournie_DB_2 value);

	/**
	 * Returns the value of the '<em><b>Port requie DB 3</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port requie DB 3</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port requie DB 3</em>' containment reference.
	 * @see #setPort_requie_DB_3(Port_requie_DB_3)
	 * @see M1.M1Package#getIDataBase_Port_requie_DB_3()
	 * @model containment="true"
	 * @generated
	 */
	Port_requie_DB_3 getPort_requie_DB_3();

	/**
	 * Sets the value of the '{@link M1.IDataBase#getPort_requie_DB_3 <em>Port requie DB 3</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port requie DB 3</em>' containment reference.
	 * @see #getPort_requie_DB_3()
	 * @generated
	 */
	void setPort_requie_DB_3(Port_requie_DB_3 value);

	/**
	 * Returns the value of the '<em><b>Port fournie DB 4</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port fournie DB 4</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port fournie DB 4</em>' containment reference.
	 * @see #setPort_fournie_DB_4(Port_fornie_DB_4)
	 * @see M1.M1Package#getIDataBase_Port_fournie_DB_4()
	 * @model containment="true"
	 * @generated
	 */
	Port_fornie_DB_4 getPort_fournie_DB_4();

	/**
	 * Sets the value of the '{@link M1.IDataBase#getPort_fournie_DB_4 <em>Port fournie DB 4</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port fournie DB 4</em>' containment reference.
	 * @see #getPort_fournie_DB_4()
	 * @generated
	 */
	void setPort_fournie_DB_4(Port_fornie_DB_4 value);

} // IDataBase
