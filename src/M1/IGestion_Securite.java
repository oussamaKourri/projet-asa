/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IGestion Securite</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.IGestion_Securite#getPort_fournie_GS_1 <em>Port fournie GS 1</em>}</li>
 *   <li>{@link M1.IGestion_Securite#getPort_requie_GS_2 <em>Port requie GS 2</em>}</li>
 *   <li>{@link M1.IGestion_Securite#getPort_fournie_GS_3 <em>Port fournie GS 3</em>}</li>
 *   <li>{@link M1.IGestion_Securite#getPort_requie_GS_4 <em>Port requie GS 4</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getIGestion_Securite()
 * @model
 * @generated
 */
public interface IGestion_Securite extends EObject {
	/**
	 * Returns the value of the '<em><b>Port fournie GS 1</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Port_Fournie_GS_1}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port fournie GS 1</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port fournie GS 1</em>' containment reference list.
	 * @see M1.M1Package#getIGestion_Securite_Port_fournie_GS_1()
	 * @model type="M1.Port_Fournie_GS_1" containment="true"
	 * @generated
	 */
	EList getPort_fournie_GS_1();

	/**
	 * Returns the value of the '<em><b>Port requie GS 2</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Port_Requie_GS_2}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port requie GS 2</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port requie GS 2</em>' containment reference list.
	 * @see M1.M1Package#getIGestion_Securite_Port_requie_GS_2()
	 * @model type="M1.Port_Requie_GS_2" containment="true"
	 * @generated
	 */
	EList getPort_requie_GS_2();

	/**
	 * Returns the value of the '<em><b>Port fournie GS 3</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Port_Fournie_GS_3}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port fournie GS 3</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port fournie GS 3</em>' containment reference list.
	 * @see M1.M1Package#getIGestion_Securite_Port_fournie_GS_3()
	 * @model type="M1.Port_Fournie_GS_3" containment="true"
	 * @generated
	 */
	EList getPort_fournie_GS_3();

	/**
	 * Returns the value of the '<em><b>Port requie GS 4</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Port_Requie_GS_4}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port requie GS 4</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port requie GS 4</em>' containment reference list.
	 * @see M1.M1Package#getIGestion_Securite_Port_requie_GS_4()
	 * @model type="M1.Port_Requie_GS_4" containment="true"
	 * @generated
	 */
	EList getPort_requie_GS_4();

} // IGestion_Securite
