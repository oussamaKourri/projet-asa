/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connecteur GC DB</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Connecteur_GC_DB#getIconnecteurBCDB <em>Iconnecteur BCDB</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getConnecteur_GC_DB()
 * @model
 * @generated
 */
public interface Connecteur_GC_DB extends EObject {
	/**
	 * Returns the value of the '<em><b>Iconnecteur BCDB</b></em>' containment reference list.
	 * The list contents are of type {@link M1.IConnecteur_GC_DB}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iconnecteur BCDB</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iconnecteur BCDB</em>' containment reference list.
	 * @see M1.M1Package#getConnecteur_GC_DB_IconnecteurBCDB()
	 * @model type="M1.IConnecteur_GC_DB" containment="true"
	 * @generated
	 */
	EList getIconnecteurBCDB();

} // Connecteur_GC_DB
