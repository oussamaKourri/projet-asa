/**
 */
package M1;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see M1.M1Package
 * @generated
 */
public interface M1Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	M1Factory eINSTANCE = M1.impl.M1FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Server Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Configuration</em>'.
	 * @generated
	 */
	Server_Configuration createServer_Configuration();

	/**
	 * Returns a new object of class '<em>Data Base</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Base</em>'.
	 * @generated
	 */
	DataBase createDataBase();

	/**
	 * Returns a new object of class '<em>IData Base</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IData Base</em>'.
	 * @generated
	 */
	IDataBase createIDataBase();

	/**
	 * Returns a new object of class '<em>Atachement DB 1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Atachement DB 1</em>'.
	 * @generated
	 */
	Atachement_DB_1 createAtachement_DB_1();

	/**
	 * Returns a new object of class '<em>Attachement DB 2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement DB 2</em>'.
	 * @generated
	 */
	Attachement_DB_2 createAttachement_DB_2();

	/**
	 * Returns a new object of class '<em>Attachement DB 3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement DB 3</em>'.
	 * @generated
	 */
	Attachement_DB_3 createAttachement_DB_3();

	/**
	 * Returns a new object of class '<em>Attachement DB 4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement DB 4</em>'.
	 * @generated
	 */
	Attachement_DB_4 createAttachement_DB_4();

	/**
	 * Returns a new object of class '<em>Role Requie DB 4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Requie DB 4</em>'.
	 * @generated
	 */
	Role_Requie_DB_4 createRole_Requie_DB_4();

	/**
	 * Returns a new object of class '<em>Port fornie DB 4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port fornie DB 4</em>'.
	 * @generated
	 */
	Port_fornie_DB_4 createPort_fornie_DB_4();

	/**
	 * Returns a new object of class '<em>Role fournie DB 3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role fournie DB 3</em>'.
	 * @generated
	 */
	Role_fournie_DB_3 createRole_fournie_DB_3();

	/**
	 * Returns a new object of class '<em>Port requie DB 3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port requie DB 3</em>'.
	 * @generated
	 */
	Port_requie_DB_3 createPort_requie_DB_3();

	/**
	 * Returns a new object of class '<em>Role Requie DB 2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Requie DB 2</em>'.
	 * @generated
	 */
	Role_Requie_DB_2 createRole_Requie_DB_2();

	/**
	 * Returns a new object of class '<em>Role fournie DB 1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role fournie DB 1</em>'.
	 * @generated
	 */
	Role_fournie_DB_1 createRole_fournie_DB_1();

	/**
	 * Returns a new object of class '<em>Port Requie DB 1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Requie DB 1</em>'.
	 * @generated
	 */
	Port_Requie_DB_1 createPort_Requie_DB_1();

	/**
	 * Returns a new object of class '<em>Port fournie DB 2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port fournie DB 2</em>'.
	 * @generated
	 */
	Port_fournie_DB_2 createPort_fournie_DB_2();

	/**
	 * Returns a new object of class '<em>Gestionnaire Connexion</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gestionnaire Connexion</em>'.
	 * @generated
	 */
	Gestionnaire_Connexion createGestionnaire_Connexion();

	/**
	 * Returns a new object of class '<em>IGestionnaire Connexion</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IGestionnaire Connexion</em>'.
	 * @generated
	 */
	IGestionnaire_Connexion createIGestionnaire_Connexion();

	/**
	 * Returns a new object of class '<em>Attachement GC 2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement GC 2</em>'.
	 * @generated
	 */
	Attachement_GC_2 createAttachement_GC_2();

	/**
	 * Returns a new object of class '<em>Attachement GC 3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement GC 3</em>'.
	 * @generated
	 */
	Attachement_GC_3 createAttachement_GC_3();

	/**
	 * Returns a new object of class '<em>Attachement GC 4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement GC 4</em>'.
	 * @generated
	 */
	Attachement_GC_4 createAttachement_GC_4();

	/**
	 * Returns a new object of class '<em>Attachement GC 1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement GC 1</em>'.
	 * @generated
	 */
	Attachement_GC_1 createAttachement_GC_1();

	/**
	 * Returns a new object of class '<em>Role Requie GC 3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Requie GC 3</em>'.
	 * @generated
	 */
	Role_Requie_GC_3 createRole_Requie_GC_3();

	/**
	 * Returns a new object of class '<em>Port Requie GC 4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Requie GC 4</em>'.
	 * @generated
	 */
	Port_Requie_GC_4 createPort_Requie_GC_4();

	/**
	 * Returns a new object of class '<em>Port Fournie GC 3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Fournie GC 3</em>'.
	 * @generated
	 */
	Port_Fournie_GC_3 createPort_Fournie_GC_3();

	/**
	 * Returns a new object of class '<em>Role Fournie GC 4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Fournie GC 4</em>'.
	 * @generated
	 */
	Role_Fournie_GC_4 createRole_Fournie_GC_4();

	/**
	 * Returns a new object of class '<em>Role Fournie GC 2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Fournie GC 2</em>'.
	 * @generated
	 */
	Role_Fournie_GC_2 createRole_Fournie_GC_2();

	/**
	 * Returns a new object of class '<em>Port Requie GC 2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Requie GC 2</em>'.
	 * @generated
	 */
	Port_Requie_GC_2 createPort_Requie_GC_2();

	/**
	 * Returns a new object of class '<em>Role Requie GC 1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Requie GC 1</em>'.
	 * @generated
	 */
	Role_Requie_GC_1 createRole_Requie_GC_1();

	/**
	 * Returns a new object of class '<em>Port Fournie GC 1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Fournie GC 1</em>'.
	 * @generated
	 */
	Port_Fournie_GC_1 createPort_Fournie_GC_1();

	/**
	 * Returns a new object of class '<em>IConnecteur GC DB</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IConnecteur GC DB</em>'.
	 * @generated
	 */
	IConnecteur_GC_DB createIConnecteur_GC_DB();

	/**
	 * Returns a new object of class '<em>Connecteur GC DB</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connecteur GC DB</em>'.
	 * @generated
	 */
	Connecteur_GC_DB createConnecteur_GC_DB();

	/**
	 * Returns a new object of class '<em>Gestion Securiterity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gestion Securiterity</em>'.
	 * @generated
	 */
	Gestion_Securiterity createGestion_Securiterity();

	/**
	 * Returns a new object of class '<em>Attachement GS 1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement GS 1</em>'.
	 * @generated
	 */
	Attachement_GS_1 createAttachement_GS_1();

	/**
	 * Returns a new object of class '<em>Port Requie GS 2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Requie GS 2</em>'.
	 * @generated
	 */
	Port_Requie_GS_2 createPort_Requie_GS_2();

	/**
	 * Returns a new object of class '<em>Attachement GS 2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement GS 2</em>'.
	 * @generated
	 */
	Attachement_GS_2 createAttachement_GS_2();

	/**
	 * Returns a new object of class '<em>Attachement GS 3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement GS 3</em>'.
	 * @generated
	 */
	Attachement_GS_3 createAttachement_GS_3();

	/**
	 * Returns a new object of class '<em>Port Fournie GS 1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Fournie GS 1</em>'.
	 * @generated
	 */
	Port_Fournie_GS_1 createPort_Fournie_GS_1();

	/**
	 * Returns a new object of class '<em>Role Requie GS 1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Requie GS 1</em>'.
	 * @generated
	 */
	Role_Requie_GS_1 createRole_Requie_GS_1();

	/**
	 * Returns a new object of class '<em>Role Fournie GS 2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Fournie GS 2</em>'.
	 * @generated
	 */
	Role_Fournie_GS_2 createRole_Fournie_GS_2();

	/**
	 * Returns a new object of class '<em>Port Fournie GS 3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Fournie GS 3</em>'.
	 * @generated
	 */
	Port_Fournie_GS_3 createPort_Fournie_GS_3();

	/**
	 * Returns a new object of class '<em>Role Requie GS 3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Requie GS 3</em>'.
	 * @generated
	 */
	Role_Requie_GS_3 createRole_Requie_GS_3();

	/**
	 * Returns a new object of class '<em>Attachement GS 4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement GS 4</em>'.
	 * @generated
	 */
	Attachement_GS_4 createAttachement_GS_4();

	/**
	 * Returns a new object of class '<em>Port Requie GS 4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Requie GS 4</em>'.
	 * @generated
	 */
	Port_Requie_GS_4 createPort_Requie_GS_4();

	/**
	 * Returns a new object of class '<em>Role Fournie GS 4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Fournie GS 4</em>'.
	 * @generated
	 */
	Role_Fournie_GS_4 createRole_Fournie_GS_4();

	/**
	 * Returns a new object of class '<em>IGestion Securite</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IGestion Securite</em>'.
	 * @generated
	 */
	IGestion_Securite createIGestion_Securite();

	/**
	 * Returns a new object of class '<em>Connecteur GC GS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connecteur GC GS</em>'.
	 * @generated
	 */
	Connecteur_GC_GS createConnecteur_GC_GS();

	/**
	 * Returns a new object of class '<em>IConnecteur GC GS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IConnecteur GC GS</em>'.
	 * @generated
	 */
	IConnecteur_GC_GS createIConnecteur_GC_GS();

	/**
	 * Returns a new object of class '<em>IConnecteur GS DB</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IConnecteur GS DB</em>'.
	 * @generated
	 */
	IConnecteur_GS_DB createIConnecteur_GS_DB();

	/**
	 * Returns a new object of class '<em>Connecteur GS DB</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connecteur GS DB</em>'.
	 * @generated
	 */
	Connecteur_GS_DB createConnecteur_GS_DB();

	/**
	 * Returns a new object of class '<em>Client Serveur</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Client Serveur</em>'.
	 * @generated
	 */
	ClientServeur createClientServeur();

	/**
	 * Returns a new object of class '<em>Client</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Client</em>'.
	 * @generated
	 */
	Client createClient();

	/**
	 * Returns a new object of class '<em>Connecteur RPC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connecteur RPC</em>'.
	 * @generated
	 */
	ConnecteurRPC createConnecteurRPC();

	/**
	 * Returns a new object of class '<em>Serveur</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Serveur</em>'.
	 * @generated
	 */
	Serveur createServeur();

	/**
	 * Returns a new object of class '<em>IClient</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IClient</em>'.
	 * @generated
	 */
	IClient createIClient();

	/**
	 * Returns a new object of class '<em>IConnecteur RPC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IConnecteur RPC</em>'.
	 * @generated
	 */
	IConnecteurRPC createIConnecteurRPC();

	/**
	 * Returns a new object of class '<em>IServeur</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IServeur</em>'.
	 * @generated
	 */
	IServeur createIServeur();

	/**
	 * Returns a new object of class '<em>Send Request</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Send Request</em>'.
	 * @generated
	 */
	SendRequest createSendRequest();

	/**
	 * Returns a new object of class '<em>Attachement Client RPC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement Client RPC</em>'.
	 * @generated
	 */
	AttachementClientRPC createAttachementClientRPC();

	/**
	 * Returns a new object of class '<em>Role Requie CR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Requie CR</em>'.
	 * @generated
	 */
	RoleRequie_CR createRoleRequie_CR();

	/**
	 * Returns a new object of class '<em>Attachement RPC Client</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement RPC Client</em>'.
	 * @generated
	 */
	AttachementRPCClient createAttachementRPCClient();

	/**
	 * Returns a new object of class '<em>Role Fournie RC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Fournie RC</em>'.
	 * @generated
	 */
	RoleFournie_RC createRoleFournie_RC();

	/**
	 * Returns a new object of class '<em>Receve Response</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Receve Response</em>'.
	 * @generated
	 */
	ReceveResponse createReceveResponse();

	/**
	 * Returns a new object of class '<em>Attachement RPC Serveur</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement RPC Serveur</em>'.
	 * @generated
	 */
	AttachementRPCServeur createAttachementRPCServeur();

	/**
	 * Returns a new object of class '<em>Role Requie RS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Requie RS</em>'.
	 * @generated
	 */
	RoleRequie_RS createRoleRequie_RS();

	/**
	 * Returns a new object of class '<em>Port Fournie RS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Fournie RS</em>'.
	 * @generated
	 */
	PortFournie_RS createPortFournie_RS();

	/**
	 * Returns a new object of class '<em>Attachement Serveur RPC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attachement Serveur RPC</em>'.
	 * @generated
	 */
	AttachementServeurRPC createAttachementServeurRPC();

	/**
	 * Returns a new object of class '<em>Role Fournie SR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Fournie SR</em>'.
	 * @generated
	 */
	RoleFournie_SR createRoleFournie_SR();

	/**
	 * Returns a new object of class '<em>Port Requie SR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port Requie SR</em>'.
	 * @generated
	 */
	PortRequie_SR createPortRequie_SR();

	/**
	 * Returns a new object of class '<em>Binding SRPC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Binding SRPC</em>'.
	 * @generated
	 */
	Binding_S_RPC createBinding_S_RPC();

	/**
	 * Returns a new object of class '<em>Port RPC S</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port RPC S</em>'.
	 * @generated
	 */
	Port_RPC_S createPort_RPC_S();

	/**
	 * Returns a new object of class '<em>Port SRPC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port SRPC</em>'.
	 * @generated
	 */
	Port_S_RPC createPort_S_RPC();

	/**
	 * Returns a new object of class '<em>Binding CM S</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Binding CM S</em>'.
	 * @generated
	 */
	Binding_CM_S createBinding_CM_S();

	/**
	 * Returns a new object of class '<em>Port SCM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port SCM</em>'.
	 * @generated
	 */
	Port_S_CM createPort_S_CM();

	/**
	 * Returns a new object of class '<em>Port CM S</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port CM S</em>'.
	 * @generated
	 */
	Port_CM_S createPort_CM_S();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	M1Package getM1Package();

} //M1Factory
