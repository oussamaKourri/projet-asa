/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attachement Client RPC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.AttachementClientRPC#getSendRequest <em>Send Request</em>}</li>
 *   <li>{@link M1.AttachementClientRPC#getRoleRequie_CR <em>Role Requie CR</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getAttachementClientRPC()
 * @model
 * @generated
 */
public interface AttachementClientRPC extends EObject {
	/**
	 * Returns the value of the '<em><b>Send Request</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Send Request</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Send Request</em>' reference.
	 * @see #setSendRequest(SendRequest)
	 * @see M1.M1Package#getAttachementClientRPC_SendRequest()
	 * @model
	 * @generated
	 */
	SendRequest getSendRequest();

	/**
	 * Sets the value of the '{@link M1.AttachementClientRPC#getSendRequest <em>Send Request</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Send Request</em>' reference.
	 * @see #getSendRequest()
	 * @generated
	 */
	void setSendRequest(SendRequest value);

	/**
	 * Returns the value of the '<em><b>Role Requie CR</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Requie CR</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Requie CR</em>' reference.
	 * @see #setRoleRequie_CR(RoleRequie_CR)
	 * @see M1.M1Package#getAttachementClientRPC_RoleRequie_CR()
	 * @model
	 * @generated
	 */
	RoleRequie_CR getRoleRequie_CR();

	/**
	 * Sets the value of the '{@link M1.AttachementClientRPC#getRoleRequie_CR <em>Role Requie CR</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role Requie CR</em>' reference.
	 * @see #getRoleRequie_CR()
	 * @generated
	 */
	void setRoleRequie_CR(RoleRequie_CR value);

} // AttachementClientRPC
