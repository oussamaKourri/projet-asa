/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IConnecteur RPC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.IConnecteurRPC#getRoleRequie_CR <em>Role Requie CR</em>}</li>
 *   <li>{@link M1.IConnecteurRPC#getRoleFournie_RC <em>Role Fournie RC</em>}</li>
 *   <li>{@link M1.IConnecteurRPC#getRoleRequie_RS <em>Role Requie RS</em>}</li>
 *   <li>{@link M1.IConnecteurRPC#getRoleFournie_SR <em>Role Fournie SR</em>}</li>
 *   <li>{@link M1.IConnecteurRPC#getPort_RPC_S <em>Port RPC S</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getIConnecteurRPC()
 * @model
 * @generated
 */
public interface IConnecteurRPC extends EObject {
	/**
	 * Returns the value of the '<em><b>Role Requie CR</b></em>' containment reference list.
	 * The list contents are of type {@link M1.RoleRequie_CR}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Requie CR</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Requie CR</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteurRPC_RoleRequie_CR()
	 * @model type="M1.RoleRequie_CR" containment="true"
	 * @generated
	 */
	EList getRoleRequie_CR();

	/**
	 * Returns the value of the '<em><b>Role Fournie RC</b></em>' containment reference list.
	 * The list contents are of type {@link M1.RoleFournie_RC}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Fournie RC</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Fournie RC</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteurRPC_RoleFournie_RC()
	 * @model type="M1.RoleFournie_RC" containment="true"
	 * @generated
	 */
	EList getRoleFournie_RC();

	/**
	 * Returns the value of the '<em><b>Role Requie RS</b></em>' containment reference list.
	 * The list contents are of type {@link M1.RoleRequie_RS}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Requie RS</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Requie RS</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteurRPC_RoleRequie_RS()
	 * @model type="M1.RoleRequie_RS" containment="true"
	 * @generated
	 */
	EList getRoleRequie_RS();

	/**
	 * Returns the value of the '<em><b>Role Fournie SR</b></em>' containment reference list.
	 * The list contents are of type {@link M1.RoleFournie_SR}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Fournie SR</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Fournie SR</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteurRPC_RoleFournie_SR()
	 * @model type="M1.RoleFournie_SR" containment="true"
	 * @generated
	 */
	EList getRoleFournie_SR();

	/**
	 * Returns the value of the '<em><b>Port RPC S</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Port_RPC_S}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port RPC S</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port RPC S</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteurRPC_Port_RPC_S()
	 * @model type="M1.Port_RPC_S" containment="true"
	 * @generated
	 */
	EList getPort_RPC_S();

} // IConnecteurRPC
