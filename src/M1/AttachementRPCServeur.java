/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attachement RPC Serveur</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.AttachementRPCServeur#getEReference0 <em>EReference0</em>}</li>
 *   <li>{@link M1.AttachementRPCServeur#getEReference1 <em>EReference1</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getAttachementRPCServeur()
 * @model
 * @generated
 */
public interface AttachementRPCServeur extends EObject {
	/**
	 * Returns the value of the '<em><b>EReference0</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.RoleRequie_RS#getAttachement_RS <em>Attachement RS</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference0</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference0</em>' reference.
	 * @see #setEReference0(RoleRequie_RS)
	 * @see M1.M1Package#getAttachementRPCServeur_EReference0()
	 * @see M1.RoleRequie_RS#getAttachement_RS
	 * @model opposite="attachement_RS"
	 * @generated
	 */
	RoleRequie_RS getEReference0();

	/**
	 * Sets the value of the '{@link M1.AttachementRPCServeur#getEReference0 <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference0</em>' reference.
	 * @see #getEReference0()
	 * @generated
	 */
	void setEReference0(RoleRequie_RS value);

	/**
	 * Returns the value of the '<em><b>EReference1</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.PortFournie_RS#getAttachement_RS <em>Attachement RS</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference1</em>' reference.
	 * @see #setEReference1(PortFournie_RS)
	 * @see M1.M1Package#getAttachementRPCServeur_EReference1()
	 * @see M1.PortFournie_RS#getAttachement_RS
	 * @model opposite="attachement_RS"
	 * @generated
	 */
	PortFournie_RS getEReference1();

	/**
	 * Sets the value of the '{@link M1.AttachementRPCServeur#getEReference1 <em>EReference1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference1</em>' reference.
	 * @see #getEReference1()
	 * @generated
	 */
	void setEReference1(PortFournie_RS value);

} // AttachementRPCServeur
