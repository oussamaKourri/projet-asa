/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Fournie GS 1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Port_Fournie_GS_1#getAttachement_GS_1 <em>Attachement GS 1</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getPort_Fournie_GS_1()
 * @model
 * @generated
 */
public interface Port_Fournie_GS_1 extends EObject {
	/**
	 * Returns the value of the '<em><b>Attachement GS 1</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Attachement_GS_1#getPort_fournie_GS_1 <em>Port fournie GS 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attachement GS 1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attachement GS 1</em>' reference.
	 * @see #setAttachement_GS_1(Attachement_GS_1)
	 * @see M1.M1Package#getPort_Fournie_GS_1_Attachement_GS_1()
	 * @see M1.Attachement_GS_1#getPort_fournie_GS_1
	 * @model opposite="port_fournie_GS_1"
	 * @generated
	 */
	Attachement_GS_1 getAttachement_GS_1();

	/**
	 * Sets the value of the '{@link M1.Port_Fournie_GS_1#getAttachement_GS_1 <em>Attachement GS 1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attachement GS 1</em>' reference.
	 * @see #getAttachement_GS_1()
	 * @generated
	 */
	void setAttachement_GS_1(Attachement_GS_1 value);

} // Port_Fournie_GS_1
