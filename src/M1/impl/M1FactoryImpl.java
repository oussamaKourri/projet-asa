/**
 */
package M1.impl;

import M1.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class M1FactoryImpl extends EFactoryImpl implements M1Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static M1Factory init() {
		try {
			M1Factory theM1Factory = (M1Factory)EPackage.Registry.INSTANCE.getEFactory("http://www.example.org/cs"); 
			if (theM1Factory != null) {
				return theM1Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new M1FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public M1FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case M1Package.SERVER_CONFIGURATION: return createServer_Configuration();
			case M1Package.DATA_BASE: return createDataBase();
			case M1Package.IDATA_BASE: return createIDataBase();
			case M1Package.ATACHEMENT_DB_1: return createAtachement_DB_1();
			case M1Package.ATTACHEMENT_DB_2: return createAttachement_DB_2();
			case M1Package.ATTACHEMENT_DB_3: return createAttachement_DB_3();
			case M1Package.ATTACHEMENT_DB_4: return createAttachement_DB_4();
			case M1Package.ROLE_REQUIE_DB_4: return createRole_Requie_DB_4();
			case M1Package.PORT_FORNIE_DB_4: return createPort_fornie_DB_4();
			case M1Package.ROLE_FOURNIE_DB_3: return createRole_fournie_DB_3();
			case M1Package.PORT_REQUIE_DB_3: return createPort_requie_DB_3();
			case M1Package.ROLE_REQUIE_DB_2: return createRole_Requie_DB_2();
			case M1Package.ROLE_FOURNIE_DB_1: return createRole_fournie_DB_1();
			case M1Package.PORT_REQUIE_DB_1: return createPort_Requie_DB_1();
			case M1Package.PORT_FOURNIE_DB_2: return createPort_fournie_DB_2();
			case M1Package.GESTIONNAIRE_CONNEXION: return createGestionnaire_Connexion();
			case M1Package.IGESTIONNAIRE_CONNEXION: return createIGestionnaire_Connexion();
			case M1Package.ATTACHEMENT_GC_2: return createAttachement_GC_2();
			case M1Package.ATTACHEMENT_GC_3: return createAttachement_GC_3();
			case M1Package.ATTACHEMENT_GC_4: return createAttachement_GC_4();
			case M1Package.ATTACHEMENT_GC_1: return createAttachement_GC_1();
			case M1Package.ROLE_REQUIE_GC_3: return createRole_Requie_GC_3();
			case M1Package.PORT_REQUIE_GC_4: return createPort_Requie_GC_4();
			case M1Package.PORT_FOURNIE_GC_3: return createPort_Fournie_GC_3();
			case M1Package.ROLE_FOURNIE_GC_4: return createRole_Fournie_GC_4();
			case M1Package.ROLE_FOURNIE_GC_2: return createRole_Fournie_GC_2();
			case M1Package.PORT_REQUIE_GC_2: return createPort_Requie_GC_2();
			case M1Package.ROLE_REQUIE_GC_1: return createRole_Requie_GC_1();
			case M1Package.PORT_FOURNIE_GC_1: return createPort_Fournie_GC_1();
			case M1Package.ICONNECTEUR_GC_DB: return createIConnecteur_GC_DB();
			case M1Package.CONNECTEUR_GC_DB: return createConnecteur_GC_DB();
			case M1Package.GESTION_SECURITERITY: return createGestion_Securiterity();
			case M1Package.ATTACHEMENT_GS_1: return createAttachement_GS_1();
			case M1Package.PORT_REQUIE_GS_2: return createPort_Requie_GS_2();
			case M1Package.ATTACHEMENT_GS_2: return createAttachement_GS_2();
			case M1Package.ATTACHEMENT_GS_3: return createAttachement_GS_3();
			case M1Package.PORT_FOURNIE_GS_1: return createPort_Fournie_GS_1();
			case M1Package.ROLE_REQUIE_GS_1: return createRole_Requie_GS_1();
			case M1Package.ROLE_FOURNIE_GS_2: return createRole_Fournie_GS_2();
			case M1Package.PORT_FOURNIE_GS_3: return createPort_Fournie_GS_3();
			case M1Package.ROLE_REQUIE_GS_3: return createRole_Requie_GS_3();
			case M1Package.ATTACHEMENT_GS_4: return createAttachement_GS_4();
			case M1Package.PORT_REQUIE_GS_4: return createPort_Requie_GS_4();
			case M1Package.ROLE_FOURNIE_GS_4: return createRole_Fournie_GS_4();
			case M1Package.IGESTION_SECURITE: return createIGestion_Securite();
			case M1Package.CONNECTEUR_GC_GS: return createConnecteur_GC_GS();
			case M1Package.ICONNECTEUR_GC_GS: return createIConnecteur_GC_GS();
			case M1Package.ICONNECTEUR_GS_DB: return createIConnecteur_GS_DB();
			case M1Package.CONNECTEUR_GS_DB: return createConnecteur_GS_DB();
			case M1Package.CLIENT_SERVEUR: return createClientServeur();
			case M1Package.CLIENT: return createClient();
			case M1Package.CONNECTEUR_RPC: return createConnecteurRPC();
			case M1Package.SERVEUR: return createServeur();
			case M1Package.ICLIENT: return createIClient();
			case M1Package.ICONNECTEUR_RPC: return createIConnecteurRPC();
			case M1Package.ISERVEUR: return createIServeur();
			case M1Package.SEND_REQUEST: return createSendRequest();
			case M1Package.ATTACHEMENT_CLIENT_RPC: return createAttachementClientRPC();
			case M1Package.ROLE_REQUIE_CR: return createRoleRequie_CR();
			case M1Package.ATTACHEMENT_RPC_CLIENT: return createAttachementRPCClient();
			case M1Package.ROLE_FOURNIE_RC: return createRoleFournie_RC();
			case M1Package.RECEVE_RESPONSE: return createReceveResponse();
			case M1Package.ATTACHEMENT_RPC_SERVEUR: return createAttachementRPCServeur();
			case M1Package.ROLE_REQUIE_RS: return createRoleRequie_RS();
			case M1Package.PORT_FOURNIE_RS: return createPortFournie_RS();
			case M1Package.ATTACHEMENT_SERVEUR_RPC: return createAttachementServeurRPC();
			case M1Package.ROLE_FOURNIE_SR: return createRoleFournie_SR();
			case M1Package.PORT_REQUIE_SR: return createPortRequie_SR();
			case M1Package.BINDING_SRPC: return createBinding_S_RPC();
			case M1Package.PORT_RPC_S: return createPort_RPC_S();
			case M1Package.PORT_SRPC: return createPort_S_RPC();
			case M1Package.BINDING_CM_S: return createBinding_CM_S();
			case M1Package.PORT_SCM: return createPort_S_CM();
			case M1Package.PORT_CM_S: return createPort_CM_S();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Server_Configuration createServer_Configuration() {
		Server_ConfigurationImpl server_Configuration = new Server_ConfigurationImpl();
		return server_Configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataBase createDataBase() {
		DataBaseImpl dataBase = new DataBaseImpl();
		return dataBase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IDataBase createIDataBase() {
		IDataBaseImpl iDataBase = new IDataBaseImpl();
		return iDataBase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Atachement_DB_1 createAtachement_DB_1() {
		Atachement_DB_1Impl atachement_DB_1 = new Atachement_DB_1Impl();
		return atachement_DB_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_DB_2 createAttachement_DB_2() {
		Attachement_DB_2Impl attachement_DB_2 = new Attachement_DB_2Impl();
		return attachement_DB_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_DB_3 createAttachement_DB_3() {
		Attachement_DB_3Impl attachement_DB_3 = new Attachement_DB_3Impl();
		return attachement_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_DB_4 createAttachement_DB_4() {
		Attachement_DB_4Impl attachement_DB_4 = new Attachement_DB_4Impl();
		return attachement_DB_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_DB_4 createRole_Requie_DB_4() {
		Role_Requie_DB_4Impl role_Requie_DB_4 = new Role_Requie_DB_4Impl();
		return role_Requie_DB_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_fornie_DB_4 createPort_fornie_DB_4() {
		Port_fornie_DB_4Impl port_fornie_DB_4 = new Port_fornie_DB_4Impl();
		return port_fornie_DB_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_fournie_DB_3 createRole_fournie_DB_3() {
		Role_fournie_DB_3Impl role_fournie_DB_3 = new Role_fournie_DB_3Impl();
		return role_fournie_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_requie_DB_3 createPort_requie_DB_3() {
		Port_requie_DB_3Impl port_requie_DB_3 = new Port_requie_DB_3Impl();
		return port_requie_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_DB_2 createRole_Requie_DB_2() {
		Role_Requie_DB_2Impl role_Requie_DB_2 = new Role_Requie_DB_2Impl();
		return role_Requie_DB_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_fournie_DB_1 createRole_fournie_DB_1() {
		Role_fournie_DB_1Impl role_fournie_DB_1 = new Role_fournie_DB_1Impl();
		return role_fournie_DB_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_DB_1 createPort_Requie_DB_1() {
		Port_Requie_DB_1Impl port_Requie_DB_1 = new Port_Requie_DB_1Impl();
		return port_Requie_DB_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_fournie_DB_2 createPort_fournie_DB_2() {
		Port_fournie_DB_2Impl port_fournie_DB_2 = new Port_fournie_DB_2Impl();
		return port_fournie_DB_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Gestionnaire_Connexion createGestionnaire_Connexion() {
		Gestionnaire_ConnexionImpl gestionnaire_Connexion = new Gestionnaire_ConnexionImpl();
		return gestionnaire_Connexion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IGestionnaire_Connexion createIGestionnaire_Connexion() {
		IGestionnaire_ConnexionImpl iGestionnaire_Connexion = new IGestionnaire_ConnexionImpl();
		return iGestionnaire_Connexion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_GC_2 createAttachement_GC_2() {
		Attachement_GC_2Impl attachement_GC_2 = new Attachement_GC_2Impl();
		return attachement_GC_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_GC_3 createAttachement_GC_3() {
		Attachement_GC_3Impl attachement_GC_3 = new Attachement_GC_3Impl();
		return attachement_GC_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_GC_4 createAttachement_GC_4() {
		Attachement_GC_4Impl attachement_GC_4 = new Attachement_GC_4Impl();
		return attachement_GC_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_GC_1 createAttachement_GC_1() {
		Attachement_GC_1Impl attachement_GC_1 = new Attachement_GC_1Impl();
		return attachement_GC_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_GC_3 createRole_Requie_GC_3() {
		Role_Requie_GC_3Impl role_Requie_GC_3 = new Role_Requie_GC_3Impl();
		return role_Requie_GC_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_GC_4 createPort_Requie_GC_4() {
		Port_Requie_GC_4Impl port_Requie_GC_4 = new Port_Requie_GC_4Impl();
		return port_Requie_GC_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Fournie_GC_3 createPort_Fournie_GC_3() {
		Port_Fournie_GC_3Impl port_Fournie_GC_3 = new Port_Fournie_GC_3Impl();
		return port_Fournie_GC_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Fournie_GC_4 createRole_Fournie_GC_4() {
		Role_Fournie_GC_4Impl role_Fournie_GC_4 = new Role_Fournie_GC_4Impl();
		return role_Fournie_GC_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Fournie_GC_2 createRole_Fournie_GC_2() {
		Role_Fournie_GC_2Impl role_Fournie_GC_2 = new Role_Fournie_GC_2Impl();
		return role_Fournie_GC_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_GC_2 createPort_Requie_GC_2() {
		Port_Requie_GC_2Impl port_Requie_GC_2 = new Port_Requie_GC_2Impl();
		return port_Requie_GC_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_GC_1 createRole_Requie_GC_1() {
		Role_Requie_GC_1Impl role_Requie_GC_1 = new Role_Requie_GC_1Impl();
		return role_Requie_GC_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Fournie_GC_1 createPort_Fournie_GC_1() {
		Port_Fournie_GC_1Impl port_Fournie_GC_1 = new Port_Fournie_GC_1Impl();
		return port_Fournie_GC_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IConnecteur_GC_DB createIConnecteur_GC_DB() {
		IConnecteur_GC_DBImpl iConnecteur_GC_DB = new IConnecteur_GC_DBImpl();
		return iConnecteur_GC_DB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connecteur_GC_DB createConnecteur_GC_DB() {
		Connecteur_GC_DBImpl connecteur_GC_DB = new Connecteur_GC_DBImpl();
		return connecteur_GC_DB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Gestion_Securiterity createGestion_Securiterity() {
		Gestion_SecuriterityImpl gestion_Securiterity = new Gestion_SecuriterityImpl();
		return gestion_Securiterity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_GS_1 createAttachement_GS_1() {
		Attachement_GS_1Impl attachement_GS_1 = new Attachement_GS_1Impl();
		return attachement_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_GS_2 createPort_Requie_GS_2() {
		Port_Requie_GS_2Impl port_Requie_GS_2 = new Port_Requie_GS_2Impl();
		return port_Requie_GS_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_GS_2 createAttachement_GS_2() {
		Attachement_GS_2Impl attachement_GS_2 = new Attachement_GS_2Impl();
		return attachement_GS_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_GS_3 createAttachement_GS_3() {
		Attachement_GS_3Impl attachement_GS_3 = new Attachement_GS_3Impl();
		return attachement_GS_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Fournie_GS_1 createPort_Fournie_GS_1() {
		Port_Fournie_GS_1Impl port_Fournie_GS_1 = new Port_Fournie_GS_1Impl();
		return port_Fournie_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_GS_1 createRole_Requie_GS_1() {
		Role_Requie_GS_1Impl role_Requie_GS_1 = new Role_Requie_GS_1Impl();
		return role_Requie_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Fournie_GS_2 createRole_Fournie_GS_2() {
		Role_Fournie_GS_2Impl role_Fournie_GS_2 = new Role_Fournie_GS_2Impl();
		return role_Fournie_GS_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Fournie_GS_3 createPort_Fournie_GS_3() {
		Port_Fournie_GS_3Impl port_Fournie_GS_3 = new Port_Fournie_GS_3Impl();
		return port_Fournie_GS_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_GS_3 createRole_Requie_GS_3() {
		Role_Requie_GS_3Impl role_Requie_GS_3 = new Role_Requie_GS_3Impl();
		return role_Requie_GS_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_GS_4 createAttachement_GS_4() {
		Attachement_GS_4Impl attachement_GS_4 = new Attachement_GS_4Impl();
		return attachement_GS_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_GS_4 createPort_Requie_GS_4() {
		Port_Requie_GS_4Impl port_Requie_GS_4 = new Port_Requie_GS_4Impl();
		return port_Requie_GS_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Fournie_GS_4 createRole_Fournie_GS_4() {
		Role_Fournie_GS_4Impl role_Fournie_GS_4 = new Role_Fournie_GS_4Impl();
		return role_Fournie_GS_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IGestion_Securite createIGestion_Securite() {
		IGestion_SecuriteImpl iGestion_Securite = new IGestion_SecuriteImpl();
		return iGestion_Securite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connecteur_GC_GS createConnecteur_GC_GS() {
		Connecteur_GC_GSImpl connecteur_GC_GS = new Connecteur_GC_GSImpl();
		return connecteur_GC_GS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IConnecteur_GC_GS createIConnecteur_GC_GS() {
		IConnecteur_GC_GSImpl iConnecteur_GC_GS = new IConnecteur_GC_GSImpl();
		return iConnecteur_GC_GS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IConnecteur_GS_DB createIConnecteur_GS_DB() {
		IConnecteur_GS_DBImpl iConnecteur_GS_DB = new IConnecteur_GS_DBImpl();
		return iConnecteur_GS_DB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connecteur_GS_DB createConnecteur_GS_DB() {
		Connecteur_GS_DBImpl connecteur_GS_DB = new Connecteur_GS_DBImpl();
		return connecteur_GS_DB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClientServeur createClientServeur() {
		ClientServeurImpl clientServeur = new ClientServeurImpl();
		return clientServeur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Client createClient() {
		ClientImpl client = new ClientImpl();
		return client;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnecteurRPC createConnecteurRPC() {
		ConnecteurRPCImpl connecteurRPC = new ConnecteurRPCImpl();
		return connecteurRPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Serveur createServeur() {
		ServeurImpl serveur = new ServeurImpl();
		return serveur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IClient createIClient() {
		IClientImpl iClient = new IClientImpl();
		return iClient;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IConnecteurRPC createIConnecteurRPC() {
		IConnecteurRPCImpl iConnecteurRPC = new IConnecteurRPCImpl();
		return iConnecteurRPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IServeur createIServeur() {
		IServeurImpl iServeur = new IServeurImpl();
		return iServeur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SendRequest createSendRequest() {
		SendRequestImpl sendRequest = new SendRequestImpl();
		return sendRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementClientRPC createAttachementClientRPC() {
		AttachementClientRPCImpl attachementClientRPC = new AttachementClientRPCImpl();
		return attachementClientRPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleRequie_CR createRoleRequie_CR() {
		RoleRequie_CRImpl roleRequie_CR = new RoleRequie_CRImpl();
		return roleRequie_CR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementRPCClient createAttachementRPCClient() {
		AttachementRPCClientImpl attachementRPCClient = new AttachementRPCClientImpl();
		return attachementRPCClient;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleFournie_RC createRoleFournie_RC() {
		RoleFournie_RCImpl roleFournie_RC = new RoleFournie_RCImpl();
		return roleFournie_RC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReceveResponse createReceveResponse() {
		ReceveResponseImpl receveResponse = new ReceveResponseImpl();
		return receveResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementRPCServeur createAttachementRPCServeur() {
		AttachementRPCServeurImpl attachementRPCServeur = new AttachementRPCServeurImpl();
		return attachementRPCServeur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleRequie_RS createRoleRequie_RS() {
		RoleRequie_RSImpl roleRequie_RS = new RoleRequie_RSImpl();
		return roleRequie_RS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortFournie_RS createPortFournie_RS() {
		PortFournie_RSImpl portFournie_RS = new PortFournie_RSImpl();
		return portFournie_RS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementServeurRPC createAttachementServeurRPC() {
		AttachementServeurRPCImpl attachementServeurRPC = new AttachementServeurRPCImpl();
		return attachementServeurRPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleFournie_SR createRoleFournie_SR() {
		RoleFournie_SRImpl roleFournie_SR = new RoleFournie_SRImpl();
		return roleFournie_SR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortRequie_SR createPortRequie_SR() {
		PortRequie_SRImpl portRequie_SR = new PortRequie_SRImpl();
		return portRequie_SR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Binding_S_RPC createBinding_S_RPC() {
		Binding_S_RPCImpl binding_S_RPC = new Binding_S_RPCImpl();
		return binding_S_RPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_RPC_S createPort_RPC_S() {
		Port_RPC_SImpl port_RPC_S = new Port_RPC_SImpl();
		return port_RPC_S;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_S_RPC createPort_S_RPC() {
		Port_S_RPCImpl port_S_RPC = new Port_S_RPCImpl();
		return port_S_RPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Binding_CM_S createBinding_CM_S() {
		Binding_CM_SImpl binding_CM_S = new Binding_CM_SImpl();
		return binding_CM_S;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_S_CM createPort_S_CM() {
		Port_S_CMImpl port_S_CM = new Port_S_CMImpl();
		return port_S_CM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_CM_S createPort_CM_S() {
		Port_CM_SImpl port_CM_S = new Port_CM_SImpl();
		return port_CM_S;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public M1Package getM1Package() {
		return (M1Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static M1Package getPackage() {
		return M1Package.eINSTANCE;
	}

} //M1FactoryImpl
