/**
 */
package M1.impl;

import M1.IDataBase;
import M1.M1Package;
import M1.Port_Requie_DB_1;
import M1.Port_fornie_DB_4;
import M1.Port_fournie_DB_2;
import M1.Port_requie_DB_3;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IData Base</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.IDataBaseImpl#getPort_requie_DB_1 <em>Port requie DB 1</em>}</li>
 *   <li>{@link M1.impl.IDataBaseImpl#getPort_fournie_DB_2 <em>Port fournie DB 2</em>}</li>
 *   <li>{@link M1.impl.IDataBaseImpl#getPort_requie_DB_3 <em>Port requie DB 3</em>}</li>
 *   <li>{@link M1.impl.IDataBaseImpl#getPort_fournie_DB_4 <em>Port fournie DB 4</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IDataBaseImpl extends EObjectImpl implements IDataBase {
	/**
	 * The cached value of the '{@link #getPort_requie_DB_1() <em>Port requie DB 1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_requie_DB_1()
	 * @generated
	 * @ordered
	 */
	protected Port_Requie_DB_1 port_requie_DB_1;

	/**
	 * The cached value of the '{@link #getPort_fournie_DB_2() <em>Port fournie DB 2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_fournie_DB_2()
	 * @generated
	 * @ordered
	 */
	protected Port_fournie_DB_2 port_fournie_DB_2;

	/**
	 * The cached value of the '{@link #getPort_requie_DB_3() <em>Port requie DB 3</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_requie_DB_3()
	 * @generated
	 * @ordered
	 */
	protected Port_requie_DB_3 port_requie_DB_3;

	/**
	 * The cached value of the '{@link #getPort_fournie_DB_4() <em>Port fournie DB 4</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_fournie_DB_4()
	 * @generated
	 * @ordered
	 */
	protected Port_fornie_DB_4 port_fournie_DB_4;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IDataBaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.IDATA_BASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_DB_1 getPort_requie_DB_1() {
		return port_requie_DB_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_requie_DB_1(Port_Requie_DB_1 newPort_requie_DB_1, NotificationChain msgs) {
		Port_Requie_DB_1 oldPort_requie_DB_1 = port_requie_DB_1;
		port_requie_DB_1 = newPort_requie_DB_1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.IDATA_BASE__PORT_REQUIE_DB_1, oldPort_requie_DB_1, newPort_requie_DB_1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_requie_DB_1(Port_Requie_DB_1 newPort_requie_DB_1) {
		if (newPort_requie_DB_1 != port_requie_DB_1) {
			NotificationChain msgs = null;
			if (port_requie_DB_1 != null)
				msgs = ((InternalEObject)port_requie_DB_1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.IDATA_BASE__PORT_REQUIE_DB_1, null, msgs);
			if (newPort_requie_DB_1 != null)
				msgs = ((InternalEObject)newPort_requie_DB_1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.IDATA_BASE__PORT_REQUIE_DB_1, null, msgs);
			msgs = basicSetPort_requie_DB_1(newPort_requie_DB_1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.IDATA_BASE__PORT_REQUIE_DB_1, newPort_requie_DB_1, newPort_requie_DB_1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_fournie_DB_2 getPort_fournie_DB_2() {
		return port_fournie_DB_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_fournie_DB_2(Port_fournie_DB_2 newPort_fournie_DB_2, NotificationChain msgs) {
		Port_fournie_DB_2 oldPort_fournie_DB_2 = port_fournie_DB_2;
		port_fournie_DB_2 = newPort_fournie_DB_2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.IDATA_BASE__PORT_FOURNIE_DB_2, oldPort_fournie_DB_2, newPort_fournie_DB_2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_fournie_DB_2(Port_fournie_DB_2 newPort_fournie_DB_2) {
		if (newPort_fournie_DB_2 != port_fournie_DB_2) {
			NotificationChain msgs = null;
			if (port_fournie_DB_2 != null)
				msgs = ((InternalEObject)port_fournie_DB_2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.IDATA_BASE__PORT_FOURNIE_DB_2, null, msgs);
			if (newPort_fournie_DB_2 != null)
				msgs = ((InternalEObject)newPort_fournie_DB_2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.IDATA_BASE__PORT_FOURNIE_DB_2, null, msgs);
			msgs = basicSetPort_fournie_DB_2(newPort_fournie_DB_2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.IDATA_BASE__PORT_FOURNIE_DB_2, newPort_fournie_DB_2, newPort_fournie_DB_2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_requie_DB_3 getPort_requie_DB_3() {
		return port_requie_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_requie_DB_3(Port_requie_DB_3 newPort_requie_DB_3, NotificationChain msgs) {
		Port_requie_DB_3 oldPort_requie_DB_3 = port_requie_DB_3;
		port_requie_DB_3 = newPort_requie_DB_3;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.IDATA_BASE__PORT_REQUIE_DB_3, oldPort_requie_DB_3, newPort_requie_DB_3);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_requie_DB_3(Port_requie_DB_3 newPort_requie_DB_3) {
		if (newPort_requie_DB_3 != port_requie_DB_3) {
			NotificationChain msgs = null;
			if (port_requie_DB_3 != null)
				msgs = ((InternalEObject)port_requie_DB_3).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.IDATA_BASE__PORT_REQUIE_DB_3, null, msgs);
			if (newPort_requie_DB_3 != null)
				msgs = ((InternalEObject)newPort_requie_DB_3).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.IDATA_BASE__PORT_REQUIE_DB_3, null, msgs);
			msgs = basicSetPort_requie_DB_3(newPort_requie_DB_3, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.IDATA_BASE__PORT_REQUIE_DB_3, newPort_requie_DB_3, newPort_requie_DB_3));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_fornie_DB_4 getPort_fournie_DB_4() {
		return port_fournie_DB_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_fournie_DB_4(Port_fornie_DB_4 newPort_fournie_DB_4, NotificationChain msgs) {
		Port_fornie_DB_4 oldPort_fournie_DB_4 = port_fournie_DB_4;
		port_fournie_DB_4 = newPort_fournie_DB_4;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.IDATA_BASE__PORT_FOURNIE_DB_4, oldPort_fournie_DB_4, newPort_fournie_DB_4);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_fournie_DB_4(Port_fornie_DB_4 newPort_fournie_DB_4) {
		if (newPort_fournie_DB_4 != port_fournie_DB_4) {
			NotificationChain msgs = null;
			if (port_fournie_DB_4 != null)
				msgs = ((InternalEObject)port_fournie_DB_4).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.IDATA_BASE__PORT_FOURNIE_DB_4, null, msgs);
			if (newPort_fournie_DB_4 != null)
				msgs = ((InternalEObject)newPort_fournie_DB_4).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.IDATA_BASE__PORT_FOURNIE_DB_4, null, msgs);
			msgs = basicSetPort_fournie_DB_4(newPort_fournie_DB_4, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.IDATA_BASE__PORT_FOURNIE_DB_4, newPort_fournie_DB_4, newPort_fournie_DB_4));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.IDATA_BASE__PORT_REQUIE_DB_1:
				return basicSetPort_requie_DB_1(null, msgs);
			case M1Package.IDATA_BASE__PORT_FOURNIE_DB_2:
				return basicSetPort_fournie_DB_2(null, msgs);
			case M1Package.IDATA_BASE__PORT_REQUIE_DB_3:
				return basicSetPort_requie_DB_3(null, msgs);
			case M1Package.IDATA_BASE__PORT_FOURNIE_DB_4:
				return basicSetPort_fournie_DB_4(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.IDATA_BASE__PORT_REQUIE_DB_1:
				return getPort_requie_DB_1();
			case M1Package.IDATA_BASE__PORT_FOURNIE_DB_2:
				return getPort_fournie_DB_2();
			case M1Package.IDATA_BASE__PORT_REQUIE_DB_3:
				return getPort_requie_DB_3();
			case M1Package.IDATA_BASE__PORT_FOURNIE_DB_4:
				return getPort_fournie_DB_4();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.IDATA_BASE__PORT_REQUIE_DB_1:
				setPort_requie_DB_1((Port_Requie_DB_1)newValue);
				return;
			case M1Package.IDATA_BASE__PORT_FOURNIE_DB_2:
				setPort_fournie_DB_2((Port_fournie_DB_2)newValue);
				return;
			case M1Package.IDATA_BASE__PORT_REQUIE_DB_3:
				setPort_requie_DB_3((Port_requie_DB_3)newValue);
				return;
			case M1Package.IDATA_BASE__PORT_FOURNIE_DB_4:
				setPort_fournie_DB_4((Port_fornie_DB_4)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.IDATA_BASE__PORT_REQUIE_DB_1:
				setPort_requie_DB_1((Port_Requie_DB_1)null);
				return;
			case M1Package.IDATA_BASE__PORT_FOURNIE_DB_2:
				setPort_fournie_DB_2((Port_fournie_DB_2)null);
				return;
			case M1Package.IDATA_BASE__PORT_REQUIE_DB_3:
				setPort_requie_DB_3((Port_requie_DB_3)null);
				return;
			case M1Package.IDATA_BASE__PORT_FOURNIE_DB_4:
				setPort_fournie_DB_4((Port_fornie_DB_4)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.IDATA_BASE__PORT_REQUIE_DB_1:
				return port_requie_DB_1 != null;
			case M1Package.IDATA_BASE__PORT_FOURNIE_DB_2:
				return port_fournie_DB_2 != null;
			case M1Package.IDATA_BASE__PORT_REQUIE_DB_3:
				return port_requie_DB_3 != null;
			case M1Package.IDATA_BASE__PORT_FOURNIE_DB_4:
				return port_fournie_DB_4 != null;
		}
		return super.eIsSet(featureID);
	}

} //IDataBaseImpl
