/**
 */
package M1.impl;

import M1.Attachement_DB_3;
import M1.M1Package;
import M1.Port_requie_DB_3;
import M1.Role_fournie_DB_3;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attachement DB 3</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Attachement_DB_3Impl#getRole_fourni_DB_3 <em>Role fourni DB 3</em>}</li>
 *   <li>{@link M1.impl.Attachement_DB_3Impl#getPort_requie_DB_3 <em>Port requie DB 3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Attachement_DB_3Impl extends EObjectImpl implements Attachement_DB_3 {
	/**
	 * The cached value of the '{@link #getRole_fourni_DB_3() <em>Role fourni DB 3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_fourni_DB_3()
	 * @generated
	 * @ordered
	 */
	protected Role_fournie_DB_3 role_fourni_DB_3;

	/**
	 * The cached value of the '{@link #getPort_requie_DB_3() <em>Port requie DB 3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_requie_DB_3()
	 * @generated
	 * @ordered
	 */
	protected Port_requie_DB_3 port_requie_DB_3;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Attachement_DB_3Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ATTACHEMENT_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_fournie_DB_3 getRole_fourni_DB_3() {
		if (role_fourni_DB_3 != null && role_fourni_DB_3.eIsProxy()) {
			InternalEObject oldRole_fourni_DB_3 = (InternalEObject)role_fourni_DB_3;
			role_fourni_DB_3 = (Role_fournie_DB_3)eResolveProxy(oldRole_fourni_DB_3);
			if (role_fourni_DB_3 != oldRole_fourni_DB_3) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3, oldRole_fourni_DB_3, role_fourni_DB_3));
			}
		}
		return role_fourni_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_fournie_DB_3 basicGetRole_fourni_DB_3() {
		return role_fourni_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRole_fourni_DB_3(Role_fournie_DB_3 newRole_fourni_DB_3, NotificationChain msgs) {
		Role_fournie_DB_3 oldRole_fourni_DB_3 = role_fourni_DB_3;
		role_fourni_DB_3 = newRole_fourni_DB_3;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3, oldRole_fourni_DB_3, newRole_fourni_DB_3);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole_fourni_DB_3(Role_fournie_DB_3 newRole_fourni_DB_3) {
		if (newRole_fourni_DB_3 != role_fourni_DB_3) {
			NotificationChain msgs = null;
			if (role_fourni_DB_3 != null)
				msgs = ((InternalEObject)role_fourni_DB_3).eInverseRemove(this, M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3, Role_fournie_DB_3.class, msgs);
			if (newRole_fourni_DB_3 != null)
				msgs = ((InternalEObject)newRole_fourni_DB_3).eInverseAdd(this, M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3, Role_fournie_DB_3.class, msgs);
			msgs = basicSetRole_fourni_DB_3(newRole_fourni_DB_3, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3, newRole_fourni_DB_3, newRole_fourni_DB_3));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_requie_DB_3 getPort_requie_DB_3() {
		if (port_requie_DB_3 != null && port_requie_DB_3.eIsProxy()) {
			InternalEObject oldPort_requie_DB_3 = (InternalEObject)port_requie_DB_3;
			port_requie_DB_3 = (Port_requie_DB_3)eResolveProxy(oldPort_requie_DB_3);
			if (port_requie_DB_3 != oldPort_requie_DB_3) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_DB_3__PORT_REQUIE_DB_3, oldPort_requie_DB_3, port_requie_DB_3));
			}
		}
		return port_requie_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_requie_DB_3 basicGetPort_requie_DB_3() {
		return port_requie_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_requie_DB_3(Port_requie_DB_3 newPort_requie_DB_3, NotificationChain msgs) {
		Port_requie_DB_3 oldPort_requie_DB_3 = port_requie_DB_3;
		port_requie_DB_3 = newPort_requie_DB_3;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_3__PORT_REQUIE_DB_3, oldPort_requie_DB_3, newPort_requie_DB_3);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_requie_DB_3(Port_requie_DB_3 newPort_requie_DB_3) {
		if (newPort_requie_DB_3 != port_requie_DB_3) {
			NotificationChain msgs = null;
			if (port_requie_DB_3 != null)
				msgs = ((InternalEObject)port_requie_DB_3).eInverseRemove(this, M1Package.PORT_REQUIE_DB_3__ATTACHEMENT_DB_3, Port_requie_DB_3.class, msgs);
			if (newPort_requie_DB_3 != null)
				msgs = ((InternalEObject)newPort_requie_DB_3).eInverseAdd(this, M1Package.PORT_REQUIE_DB_3__ATTACHEMENT_DB_3, Port_requie_DB_3.class, msgs);
			msgs = basicSetPort_requie_DB_3(newPort_requie_DB_3, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_3__PORT_REQUIE_DB_3, newPort_requie_DB_3, newPort_requie_DB_3));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3:
				if (role_fourni_DB_3 != null)
					msgs = ((InternalEObject)role_fourni_DB_3).eInverseRemove(this, M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3, Role_fournie_DB_3.class, msgs);
				return basicSetRole_fourni_DB_3((Role_fournie_DB_3)otherEnd, msgs);
			case M1Package.ATTACHEMENT_DB_3__PORT_REQUIE_DB_3:
				if (port_requie_DB_3 != null)
					msgs = ((InternalEObject)port_requie_DB_3).eInverseRemove(this, M1Package.PORT_REQUIE_DB_3__ATTACHEMENT_DB_3, Port_requie_DB_3.class, msgs);
				return basicSetPort_requie_DB_3((Port_requie_DB_3)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3:
				return basicSetRole_fourni_DB_3(null, msgs);
			case M1Package.ATTACHEMENT_DB_3__PORT_REQUIE_DB_3:
				return basicSetPort_requie_DB_3(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3:
				if (resolve) return getRole_fourni_DB_3();
				return basicGetRole_fourni_DB_3();
			case M1Package.ATTACHEMENT_DB_3__PORT_REQUIE_DB_3:
				if (resolve) return getPort_requie_DB_3();
				return basicGetPort_requie_DB_3();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3:
				setRole_fourni_DB_3((Role_fournie_DB_3)newValue);
				return;
			case M1Package.ATTACHEMENT_DB_3__PORT_REQUIE_DB_3:
				setPort_requie_DB_3((Port_requie_DB_3)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3:
				setRole_fourni_DB_3((Role_fournie_DB_3)null);
				return;
			case M1Package.ATTACHEMENT_DB_3__PORT_REQUIE_DB_3:
				setPort_requie_DB_3((Port_requie_DB_3)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3:
				return role_fourni_DB_3 != null;
			case M1Package.ATTACHEMENT_DB_3__PORT_REQUIE_DB_3:
				return port_requie_DB_3 != null;
		}
		return super.eIsSet(featureID);
	}

} //Attachement_DB_3Impl
