/**
 */
package M1.impl;

import M1.ConnecteurRPC;
import M1.IConnecteurRPC;
import M1.M1Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connecteur RPC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.ConnecteurRPCImpl#getInterfaceConnecteur <em>Interface Connecteur</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConnecteurRPCImpl extends EObjectImpl implements ConnecteurRPC {
	/**
	 * The cached value of the '{@link #getInterfaceConnecteur() <em>Interface Connecteur</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceConnecteur()
	 * @generated
	 * @ordered
	 */
	protected EList interfaceConnecteur;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnecteurRPCImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.CONNECTEUR_RPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getInterfaceConnecteur() {
		if (interfaceConnecteur == null) {
			interfaceConnecteur = new EObjectContainmentEList(IConnecteurRPC.class, this, M1Package.CONNECTEUR_RPC__INTERFACE_CONNECTEUR);
		}
		return interfaceConnecteur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.CONNECTEUR_RPC__INTERFACE_CONNECTEUR:
				return ((InternalEList)getInterfaceConnecteur()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.CONNECTEUR_RPC__INTERFACE_CONNECTEUR:
				return getInterfaceConnecteur();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.CONNECTEUR_RPC__INTERFACE_CONNECTEUR:
				getInterfaceConnecteur().clear();
				getInterfaceConnecteur().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.CONNECTEUR_RPC__INTERFACE_CONNECTEUR:
				getInterfaceConnecteur().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.CONNECTEUR_RPC__INTERFACE_CONNECTEUR:
				return interfaceConnecteur != null && !interfaceConnecteur.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ConnecteurRPCImpl
