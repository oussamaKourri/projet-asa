/**
 */
package M1.impl;

import M1.Attachement_DB_4;
import M1.M1Package;
import M1.Role_Requie_DB_4;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Role Requie DB 4</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Role_Requie_DB_4Impl#getAttachement_DB_4 <em>Attachement DB 4</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Role_Requie_DB_4Impl extends Attachement_DB_4Impl implements Role_Requie_DB_4 {
	/**
	 * The cached value of the '{@link #getAttachement_DB_4() <em>Attachement DB 4</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachement_DB_4()
	 * @generated
	 * @ordered
	 */
	protected Attachement_DB_4 attachement_DB_4;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Role_Requie_DB_4Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ROLE_REQUIE_DB_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_DB_4 getAttachement_DB_4() {
		if (attachement_DB_4 != null && attachement_DB_4.eIsProxy()) {
			InternalEObject oldAttachement_DB_4 = (InternalEObject)attachement_DB_4;
			attachement_DB_4 = (Attachement_DB_4)eResolveProxy(oldAttachement_DB_4);
			if (attachement_DB_4 != oldAttachement_DB_4) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4, oldAttachement_DB_4, attachement_DB_4));
			}
		}
		return attachement_DB_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_DB_4 basicGetAttachement_DB_4() {
		return attachement_DB_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachement_DB_4(Attachement_DB_4 newAttachement_DB_4, NotificationChain msgs) {
		Attachement_DB_4 oldAttachement_DB_4 = attachement_DB_4;
		attachement_DB_4 = newAttachement_DB_4;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4, oldAttachement_DB_4, newAttachement_DB_4);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachement_DB_4(Attachement_DB_4 newAttachement_DB_4) {
		if (newAttachement_DB_4 != attachement_DB_4) {
			NotificationChain msgs = null;
			if (attachement_DB_4 != null)
				msgs = ((InternalEObject)attachement_DB_4).eInverseRemove(this, M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4, Attachement_DB_4.class, msgs);
			if (newAttachement_DB_4 != null)
				msgs = ((InternalEObject)newAttachement_DB_4).eInverseAdd(this, M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4, Attachement_DB_4.class, msgs);
			msgs = basicSetAttachement_DB_4(newAttachement_DB_4, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4, newAttachement_DB_4, newAttachement_DB_4));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4:
				if (attachement_DB_4 != null)
					msgs = ((InternalEObject)attachement_DB_4).eInverseRemove(this, M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4, Attachement_DB_4.class, msgs);
				return basicSetAttachement_DB_4((Attachement_DB_4)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4:
				return basicSetAttachement_DB_4(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4:
				if (resolve) return getAttachement_DB_4();
				return basicGetAttachement_DB_4();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4:
				setAttachement_DB_4((Attachement_DB_4)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4:
				setAttachement_DB_4((Attachement_DB_4)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4:
				return attachement_DB_4 != null;
		}
		return super.eIsSet(featureID);
	}

} //Role_Requie_DB_4Impl
