/**
 */
package M1.impl;

import M1.AttachementRPCClient;
import M1.M1Package;
import M1.ReceveResponse;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Receve Response</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.ReceveResponseImpl#getAttachementRC <em>Attachement RC</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReceveResponseImpl extends EObjectImpl implements ReceveResponse {
	/**
	 * The cached value of the '{@link #getAttachementRC() <em>Attachement RC</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachementRC()
	 * @generated
	 * @ordered
	 */
	protected AttachementRPCClient attachementRC;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReceveResponseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.RECEVE_RESPONSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementRPCClient getAttachementRC() {
		if (attachementRC != null && attachementRC.eIsProxy()) {
			InternalEObject oldAttachementRC = (InternalEObject)attachementRC;
			attachementRC = (AttachementRPCClient)eResolveProxy(oldAttachementRC);
			if (attachementRC != oldAttachementRC) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC, oldAttachementRC, attachementRC));
			}
		}
		return attachementRC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementRPCClient basicGetAttachementRC() {
		return attachementRC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachementRC(AttachementRPCClient newAttachementRC, NotificationChain msgs) {
		AttachementRPCClient oldAttachementRC = attachementRC;
		attachementRC = newAttachementRC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC, oldAttachementRC, newAttachementRC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachementRC(AttachementRPCClient newAttachementRC) {
		if (newAttachementRC != attachementRC) {
			NotificationChain msgs = null;
			if (attachementRC != null)
				msgs = ((InternalEObject)attachementRC).eInverseRemove(this, M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE, AttachementRPCClient.class, msgs);
			if (newAttachementRC != null)
				msgs = ((InternalEObject)newAttachementRC).eInverseAdd(this, M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE, AttachementRPCClient.class, msgs);
			msgs = basicSetAttachementRC(newAttachementRC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC, newAttachementRC, newAttachementRC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC:
				if (attachementRC != null)
					msgs = ((InternalEObject)attachementRC).eInverseRemove(this, M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE, AttachementRPCClient.class, msgs);
				return basicSetAttachementRC((AttachementRPCClient)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC:
				return basicSetAttachementRC(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC:
				if (resolve) return getAttachementRC();
				return basicGetAttachementRC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC:
				setAttachementRC((AttachementRPCClient)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC:
				setAttachementRC((AttachementRPCClient)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC:
				return attachementRC != null;
		}
		return super.eIsSet(featureID);
	}

} //ReceveResponseImpl
