/**
 */
package M1.impl;

import M1.Connecteur_GC_GS;
import M1.IConnecteur_GC_GS;
import M1.M1Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connecteur GC GS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Connecteur_GC_GSImpl#getIconnecteur_GC_GS <em>Iconnecteur GC GS</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Connecteur_GC_GSImpl extends EObjectImpl implements Connecteur_GC_GS {
	/**
	 * The cached value of the '{@link #getIconnecteur_GC_GS() <em>Iconnecteur GC GS</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIconnecteur_GC_GS()
	 * @generated
	 * @ordered
	 */
	protected EList iconnecteur_GC_GS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Connecteur_GC_GSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.CONNECTEUR_GC_GS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getIconnecteur_GC_GS() {
		if (iconnecteur_GC_GS == null) {
			iconnecteur_GC_GS = new EObjectContainmentEList(IConnecteur_GC_GS.class, this, M1Package.CONNECTEUR_GC_GS__ICONNECTEUR_GC_GS);
		}
		return iconnecteur_GC_GS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GC_GS__ICONNECTEUR_GC_GS:
				return ((InternalEList)getIconnecteur_GC_GS()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GC_GS__ICONNECTEUR_GC_GS:
				return getIconnecteur_GC_GS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GC_GS__ICONNECTEUR_GC_GS:
				getIconnecteur_GC_GS().clear();
				getIconnecteur_GC_GS().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GC_GS__ICONNECTEUR_GC_GS:
				getIconnecteur_GC_GS().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GC_GS__ICONNECTEUR_GC_GS:
				return iconnecteur_GC_GS != null && !iconnecteur_GC_GS.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //Connecteur_GC_GSImpl
