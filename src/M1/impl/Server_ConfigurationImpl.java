/**
 */
package M1.impl;

import M1.Connecteur_GC_DB;
import M1.Connecteur_GC_GS;
import M1.Connecteur_GS_DB;
import M1.DataBase;
import M1.Gestion_Securiterity;
import M1.Gestionnaire_Connexion;
import M1.M1Package;
import M1.Server_Configuration;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Server Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Server_ConfigurationImpl#getEReference0 <em>EReference0</em>}</li>
 *   <li>{@link M1.impl.Server_ConfigurationImpl#getDataBase <em>Data Base</em>}</li>
 *   <li>{@link M1.impl.Server_ConfigurationImpl#getGestionnaire_Connexion <em>Gestionnaire Connexion</em>}</li>
 *   <li>{@link M1.impl.Server_ConfigurationImpl#getConnecteur_GC_DB <em>Connecteur GC DB</em>}</li>
 *   <li>{@link M1.impl.Server_ConfigurationImpl#getGestion_securite <em>Gestion securite</em>}</li>
 *   <li>{@link M1.impl.Server_ConfigurationImpl#getConnecteur_GC_GS <em>Connecteur GC GS</em>}</li>
 *   <li>{@link M1.impl.Server_ConfigurationImpl#getConnecteur_GS_DB <em>Connecteur GS DB</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Server_ConfigurationImpl extends EObjectImpl implements Server_Configuration {
	/**
	 * The cached value of the '{@link #getEReference0() <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEReference0()
	 * @generated
	 * @ordered
	 */
	protected DataBase eReference0;

	/**
	 * The cached value of the '{@link #getDataBase() <em>Data Base</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataBase()
	 * @generated
	 * @ordered
	 */
	protected EList dataBase;

	/**
	 * The cached value of the '{@link #getGestionnaire_Connexion() <em>Gestionnaire Connexion</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGestionnaire_Connexion()
	 * @generated
	 * @ordered
	 */
	protected EList gestionnaire_Connexion;

	/**
	 * The cached value of the '{@link #getConnecteur_GC_DB() <em>Connecteur GC DB</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnecteur_GC_DB()
	 * @generated
	 * @ordered
	 */
	protected EList connecteur_GC_DB;

	/**
	 * The cached value of the '{@link #getGestion_securite() <em>Gestion securite</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGestion_securite()
	 * @generated
	 * @ordered
	 */
	protected EList gestion_securite;

	/**
	 * The cached value of the '{@link #getConnecteur_GC_GS() <em>Connecteur GC GS</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnecteur_GC_GS()
	 * @generated
	 * @ordered
	 */
	protected EList connecteur_GC_GS;

	/**
	 * The cached value of the '{@link #getConnecteur_GS_DB() <em>Connecteur GS DB</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnecteur_GS_DB()
	 * @generated
	 * @ordered
	 */
	protected EList connecteur_GS_DB;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Server_ConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.SERVER_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataBase getEReference0() {
		if (eReference0 != null && eReference0.eIsProxy()) {
			InternalEObject oldEReference0 = (InternalEObject)eReference0;
			eReference0 = (DataBase)eResolveProxy(oldEReference0);
			if (eReference0 != oldEReference0) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.SERVER_CONFIGURATION__EREFERENCE0, oldEReference0, eReference0));
			}
		}
		return eReference0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataBase basicGetEReference0() {
		return eReference0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEReference0(DataBase newEReference0) {
		DataBase oldEReference0 = eReference0;
		eReference0 = newEReference0;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.SERVER_CONFIGURATION__EREFERENCE0, oldEReference0, eReference0));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getDataBase() {
		if (dataBase == null) {
			dataBase = new EObjectContainmentEList(DataBase.class, this, M1Package.SERVER_CONFIGURATION__DATA_BASE);
		}
		return dataBase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getGestionnaire_Connexion() {
		if (gestionnaire_Connexion == null) {
			gestionnaire_Connexion = new EObjectContainmentEList(Gestionnaire_Connexion.class, this, M1Package.SERVER_CONFIGURATION__GESTIONNAIRE_CONNEXION);
		}
		return gestionnaire_Connexion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getConnecteur_GC_DB() {
		if (connecteur_GC_DB == null) {
			connecteur_GC_DB = new EObjectContainmentEList(Connecteur_GC_DB.class, this, M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_DB);
		}
		return connecteur_GC_DB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getGestion_securite() {
		if (gestion_securite == null) {
			gestion_securite = new EObjectContainmentEList(Gestion_Securiterity.class, this, M1Package.SERVER_CONFIGURATION__GESTION_SECURITE);
		}
		return gestion_securite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getConnecteur_GC_GS() {
		if (connecteur_GC_GS == null) {
			connecteur_GC_GS = new EObjectContainmentEList(Connecteur_GC_GS.class, this, M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_GS);
		}
		return connecteur_GC_GS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getConnecteur_GS_DB() {
		if (connecteur_GS_DB == null) {
			connecteur_GS_DB = new EObjectContainmentEList(Connecteur_GS_DB.class, this, M1Package.SERVER_CONFIGURATION__CONNECTEUR_GS_DB);
		}
		return connecteur_GS_DB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.SERVER_CONFIGURATION__DATA_BASE:
				return ((InternalEList)getDataBase()).basicRemove(otherEnd, msgs);
			case M1Package.SERVER_CONFIGURATION__GESTIONNAIRE_CONNEXION:
				return ((InternalEList)getGestionnaire_Connexion()).basicRemove(otherEnd, msgs);
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_DB:
				return ((InternalEList)getConnecteur_GC_DB()).basicRemove(otherEnd, msgs);
			case M1Package.SERVER_CONFIGURATION__GESTION_SECURITE:
				return ((InternalEList)getGestion_securite()).basicRemove(otherEnd, msgs);
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_GS:
				return ((InternalEList)getConnecteur_GC_GS()).basicRemove(otherEnd, msgs);
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GS_DB:
				return ((InternalEList)getConnecteur_GS_DB()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.SERVER_CONFIGURATION__EREFERENCE0:
				if (resolve) return getEReference0();
				return basicGetEReference0();
			case M1Package.SERVER_CONFIGURATION__DATA_BASE:
				return getDataBase();
			case M1Package.SERVER_CONFIGURATION__GESTIONNAIRE_CONNEXION:
				return getGestionnaire_Connexion();
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_DB:
				return getConnecteur_GC_DB();
			case M1Package.SERVER_CONFIGURATION__GESTION_SECURITE:
				return getGestion_securite();
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_GS:
				return getConnecteur_GC_GS();
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GS_DB:
				return getConnecteur_GS_DB();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.SERVER_CONFIGURATION__EREFERENCE0:
				setEReference0((DataBase)newValue);
				return;
			case M1Package.SERVER_CONFIGURATION__DATA_BASE:
				getDataBase().clear();
				getDataBase().addAll((Collection)newValue);
				return;
			case M1Package.SERVER_CONFIGURATION__GESTIONNAIRE_CONNEXION:
				getGestionnaire_Connexion().clear();
				getGestionnaire_Connexion().addAll((Collection)newValue);
				return;
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_DB:
				getConnecteur_GC_DB().clear();
				getConnecteur_GC_DB().addAll((Collection)newValue);
				return;
			case M1Package.SERVER_CONFIGURATION__GESTION_SECURITE:
				getGestion_securite().clear();
				getGestion_securite().addAll((Collection)newValue);
				return;
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_GS:
				getConnecteur_GC_GS().clear();
				getConnecteur_GC_GS().addAll((Collection)newValue);
				return;
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GS_DB:
				getConnecteur_GS_DB().clear();
				getConnecteur_GS_DB().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.SERVER_CONFIGURATION__EREFERENCE0:
				setEReference0((DataBase)null);
				return;
			case M1Package.SERVER_CONFIGURATION__DATA_BASE:
				getDataBase().clear();
				return;
			case M1Package.SERVER_CONFIGURATION__GESTIONNAIRE_CONNEXION:
				getGestionnaire_Connexion().clear();
				return;
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_DB:
				getConnecteur_GC_DB().clear();
				return;
			case M1Package.SERVER_CONFIGURATION__GESTION_SECURITE:
				getGestion_securite().clear();
				return;
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_GS:
				getConnecteur_GC_GS().clear();
				return;
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GS_DB:
				getConnecteur_GS_DB().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.SERVER_CONFIGURATION__EREFERENCE0:
				return eReference0 != null;
			case M1Package.SERVER_CONFIGURATION__DATA_BASE:
				return dataBase != null && !dataBase.isEmpty();
			case M1Package.SERVER_CONFIGURATION__GESTIONNAIRE_CONNEXION:
				return gestionnaire_Connexion != null && !gestionnaire_Connexion.isEmpty();
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_DB:
				return connecteur_GC_DB != null && !connecteur_GC_DB.isEmpty();
			case M1Package.SERVER_CONFIGURATION__GESTION_SECURITE:
				return gestion_securite != null && !gestion_securite.isEmpty();
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GC_GS:
				return connecteur_GC_GS != null && !connecteur_GC_GS.isEmpty();
			case M1Package.SERVER_CONFIGURATION__CONNECTEUR_GS_DB:
				return connecteur_GS_DB != null && !connecteur_GS_DB.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //Server_ConfigurationImpl
