/**
 */
package M1.impl;

import M1.Attachement_DB_2;
import M1.M1Package;
import M1.Role_Requie_DB_2;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Role Requie DB 2</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Role_Requie_DB_2Impl#getAttachement_DB_2 <em>Attachement DB 2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Role_Requie_DB_2Impl extends EObjectImpl implements Role_Requie_DB_2 {
	/**
	 * The cached value of the '{@link #getAttachement_DB_2() <em>Attachement DB 2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachement_DB_2()
	 * @generated
	 * @ordered
	 */
	protected Attachement_DB_2 attachement_DB_2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Role_Requie_DB_2Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ROLE_REQUIE_DB_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_DB_2 getAttachement_DB_2() {
		if (attachement_DB_2 != null && attachement_DB_2.eIsProxy()) {
			InternalEObject oldAttachement_DB_2 = (InternalEObject)attachement_DB_2;
			attachement_DB_2 = (Attachement_DB_2)eResolveProxy(oldAttachement_DB_2);
			if (attachement_DB_2 != oldAttachement_DB_2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2, oldAttachement_DB_2, attachement_DB_2));
			}
		}
		return attachement_DB_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_DB_2 basicGetAttachement_DB_2() {
		return attachement_DB_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachement_DB_2(Attachement_DB_2 newAttachement_DB_2, NotificationChain msgs) {
		Attachement_DB_2 oldAttachement_DB_2 = attachement_DB_2;
		attachement_DB_2 = newAttachement_DB_2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2, oldAttachement_DB_2, newAttachement_DB_2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachement_DB_2(Attachement_DB_2 newAttachement_DB_2) {
		if (newAttachement_DB_2 != attachement_DB_2) {
			NotificationChain msgs = null;
			if (attachement_DB_2 != null)
				msgs = ((InternalEObject)attachement_DB_2).eInverseRemove(this, M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2, Attachement_DB_2.class, msgs);
			if (newAttachement_DB_2 != null)
				msgs = ((InternalEObject)newAttachement_DB_2).eInverseAdd(this, M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2, Attachement_DB_2.class, msgs);
			msgs = basicSetAttachement_DB_2(newAttachement_DB_2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2, newAttachement_DB_2, newAttachement_DB_2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2:
				if (attachement_DB_2 != null)
					msgs = ((InternalEObject)attachement_DB_2).eInverseRemove(this, M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2, Attachement_DB_2.class, msgs);
				return basicSetAttachement_DB_2((Attachement_DB_2)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2:
				return basicSetAttachement_DB_2(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2:
				if (resolve) return getAttachement_DB_2();
				return basicGetAttachement_DB_2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2:
				setAttachement_DB_2((Attachement_DB_2)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2:
				setAttachement_DB_2((Attachement_DB_2)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2:
				return attachement_DB_2 != null;
		}
		return super.eIsSet(featureID);
	}

} //Role_Requie_DB_2Impl
