/**
 */
package M1.impl;

import M1.IClient;
import M1.M1Package;
import M1.ReceveResponse;
import M1.SendRequest;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IClient</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.IClientImpl#getSendRequest <em>Send Request</em>}</li>
 *   <li>{@link M1.impl.IClientImpl#getReceveRequest <em>Receve Request</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IClientImpl extends EObjectImpl implements IClient {
	/**
	 * The cached value of the '{@link #getSendRequest() <em>Send Request</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSendRequest()
	 * @generated
	 * @ordered
	 */
	protected EList sendRequest;

	/**
	 * The cached value of the '{@link #getReceveRequest() <em>Receve Request</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceveRequest()
	 * @generated
	 * @ordered
	 */
	protected EList receveRequest;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IClientImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ICLIENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getSendRequest() {
		if (sendRequest == null) {
			sendRequest = new EObjectContainmentEList(SendRequest.class, this, M1Package.ICLIENT__SEND_REQUEST);
		}
		return sendRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getReceveRequest() {
		if (receveRequest == null) {
			receveRequest = new EObjectContainmentEList(ReceveResponse.class, this, M1Package.ICLIENT__RECEVE_REQUEST);
		}
		return receveRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ICLIENT__SEND_REQUEST:
				return ((InternalEList)getSendRequest()).basicRemove(otherEnd, msgs);
			case M1Package.ICLIENT__RECEVE_REQUEST:
				return ((InternalEList)getReceveRequest()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ICLIENT__SEND_REQUEST:
				return getSendRequest();
			case M1Package.ICLIENT__RECEVE_REQUEST:
				return getReceveRequest();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ICLIENT__SEND_REQUEST:
				getSendRequest().clear();
				getSendRequest().addAll((Collection)newValue);
				return;
			case M1Package.ICLIENT__RECEVE_REQUEST:
				getReceveRequest().clear();
				getReceveRequest().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ICLIENT__SEND_REQUEST:
				getSendRequest().clear();
				return;
			case M1Package.ICLIENT__RECEVE_REQUEST:
				getReceveRequest().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ICLIENT__SEND_REQUEST:
				return sendRequest != null && !sendRequest.isEmpty();
			case M1Package.ICLIENT__RECEVE_REQUEST:
				return receveRequest != null && !receveRequest.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //IClientImpl
