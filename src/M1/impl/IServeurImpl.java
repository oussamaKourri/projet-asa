/**
 */
package M1.impl;

import M1.IServeur;
import M1.M1Package;
import M1.PortFournie_RS;
import M1.PortRequie_SR;
import M1.Port_S_CM;
import M1.Port_S_RPC;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IServeur</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.IServeurImpl#getPortFournie_RS <em>Port Fournie RS</em>}</li>
 *   <li>{@link M1.impl.IServeurImpl#getPortRequie_SR <em>Port Requie SR</em>}</li>
 *   <li>{@link M1.impl.IServeurImpl#getPert_S_CM <em>Pert SCM</em>}</li>
 *   <li>{@link M1.impl.IServeurImpl#getPort_S_RPC <em>Port SRPC</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IServeurImpl extends EObjectImpl implements IServeur {
	/**
	 * The cached value of the '{@link #getPortFournie_RS() <em>Port Fournie RS</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortFournie_RS()
	 * @generated
	 * @ordered
	 */
	protected EList portFournie_RS;

	/**
	 * The cached value of the '{@link #getPortRequie_SR() <em>Port Requie SR</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortRequie_SR()
	 * @generated
	 * @ordered
	 */
	protected EList portRequie_SR;

	/**
	 * The cached value of the '{@link #getPert_S_CM() <em>Pert SCM</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPert_S_CM()
	 * @generated
	 * @ordered
	 */
	protected EList pert_S_CM;

	/**
	 * The cached value of the '{@link #getPort_S_RPC() <em>Port SRPC</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_S_RPC()
	 * @generated
	 * @ordered
	 */
	protected EList port_S_RPC;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IServeurImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ISERVEUR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPortFournie_RS() {
		if (portFournie_RS == null) {
			portFournie_RS = new EObjectContainmentEList(PortFournie_RS.class, this, M1Package.ISERVEUR__PORT_FOURNIE_RS);
		}
		return portFournie_RS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPortRequie_SR() {
		if (portRequie_SR == null) {
			portRequie_SR = new EObjectContainmentEList(PortRequie_SR.class, this, M1Package.ISERVEUR__PORT_REQUIE_SR);
		}
		return portRequie_SR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPert_S_CM() {
		if (pert_S_CM == null) {
			pert_S_CM = new EObjectContainmentEList(Port_S_CM.class, this, M1Package.ISERVEUR__PERT_SCM);
		}
		return pert_S_CM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPort_S_RPC() {
		if (port_S_RPC == null) {
			port_S_RPC = new EObjectContainmentEList(Port_S_RPC.class, this, M1Package.ISERVEUR__PORT_SRPC);
		}
		return port_S_RPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ISERVEUR__PORT_FOURNIE_RS:
				return ((InternalEList)getPortFournie_RS()).basicRemove(otherEnd, msgs);
			case M1Package.ISERVEUR__PORT_REQUIE_SR:
				return ((InternalEList)getPortRequie_SR()).basicRemove(otherEnd, msgs);
			case M1Package.ISERVEUR__PERT_SCM:
				return ((InternalEList)getPert_S_CM()).basicRemove(otherEnd, msgs);
			case M1Package.ISERVEUR__PORT_SRPC:
				return ((InternalEList)getPort_S_RPC()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ISERVEUR__PORT_FOURNIE_RS:
				return getPortFournie_RS();
			case M1Package.ISERVEUR__PORT_REQUIE_SR:
				return getPortRequie_SR();
			case M1Package.ISERVEUR__PERT_SCM:
				return getPert_S_CM();
			case M1Package.ISERVEUR__PORT_SRPC:
				return getPort_S_RPC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ISERVEUR__PORT_FOURNIE_RS:
				getPortFournie_RS().clear();
				getPortFournie_RS().addAll((Collection)newValue);
				return;
			case M1Package.ISERVEUR__PORT_REQUIE_SR:
				getPortRequie_SR().clear();
				getPortRequie_SR().addAll((Collection)newValue);
				return;
			case M1Package.ISERVEUR__PERT_SCM:
				getPert_S_CM().clear();
				getPert_S_CM().addAll((Collection)newValue);
				return;
			case M1Package.ISERVEUR__PORT_SRPC:
				getPort_S_RPC().clear();
				getPort_S_RPC().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ISERVEUR__PORT_FOURNIE_RS:
				getPortFournie_RS().clear();
				return;
			case M1Package.ISERVEUR__PORT_REQUIE_SR:
				getPortRequie_SR().clear();
				return;
			case M1Package.ISERVEUR__PERT_SCM:
				getPert_S_CM().clear();
				return;
			case M1Package.ISERVEUR__PORT_SRPC:
				getPort_S_RPC().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ISERVEUR__PORT_FOURNIE_RS:
				return portFournie_RS != null && !portFournie_RS.isEmpty();
			case M1Package.ISERVEUR__PORT_REQUIE_SR:
				return portRequie_SR != null && !portRequie_SR.isEmpty();
			case M1Package.ISERVEUR__PERT_SCM:
				return pert_S_CM != null && !pert_S_CM.isEmpty();
			case M1Package.ISERVEUR__PORT_SRPC:
				return port_S_RPC != null && !port_S_RPC.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //IServeurImpl
