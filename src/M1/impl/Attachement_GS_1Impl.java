/**
 */
package M1.impl;

import M1.Attachement_GS_1;
import M1.M1Package;
import M1.Port_Fournie_GS_1;
import M1.Role_Requie_GS_1;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attachement GS 1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Attachement_GS_1Impl#getPort_fournie_GS_1 <em>Port fournie GS 1</em>}</li>
 *   <li>{@link M1.impl.Attachement_GS_1Impl#getRole_requie_GS_1 <em>Role requie GS 1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Attachement_GS_1Impl extends EObjectImpl implements Attachement_GS_1 {
	/**
	 * The cached value of the '{@link #getPort_fournie_GS_1() <em>Port fournie GS 1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_fournie_GS_1()
	 * @generated
	 * @ordered
	 */
	protected Port_Fournie_GS_1 port_fournie_GS_1;

	/**
	 * The cached value of the '{@link #getRole_requie_GS_1() <em>Role requie GS 1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_requie_GS_1()
	 * @generated
	 * @ordered
	 */
	protected Role_Requie_GS_1 role_requie_GS_1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Attachement_GS_1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ATTACHEMENT_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Fournie_GS_1 getPort_fournie_GS_1() {
		if (port_fournie_GS_1 != null && port_fournie_GS_1.eIsProxy()) {
			InternalEObject oldPort_fournie_GS_1 = (InternalEObject)port_fournie_GS_1;
			port_fournie_GS_1 = (Port_Fournie_GS_1)eResolveProxy(oldPort_fournie_GS_1);
			if (port_fournie_GS_1 != oldPort_fournie_GS_1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1, oldPort_fournie_GS_1, port_fournie_GS_1));
			}
		}
		return port_fournie_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Fournie_GS_1 basicGetPort_fournie_GS_1() {
		return port_fournie_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_fournie_GS_1(Port_Fournie_GS_1 newPort_fournie_GS_1, NotificationChain msgs) {
		Port_Fournie_GS_1 oldPort_fournie_GS_1 = port_fournie_GS_1;
		port_fournie_GS_1 = newPort_fournie_GS_1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1, oldPort_fournie_GS_1, newPort_fournie_GS_1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_fournie_GS_1(Port_Fournie_GS_1 newPort_fournie_GS_1) {
		if (newPort_fournie_GS_1 != port_fournie_GS_1) {
			NotificationChain msgs = null;
			if (port_fournie_GS_1 != null)
				msgs = ((InternalEObject)port_fournie_GS_1).eInverseRemove(this, M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1, Port_Fournie_GS_1.class, msgs);
			if (newPort_fournie_GS_1 != null)
				msgs = ((InternalEObject)newPort_fournie_GS_1).eInverseAdd(this, M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1, Port_Fournie_GS_1.class, msgs);
			msgs = basicSetPort_fournie_GS_1(newPort_fournie_GS_1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1, newPort_fournie_GS_1, newPort_fournie_GS_1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_GS_1 getRole_requie_GS_1() {
		if (role_requie_GS_1 != null && role_requie_GS_1.eIsProxy()) {
			InternalEObject oldRole_requie_GS_1 = (InternalEObject)role_requie_GS_1;
			role_requie_GS_1 = (Role_Requie_GS_1)eResolveProxy(oldRole_requie_GS_1);
			if (role_requie_GS_1 != oldRole_requie_GS_1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1, oldRole_requie_GS_1, role_requie_GS_1));
			}
		}
		return role_requie_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_GS_1 basicGetRole_requie_GS_1() {
		return role_requie_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRole_requie_GS_1(Role_Requie_GS_1 newRole_requie_GS_1, NotificationChain msgs) {
		Role_Requie_GS_1 oldRole_requie_GS_1 = role_requie_GS_1;
		role_requie_GS_1 = newRole_requie_GS_1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1, oldRole_requie_GS_1, newRole_requie_GS_1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole_requie_GS_1(Role_Requie_GS_1 newRole_requie_GS_1) {
		if (newRole_requie_GS_1 != role_requie_GS_1) {
			NotificationChain msgs = null;
			if (role_requie_GS_1 != null)
				msgs = ((InternalEObject)role_requie_GS_1).eInverseRemove(this, M1Package.ROLE_REQUIE_GS_1__ATTACHEMENT_GS_1, Role_Requie_GS_1.class, msgs);
			if (newRole_requie_GS_1 != null)
				msgs = ((InternalEObject)newRole_requie_GS_1).eInverseAdd(this, M1Package.ROLE_REQUIE_GS_1__ATTACHEMENT_GS_1, Role_Requie_GS_1.class, msgs);
			msgs = basicSetRole_requie_GS_1(newRole_requie_GS_1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1, newRole_requie_GS_1, newRole_requie_GS_1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1:
				if (port_fournie_GS_1 != null)
					msgs = ((InternalEObject)port_fournie_GS_1).eInverseRemove(this, M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1, Port_Fournie_GS_1.class, msgs);
				return basicSetPort_fournie_GS_1((Port_Fournie_GS_1)otherEnd, msgs);
			case M1Package.ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1:
				if (role_requie_GS_1 != null)
					msgs = ((InternalEObject)role_requie_GS_1).eInverseRemove(this, M1Package.ROLE_REQUIE_GS_1__ATTACHEMENT_GS_1, Role_Requie_GS_1.class, msgs);
				return basicSetRole_requie_GS_1((Role_Requie_GS_1)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1:
				return basicSetPort_fournie_GS_1(null, msgs);
			case M1Package.ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1:
				return basicSetRole_requie_GS_1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1:
				if (resolve) return getPort_fournie_GS_1();
				return basicGetPort_fournie_GS_1();
			case M1Package.ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1:
				if (resolve) return getRole_requie_GS_1();
				return basicGetRole_requie_GS_1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1:
				setPort_fournie_GS_1((Port_Fournie_GS_1)newValue);
				return;
			case M1Package.ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1:
				setRole_requie_GS_1((Role_Requie_GS_1)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1:
				setPort_fournie_GS_1((Port_Fournie_GS_1)null);
				return;
			case M1Package.ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1:
				setRole_requie_GS_1((Role_Requie_GS_1)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1:
				return port_fournie_GS_1 != null;
			case M1Package.ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1:
				return role_requie_GS_1 != null;
		}
		return super.eIsSet(featureID);
	}

} //Attachement_GS_1Impl
