/**
 */
package M1.impl;

import M1.Attachement_GS_1;
import M1.M1Package;
import M1.Port_Fournie_GS_1;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Fournie GS 1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Port_Fournie_GS_1Impl#getAttachement_GS_1 <em>Attachement GS 1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Port_Fournie_GS_1Impl extends EObjectImpl implements Port_Fournie_GS_1 {
	/**
	 * The cached value of the '{@link #getAttachement_GS_1() <em>Attachement GS 1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachement_GS_1()
	 * @generated
	 * @ordered
	 */
	protected Attachement_GS_1 attachement_GS_1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Port_Fournie_GS_1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.PORT_FOURNIE_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_GS_1 getAttachement_GS_1() {
		if (attachement_GS_1 != null && attachement_GS_1.eIsProxy()) {
			InternalEObject oldAttachement_GS_1 = (InternalEObject)attachement_GS_1;
			attachement_GS_1 = (Attachement_GS_1)eResolveProxy(oldAttachement_GS_1);
			if (attachement_GS_1 != oldAttachement_GS_1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1, oldAttachement_GS_1, attachement_GS_1));
			}
		}
		return attachement_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_GS_1 basicGetAttachement_GS_1() {
		return attachement_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachement_GS_1(Attachement_GS_1 newAttachement_GS_1, NotificationChain msgs) {
		Attachement_GS_1 oldAttachement_GS_1 = attachement_GS_1;
		attachement_GS_1 = newAttachement_GS_1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1, oldAttachement_GS_1, newAttachement_GS_1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachement_GS_1(Attachement_GS_1 newAttachement_GS_1) {
		if (newAttachement_GS_1 != attachement_GS_1) {
			NotificationChain msgs = null;
			if (attachement_GS_1 != null)
				msgs = ((InternalEObject)attachement_GS_1).eInverseRemove(this, M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1, Attachement_GS_1.class, msgs);
			if (newAttachement_GS_1 != null)
				msgs = ((InternalEObject)newAttachement_GS_1).eInverseAdd(this, M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1, Attachement_GS_1.class, msgs);
			msgs = basicSetAttachement_GS_1(newAttachement_GS_1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1, newAttachement_GS_1, newAttachement_GS_1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1:
				if (attachement_GS_1 != null)
					msgs = ((InternalEObject)attachement_GS_1).eInverseRemove(this, M1Package.ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1, Attachement_GS_1.class, msgs);
				return basicSetAttachement_GS_1((Attachement_GS_1)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1:
				return basicSetAttachement_GS_1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1:
				if (resolve) return getAttachement_GS_1();
				return basicGetAttachement_GS_1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1:
				setAttachement_GS_1((Attachement_GS_1)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1:
				setAttachement_GS_1((Attachement_GS_1)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1:
				return attachement_GS_1 != null;
		}
		return super.eIsSet(featureID);
	}

} //Port_Fournie_GS_1Impl
