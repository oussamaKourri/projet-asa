/**
 */
package M1.impl;

import M1.AttachementClientRPC;
import M1.M1Package;
import M1.RoleRequie_CR;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Role Requie CR</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.RoleRequie_CRImpl#getAttachement_CR <em>Attachement CR</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RoleRequie_CRImpl extends EObjectImpl implements RoleRequie_CR {
	/**
	 * The cached value of the '{@link #getAttachement_CR() <em>Attachement CR</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachement_CR()
	 * @generated
	 * @ordered
	 */
	protected AttachementClientRPC attachement_CR;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleRequie_CRImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ROLE_REQUIE_CR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementClientRPC getAttachement_CR() {
		if (attachement_CR != null && attachement_CR.eIsProxy()) {
			InternalEObject oldAttachement_CR = (InternalEObject)attachement_CR;
			attachement_CR = (AttachementClientRPC)eResolveProxy(oldAttachement_CR);
			if (attachement_CR != oldAttachement_CR) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ROLE_REQUIE_CR__ATTACHEMENT_CR, oldAttachement_CR, attachement_CR));
			}
		}
		return attachement_CR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementClientRPC basicGetAttachement_CR() {
		return attachement_CR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachement_CR(AttachementClientRPC newAttachement_CR) {
		AttachementClientRPC oldAttachement_CR = attachement_CR;
		attachement_CR = newAttachement_CR;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ROLE_REQUIE_CR__ATTACHEMENT_CR, oldAttachement_CR, attachement_CR));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_CR__ATTACHEMENT_CR:
				if (resolve) return getAttachement_CR();
				return basicGetAttachement_CR();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_CR__ATTACHEMENT_CR:
				setAttachement_CR((AttachementClientRPC)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_CR__ATTACHEMENT_CR:
				setAttachement_CR((AttachementClientRPC)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_CR__ATTACHEMENT_CR:
				return attachement_CR != null;
		}
		return super.eIsSet(featureID);
	}

} //RoleRequie_CRImpl
