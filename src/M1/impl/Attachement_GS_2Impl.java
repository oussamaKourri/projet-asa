/**
 */
package M1.impl;

import M1.Attachement_GS_2;
import M1.M1Package;
import M1.Port_Requie_GS_2;
import M1.Role_Fournie_GS_2;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attachement GS 2</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Attachement_GS_2Impl#getPort_requie_GS_2 <em>Port requie GS 2</em>}</li>
 *   <li>{@link M1.impl.Attachement_GS_2Impl#getRole_fournie_GS_2 <em>Role fournie GS 2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Attachement_GS_2Impl extends EObjectImpl implements Attachement_GS_2 {
	/**
	 * The cached value of the '{@link #getPort_requie_GS_2() <em>Port requie GS 2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_requie_GS_2()
	 * @generated
	 * @ordered
	 */
	protected Port_Requie_GS_2 port_requie_GS_2;

	/**
	 * The cached value of the '{@link #getRole_fournie_GS_2() <em>Role fournie GS 2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_fournie_GS_2()
	 * @generated
	 * @ordered
	 */
	protected Role_Fournie_GS_2 role_fournie_GS_2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Attachement_GS_2Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ATTACHEMENT_GS_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_GS_2 getPort_requie_GS_2() {
		if (port_requie_GS_2 != null && port_requie_GS_2.eIsProxy()) {
			InternalEObject oldPort_requie_GS_2 = (InternalEObject)port_requie_GS_2;
			port_requie_GS_2 = (Port_Requie_GS_2)eResolveProxy(oldPort_requie_GS_2);
			if (port_requie_GS_2 != oldPort_requie_GS_2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_GS_2__PORT_REQUIE_GS_2, oldPort_requie_GS_2, port_requie_GS_2));
			}
		}
		return port_requie_GS_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_GS_2 basicGetPort_requie_GS_2() {
		return port_requie_GS_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_requie_GS_2(Port_Requie_GS_2 newPort_requie_GS_2, NotificationChain msgs) {
		Port_Requie_GS_2 oldPort_requie_GS_2 = port_requie_GS_2;
		port_requie_GS_2 = newPort_requie_GS_2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_2__PORT_REQUIE_GS_2, oldPort_requie_GS_2, newPort_requie_GS_2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_requie_GS_2(Port_Requie_GS_2 newPort_requie_GS_2) {
		if (newPort_requie_GS_2 != port_requie_GS_2) {
			NotificationChain msgs = null;
			if (port_requie_GS_2 != null)
				msgs = ((InternalEObject)port_requie_GS_2).eInverseRemove(this, M1Package.PORT_REQUIE_GS_2__ATTACHEMENT_GS_2, Port_Requie_GS_2.class, msgs);
			if (newPort_requie_GS_2 != null)
				msgs = ((InternalEObject)newPort_requie_GS_2).eInverseAdd(this, M1Package.PORT_REQUIE_GS_2__ATTACHEMENT_GS_2, Port_Requie_GS_2.class, msgs);
			msgs = basicSetPort_requie_GS_2(newPort_requie_GS_2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_2__PORT_REQUIE_GS_2, newPort_requie_GS_2, newPort_requie_GS_2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Fournie_GS_2 getRole_fournie_GS_2() {
		if (role_fournie_GS_2 != null && role_fournie_GS_2.eIsProxy()) {
			InternalEObject oldRole_fournie_GS_2 = (InternalEObject)role_fournie_GS_2;
			role_fournie_GS_2 = (Role_Fournie_GS_2)eResolveProxy(oldRole_fournie_GS_2);
			if (role_fournie_GS_2 != oldRole_fournie_GS_2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2, oldRole_fournie_GS_2, role_fournie_GS_2));
			}
		}
		return role_fournie_GS_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Fournie_GS_2 basicGetRole_fournie_GS_2() {
		return role_fournie_GS_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRole_fournie_GS_2(Role_Fournie_GS_2 newRole_fournie_GS_2, NotificationChain msgs) {
		Role_Fournie_GS_2 oldRole_fournie_GS_2 = role_fournie_GS_2;
		role_fournie_GS_2 = newRole_fournie_GS_2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2, oldRole_fournie_GS_2, newRole_fournie_GS_2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole_fournie_GS_2(Role_Fournie_GS_2 newRole_fournie_GS_2) {
		if (newRole_fournie_GS_2 != role_fournie_GS_2) {
			NotificationChain msgs = null;
			if (role_fournie_GS_2 != null)
				msgs = ((InternalEObject)role_fournie_GS_2).eInverseRemove(this, M1Package.ROLE_FOURNIE_GS_2__ATTACHEMENT_GS_2, Role_Fournie_GS_2.class, msgs);
			if (newRole_fournie_GS_2 != null)
				msgs = ((InternalEObject)newRole_fournie_GS_2).eInverseAdd(this, M1Package.ROLE_FOURNIE_GS_2__ATTACHEMENT_GS_2, Role_Fournie_GS_2.class, msgs);
			msgs = basicSetRole_fournie_GS_2(newRole_fournie_GS_2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2, newRole_fournie_GS_2, newRole_fournie_GS_2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_2__PORT_REQUIE_GS_2:
				if (port_requie_GS_2 != null)
					msgs = ((InternalEObject)port_requie_GS_2).eInverseRemove(this, M1Package.PORT_REQUIE_GS_2__ATTACHEMENT_GS_2, Port_Requie_GS_2.class, msgs);
				return basicSetPort_requie_GS_2((Port_Requie_GS_2)otherEnd, msgs);
			case M1Package.ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2:
				if (role_fournie_GS_2 != null)
					msgs = ((InternalEObject)role_fournie_GS_2).eInverseRemove(this, M1Package.ROLE_FOURNIE_GS_2__ATTACHEMENT_GS_2, Role_Fournie_GS_2.class, msgs);
				return basicSetRole_fournie_GS_2((Role_Fournie_GS_2)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_2__PORT_REQUIE_GS_2:
				return basicSetPort_requie_GS_2(null, msgs);
			case M1Package.ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2:
				return basicSetRole_fournie_GS_2(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_2__PORT_REQUIE_GS_2:
				if (resolve) return getPort_requie_GS_2();
				return basicGetPort_requie_GS_2();
			case M1Package.ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2:
				if (resolve) return getRole_fournie_GS_2();
				return basicGetRole_fournie_GS_2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_2__PORT_REQUIE_GS_2:
				setPort_requie_GS_2((Port_Requie_GS_2)newValue);
				return;
			case M1Package.ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2:
				setRole_fournie_GS_2((Role_Fournie_GS_2)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_2__PORT_REQUIE_GS_2:
				setPort_requie_GS_2((Port_Requie_GS_2)null);
				return;
			case M1Package.ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2:
				setRole_fournie_GS_2((Role_Fournie_GS_2)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_2__PORT_REQUIE_GS_2:
				return port_requie_GS_2 != null;
			case M1Package.ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2:
				return role_fournie_GS_2 != null;
		}
		return super.eIsSet(featureID);
	}

} //Attachement_GS_2Impl
