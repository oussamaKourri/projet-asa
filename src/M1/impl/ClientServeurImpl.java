/**
 */
package M1.impl;

import M1.Client;
import M1.ClientServeur;
import M1.ConnecteurRPC;
import M1.M1Package;
import M1.Serveur;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Client Serveur</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.ClientServeurImpl#getClient <em>Client</em>}</li>
 *   <li>{@link M1.impl.ClientServeurImpl#getConnecteurRPC <em>Connecteur RPC</em>}</li>
 *   <li>{@link M1.impl.ClientServeurImpl#getServeur <em>Serveur</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClientServeurImpl extends EObjectImpl implements ClientServeur {
	/**
	 * The cached value of the '{@link #getClient() <em>Client</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClient()
	 * @generated
	 * @ordered
	 */
	protected EList client;

	/**
	 * The cached value of the '{@link #getConnecteurRPC() <em>Connecteur RPC</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnecteurRPC()
	 * @generated
	 * @ordered
	 */
	protected EList connecteurRPC;

	/**
	 * The cached value of the '{@link #getServeur() <em>Serveur</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServeur()
	 * @generated
	 * @ordered
	 */
	protected EList serveur;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClientServeurImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.CLIENT_SERVEUR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getClient() {
		if (client == null) {
			client = new EObjectContainmentEList(Client.class, this, M1Package.CLIENT_SERVEUR__CLIENT);
		}
		return client;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getConnecteurRPC() {
		if (connecteurRPC == null) {
			connecteurRPC = new EObjectContainmentEList(ConnecteurRPC.class, this, M1Package.CLIENT_SERVEUR__CONNECTEUR_RPC);
		}
		return connecteurRPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getServeur() {
		if (serveur == null) {
			serveur = new EObjectContainmentEList(Serveur.class, this, M1Package.CLIENT_SERVEUR__SERVEUR);
		}
		return serveur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.CLIENT_SERVEUR__CLIENT:
				return ((InternalEList)getClient()).basicRemove(otherEnd, msgs);
			case M1Package.CLIENT_SERVEUR__CONNECTEUR_RPC:
				return ((InternalEList)getConnecteurRPC()).basicRemove(otherEnd, msgs);
			case M1Package.CLIENT_SERVEUR__SERVEUR:
				return ((InternalEList)getServeur()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.CLIENT_SERVEUR__CLIENT:
				return getClient();
			case M1Package.CLIENT_SERVEUR__CONNECTEUR_RPC:
				return getConnecteurRPC();
			case M1Package.CLIENT_SERVEUR__SERVEUR:
				return getServeur();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.CLIENT_SERVEUR__CLIENT:
				getClient().clear();
				getClient().addAll((Collection)newValue);
				return;
			case M1Package.CLIENT_SERVEUR__CONNECTEUR_RPC:
				getConnecteurRPC().clear();
				getConnecteurRPC().addAll((Collection)newValue);
				return;
			case M1Package.CLIENT_SERVEUR__SERVEUR:
				getServeur().clear();
				getServeur().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.CLIENT_SERVEUR__CLIENT:
				getClient().clear();
				return;
			case M1Package.CLIENT_SERVEUR__CONNECTEUR_RPC:
				getConnecteurRPC().clear();
				return;
			case M1Package.CLIENT_SERVEUR__SERVEUR:
				getServeur().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.CLIENT_SERVEUR__CLIENT:
				return client != null && !client.isEmpty();
			case M1Package.CLIENT_SERVEUR__CONNECTEUR_RPC:
				return connecteurRPC != null && !connecteurRPC.isEmpty();
			case M1Package.CLIENT_SERVEUR__SERVEUR:
				return serveur != null && !serveur.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ClientServeurImpl
