/**
 */
package M1.impl;

import M1.Connecteur_GC_DB;
import M1.IConnecteur_GC_DB;
import M1.M1Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connecteur GC DB</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Connecteur_GC_DBImpl#getIconnecteurBCDB <em>Iconnecteur BCDB</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Connecteur_GC_DBImpl extends EObjectImpl implements Connecteur_GC_DB {
	/**
	 * The cached value of the '{@link #getIconnecteurBCDB() <em>Iconnecteur BCDB</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIconnecteurBCDB()
	 * @generated
	 * @ordered
	 */
	protected EList iconnecteurBCDB;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Connecteur_GC_DBImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.CONNECTEUR_GC_DB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getIconnecteurBCDB() {
		if (iconnecteurBCDB == null) {
			iconnecteurBCDB = new EObjectContainmentEList(IConnecteur_GC_DB.class, this, M1Package.CONNECTEUR_GC_DB__ICONNECTEUR_BCDB);
		}
		return iconnecteurBCDB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GC_DB__ICONNECTEUR_BCDB:
				return ((InternalEList)getIconnecteurBCDB()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GC_DB__ICONNECTEUR_BCDB:
				return getIconnecteurBCDB();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GC_DB__ICONNECTEUR_BCDB:
				getIconnecteurBCDB().clear();
				getIconnecteurBCDB().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GC_DB__ICONNECTEUR_BCDB:
				getIconnecteurBCDB().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GC_DB__ICONNECTEUR_BCDB:
				return iconnecteurBCDB != null && !iconnecteurBCDB.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //Connecteur_GC_DBImpl
