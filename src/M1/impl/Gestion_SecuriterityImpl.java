/**
 */
package M1.impl;

import M1.Gestion_Securiterity;
import M1.IGestion_Securite;
import M1.M1Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gestion Securiterity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Gestion_SecuriterityImpl#getIgestionSecurite <em>Igestion Securite</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Gestion_SecuriterityImpl extends EObjectImpl implements Gestion_Securiterity {
	/**
	 * The cached value of the '{@link #getIgestionSecurite() <em>Igestion Securite</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIgestionSecurite()
	 * @generated
	 * @ordered
	 */
	protected EList igestionSecurite;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Gestion_SecuriterityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.GESTION_SECURITERITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getIgestionSecurite() {
		if (igestionSecurite == null) {
			igestionSecurite = new EObjectContainmentEList(IGestion_Securite.class, this, M1Package.GESTION_SECURITERITY__IGESTION_SECURITE);
		}
		return igestionSecurite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.GESTION_SECURITERITY__IGESTION_SECURITE:
				return ((InternalEList)getIgestionSecurite()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.GESTION_SECURITERITY__IGESTION_SECURITE:
				return getIgestionSecurite();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.GESTION_SECURITERITY__IGESTION_SECURITE:
				getIgestionSecurite().clear();
				getIgestionSecurite().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.GESTION_SECURITERITY__IGESTION_SECURITE:
				getIgestionSecurite().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.GESTION_SECURITERITY__IGESTION_SECURITE:
				return igestionSecurite != null && !igestionSecurite.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //Gestion_SecuriterityImpl
