/**
 */
package M1.impl;

import M1.Gestionnaire_Connexion;
import M1.IGestionnaire_Connexion;
import M1.M1Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gestionnaire Connexion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Gestionnaire_ConnexionImpl#getIgestionnaireConnexion <em>Igestionnaire Connexion</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Gestionnaire_ConnexionImpl extends EObjectImpl implements Gestionnaire_Connexion {
	/**
	 * The cached value of the '{@link #getIgestionnaireConnexion() <em>Igestionnaire Connexion</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIgestionnaireConnexion()
	 * @generated
	 * @ordered
	 */
	protected EList igestionnaireConnexion;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Gestionnaire_ConnexionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.GESTIONNAIRE_CONNEXION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getIgestionnaireConnexion() {
		if (igestionnaireConnexion == null) {
			igestionnaireConnexion = new EObjectContainmentEList(IGestionnaire_Connexion.class, this, M1Package.GESTIONNAIRE_CONNEXION__IGESTIONNAIRE_CONNEXION);
		}
		return igestionnaireConnexion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.GESTIONNAIRE_CONNEXION__IGESTIONNAIRE_CONNEXION:
				return ((InternalEList)getIgestionnaireConnexion()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.GESTIONNAIRE_CONNEXION__IGESTIONNAIRE_CONNEXION:
				return getIgestionnaireConnexion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.GESTIONNAIRE_CONNEXION__IGESTIONNAIRE_CONNEXION:
				getIgestionnaireConnexion().clear();
				getIgestionnaireConnexion().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.GESTIONNAIRE_CONNEXION__IGESTIONNAIRE_CONNEXION:
				getIgestionnaireConnexion().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.GESTIONNAIRE_CONNEXION__IGESTIONNAIRE_CONNEXION:
				return igestionnaireConnexion != null && !igestionnaireConnexion.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //Gestionnaire_ConnexionImpl
