/**
 */
package M1.impl;

import M1.Binding_CM_S;
import M1.M1Package;
import M1.Port_CM_S;
import M1.Port_S_CM;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binding CM S</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Binding_CM_SImpl#getEReference0 <em>EReference0</em>}</li>
 *   <li>{@link M1.impl.Binding_CM_SImpl#getEReference1 <em>EReference1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Binding_CM_SImpl extends EObjectImpl implements Binding_CM_S {
	/**
	 * The cached value of the '{@link #getEReference0() <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEReference0()
	 * @generated
	 * @ordered
	 */
	protected Port_S_CM eReference0;

	/**
	 * The cached value of the '{@link #getEReference1() <em>EReference1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEReference1()
	 * @generated
	 * @ordered
	 */
	protected Port_CM_S eReference1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Binding_CM_SImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.BINDING_CM_S;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_S_CM getEReference0() {
		if (eReference0 != null && eReference0.eIsProxy()) {
			InternalEObject oldEReference0 = (InternalEObject)eReference0;
			eReference0 = (Port_S_CM)eResolveProxy(oldEReference0);
			if (eReference0 != oldEReference0) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.BINDING_CM_S__EREFERENCE0, oldEReference0, eReference0));
			}
		}
		return eReference0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_S_CM basicGetEReference0() {
		return eReference0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEReference0(Port_S_CM newEReference0, NotificationChain msgs) {
		Port_S_CM oldEReference0 = eReference0;
		eReference0 = newEReference0;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.BINDING_CM_S__EREFERENCE0, oldEReference0, newEReference0);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEReference0(Port_S_CM newEReference0) {
		if (newEReference0 != eReference0) {
			NotificationChain msgs = null;
			if (eReference0 != null)
				msgs = ((InternalEObject)eReference0).eInverseRemove(this, M1Package.PORT_SCM__EREFERENCE0, Port_S_CM.class, msgs);
			if (newEReference0 != null)
				msgs = ((InternalEObject)newEReference0).eInverseAdd(this, M1Package.PORT_SCM__EREFERENCE0, Port_S_CM.class, msgs);
			msgs = basicSetEReference0(newEReference0, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.BINDING_CM_S__EREFERENCE0, newEReference0, newEReference0));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_CM_S getEReference1() {
		if (eReference1 != null && eReference1.eIsProxy()) {
			InternalEObject oldEReference1 = (InternalEObject)eReference1;
			eReference1 = (Port_CM_S)eResolveProxy(oldEReference1);
			if (eReference1 != oldEReference1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.BINDING_CM_S__EREFERENCE1, oldEReference1, eReference1));
			}
		}
		return eReference1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_CM_S basicGetEReference1() {
		return eReference1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEReference1(Port_CM_S newEReference1, NotificationChain msgs) {
		Port_CM_S oldEReference1 = eReference1;
		eReference1 = newEReference1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.BINDING_CM_S__EREFERENCE1, oldEReference1, newEReference1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEReference1(Port_CM_S newEReference1) {
		if (newEReference1 != eReference1) {
			NotificationChain msgs = null;
			if (eReference1 != null)
				msgs = ((InternalEObject)eReference1).eInverseRemove(this, M1Package.PORT_CM_S__EREFERENCE0, Port_CM_S.class, msgs);
			if (newEReference1 != null)
				msgs = ((InternalEObject)newEReference1).eInverseAdd(this, M1Package.PORT_CM_S__EREFERENCE0, Port_CM_S.class, msgs);
			msgs = basicSetEReference1(newEReference1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.BINDING_CM_S__EREFERENCE1, newEReference1, newEReference1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.BINDING_CM_S__EREFERENCE0:
				if (eReference0 != null)
					msgs = ((InternalEObject)eReference0).eInverseRemove(this, M1Package.PORT_SCM__EREFERENCE0, Port_S_CM.class, msgs);
				return basicSetEReference0((Port_S_CM)otherEnd, msgs);
			case M1Package.BINDING_CM_S__EREFERENCE1:
				if (eReference1 != null)
					msgs = ((InternalEObject)eReference1).eInverseRemove(this, M1Package.PORT_CM_S__EREFERENCE0, Port_CM_S.class, msgs);
				return basicSetEReference1((Port_CM_S)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.BINDING_CM_S__EREFERENCE0:
				return basicSetEReference0(null, msgs);
			case M1Package.BINDING_CM_S__EREFERENCE1:
				return basicSetEReference1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.BINDING_CM_S__EREFERENCE0:
				if (resolve) return getEReference0();
				return basicGetEReference0();
			case M1Package.BINDING_CM_S__EREFERENCE1:
				if (resolve) return getEReference1();
				return basicGetEReference1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.BINDING_CM_S__EREFERENCE0:
				setEReference0((Port_S_CM)newValue);
				return;
			case M1Package.BINDING_CM_S__EREFERENCE1:
				setEReference1((Port_CM_S)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.BINDING_CM_S__EREFERENCE0:
				setEReference0((Port_S_CM)null);
				return;
			case M1Package.BINDING_CM_S__EREFERENCE1:
				setEReference1((Port_CM_S)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.BINDING_CM_S__EREFERENCE0:
				return eReference0 != null;
			case M1Package.BINDING_CM_S__EREFERENCE1:
				return eReference1 != null;
		}
		return super.eIsSet(featureID);
	}

} //Binding_CM_SImpl
