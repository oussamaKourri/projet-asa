/**
 */
package M1.impl;

import M1.Attachement_DB_2;
import M1.M1Package;
import M1.Port_fournie_DB_2;
import M1.Role_Requie_DB_2;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attachement DB 2</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Attachement_DB_2Impl#getRole_requie_2 <em>Role requie 2</em>}</li>
 *   <li>{@link M1.impl.Attachement_DB_2Impl#getPort_fournie_DB_2 <em>Port fournie DB 2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Attachement_DB_2Impl extends EObjectImpl implements Attachement_DB_2 {
	/**
	 * The cached value of the '{@link #getRole_requie_2() <em>Role requie 2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_requie_2()
	 * @generated
	 * @ordered
	 */
	protected Role_Requie_DB_2 role_requie_2;

	/**
	 * The cached value of the '{@link #getPort_fournie_DB_2() <em>Port fournie DB 2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_fournie_DB_2()
	 * @generated
	 * @ordered
	 */
	protected Port_fournie_DB_2 port_fournie_DB_2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Attachement_DB_2Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ATTACHEMENT_DB_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_DB_2 getRole_requie_2() {
		if (role_requie_2 != null && role_requie_2.eIsProxy()) {
			InternalEObject oldRole_requie_2 = (InternalEObject)role_requie_2;
			role_requie_2 = (Role_Requie_DB_2)eResolveProxy(oldRole_requie_2);
			if (role_requie_2 != oldRole_requie_2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2, oldRole_requie_2, role_requie_2));
			}
		}
		return role_requie_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_DB_2 basicGetRole_requie_2() {
		return role_requie_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRole_requie_2(Role_Requie_DB_2 newRole_requie_2, NotificationChain msgs) {
		Role_Requie_DB_2 oldRole_requie_2 = role_requie_2;
		role_requie_2 = newRole_requie_2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2, oldRole_requie_2, newRole_requie_2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole_requie_2(Role_Requie_DB_2 newRole_requie_2) {
		if (newRole_requie_2 != role_requie_2) {
			NotificationChain msgs = null;
			if (role_requie_2 != null)
				msgs = ((InternalEObject)role_requie_2).eInverseRemove(this, M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2, Role_Requie_DB_2.class, msgs);
			if (newRole_requie_2 != null)
				msgs = ((InternalEObject)newRole_requie_2).eInverseAdd(this, M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2, Role_Requie_DB_2.class, msgs);
			msgs = basicSetRole_requie_2(newRole_requie_2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2, newRole_requie_2, newRole_requie_2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_fournie_DB_2 getPort_fournie_DB_2() {
		if (port_fournie_DB_2 != null && port_fournie_DB_2.eIsProxy()) {
			InternalEObject oldPort_fournie_DB_2 = (InternalEObject)port_fournie_DB_2;
			port_fournie_DB_2 = (Port_fournie_DB_2)eResolveProxy(oldPort_fournie_DB_2);
			if (port_fournie_DB_2 != oldPort_fournie_DB_2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2, oldPort_fournie_DB_2, port_fournie_DB_2));
			}
		}
		return port_fournie_DB_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_fournie_DB_2 basicGetPort_fournie_DB_2() {
		return port_fournie_DB_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_fournie_DB_2(Port_fournie_DB_2 newPort_fournie_DB_2, NotificationChain msgs) {
		Port_fournie_DB_2 oldPort_fournie_DB_2 = port_fournie_DB_2;
		port_fournie_DB_2 = newPort_fournie_DB_2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2, oldPort_fournie_DB_2, newPort_fournie_DB_2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_fournie_DB_2(Port_fournie_DB_2 newPort_fournie_DB_2) {
		if (newPort_fournie_DB_2 != port_fournie_DB_2) {
			NotificationChain msgs = null;
			if (port_fournie_DB_2 != null)
				msgs = ((InternalEObject)port_fournie_DB_2).eInverseRemove(this, M1Package.PORT_FOURNIE_DB_2__ATTACHEMENT_DB_2, Port_fournie_DB_2.class, msgs);
			if (newPort_fournie_DB_2 != null)
				msgs = ((InternalEObject)newPort_fournie_DB_2).eInverseAdd(this, M1Package.PORT_FOURNIE_DB_2__ATTACHEMENT_DB_2, Port_fournie_DB_2.class, msgs);
			msgs = basicSetPort_fournie_DB_2(newPort_fournie_DB_2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2, newPort_fournie_DB_2, newPort_fournie_DB_2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2:
				if (role_requie_2 != null)
					msgs = ((InternalEObject)role_requie_2).eInverseRemove(this, M1Package.ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2, Role_Requie_DB_2.class, msgs);
				return basicSetRole_requie_2((Role_Requie_DB_2)otherEnd, msgs);
			case M1Package.ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2:
				if (port_fournie_DB_2 != null)
					msgs = ((InternalEObject)port_fournie_DB_2).eInverseRemove(this, M1Package.PORT_FOURNIE_DB_2__ATTACHEMENT_DB_2, Port_fournie_DB_2.class, msgs);
				return basicSetPort_fournie_DB_2((Port_fournie_DB_2)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2:
				return basicSetRole_requie_2(null, msgs);
			case M1Package.ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2:
				return basicSetPort_fournie_DB_2(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2:
				if (resolve) return getRole_requie_2();
				return basicGetRole_requie_2();
			case M1Package.ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2:
				if (resolve) return getPort_fournie_DB_2();
				return basicGetPort_fournie_DB_2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2:
				setRole_requie_2((Role_Requie_DB_2)newValue);
				return;
			case M1Package.ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2:
				setPort_fournie_DB_2((Port_fournie_DB_2)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2:
				setRole_requie_2((Role_Requie_DB_2)null);
				return;
			case M1Package.ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2:
				setPort_fournie_DB_2((Port_fournie_DB_2)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_2__ROLE_REQUIE_2:
				return role_requie_2 != null;
			case M1Package.ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2:
				return port_fournie_DB_2 != null;
		}
		return super.eIsSet(featureID);
	}

} //Attachement_DB_2Impl
