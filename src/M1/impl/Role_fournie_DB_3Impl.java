/**
 */
package M1.impl;

import M1.Attachement_DB_3;
import M1.M1Package;
import M1.Role_fournie_DB_3;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Role fournie DB 3</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Role_fournie_DB_3Impl#getAttachement_DB_3 <em>Attachement DB 3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Role_fournie_DB_3Impl extends EObjectImpl implements Role_fournie_DB_3 {
	/**
	 * The cached value of the '{@link #getAttachement_DB_3() <em>Attachement DB 3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachement_DB_3()
	 * @generated
	 * @ordered
	 */
	protected Attachement_DB_3 attachement_DB_3;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Role_fournie_DB_3Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ROLE_FOURNIE_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_DB_3 getAttachement_DB_3() {
		if (attachement_DB_3 != null && attachement_DB_3.eIsProxy()) {
			InternalEObject oldAttachement_DB_3 = (InternalEObject)attachement_DB_3;
			attachement_DB_3 = (Attachement_DB_3)eResolveProxy(oldAttachement_DB_3);
			if (attachement_DB_3 != oldAttachement_DB_3) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3, oldAttachement_DB_3, attachement_DB_3));
			}
		}
		return attachement_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attachement_DB_3 basicGetAttachement_DB_3() {
		return attachement_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachement_DB_3(Attachement_DB_3 newAttachement_DB_3, NotificationChain msgs) {
		Attachement_DB_3 oldAttachement_DB_3 = attachement_DB_3;
		attachement_DB_3 = newAttachement_DB_3;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3, oldAttachement_DB_3, newAttachement_DB_3);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachement_DB_3(Attachement_DB_3 newAttachement_DB_3) {
		if (newAttachement_DB_3 != attachement_DB_3) {
			NotificationChain msgs = null;
			if (attachement_DB_3 != null)
				msgs = ((InternalEObject)attachement_DB_3).eInverseRemove(this, M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3, Attachement_DB_3.class, msgs);
			if (newAttachement_DB_3 != null)
				msgs = ((InternalEObject)newAttachement_DB_3).eInverseAdd(this, M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3, Attachement_DB_3.class, msgs);
			msgs = basicSetAttachement_DB_3(newAttachement_DB_3, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3, newAttachement_DB_3, newAttachement_DB_3));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3:
				if (attachement_DB_3 != null)
					msgs = ((InternalEObject)attachement_DB_3).eInverseRemove(this, M1Package.ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3, Attachement_DB_3.class, msgs);
				return basicSetAttachement_DB_3((Attachement_DB_3)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3:
				return basicSetAttachement_DB_3(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3:
				if (resolve) return getAttachement_DB_3();
				return basicGetAttachement_DB_3();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3:
				setAttachement_DB_3((Attachement_DB_3)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3:
				setAttachement_DB_3((Attachement_DB_3)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3:
				return attachement_DB_3 != null;
		}
		return super.eIsSet(featureID);
	}

} //Role_fournie_DB_3Impl
