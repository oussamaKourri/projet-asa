/**
 */
package M1.impl;

import M1.AttachementServeurRPC;
import M1.M1Package;
import M1.PortRequie_SR;
import M1.RoleFournie_SR;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attachement Serveur RPC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.AttachementServeurRPCImpl#getEReference0 <em>EReference0</em>}</li>
 *   <li>{@link M1.impl.AttachementServeurRPCImpl#getEReference1 <em>EReference1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttachementServeurRPCImpl extends EObjectImpl implements AttachementServeurRPC {
	/**
	 * The cached value of the '{@link #getEReference0() <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEReference0()
	 * @generated
	 * @ordered
	 */
	protected RoleFournie_SR eReference0;

	/**
	 * The cached value of the '{@link #getEReference1() <em>EReference1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEReference1()
	 * @generated
	 * @ordered
	 */
	protected PortRequie_SR eReference1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttachementServeurRPCImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ATTACHEMENT_SERVEUR_RPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleFournie_SR getEReference0() {
		if (eReference0 != null && eReference0.eIsProxy()) {
			InternalEObject oldEReference0 = (InternalEObject)eReference0;
			eReference0 = (RoleFournie_SR)eResolveProxy(oldEReference0);
			if (eReference0 != oldEReference0) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE0, oldEReference0, eReference0));
			}
		}
		return eReference0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleFournie_SR basicGetEReference0() {
		return eReference0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEReference0(RoleFournie_SR newEReference0, NotificationChain msgs) {
		RoleFournie_SR oldEReference0 = eReference0;
		eReference0 = newEReference0;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE0, oldEReference0, newEReference0);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEReference0(RoleFournie_SR newEReference0) {
		if (newEReference0 != eReference0) {
			NotificationChain msgs = null;
			if (eReference0 != null)
				msgs = ((InternalEObject)eReference0).eInverseRemove(this, M1Package.ROLE_FOURNIE_SR__ATTACHEMENT_SR, RoleFournie_SR.class, msgs);
			if (newEReference0 != null)
				msgs = ((InternalEObject)newEReference0).eInverseAdd(this, M1Package.ROLE_FOURNIE_SR__ATTACHEMENT_SR, RoleFournie_SR.class, msgs);
			msgs = basicSetEReference0(newEReference0, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE0, newEReference0, newEReference0));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortRequie_SR getEReference1() {
		if (eReference1 != null && eReference1.eIsProxy()) {
			InternalEObject oldEReference1 = (InternalEObject)eReference1;
			eReference1 = (PortRequie_SR)eResolveProxy(oldEReference1);
			if (eReference1 != oldEReference1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1, oldEReference1, eReference1));
			}
		}
		return eReference1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortRequie_SR basicGetEReference1() {
		return eReference1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEReference1(PortRequie_SR newEReference1, NotificationChain msgs) {
		PortRequie_SR oldEReference1 = eReference1;
		eReference1 = newEReference1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1, oldEReference1, newEReference1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEReference1(PortRequie_SR newEReference1) {
		if (newEReference1 != eReference1) {
			NotificationChain msgs = null;
			if (eReference1 != null)
				msgs = ((InternalEObject)eReference1).eInverseRemove(this, M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR, PortRequie_SR.class, msgs);
			if (newEReference1 != null)
				msgs = ((InternalEObject)newEReference1).eInverseAdd(this, M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR, PortRequie_SR.class, msgs);
			msgs = basicSetEReference1(newEReference1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1, newEReference1, newEReference1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE0:
				if (eReference0 != null)
					msgs = ((InternalEObject)eReference0).eInverseRemove(this, M1Package.ROLE_FOURNIE_SR__ATTACHEMENT_SR, RoleFournie_SR.class, msgs);
				return basicSetEReference0((RoleFournie_SR)otherEnd, msgs);
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1:
				if (eReference1 != null)
					msgs = ((InternalEObject)eReference1).eInverseRemove(this, M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR, PortRequie_SR.class, msgs);
				return basicSetEReference1((PortRequie_SR)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE0:
				return basicSetEReference0(null, msgs);
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1:
				return basicSetEReference1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE0:
				if (resolve) return getEReference0();
				return basicGetEReference0();
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1:
				if (resolve) return getEReference1();
				return basicGetEReference1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE0:
				setEReference0((RoleFournie_SR)newValue);
				return;
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1:
				setEReference1((PortRequie_SR)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE0:
				setEReference0((RoleFournie_SR)null);
				return;
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1:
				setEReference1((PortRequie_SR)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE0:
				return eReference0 != null;
			case M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1:
				return eReference1 != null;
		}
		return super.eIsSet(featureID);
	}

} //AttachementServeurRPCImpl
