/**
 */
package M1.impl;

import M1.IConnecteur_GC_DB;
import M1.M1Package;
import M1.Role_Fournie_GC_2;
import M1.Role_Requie_DB_2;
import M1.Role_Requie_GC_1;
import M1.Role_fournie_DB_1;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IConnecteur GC DB</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.IConnecteur_GC_DBImpl#getEReference0 <em>EReference0</em>}</li>
 *   <li>{@link M1.impl.IConnecteur_GC_DBImpl#getEReference1 <em>EReference1</em>}</li>
 *   <li>{@link M1.impl.IConnecteur_GC_DBImpl#getEReference2 <em>EReference2</em>}</li>
 *   <li>{@link M1.impl.IConnecteur_GC_DBImpl#getEReference3 <em>EReference3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IConnecteur_GC_DBImpl extends EObjectImpl implements IConnecteur_GC_DB {
	/**
	 * The cached value of the '{@link #getEReference0() <em>EReference0</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEReference0()
	 * @generated
	 * @ordered
	 */
	protected Role_fournie_DB_1 eReference0;

	/**
	 * The cached value of the '{@link #getEReference1() <em>EReference1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEReference1()
	 * @generated
	 * @ordered
	 */
	protected Role_Requie_DB_2 eReference1;

	/**
	 * The cached value of the '{@link #getEReference2() <em>EReference2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEReference2()
	 * @generated
	 * @ordered
	 */
	protected Role_Requie_GC_1 eReference2;

	/**
	 * The cached value of the '{@link #getEReference3() <em>EReference3</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEReference3()
	 * @generated
	 * @ordered
	 */
	protected Role_Fournie_GC_2 eReference3;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IConnecteur_GC_DBImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ICONNECTEUR_GC_DB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_fournie_DB_1 getEReference0() {
		return eReference0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEReference0(Role_fournie_DB_1 newEReference0, NotificationChain msgs) {
		Role_fournie_DB_1 oldEReference0 = eReference0;
		eReference0 = newEReference0;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ICONNECTEUR_GC_DB__EREFERENCE0, oldEReference0, newEReference0);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEReference0(Role_fournie_DB_1 newEReference0) {
		if (newEReference0 != eReference0) {
			NotificationChain msgs = null;
			if (eReference0 != null)
				msgs = ((InternalEObject)eReference0).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.ICONNECTEUR_GC_DB__EREFERENCE0, null, msgs);
			if (newEReference0 != null)
				msgs = ((InternalEObject)newEReference0).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.ICONNECTEUR_GC_DB__EREFERENCE0, null, msgs);
			msgs = basicSetEReference0(newEReference0, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ICONNECTEUR_GC_DB__EREFERENCE0, newEReference0, newEReference0));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_DB_2 getEReference1() {
		return eReference1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEReference1(Role_Requie_DB_2 newEReference1, NotificationChain msgs) {
		Role_Requie_DB_2 oldEReference1 = eReference1;
		eReference1 = newEReference1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ICONNECTEUR_GC_DB__EREFERENCE1, oldEReference1, newEReference1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEReference1(Role_Requie_DB_2 newEReference1) {
		if (newEReference1 != eReference1) {
			NotificationChain msgs = null;
			if (eReference1 != null)
				msgs = ((InternalEObject)eReference1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.ICONNECTEUR_GC_DB__EREFERENCE1, null, msgs);
			if (newEReference1 != null)
				msgs = ((InternalEObject)newEReference1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.ICONNECTEUR_GC_DB__EREFERENCE1, null, msgs);
			msgs = basicSetEReference1(newEReference1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ICONNECTEUR_GC_DB__EREFERENCE1, newEReference1, newEReference1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_GC_1 getEReference2() {
		return eReference2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEReference2(Role_Requie_GC_1 newEReference2, NotificationChain msgs) {
		Role_Requie_GC_1 oldEReference2 = eReference2;
		eReference2 = newEReference2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ICONNECTEUR_GC_DB__EREFERENCE2, oldEReference2, newEReference2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEReference2(Role_Requie_GC_1 newEReference2) {
		if (newEReference2 != eReference2) {
			NotificationChain msgs = null;
			if (eReference2 != null)
				msgs = ((InternalEObject)eReference2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.ICONNECTEUR_GC_DB__EREFERENCE2, null, msgs);
			if (newEReference2 != null)
				msgs = ((InternalEObject)newEReference2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.ICONNECTEUR_GC_DB__EREFERENCE2, null, msgs);
			msgs = basicSetEReference2(newEReference2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ICONNECTEUR_GC_DB__EREFERENCE2, newEReference2, newEReference2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Fournie_GC_2 getEReference3() {
		return eReference3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEReference3(Role_Fournie_GC_2 newEReference3, NotificationChain msgs) {
		Role_Fournie_GC_2 oldEReference3 = eReference3;
		eReference3 = newEReference3;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ICONNECTEUR_GC_DB__EREFERENCE3, oldEReference3, newEReference3);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEReference3(Role_Fournie_GC_2 newEReference3) {
		if (newEReference3 != eReference3) {
			NotificationChain msgs = null;
			if (eReference3 != null)
				msgs = ((InternalEObject)eReference3).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.ICONNECTEUR_GC_DB__EREFERENCE3, null, msgs);
			if (newEReference3 != null)
				msgs = ((InternalEObject)newEReference3).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.ICONNECTEUR_GC_DB__EREFERENCE3, null, msgs);
			msgs = basicSetEReference3(newEReference3, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ICONNECTEUR_GC_DB__EREFERENCE3, newEReference3, newEReference3));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE0:
				return basicSetEReference0(null, msgs);
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE1:
				return basicSetEReference1(null, msgs);
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE2:
				return basicSetEReference2(null, msgs);
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE3:
				return basicSetEReference3(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE0:
				return getEReference0();
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE1:
				return getEReference1();
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE2:
				return getEReference2();
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE3:
				return getEReference3();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE0:
				setEReference0((Role_fournie_DB_1)newValue);
				return;
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE1:
				setEReference1((Role_Requie_DB_2)newValue);
				return;
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE2:
				setEReference2((Role_Requie_GC_1)newValue);
				return;
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE3:
				setEReference3((Role_Fournie_GC_2)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE0:
				setEReference0((Role_fournie_DB_1)null);
				return;
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE1:
				setEReference1((Role_Requie_DB_2)null);
				return;
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE2:
				setEReference2((Role_Requie_GC_1)null);
				return;
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE3:
				setEReference3((Role_Fournie_GC_2)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE0:
				return eReference0 != null;
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE1:
				return eReference1 != null;
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE2:
				return eReference2 != null;
			case M1Package.ICONNECTEUR_GC_DB__EREFERENCE3:
				return eReference3 != null;
		}
		return super.eIsSet(featureID);
	}

} //IConnecteur_GC_DBImpl
