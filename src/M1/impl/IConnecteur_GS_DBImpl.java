/**
 */
package M1.impl;

import M1.IConnecteur_GS_DB;
import M1.M1Package;
import M1.Role_Fournie_GS_4;
import M1.Role_Requie_DB_4;
import M1.Role_Requie_GS_3;
import M1.Role_fournie_DB_3;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IConnecteur GS DB</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.IConnecteur_GS_DBImpl#getRole_fournie_DB_3 <em>Role fournie DB 3</em>}</li>
 *   <li>{@link M1.impl.IConnecteur_GS_DBImpl#getRole_requie_DB_4 <em>Role requie DB 4</em>}</li>
 *   <li>{@link M1.impl.IConnecteur_GS_DBImpl#getRole_fournie_GS_4 <em>Role fournie GS 4</em>}</li>
 *   <li>{@link M1.impl.IConnecteur_GS_DBImpl#getRole_requie_GS_3 <em>Role requie GS 3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IConnecteur_GS_DBImpl extends EObjectImpl implements IConnecteur_GS_DB {
	/**
	 * The cached value of the '{@link #getRole_fournie_DB_3() <em>Role fournie DB 3</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_fournie_DB_3()
	 * @generated
	 * @ordered
	 */
	protected EList role_fournie_DB_3;

	/**
	 * The cached value of the '{@link #getRole_requie_DB_4() <em>Role requie DB 4</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_requie_DB_4()
	 * @generated
	 * @ordered
	 */
	protected EList role_requie_DB_4;

	/**
	 * The cached value of the '{@link #getRole_fournie_GS_4() <em>Role fournie GS 4</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_fournie_GS_4()
	 * @generated
	 * @ordered
	 */
	protected EList role_fournie_GS_4;

	/**
	 * The cached value of the '{@link #getRole_requie_GS_3() <em>Role requie GS 3</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_requie_GS_3()
	 * @generated
	 * @ordered
	 */
	protected EList role_requie_GS_3;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IConnecteur_GS_DBImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ICONNECTEUR_GS_DB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRole_fournie_DB_3() {
		if (role_fournie_DB_3 == null) {
			role_fournie_DB_3 = new EObjectContainmentEList(Role_fournie_DB_3.class, this, M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_DB_3);
		}
		return role_fournie_DB_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRole_requie_DB_4() {
		if (role_requie_DB_4 == null) {
			role_requie_DB_4 = new EObjectContainmentEList(Role_Requie_DB_4.class, this, M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_DB_4);
		}
		return role_requie_DB_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRole_fournie_GS_4() {
		if (role_fournie_GS_4 == null) {
			role_fournie_GS_4 = new EObjectContainmentEList(Role_Fournie_GS_4.class, this, M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_GS_4);
		}
		return role_fournie_GS_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRole_requie_GS_3() {
		if (role_requie_GS_3 == null) {
			role_requie_GS_3 = new EObjectContainmentEList(Role_Requie_GS_3.class, this, M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_GS_3);
		}
		return role_requie_GS_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_DB_3:
				return ((InternalEList)getRole_fournie_DB_3()).basicRemove(otherEnd, msgs);
			case M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_DB_4:
				return ((InternalEList)getRole_requie_DB_4()).basicRemove(otherEnd, msgs);
			case M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_GS_4:
				return ((InternalEList)getRole_fournie_GS_4()).basicRemove(otherEnd, msgs);
			case M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_GS_3:
				return ((InternalEList)getRole_requie_GS_3()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_DB_3:
				return getRole_fournie_DB_3();
			case M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_DB_4:
				return getRole_requie_DB_4();
			case M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_GS_4:
				return getRole_fournie_GS_4();
			case M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_GS_3:
				return getRole_requie_GS_3();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_DB_3:
				getRole_fournie_DB_3().clear();
				getRole_fournie_DB_3().addAll((Collection)newValue);
				return;
			case M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_DB_4:
				getRole_requie_DB_4().clear();
				getRole_requie_DB_4().addAll((Collection)newValue);
				return;
			case M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_GS_4:
				getRole_fournie_GS_4().clear();
				getRole_fournie_GS_4().addAll((Collection)newValue);
				return;
			case M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_GS_3:
				getRole_requie_GS_3().clear();
				getRole_requie_GS_3().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_DB_3:
				getRole_fournie_DB_3().clear();
				return;
			case M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_DB_4:
				getRole_requie_DB_4().clear();
				return;
			case M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_GS_4:
				getRole_fournie_GS_4().clear();
				return;
			case M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_GS_3:
				getRole_requie_GS_3().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_DB_3:
				return role_fournie_DB_3 != null && !role_fournie_DB_3.isEmpty();
			case M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_DB_4:
				return role_requie_DB_4 != null && !role_requie_DB_4.isEmpty();
			case M1Package.ICONNECTEUR_GS_DB__ROLE_FOURNIE_GS_4:
				return role_fournie_GS_4 != null && !role_fournie_GS_4.isEmpty();
			case M1Package.ICONNECTEUR_GS_DB__ROLE_REQUIE_GS_3:
				return role_requie_GS_3 != null && !role_requie_GS_3.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //IConnecteur_GS_DBImpl
