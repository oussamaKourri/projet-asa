/**
 */
package M1.impl;

import M1.AttachementClientRPC;
import M1.M1Package;
import M1.RoleRequie_CR;
import M1.SendRequest;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attachement Client RPC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.AttachementClientRPCImpl#getSendRequest <em>Send Request</em>}</li>
 *   <li>{@link M1.impl.AttachementClientRPCImpl#getRoleRequie_CR <em>Role Requie CR</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttachementClientRPCImpl extends EObjectImpl implements AttachementClientRPC {
	/**
	 * The cached value of the '{@link #getSendRequest() <em>Send Request</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSendRequest()
	 * @generated
	 * @ordered
	 */
	protected SendRequest sendRequest;

	/**
	 * The cached value of the '{@link #getRoleRequie_CR() <em>Role Requie CR</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleRequie_CR()
	 * @generated
	 * @ordered
	 */
	protected RoleRequie_CR roleRequie_CR;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttachementClientRPCImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ATTACHEMENT_CLIENT_RPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SendRequest getSendRequest() {
		if (sendRequest != null && sendRequest.eIsProxy()) {
			InternalEObject oldSendRequest = (InternalEObject)sendRequest;
			sendRequest = (SendRequest)eResolveProxy(oldSendRequest);
			if (sendRequest != oldSendRequest) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_CLIENT_RPC__SEND_REQUEST, oldSendRequest, sendRequest));
			}
		}
		return sendRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SendRequest basicGetSendRequest() {
		return sendRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSendRequest(SendRequest newSendRequest) {
		SendRequest oldSendRequest = sendRequest;
		sendRequest = newSendRequest;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_CLIENT_RPC__SEND_REQUEST, oldSendRequest, sendRequest));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleRequie_CR getRoleRequie_CR() {
		if (roleRequie_CR != null && roleRequie_CR.eIsProxy()) {
			InternalEObject oldRoleRequie_CR = (InternalEObject)roleRequie_CR;
			roleRequie_CR = (RoleRequie_CR)eResolveProxy(oldRoleRequie_CR);
			if (roleRequie_CR != oldRoleRequie_CR) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_CLIENT_RPC__ROLE_REQUIE_CR, oldRoleRequie_CR, roleRequie_CR));
			}
		}
		return roleRequie_CR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleRequie_CR basicGetRoleRequie_CR() {
		return roleRequie_CR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoleRequie_CR(RoleRequie_CR newRoleRequie_CR) {
		RoleRequie_CR oldRoleRequie_CR = roleRequie_CR;
		roleRequie_CR = newRoleRequie_CR;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_CLIENT_RPC__ROLE_REQUIE_CR, oldRoleRequie_CR, roleRequie_CR));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_CLIENT_RPC__SEND_REQUEST:
				if (resolve) return getSendRequest();
				return basicGetSendRequest();
			case M1Package.ATTACHEMENT_CLIENT_RPC__ROLE_REQUIE_CR:
				if (resolve) return getRoleRequie_CR();
				return basicGetRoleRequie_CR();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_CLIENT_RPC__SEND_REQUEST:
				setSendRequest((SendRequest)newValue);
				return;
			case M1Package.ATTACHEMENT_CLIENT_RPC__ROLE_REQUIE_CR:
				setRoleRequie_CR((RoleRequie_CR)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_CLIENT_RPC__SEND_REQUEST:
				setSendRequest((SendRequest)null);
				return;
			case M1Package.ATTACHEMENT_CLIENT_RPC__ROLE_REQUIE_CR:
				setRoleRequie_CR((RoleRequie_CR)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_CLIENT_RPC__SEND_REQUEST:
				return sendRequest != null;
			case M1Package.ATTACHEMENT_CLIENT_RPC__ROLE_REQUIE_CR:
				return roleRequie_CR != null;
		}
		return super.eIsSet(featureID);
	}

} //AttachementClientRPCImpl
