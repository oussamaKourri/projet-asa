/**
 */
package M1.impl;

import M1.Attachement_DB_4;
import M1.M1Package;
import M1.Port_fornie_DB_4;
import M1.Role_Requie_DB_4;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attachement DB 4</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Attachement_DB_4Impl#getRole_Requie_4 <em>Role Requie 4</em>}</li>
 *   <li>{@link M1.impl.Attachement_DB_4Impl#getPort_fourne_4 <em>Port fourne 4</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Attachement_DB_4Impl extends EObjectImpl implements Attachement_DB_4 {
	/**
	 * The cached value of the '{@link #getRole_Requie_4() <em>Role Requie 4</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_Requie_4()
	 * @generated
	 * @ordered
	 */
	protected Role_Requie_DB_4 role_Requie_4;

	/**
	 * The cached value of the '{@link #getPort_fourne_4() <em>Port fourne 4</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_fourne_4()
	 * @generated
	 * @ordered
	 */
	protected Port_fornie_DB_4 port_fourne_4;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Attachement_DB_4Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ATTACHEMENT_DB_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_DB_4 getRole_Requie_4() {
		if (role_Requie_4 != null && role_Requie_4.eIsProxy()) {
			InternalEObject oldRole_Requie_4 = (InternalEObject)role_Requie_4;
			role_Requie_4 = (Role_Requie_DB_4)eResolveProxy(oldRole_Requie_4);
			if (role_Requie_4 != oldRole_Requie_4) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4, oldRole_Requie_4, role_Requie_4));
			}
		}
		return role_Requie_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Requie_DB_4 basicGetRole_Requie_4() {
		return role_Requie_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRole_Requie_4(Role_Requie_DB_4 newRole_Requie_4, NotificationChain msgs) {
		Role_Requie_DB_4 oldRole_Requie_4 = role_Requie_4;
		role_Requie_4 = newRole_Requie_4;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4, oldRole_Requie_4, newRole_Requie_4);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole_Requie_4(Role_Requie_DB_4 newRole_Requie_4) {
		if (newRole_Requie_4 != role_Requie_4) {
			NotificationChain msgs = null;
			if (role_Requie_4 != null)
				msgs = ((InternalEObject)role_Requie_4).eInverseRemove(this, M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4, Role_Requie_DB_4.class, msgs);
			if (newRole_Requie_4 != null)
				msgs = ((InternalEObject)newRole_Requie_4).eInverseAdd(this, M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4, Role_Requie_DB_4.class, msgs);
			msgs = basicSetRole_Requie_4(newRole_Requie_4, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4, newRole_Requie_4, newRole_Requie_4));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_fornie_DB_4 getPort_fourne_4() {
		if (port_fourne_4 != null && port_fourne_4.eIsProxy()) {
			InternalEObject oldPort_fourne_4 = (InternalEObject)port_fourne_4;
			port_fourne_4 = (Port_fornie_DB_4)eResolveProxy(oldPort_fourne_4);
			if (port_fourne_4 != oldPort_fourne_4) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_DB_4__PORT_FOURNE_4, oldPort_fourne_4, port_fourne_4));
			}
		}
		return port_fourne_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_fornie_DB_4 basicGetPort_fourne_4() {
		return port_fourne_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_fourne_4(Port_fornie_DB_4 newPort_fourne_4, NotificationChain msgs) {
		Port_fornie_DB_4 oldPort_fourne_4 = port_fourne_4;
		port_fourne_4 = newPort_fourne_4;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_4__PORT_FOURNE_4, oldPort_fourne_4, newPort_fourne_4);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_fourne_4(Port_fornie_DB_4 newPort_fourne_4) {
		if (newPort_fourne_4 != port_fourne_4) {
			NotificationChain msgs = null;
			if (port_fourne_4 != null)
				msgs = ((InternalEObject)port_fourne_4).eInverseRemove(this, M1Package.PORT_FORNIE_DB_4__ATTACHEMENT_DB_4, Port_fornie_DB_4.class, msgs);
			if (newPort_fourne_4 != null)
				msgs = ((InternalEObject)newPort_fourne_4).eInverseAdd(this, M1Package.PORT_FORNIE_DB_4__ATTACHEMENT_DB_4, Port_fornie_DB_4.class, msgs);
			msgs = basicSetPort_fourne_4(newPort_fourne_4, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_DB_4__PORT_FOURNE_4, newPort_fourne_4, newPort_fourne_4));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4:
				if (role_Requie_4 != null)
					msgs = ((InternalEObject)role_Requie_4).eInverseRemove(this, M1Package.ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4, Role_Requie_DB_4.class, msgs);
				return basicSetRole_Requie_4((Role_Requie_DB_4)otherEnd, msgs);
			case M1Package.ATTACHEMENT_DB_4__PORT_FOURNE_4:
				if (port_fourne_4 != null)
					msgs = ((InternalEObject)port_fourne_4).eInverseRemove(this, M1Package.PORT_FORNIE_DB_4__ATTACHEMENT_DB_4, Port_fornie_DB_4.class, msgs);
				return basicSetPort_fourne_4((Port_fornie_DB_4)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4:
				return basicSetRole_Requie_4(null, msgs);
			case M1Package.ATTACHEMENT_DB_4__PORT_FOURNE_4:
				return basicSetPort_fourne_4(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4:
				if (resolve) return getRole_Requie_4();
				return basicGetRole_Requie_4();
			case M1Package.ATTACHEMENT_DB_4__PORT_FOURNE_4:
				if (resolve) return getPort_fourne_4();
				return basicGetPort_fourne_4();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4:
				setRole_Requie_4((Role_Requie_DB_4)newValue);
				return;
			case M1Package.ATTACHEMENT_DB_4__PORT_FOURNE_4:
				setPort_fourne_4((Port_fornie_DB_4)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4:
				setRole_Requie_4((Role_Requie_DB_4)null);
				return;
			case M1Package.ATTACHEMENT_DB_4__PORT_FOURNE_4:
				setPort_fourne_4((Port_fornie_DB_4)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_DB_4__ROLE_REQUIE_4:
				return role_Requie_4 != null;
			case M1Package.ATTACHEMENT_DB_4__PORT_FOURNE_4:
				return port_fourne_4 != null;
		}
		return super.eIsSet(featureID);
	}

} //Attachement_DB_4Impl
