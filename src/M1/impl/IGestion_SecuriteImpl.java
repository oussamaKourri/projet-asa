/**
 */
package M1.impl;

import M1.IGestion_Securite;
import M1.M1Package;
import M1.Port_Fournie_GS_1;
import M1.Port_Fournie_GS_3;
import M1.Port_Requie_GS_2;
import M1.Port_Requie_GS_4;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IGestion Securite</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.IGestion_SecuriteImpl#getPort_fournie_GS_1 <em>Port fournie GS 1</em>}</li>
 *   <li>{@link M1.impl.IGestion_SecuriteImpl#getPort_requie_GS_2 <em>Port requie GS 2</em>}</li>
 *   <li>{@link M1.impl.IGestion_SecuriteImpl#getPort_fournie_GS_3 <em>Port fournie GS 3</em>}</li>
 *   <li>{@link M1.impl.IGestion_SecuriteImpl#getPort_requie_GS_4 <em>Port requie GS 4</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IGestion_SecuriteImpl extends EObjectImpl implements IGestion_Securite {
	/**
	 * The cached value of the '{@link #getPort_fournie_GS_1() <em>Port fournie GS 1</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_fournie_GS_1()
	 * @generated
	 * @ordered
	 */
	protected EList port_fournie_GS_1;

	/**
	 * The cached value of the '{@link #getPort_requie_GS_2() <em>Port requie GS 2</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_requie_GS_2()
	 * @generated
	 * @ordered
	 */
	protected EList port_requie_GS_2;

	/**
	 * The cached value of the '{@link #getPort_fournie_GS_3() <em>Port fournie GS 3</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_fournie_GS_3()
	 * @generated
	 * @ordered
	 */
	protected EList port_fournie_GS_3;

	/**
	 * The cached value of the '{@link #getPort_requie_GS_4() <em>Port requie GS 4</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_requie_GS_4()
	 * @generated
	 * @ordered
	 */
	protected EList port_requie_GS_4;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IGestion_SecuriteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.IGESTION_SECURITE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPort_fournie_GS_1() {
		if (port_fournie_GS_1 == null) {
			port_fournie_GS_1 = new EObjectContainmentEList(Port_Fournie_GS_1.class, this, M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_1);
		}
		return port_fournie_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPort_requie_GS_2() {
		if (port_requie_GS_2 == null) {
			port_requie_GS_2 = new EObjectContainmentEList(Port_Requie_GS_2.class, this, M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_2);
		}
		return port_requie_GS_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPort_fournie_GS_3() {
		if (port_fournie_GS_3 == null) {
			port_fournie_GS_3 = new EObjectContainmentEList(Port_Fournie_GS_3.class, this, M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_3);
		}
		return port_fournie_GS_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPort_requie_GS_4() {
		if (port_requie_GS_4 == null) {
			port_requie_GS_4 = new EObjectContainmentEList(Port_Requie_GS_4.class, this, M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_4);
		}
		return port_requie_GS_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_1:
				return ((InternalEList)getPort_fournie_GS_1()).basicRemove(otherEnd, msgs);
			case M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_2:
				return ((InternalEList)getPort_requie_GS_2()).basicRemove(otherEnd, msgs);
			case M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_3:
				return ((InternalEList)getPort_fournie_GS_3()).basicRemove(otherEnd, msgs);
			case M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_4:
				return ((InternalEList)getPort_requie_GS_4()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_1:
				return getPort_fournie_GS_1();
			case M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_2:
				return getPort_requie_GS_2();
			case M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_3:
				return getPort_fournie_GS_3();
			case M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_4:
				return getPort_requie_GS_4();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_1:
				getPort_fournie_GS_1().clear();
				getPort_fournie_GS_1().addAll((Collection)newValue);
				return;
			case M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_2:
				getPort_requie_GS_2().clear();
				getPort_requie_GS_2().addAll((Collection)newValue);
				return;
			case M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_3:
				getPort_fournie_GS_3().clear();
				getPort_fournie_GS_3().addAll((Collection)newValue);
				return;
			case M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_4:
				getPort_requie_GS_4().clear();
				getPort_requie_GS_4().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_1:
				getPort_fournie_GS_1().clear();
				return;
			case M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_2:
				getPort_requie_GS_2().clear();
				return;
			case M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_3:
				getPort_fournie_GS_3().clear();
				return;
			case M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_4:
				getPort_requie_GS_4().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_1:
				return port_fournie_GS_1 != null && !port_fournie_GS_1.isEmpty();
			case M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_2:
				return port_requie_GS_2 != null && !port_requie_GS_2.isEmpty();
			case M1Package.IGESTION_SECURITE__PORT_FOURNIE_GS_3:
				return port_fournie_GS_3 != null && !port_fournie_GS_3.isEmpty();
			case M1Package.IGESTION_SECURITE__PORT_REQUIE_GS_4:
				return port_requie_GS_4 != null && !port_requie_GS_4.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //IGestion_SecuriteImpl
