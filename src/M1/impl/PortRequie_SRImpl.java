/**
 */
package M1.impl;

import M1.AttachementServeurRPC;
import M1.M1Package;
import M1.PortRequie_SR;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Requie SR</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.PortRequie_SRImpl#getAttachement_SR <em>Attachement SR</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PortRequie_SRImpl extends EObjectImpl implements PortRequie_SR {
	/**
	 * The cached value of the '{@link #getAttachement_SR() <em>Attachement SR</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachement_SR()
	 * @generated
	 * @ordered
	 */
	protected AttachementServeurRPC attachement_SR;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortRequie_SRImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.PORT_REQUIE_SR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementServeurRPC getAttachement_SR() {
		if (attachement_SR != null && attachement_SR.eIsProxy()) {
			InternalEObject oldAttachement_SR = (InternalEObject)attachement_SR;
			attachement_SR = (AttachementServeurRPC)eResolveProxy(oldAttachement_SR);
			if (attachement_SR != oldAttachement_SR) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR, oldAttachement_SR, attachement_SR));
			}
		}
		return attachement_SR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementServeurRPC basicGetAttachement_SR() {
		return attachement_SR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachement_SR(AttachementServeurRPC newAttachement_SR, NotificationChain msgs) {
		AttachementServeurRPC oldAttachement_SR = attachement_SR;
		attachement_SR = newAttachement_SR;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR, oldAttachement_SR, newAttachement_SR);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachement_SR(AttachementServeurRPC newAttachement_SR) {
		if (newAttachement_SR != attachement_SR) {
			NotificationChain msgs = null;
			if (attachement_SR != null)
				msgs = ((InternalEObject)attachement_SR).eInverseRemove(this, M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1, AttachementServeurRPC.class, msgs);
			if (newAttachement_SR != null)
				msgs = ((InternalEObject)newAttachement_SR).eInverseAdd(this, M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1, AttachementServeurRPC.class, msgs);
			msgs = basicSetAttachement_SR(newAttachement_SR, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR, newAttachement_SR, newAttachement_SR));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR:
				if (attachement_SR != null)
					msgs = ((InternalEObject)attachement_SR).eInverseRemove(this, M1Package.ATTACHEMENT_SERVEUR_RPC__EREFERENCE1, AttachementServeurRPC.class, msgs);
				return basicSetAttachement_SR((AttachementServeurRPC)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR:
				return basicSetAttachement_SR(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR:
				if (resolve) return getAttachement_SR();
				return basicGetAttachement_SR();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR:
				setAttachement_SR((AttachementServeurRPC)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR:
				setAttachement_SR((AttachementServeurRPC)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.PORT_REQUIE_SR__ATTACHEMENT_SR:
				return attachement_SR != null;
		}
		return super.eIsSet(featureID);
	}

} //PortRequie_SRImpl
