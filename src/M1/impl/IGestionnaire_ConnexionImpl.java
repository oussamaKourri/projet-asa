/**
 */
package M1.impl;

import M1.IGestionnaire_Connexion;
import M1.M1Package;
import M1.Port_CM_S;
import M1.Port_Fournie_GC_1;
import M1.Port_Fournie_GC_3;
import M1.Port_Requie_GC_2;
import M1.Port_Requie_GC_4;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IGestionnaire Connexion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.IGestionnaire_ConnexionImpl#getPort_Fournie_Gc_1 <em>Port Fournie Gc 1</em>}</li>
 *   <li>{@link M1.impl.IGestionnaire_ConnexionImpl#getPort_Requie_Gc_2 <em>Port Requie Gc 2</em>}</li>
 *   <li>{@link M1.impl.IGestionnaire_ConnexionImpl#getPort_Fournie_Gc_3 <em>Port Fournie Gc 3</em>}</li>
 *   <li>{@link M1.impl.IGestionnaire_ConnexionImpl#getPort_Requie_Gc_4 <em>Port Requie Gc 4</em>}</li>
 *   <li>{@link M1.impl.IGestionnaire_ConnexionImpl#getPort_CM_S <em>Port CM S</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IGestionnaire_ConnexionImpl extends EObjectImpl implements IGestionnaire_Connexion {
	/**
	 * The cached value of the '{@link #getPort_Fournie_Gc_1() <em>Port Fournie Gc 1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_Fournie_Gc_1()
	 * @generated
	 * @ordered
	 */
	protected Port_Fournie_GC_1 port_Fournie_Gc_1;

	/**
	 * The cached value of the '{@link #getPort_Requie_Gc_2() <em>Port Requie Gc 2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_Requie_Gc_2()
	 * @generated
	 * @ordered
	 */
	protected Port_Requie_GC_2 port_Requie_Gc_2;

	/**
	 * The cached value of the '{@link #getPort_Fournie_Gc_3() <em>Port Fournie Gc 3</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_Fournie_Gc_3()
	 * @generated
	 * @ordered
	 */
	protected Port_Fournie_GC_3 port_Fournie_Gc_3;

	/**
	 * The cached value of the '{@link #getPort_Requie_Gc_4() <em>Port Requie Gc 4</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_Requie_Gc_4()
	 * @generated
	 * @ordered
	 */
	protected Port_Requie_GC_4 port_Requie_Gc_4;

	/**
	 * The cached value of the '{@link #getPort_CM_S() <em>Port CM S</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_CM_S()
	 * @generated
	 * @ordered
	 */
	protected EList port_CM_S;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IGestionnaire_ConnexionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.IGESTIONNAIRE_CONNEXION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Fournie_GC_1 getPort_Fournie_Gc_1() {
		return port_Fournie_Gc_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_Fournie_Gc_1(Port_Fournie_GC_1 newPort_Fournie_Gc_1, NotificationChain msgs) {
		Port_Fournie_GC_1 oldPort_Fournie_Gc_1 = port_Fournie_Gc_1;
		port_Fournie_Gc_1 = newPort_Fournie_Gc_1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1, oldPort_Fournie_Gc_1, newPort_Fournie_Gc_1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_Fournie_Gc_1(Port_Fournie_GC_1 newPort_Fournie_Gc_1) {
		if (newPort_Fournie_Gc_1 != port_Fournie_Gc_1) {
			NotificationChain msgs = null;
			if (port_Fournie_Gc_1 != null)
				msgs = ((InternalEObject)port_Fournie_Gc_1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1, null, msgs);
			if (newPort_Fournie_Gc_1 != null)
				msgs = ((InternalEObject)newPort_Fournie_Gc_1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1, null, msgs);
			msgs = basicSetPort_Fournie_Gc_1(newPort_Fournie_Gc_1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1, newPort_Fournie_Gc_1, newPort_Fournie_Gc_1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_GC_2 getPort_Requie_Gc_2() {
		return port_Requie_Gc_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_Requie_Gc_2(Port_Requie_GC_2 newPort_Requie_Gc_2, NotificationChain msgs) {
		Port_Requie_GC_2 oldPort_Requie_Gc_2 = port_Requie_Gc_2;
		port_Requie_Gc_2 = newPort_Requie_Gc_2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2, oldPort_Requie_Gc_2, newPort_Requie_Gc_2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_Requie_Gc_2(Port_Requie_GC_2 newPort_Requie_Gc_2) {
		if (newPort_Requie_Gc_2 != port_Requie_Gc_2) {
			NotificationChain msgs = null;
			if (port_Requie_Gc_2 != null)
				msgs = ((InternalEObject)port_Requie_Gc_2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2, null, msgs);
			if (newPort_Requie_Gc_2 != null)
				msgs = ((InternalEObject)newPort_Requie_Gc_2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2, null, msgs);
			msgs = basicSetPort_Requie_Gc_2(newPort_Requie_Gc_2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2, newPort_Requie_Gc_2, newPort_Requie_Gc_2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Fournie_GC_3 getPort_Fournie_Gc_3() {
		return port_Fournie_Gc_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_Fournie_Gc_3(Port_Fournie_GC_3 newPort_Fournie_Gc_3, NotificationChain msgs) {
		Port_Fournie_GC_3 oldPort_Fournie_Gc_3 = port_Fournie_Gc_3;
		port_Fournie_Gc_3 = newPort_Fournie_Gc_3;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3, oldPort_Fournie_Gc_3, newPort_Fournie_Gc_3);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_Fournie_Gc_3(Port_Fournie_GC_3 newPort_Fournie_Gc_3) {
		if (newPort_Fournie_Gc_3 != port_Fournie_Gc_3) {
			NotificationChain msgs = null;
			if (port_Fournie_Gc_3 != null)
				msgs = ((InternalEObject)port_Fournie_Gc_3).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3, null, msgs);
			if (newPort_Fournie_Gc_3 != null)
				msgs = ((InternalEObject)newPort_Fournie_Gc_3).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3, null, msgs);
			msgs = basicSetPort_Fournie_Gc_3(newPort_Fournie_Gc_3, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3, newPort_Fournie_Gc_3, newPort_Fournie_Gc_3));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_GC_4 getPort_Requie_Gc_4() {
		return port_Requie_Gc_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_Requie_Gc_4(Port_Requie_GC_4 newPort_Requie_Gc_4, NotificationChain msgs) {
		Port_Requie_GC_4 oldPort_Requie_Gc_4 = port_Requie_Gc_4;
		port_Requie_Gc_4 = newPort_Requie_Gc_4;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4, oldPort_Requie_Gc_4, newPort_Requie_Gc_4);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_Requie_Gc_4(Port_Requie_GC_4 newPort_Requie_Gc_4) {
		if (newPort_Requie_Gc_4 != port_Requie_Gc_4) {
			NotificationChain msgs = null;
			if (port_Requie_Gc_4 != null)
				msgs = ((InternalEObject)port_Requie_Gc_4).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4, null, msgs);
			if (newPort_Requie_Gc_4 != null)
				msgs = ((InternalEObject)newPort_Requie_Gc_4).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4, null, msgs);
			msgs = basicSetPort_Requie_Gc_4(newPort_Requie_Gc_4, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4, newPort_Requie_Gc_4, newPort_Requie_Gc_4));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPort_CM_S() {
		if (port_CM_S == null) {
			port_CM_S = new EObjectContainmentEList(Port_CM_S.class, this, M1Package.IGESTIONNAIRE_CONNEXION__PORT_CM_S);
		}
		return port_CM_S;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1:
				return basicSetPort_Fournie_Gc_1(null, msgs);
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2:
				return basicSetPort_Requie_Gc_2(null, msgs);
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3:
				return basicSetPort_Fournie_Gc_3(null, msgs);
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4:
				return basicSetPort_Requie_Gc_4(null, msgs);
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_CM_S:
				return ((InternalEList)getPort_CM_S()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1:
				return getPort_Fournie_Gc_1();
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2:
				return getPort_Requie_Gc_2();
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3:
				return getPort_Fournie_Gc_3();
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4:
				return getPort_Requie_Gc_4();
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_CM_S:
				return getPort_CM_S();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1:
				setPort_Fournie_Gc_1((Port_Fournie_GC_1)newValue);
				return;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2:
				setPort_Requie_Gc_2((Port_Requie_GC_2)newValue);
				return;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3:
				setPort_Fournie_Gc_3((Port_Fournie_GC_3)newValue);
				return;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4:
				setPort_Requie_Gc_4((Port_Requie_GC_4)newValue);
				return;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_CM_S:
				getPort_CM_S().clear();
				getPort_CM_S().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1:
				setPort_Fournie_Gc_1((Port_Fournie_GC_1)null);
				return;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2:
				setPort_Requie_Gc_2((Port_Requie_GC_2)null);
				return;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3:
				setPort_Fournie_Gc_3((Port_Fournie_GC_3)null);
				return;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4:
				setPort_Requie_Gc_4((Port_Requie_GC_4)null);
				return;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_CM_S:
				getPort_CM_S().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1:
				return port_Fournie_Gc_1 != null;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2:
				return port_Requie_Gc_2 != null;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3:
				return port_Fournie_Gc_3 != null;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4:
				return port_Requie_Gc_4 != null;
			case M1Package.IGESTIONNAIRE_CONNEXION__PORT_CM_S:
				return port_CM_S != null && !port_CM_S.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //IGestionnaire_ConnexionImpl
