/**
 */
package M1.impl;

import M1.AttachementRPCServeur;
import M1.M1Package;
import M1.RoleRequie_RS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Role Requie RS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.RoleRequie_RSImpl#getAttachement_RS <em>Attachement RS</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RoleRequie_RSImpl extends EObjectImpl implements RoleRequie_RS {
	/**
	 * The cached value of the '{@link #getAttachement_RS() <em>Attachement RS</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachement_RS()
	 * @generated
	 * @ordered
	 */
	protected AttachementRPCServeur attachement_RS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleRequie_RSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ROLE_REQUIE_RS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementRPCServeur getAttachement_RS() {
		if (attachement_RS != null && attachement_RS.eIsProxy()) {
			InternalEObject oldAttachement_RS = (InternalEObject)attachement_RS;
			attachement_RS = (AttachementRPCServeur)eResolveProxy(oldAttachement_RS);
			if (attachement_RS != oldAttachement_RS) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ROLE_REQUIE_RS__ATTACHEMENT_RS, oldAttachement_RS, attachement_RS));
			}
		}
		return attachement_RS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachementRPCServeur basicGetAttachement_RS() {
		return attachement_RS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachement_RS(AttachementRPCServeur newAttachement_RS, NotificationChain msgs) {
		AttachementRPCServeur oldAttachement_RS = attachement_RS;
		attachement_RS = newAttachement_RS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ROLE_REQUIE_RS__ATTACHEMENT_RS, oldAttachement_RS, newAttachement_RS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachement_RS(AttachementRPCServeur newAttachement_RS) {
		if (newAttachement_RS != attachement_RS) {
			NotificationChain msgs = null;
			if (attachement_RS != null)
				msgs = ((InternalEObject)attachement_RS).eInverseRemove(this, M1Package.ATTACHEMENT_RPC_SERVEUR__EREFERENCE0, AttachementRPCServeur.class, msgs);
			if (newAttachement_RS != null)
				msgs = ((InternalEObject)newAttachement_RS).eInverseAdd(this, M1Package.ATTACHEMENT_RPC_SERVEUR__EREFERENCE0, AttachementRPCServeur.class, msgs);
			msgs = basicSetAttachement_RS(newAttachement_RS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ROLE_REQUIE_RS__ATTACHEMENT_RS, newAttachement_RS, newAttachement_RS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_RS__ATTACHEMENT_RS:
				if (attachement_RS != null)
					msgs = ((InternalEObject)attachement_RS).eInverseRemove(this, M1Package.ATTACHEMENT_RPC_SERVEUR__EREFERENCE0, AttachementRPCServeur.class, msgs);
				return basicSetAttachement_RS((AttachementRPCServeur)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_RS__ATTACHEMENT_RS:
				return basicSetAttachement_RS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_RS__ATTACHEMENT_RS:
				if (resolve) return getAttachement_RS();
				return basicGetAttachement_RS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_RS__ATTACHEMENT_RS:
				setAttachement_RS((AttachementRPCServeur)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_RS__ATTACHEMENT_RS:
				setAttachement_RS((AttachementRPCServeur)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ROLE_REQUIE_RS__ATTACHEMENT_RS:
				return attachement_RS != null;
		}
		return super.eIsSet(featureID);
	}

} //RoleRequie_RSImpl
