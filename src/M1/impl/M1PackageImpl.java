/**
 */
package M1.impl;

import M1.Atachement_DB_1;
import M1.AttachementClientRPC;
import M1.AttachementRPCClient;
import M1.AttachementRPCServeur;
import M1.AttachementServeurRPC;
import M1.Attachement_DB_2;
import M1.Attachement_DB_3;
import M1.Attachement_DB_4;
import M1.Attachement_GC_1;
import M1.Attachement_GC_2;
import M1.Attachement_GC_3;
import M1.Attachement_GC_4;
import M1.Attachement_GS_1;
import M1.Attachement_GS_2;
import M1.Attachement_GS_3;
import M1.Attachement_GS_4;
import M1.Binding_CM_S;
import M1.Binding_S_RPC;
import M1.Client;
import M1.ClientServeur;
import M1.ConnecteurRPC;
import M1.Connecteur_GC_DB;
import M1.Connecteur_GC_GS;
import M1.Connecteur_GS_DB;
import M1.DataBase;
import M1.Gestion_Securiterity;
import M1.Gestionnaire_Connexion;
import M1.IClient;
import M1.IConnecteurRPC;
import M1.IConnecteur_GC_DB;
import M1.IConnecteur_GC_GS;
import M1.IConnecteur_GS_DB;
import M1.IDataBase;
import M1.IGestion_Securite;
import M1.IGestionnaire_Connexion;
import M1.IServeur;
import M1.M1Factory;
import M1.M1Package;
import M1.PortFournie_RS;
import M1.PortRequie_SR;
import M1.Port_CM_S;
import M1.Port_Fournie_GC_1;
import M1.Port_Fournie_GC_3;
import M1.Port_Fournie_GS_1;
import M1.Port_Fournie_GS_3;
import M1.Port_RPC_S;
import M1.Port_Requie_DB_1;
import M1.Port_Requie_GC_2;
import M1.Port_Requie_GC_4;
import M1.Port_Requie_GS_2;
import M1.Port_Requie_GS_4;
import M1.Port_S_CM;
import M1.Port_S_RPC;
import M1.Port_fornie_DB_4;
import M1.Port_fournie_DB_2;
import M1.Port_requie_DB_3;
import M1.ReceveResponse;
import M1.RoleFournie_RC;
import M1.RoleFournie_SR;
import M1.RoleRequie_CR;
import M1.RoleRequie_RS;
import M1.Role_Fournie_GC_2;
import M1.Role_Fournie_GC_4;
import M1.Role_Fournie_GS_2;
import M1.Role_Fournie_GS_4;
import M1.Role_Requie_DB_2;
import M1.Role_Requie_DB_4;
import M1.Role_Requie_GC_1;
import M1.Role_Requie_GC_3;
import M1.Role_Requie_GS_1;
import M1.Role_Requie_GS_3;
import M1.Role_fournie_DB_1;
import M1.Role_fournie_DB_3;
import M1.SendRequest;
import M1.Server_Configuration;
import M1.Serveur;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class M1PackageImpl extends EPackageImpl implements M1Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass server_ConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataBaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iDataBaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass atachement_DB_1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachement_DB_2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachement_DB_3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachement_DB_4EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_Requie_DB_4EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_fornie_DB_4EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_fournie_DB_3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_requie_DB_3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_Requie_DB_2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_fournie_DB_1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_Requie_DB_1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_fournie_DB_2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gestionnaire_ConnexionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iGestionnaire_ConnexionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachement_GC_2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachement_GC_3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachement_GC_4EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachement_GC_1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_Requie_GC_3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_Requie_GC_4EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_Fournie_GC_3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_Fournie_GC_4EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_Fournie_GC_2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_Requie_GC_2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_Requie_GC_1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_Fournie_GC_1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iConnecteur_GC_DBEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connecteur_GC_DBEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gestion_SecuriterityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachement_GS_1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_Requie_GS_2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachement_GS_2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachement_GS_3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_Fournie_GS_1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_Requie_GS_1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_Fournie_GS_2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_Fournie_GS_3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_Requie_GS_3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachement_GS_4EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_Requie_GS_4EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass role_Fournie_GS_4EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iGestion_SecuriteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connecteur_GC_GSEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iConnecteur_GC_GSEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iConnecteur_GS_DBEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connecteur_GS_DBEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clientServeurEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clientEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connecteurRPCEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serveurEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iClientEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iConnecteurRPCEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iServeurEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sendRequestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachementClientRPCEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleRequie_CREClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachementRPCClientEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleFournie_RCEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass receveResponseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachementRPCServeurEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleRequie_RSEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portFournie_RSEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attachementServeurRPCEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleFournie_SREClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portRequie_SREClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binding_S_RPCEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_RPC_SEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_S_RPCEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binding_CM_SEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_S_CMEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass port_CM_SEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see M1.M1Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private M1PackageImpl() {
		super(eNS_URI, M1Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link M1Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static M1Package init() {
		if (isInited) return (M1Package)EPackage.Registry.INSTANCE.getEPackage(M1Package.eNS_URI);

		// Obtain or create and register package
		M1PackageImpl theM1Package = (M1PackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof M1PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new M1PackageImpl());

		isInited = true;

		// Create package meta-data objects
		theM1Package.createPackageContents();

		// Initialize created meta-data
		theM1Package.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theM1Package.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(M1Package.eNS_URI, theM1Package);
		return theM1Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServer_Configuration() {
		return server_ConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServer_Configuration_EReference0() {
		return (EReference)server_ConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServer_Configuration_DataBase() {
		return (EReference)server_ConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServer_Configuration_Gestionnaire_Connexion() {
		return (EReference)server_ConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServer_Configuration_Connecteur_GC_DB() {
		return (EReference)server_ConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServer_Configuration_Gestion_securite() {
		return (EReference)server_ConfigurationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServer_Configuration_Connecteur_GC_GS() {
		return (EReference)server_ConfigurationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServer_Configuration_Connecteur_GS_DB() {
		return (EReference)server_ConfigurationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataBase() {
		return dataBaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataBase_IdataBase() {
		return (EReference)dataBaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIDataBase() {
		return iDataBaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIDataBase_Port_requie_DB_1() {
		return (EReference)iDataBaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIDataBase_Port_fournie_DB_2() {
		return (EReference)iDataBaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIDataBase_Port_requie_DB_3() {
		return (EReference)iDataBaseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIDataBase_Port_fournie_DB_4() {
		return (EReference)iDataBaseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAtachement_DB_1() {
		return atachement_DB_1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtachement_DB_1_Role_fournie_DB_1() {
		return (EReference)atachement_DB_1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAtachement_DB_1_Port_requie_DB_1() {
		return (EReference)atachement_DB_1EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachement_DB_2() {
		return attachement_DB_2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_DB_2_Role_requie_2() {
		return (EReference)attachement_DB_2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_DB_2_Port_fournie_DB_2() {
		return (EReference)attachement_DB_2EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachement_DB_3() {
		return attachement_DB_3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_DB_3_Role_fourni_DB_3() {
		return (EReference)attachement_DB_3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_DB_3_Port_requie_DB_3() {
		return (EReference)attachement_DB_3EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachement_DB_4() {
		return attachement_DB_4EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_DB_4_Role_Requie_4() {
		return (EReference)attachement_DB_4EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_DB_4_Port_fourne_4() {
		return (EReference)attachement_DB_4EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_Requie_DB_4() {
		return role_Requie_DB_4EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Requie_DB_4_Attachement_DB_4() {
		return (EReference)role_Requie_DB_4EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_fornie_DB_4() {
		return port_fornie_DB_4EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_fornie_DB_4_Attachement_DB_4() {
		return (EReference)port_fornie_DB_4EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_fournie_DB_3() {
		return role_fournie_DB_3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_fournie_DB_3_Attachement_DB_3() {
		return (EReference)role_fournie_DB_3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_requie_DB_3() {
		return port_requie_DB_3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_requie_DB_3_Attachement_DB_3() {
		return (EReference)port_requie_DB_3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_Requie_DB_2() {
		return role_Requie_DB_2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Requie_DB_2_Attachement_DB_2() {
		return (EReference)role_Requie_DB_2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_fournie_DB_1() {
		return role_fournie_DB_1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_fournie_DB_1_Attachement_DB_1() {
		return (EReference)role_fournie_DB_1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_Requie_DB_1() {
		return port_Requie_DB_1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Requie_DB_1_Attachement_DB_1() {
		return (EReference)port_Requie_DB_1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_fournie_DB_2() {
		return port_fournie_DB_2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_fournie_DB_2_Attachement_DB_2() {
		return (EReference)port_fournie_DB_2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGestionnaire_Connexion() {
		return gestionnaire_ConnexionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGestionnaire_Connexion_IgestionnaireConnexion() {
		return (EReference)gestionnaire_ConnexionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIGestionnaire_Connexion() {
		return iGestionnaire_ConnexionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIGestionnaire_Connexion_Port_Fournie_Gc_1() {
		return (EReference)iGestionnaire_ConnexionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIGestionnaire_Connexion_Port_Requie_Gc_2() {
		return (EReference)iGestionnaire_ConnexionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIGestionnaire_Connexion_Port_Fournie_Gc_3() {
		return (EReference)iGestionnaire_ConnexionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIGestionnaire_Connexion_Port_Requie_Gc_4() {
		return (EReference)iGestionnaire_ConnexionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIGestionnaire_Connexion_Port_CM_S() {
		return (EReference)iGestionnaire_ConnexionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachement_GC_2() {
		return attachement_GC_2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GC_2_EReference0() {
		return (EReference)attachement_GC_2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GC_2_EReference1() {
		return (EReference)attachement_GC_2EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachement_GC_3() {
		return attachement_GC_3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GC_3_EReference0() {
		return (EReference)attachement_GC_3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GC_3_EReference1() {
		return (EReference)attachement_GC_3EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachement_GC_4() {
		return attachement_GC_4EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GC_4_EReference0() {
		return (EReference)attachement_GC_4EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GC_4_EReference1() {
		return (EReference)attachement_GC_4EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachement_GC_1() {
		return attachement_GC_1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GC_1_EReference0() {
		return (EReference)attachement_GC_1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GC_1_EReference1() {
		return (EReference)attachement_GC_1EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_Requie_GC_3() {
		return role_Requie_GC_3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Requie_GC_3_EReference0() {
		return (EReference)role_Requie_GC_3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_Requie_GC_4() {
		return port_Requie_GC_4EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Requie_GC_4_EReference0() {
		return (EReference)port_Requie_GC_4EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_Fournie_GC_3() {
		return port_Fournie_GC_3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Fournie_GC_3_EReference0() {
		return (EReference)port_Fournie_GC_3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_Fournie_GC_4() {
		return role_Fournie_GC_4EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Fournie_GC_4_EReference0() {
		return (EReference)role_Fournie_GC_4EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_Fournie_GC_2() {
		return role_Fournie_GC_2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Fournie_GC_2_EReference0() {
		return (EReference)role_Fournie_GC_2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_Requie_GC_2() {
		return port_Requie_GC_2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Requie_GC_2_EReference0() {
		return (EReference)port_Requie_GC_2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_Requie_GC_1() {
		return role_Requie_GC_1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Requie_GC_1_EReference0() {
		return (EReference)role_Requie_GC_1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_Fournie_GC_1() {
		return port_Fournie_GC_1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Fournie_GC_1_EReference0() {
		return (EReference)port_Fournie_GC_1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIConnecteur_GC_DB() {
		return iConnecteur_GC_DBEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GC_DB_EReference0() {
		return (EReference)iConnecteur_GC_DBEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GC_DB_EReference1() {
		return (EReference)iConnecteur_GC_DBEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GC_DB_EReference2() {
		return (EReference)iConnecteur_GC_DBEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GC_DB_EReference3() {
		return (EReference)iConnecteur_GC_DBEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnecteur_GC_DB() {
		return connecteur_GC_DBEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnecteur_GC_DB_IconnecteurBCDB() {
		return (EReference)connecteur_GC_DBEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGestion_Securiterity() {
		return gestion_SecuriterityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGestion_Securiterity_IgestionSecurite() {
		return (EReference)gestion_SecuriterityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachement_GS_1() {
		return attachement_GS_1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GS_1_Port_fournie_GS_1() {
		return (EReference)attachement_GS_1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GS_1_Role_requie_GS_1() {
		return (EReference)attachement_GS_1EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_Requie_GS_2() {
		return port_Requie_GS_2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Requie_GS_2_Attachement_GS_2() {
		return (EReference)port_Requie_GS_2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachement_GS_2() {
		return attachement_GS_2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GS_2_Port_requie_GS_2() {
		return (EReference)attachement_GS_2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GS_2_Role_fournie_GS_2() {
		return (EReference)attachement_GS_2EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachement_GS_3() {
		return attachement_GS_3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GS_3_Port_fournie_GS_3() {
		return (EReference)attachement_GS_3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GS_3_Role_requie_GS_3() {
		return (EReference)attachement_GS_3EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_Fournie_GS_1() {
		return port_Fournie_GS_1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Fournie_GS_1_Attachement_GS_1() {
		return (EReference)port_Fournie_GS_1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_Requie_GS_1() {
		return role_Requie_GS_1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Requie_GS_1_Attachement_GS_1() {
		return (EReference)role_Requie_GS_1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_Fournie_GS_2() {
		return role_Fournie_GS_2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Fournie_GS_2_Attachement_GS_2() {
		return (EReference)role_Fournie_GS_2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_Fournie_GS_3() {
		return port_Fournie_GS_3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Fournie_GS_3_Attachement_GS_3() {
		return (EReference)port_Fournie_GS_3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_Requie_GS_3() {
		return role_Requie_GS_3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Requie_GS_3_Attachement_GS_3() {
		return (EReference)role_Requie_GS_3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachement_GS_4() {
		return attachement_GS_4EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GS_4_Port_requie_GS_4() {
		return (EReference)attachement_GS_4EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachement_GS_4_Role_fournie_GS_4() {
		return (EReference)attachement_GS_4EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_Requie_GS_4() {
		return port_Requie_GS_4EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Requie_GS_4_Attachement_GS_4() {
		return (EReference)port_Requie_GS_4EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole_Fournie_GS_4() {
		return role_Fournie_GS_4EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Fournie_GS_4_Attachement_GS_4() {
		return (EReference)role_Fournie_GS_4EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIGestion_Securite() {
		return iGestion_SecuriteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIGestion_Securite_Port_fournie_GS_1() {
		return (EReference)iGestion_SecuriteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIGestion_Securite_Port_requie_GS_2() {
		return (EReference)iGestion_SecuriteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIGestion_Securite_Port_fournie_GS_3() {
		return (EReference)iGestion_SecuriteEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIGestion_Securite_Port_requie_GS_4() {
		return (EReference)iGestion_SecuriteEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnecteur_GC_GS() {
		return connecteur_GC_GSEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnecteur_GC_GS_Iconnecteur_GC_GS() {
		return (EReference)connecteur_GC_GSEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIConnecteur_GC_GS() {
		return iConnecteur_GC_GSEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GC_GS_Role_requie_GC_3() {
		return (EReference)iConnecteur_GC_GSEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GC_GS_Role_fournie_GC_4() {
		return (EReference)iConnecteur_GC_GSEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GC_GS_Role_requie_GS_1() {
		return (EReference)iConnecteur_GC_GSEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GC_GS_Role_fournie_GS_2() {
		return (EReference)iConnecteur_GC_GSEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIConnecteur_GS_DB() {
		return iConnecteur_GS_DBEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GS_DB_Role_fournie_DB_3() {
		return (EReference)iConnecteur_GS_DBEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GS_DB_Role_requie_DB_4() {
		return (EReference)iConnecteur_GS_DBEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GS_DB_Role_fournie_GS_4() {
		return (EReference)iConnecteur_GS_DBEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteur_GS_DB_Role_requie_GS_3() {
		return (EReference)iConnecteur_GS_DBEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnecteur_GS_DB() {
		return connecteur_GS_DBEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnecteur_GS_DB_Iconnecteur_GS_DB() {
		return (EReference)connecteur_GS_DBEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClientServeur() {
		return clientServeurEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClientServeur_Client() {
		return (EReference)clientServeurEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClientServeur_ConnecteurRPC() {
		return (EReference)clientServeurEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClientServeur_Serveur() {
		return (EReference)clientServeurEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClient() {
		return clientEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClient_InterfaceClient() {
		return (EReference)clientEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnecteurRPC() {
		return connecteurRPCEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnecteurRPC_InterfaceConnecteur() {
		return (EReference)connecteurRPCEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServeur() {
		return serveurEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServeur_InterfaceServeur() {
		return (EReference)serveurEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServeur_Serveur_Config() {
		return (EReference)serveurEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIClient() {
		return iClientEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIClient_SendRequest() {
		return (EReference)iClientEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIClient_ReceveRequest() {
		return (EReference)iClientEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIConnecteurRPC() {
		return iConnecteurRPCEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteurRPC_RoleRequie_CR() {
		return (EReference)iConnecteurRPCEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteurRPC_RoleFournie_RC() {
		return (EReference)iConnecteurRPCEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteurRPC_RoleRequie_RS() {
		return (EReference)iConnecteurRPCEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteurRPC_RoleFournie_SR() {
		return (EReference)iConnecteurRPCEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIConnecteurRPC_Port_RPC_S() {
		return (EReference)iConnecteurRPCEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIServeur() {
		return iServeurEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIServeur_PortFournie_RS() {
		return (EReference)iServeurEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIServeur_PortRequie_SR() {
		return (EReference)iServeurEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIServeur_Pert_S_CM() {
		return (EReference)iServeurEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIServeur_Port_S_RPC() {
		return (EReference)iServeurEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSendRequest() {
		return sendRequestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSendRequest_Attachement_CR() {
		return (EReference)sendRequestEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachementClientRPC() {
		return attachementClientRPCEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachementClientRPC_SendRequest() {
		return (EReference)attachementClientRPCEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachementClientRPC_RoleRequie_CR() {
		return (EReference)attachementClientRPCEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleRequie_CR() {
		return roleRequie_CREClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleRequie_CR_Attachement_CR() {
		return (EReference)roleRequie_CREClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachementRPCClient() {
		return attachementRPCClientEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachementRPCClient_ReceveResponce() {
		return (EReference)attachementRPCClientEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachementRPCClient_RoleFourie_RC() {
		return (EReference)attachementRPCClientEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleFournie_RC() {
		return roleFournie_RCEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleFournie_RC_AttachementRC() {
		return (EReference)roleFournie_RCEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReceveResponse() {
		return receveResponseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReceveResponse_AttachementRC() {
		return (EReference)receveResponseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachementRPCServeur() {
		return attachementRPCServeurEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachementRPCServeur_EReference0() {
		return (EReference)attachementRPCServeurEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachementRPCServeur_EReference1() {
		return (EReference)attachementRPCServeurEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleRequie_RS() {
		return roleRequie_RSEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleRequie_RS_Attachement_RS() {
		return (EReference)roleRequie_RSEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortFournie_RS() {
		return portFournie_RSEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortFournie_RS_Attachement_RS() {
		return (EReference)portFournie_RSEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttachementServeurRPC() {
		return attachementServeurRPCEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachementServeurRPC_EReference0() {
		return (EReference)attachementServeurRPCEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttachementServeurRPC_EReference1() {
		return (EReference)attachementServeurRPCEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleFournie_SR() {
		return roleFournie_SREClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleFournie_SR_Attachement_SR() {
		return (EReference)roleFournie_SREClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortRequie_SR() {
		return portRequie_SREClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortRequie_SR_Attachement_SR() {
		return (EReference)portRequie_SREClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBinding_S_RPC() {
		return binding_S_RPCEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinding_S_RPC_EReference0() {
		return (EReference)binding_S_RPCEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinding_S_RPC_EReference1() {
		return (EReference)binding_S_RPCEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_RPC_S() {
		return port_RPC_SEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_RPC_S_EReference0() {
		return (EReference)port_RPC_SEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_S_RPC() {
		return port_S_RPCEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_S_RPC_EReference0() {
		return (EReference)port_S_RPCEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBinding_CM_S() {
		return binding_CM_SEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinding_CM_S_EReference0() {
		return (EReference)binding_CM_SEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinding_CM_S_EReference1() {
		return (EReference)binding_CM_SEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_S_CM() {
		return port_S_CMEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_S_CM_EReference0() {
		return (EReference)port_S_CMEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort_CM_S() {
		return port_CM_SEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_CM_S_EReference0() {
		return (EReference)port_CM_SEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public M1Factory getM1Factory() {
		return (M1Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		server_ConfigurationEClass = createEClass(SERVER_CONFIGURATION);
		createEReference(server_ConfigurationEClass, SERVER_CONFIGURATION__EREFERENCE0);
		createEReference(server_ConfigurationEClass, SERVER_CONFIGURATION__DATA_BASE);
		createEReference(server_ConfigurationEClass, SERVER_CONFIGURATION__GESTIONNAIRE_CONNEXION);
		createEReference(server_ConfigurationEClass, SERVER_CONFIGURATION__CONNECTEUR_GC_DB);
		createEReference(server_ConfigurationEClass, SERVER_CONFIGURATION__GESTION_SECURITE);
		createEReference(server_ConfigurationEClass, SERVER_CONFIGURATION__CONNECTEUR_GC_GS);
		createEReference(server_ConfigurationEClass, SERVER_CONFIGURATION__CONNECTEUR_GS_DB);

		dataBaseEClass = createEClass(DATA_BASE);
		createEReference(dataBaseEClass, DATA_BASE__IDATA_BASE);

		iDataBaseEClass = createEClass(IDATA_BASE);
		createEReference(iDataBaseEClass, IDATA_BASE__PORT_REQUIE_DB_1);
		createEReference(iDataBaseEClass, IDATA_BASE__PORT_FOURNIE_DB_2);
		createEReference(iDataBaseEClass, IDATA_BASE__PORT_REQUIE_DB_3);
		createEReference(iDataBaseEClass, IDATA_BASE__PORT_FOURNIE_DB_4);

		atachement_DB_1EClass = createEClass(ATACHEMENT_DB_1);
		createEReference(atachement_DB_1EClass, ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1);
		createEReference(atachement_DB_1EClass, ATACHEMENT_DB_1__PORT_REQUIE_DB_1);

		attachement_DB_2EClass = createEClass(ATTACHEMENT_DB_2);
		createEReference(attachement_DB_2EClass, ATTACHEMENT_DB_2__ROLE_REQUIE_2);
		createEReference(attachement_DB_2EClass, ATTACHEMENT_DB_2__PORT_FOURNIE_DB_2);

		attachement_DB_3EClass = createEClass(ATTACHEMENT_DB_3);
		createEReference(attachement_DB_3EClass, ATTACHEMENT_DB_3__ROLE_FOURNI_DB_3);
		createEReference(attachement_DB_3EClass, ATTACHEMENT_DB_3__PORT_REQUIE_DB_3);

		attachement_DB_4EClass = createEClass(ATTACHEMENT_DB_4);
		createEReference(attachement_DB_4EClass, ATTACHEMENT_DB_4__ROLE_REQUIE_4);
		createEReference(attachement_DB_4EClass, ATTACHEMENT_DB_4__PORT_FOURNE_4);

		role_Requie_DB_4EClass = createEClass(ROLE_REQUIE_DB_4);
		createEReference(role_Requie_DB_4EClass, ROLE_REQUIE_DB_4__ATTACHEMENT_DB_4);

		port_fornie_DB_4EClass = createEClass(PORT_FORNIE_DB_4);
		createEReference(port_fornie_DB_4EClass, PORT_FORNIE_DB_4__ATTACHEMENT_DB_4);

		role_fournie_DB_3EClass = createEClass(ROLE_FOURNIE_DB_3);
		createEReference(role_fournie_DB_3EClass, ROLE_FOURNIE_DB_3__ATTACHEMENT_DB_3);

		port_requie_DB_3EClass = createEClass(PORT_REQUIE_DB_3);
		createEReference(port_requie_DB_3EClass, PORT_REQUIE_DB_3__ATTACHEMENT_DB_3);

		role_Requie_DB_2EClass = createEClass(ROLE_REQUIE_DB_2);
		createEReference(role_Requie_DB_2EClass, ROLE_REQUIE_DB_2__ATTACHEMENT_DB_2);

		role_fournie_DB_1EClass = createEClass(ROLE_FOURNIE_DB_1);
		createEReference(role_fournie_DB_1EClass, ROLE_FOURNIE_DB_1__ATTACHEMENT_DB_1);

		port_Requie_DB_1EClass = createEClass(PORT_REQUIE_DB_1);
		createEReference(port_Requie_DB_1EClass, PORT_REQUIE_DB_1__ATTACHEMENT_DB_1);

		port_fournie_DB_2EClass = createEClass(PORT_FOURNIE_DB_2);
		createEReference(port_fournie_DB_2EClass, PORT_FOURNIE_DB_2__ATTACHEMENT_DB_2);

		gestionnaire_ConnexionEClass = createEClass(GESTIONNAIRE_CONNEXION);
		createEReference(gestionnaire_ConnexionEClass, GESTIONNAIRE_CONNEXION__IGESTIONNAIRE_CONNEXION);

		iGestionnaire_ConnexionEClass = createEClass(IGESTIONNAIRE_CONNEXION);
		createEReference(iGestionnaire_ConnexionEClass, IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_1);
		createEReference(iGestionnaire_ConnexionEClass, IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_2);
		createEReference(iGestionnaire_ConnexionEClass, IGESTIONNAIRE_CONNEXION__PORT_FOURNIE_GC_3);
		createEReference(iGestionnaire_ConnexionEClass, IGESTIONNAIRE_CONNEXION__PORT_REQUIE_GC_4);
		createEReference(iGestionnaire_ConnexionEClass, IGESTIONNAIRE_CONNEXION__PORT_CM_S);

		attachement_GC_2EClass = createEClass(ATTACHEMENT_GC_2);
		createEReference(attachement_GC_2EClass, ATTACHEMENT_GC_2__EREFERENCE0);
		createEReference(attachement_GC_2EClass, ATTACHEMENT_GC_2__EREFERENCE1);

		attachement_GC_3EClass = createEClass(ATTACHEMENT_GC_3);
		createEReference(attachement_GC_3EClass, ATTACHEMENT_GC_3__EREFERENCE0);
		createEReference(attachement_GC_3EClass, ATTACHEMENT_GC_3__EREFERENCE1);

		attachement_GC_4EClass = createEClass(ATTACHEMENT_GC_4);
		createEReference(attachement_GC_4EClass, ATTACHEMENT_GC_4__EREFERENCE0);
		createEReference(attachement_GC_4EClass, ATTACHEMENT_GC_4__EREFERENCE1);

		attachement_GC_1EClass = createEClass(ATTACHEMENT_GC_1);
		createEReference(attachement_GC_1EClass, ATTACHEMENT_GC_1__EREFERENCE0);
		createEReference(attachement_GC_1EClass, ATTACHEMENT_GC_1__EREFERENCE1);

		role_Requie_GC_3EClass = createEClass(ROLE_REQUIE_GC_3);
		createEReference(role_Requie_GC_3EClass, ROLE_REQUIE_GC_3__EREFERENCE0);

		port_Requie_GC_4EClass = createEClass(PORT_REQUIE_GC_4);
		createEReference(port_Requie_GC_4EClass, PORT_REQUIE_GC_4__EREFERENCE0);

		port_Fournie_GC_3EClass = createEClass(PORT_FOURNIE_GC_3);
		createEReference(port_Fournie_GC_3EClass, PORT_FOURNIE_GC_3__EREFERENCE0);

		role_Fournie_GC_4EClass = createEClass(ROLE_FOURNIE_GC_4);
		createEReference(role_Fournie_GC_4EClass, ROLE_FOURNIE_GC_4__EREFERENCE0);

		role_Fournie_GC_2EClass = createEClass(ROLE_FOURNIE_GC_2);
		createEReference(role_Fournie_GC_2EClass, ROLE_FOURNIE_GC_2__EREFERENCE0);

		port_Requie_GC_2EClass = createEClass(PORT_REQUIE_GC_2);
		createEReference(port_Requie_GC_2EClass, PORT_REQUIE_GC_2__EREFERENCE0);

		role_Requie_GC_1EClass = createEClass(ROLE_REQUIE_GC_1);
		createEReference(role_Requie_GC_1EClass, ROLE_REQUIE_GC_1__EREFERENCE0);

		port_Fournie_GC_1EClass = createEClass(PORT_FOURNIE_GC_1);
		createEReference(port_Fournie_GC_1EClass, PORT_FOURNIE_GC_1__EREFERENCE0);

		iConnecteur_GC_DBEClass = createEClass(ICONNECTEUR_GC_DB);
		createEReference(iConnecteur_GC_DBEClass, ICONNECTEUR_GC_DB__EREFERENCE0);
		createEReference(iConnecteur_GC_DBEClass, ICONNECTEUR_GC_DB__EREFERENCE1);
		createEReference(iConnecteur_GC_DBEClass, ICONNECTEUR_GC_DB__EREFERENCE2);
		createEReference(iConnecteur_GC_DBEClass, ICONNECTEUR_GC_DB__EREFERENCE3);

		connecteur_GC_DBEClass = createEClass(CONNECTEUR_GC_DB);
		createEReference(connecteur_GC_DBEClass, CONNECTEUR_GC_DB__ICONNECTEUR_BCDB);

		gestion_SecuriterityEClass = createEClass(GESTION_SECURITERITY);
		createEReference(gestion_SecuriterityEClass, GESTION_SECURITERITY__IGESTION_SECURITE);

		attachement_GS_1EClass = createEClass(ATTACHEMENT_GS_1);
		createEReference(attachement_GS_1EClass, ATTACHEMENT_GS_1__PORT_FOURNIE_GS_1);
		createEReference(attachement_GS_1EClass, ATTACHEMENT_GS_1__ROLE_REQUIE_GS_1);

		port_Requie_GS_2EClass = createEClass(PORT_REQUIE_GS_2);
		createEReference(port_Requie_GS_2EClass, PORT_REQUIE_GS_2__ATTACHEMENT_GS_2);

		attachement_GS_2EClass = createEClass(ATTACHEMENT_GS_2);
		createEReference(attachement_GS_2EClass, ATTACHEMENT_GS_2__PORT_REQUIE_GS_2);
		createEReference(attachement_GS_2EClass, ATTACHEMENT_GS_2__ROLE_FOURNIE_GS_2);

		attachement_GS_3EClass = createEClass(ATTACHEMENT_GS_3);
		createEReference(attachement_GS_3EClass, ATTACHEMENT_GS_3__PORT_FOURNIE_GS_3);
		createEReference(attachement_GS_3EClass, ATTACHEMENT_GS_3__ROLE_REQUIE_GS_3);

		port_Fournie_GS_1EClass = createEClass(PORT_FOURNIE_GS_1);
		createEReference(port_Fournie_GS_1EClass, PORT_FOURNIE_GS_1__ATTACHEMENT_GS_1);

		role_Requie_GS_1EClass = createEClass(ROLE_REQUIE_GS_1);
		createEReference(role_Requie_GS_1EClass, ROLE_REQUIE_GS_1__ATTACHEMENT_GS_1);

		role_Fournie_GS_2EClass = createEClass(ROLE_FOURNIE_GS_2);
		createEReference(role_Fournie_GS_2EClass, ROLE_FOURNIE_GS_2__ATTACHEMENT_GS_2);

		port_Fournie_GS_3EClass = createEClass(PORT_FOURNIE_GS_3);
		createEReference(port_Fournie_GS_3EClass, PORT_FOURNIE_GS_3__ATTACHEMENT_GS_3);

		role_Requie_GS_3EClass = createEClass(ROLE_REQUIE_GS_3);
		createEReference(role_Requie_GS_3EClass, ROLE_REQUIE_GS_3__ATTACHEMENT_GS_3);

		attachement_GS_4EClass = createEClass(ATTACHEMENT_GS_4);
		createEReference(attachement_GS_4EClass, ATTACHEMENT_GS_4__PORT_REQUIE_GS_4);
		createEReference(attachement_GS_4EClass, ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4);

		port_Requie_GS_4EClass = createEClass(PORT_REQUIE_GS_4);
		createEReference(port_Requie_GS_4EClass, PORT_REQUIE_GS_4__ATTACHEMENT_GS_4);

		role_Fournie_GS_4EClass = createEClass(ROLE_FOURNIE_GS_4);
		createEReference(role_Fournie_GS_4EClass, ROLE_FOURNIE_GS_4__ATTACHEMENT_GS_4);

		iGestion_SecuriteEClass = createEClass(IGESTION_SECURITE);
		createEReference(iGestion_SecuriteEClass, IGESTION_SECURITE__PORT_FOURNIE_GS_1);
		createEReference(iGestion_SecuriteEClass, IGESTION_SECURITE__PORT_REQUIE_GS_2);
		createEReference(iGestion_SecuriteEClass, IGESTION_SECURITE__PORT_FOURNIE_GS_3);
		createEReference(iGestion_SecuriteEClass, IGESTION_SECURITE__PORT_REQUIE_GS_4);

		connecteur_GC_GSEClass = createEClass(CONNECTEUR_GC_GS);
		createEReference(connecteur_GC_GSEClass, CONNECTEUR_GC_GS__ICONNECTEUR_GC_GS);

		iConnecteur_GC_GSEClass = createEClass(ICONNECTEUR_GC_GS);
		createEReference(iConnecteur_GC_GSEClass, ICONNECTEUR_GC_GS__ROLE_REQUIE_GC_3);
		createEReference(iConnecteur_GC_GSEClass, ICONNECTEUR_GC_GS__ROLE_FOURNIE_GC_4);
		createEReference(iConnecteur_GC_GSEClass, ICONNECTEUR_GC_GS__ROLE_REQUIE_GS_1);
		createEReference(iConnecteur_GC_GSEClass, ICONNECTEUR_GC_GS__ROLE_FOURNIE_GS_2);

		iConnecteur_GS_DBEClass = createEClass(ICONNECTEUR_GS_DB);
		createEReference(iConnecteur_GS_DBEClass, ICONNECTEUR_GS_DB__ROLE_FOURNIE_DB_3);
		createEReference(iConnecteur_GS_DBEClass, ICONNECTEUR_GS_DB__ROLE_REQUIE_DB_4);
		createEReference(iConnecteur_GS_DBEClass, ICONNECTEUR_GS_DB__ROLE_FOURNIE_GS_4);
		createEReference(iConnecteur_GS_DBEClass, ICONNECTEUR_GS_DB__ROLE_REQUIE_GS_3);

		connecteur_GS_DBEClass = createEClass(CONNECTEUR_GS_DB);
		createEReference(connecteur_GS_DBEClass, CONNECTEUR_GS_DB__ICONNECTEUR_GS_DB);

		clientServeurEClass = createEClass(CLIENT_SERVEUR);
		createEReference(clientServeurEClass, CLIENT_SERVEUR__CLIENT);
		createEReference(clientServeurEClass, CLIENT_SERVEUR__CONNECTEUR_RPC);
		createEReference(clientServeurEClass, CLIENT_SERVEUR__SERVEUR);

		clientEClass = createEClass(CLIENT);
		createEReference(clientEClass, CLIENT__INTERFACE_CLIENT);

		connecteurRPCEClass = createEClass(CONNECTEUR_RPC);
		createEReference(connecteurRPCEClass, CONNECTEUR_RPC__INTERFACE_CONNECTEUR);

		serveurEClass = createEClass(SERVEUR);
		createEReference(serveurEClass, SERVEUR__INTERFACE_SERVEUR);
		createEReference(serveurEClass, SERVEUR__SERVEUR_CONFIG);

		iClientEClass = createEClass(ICLIENT);
		createEReference(iClientEClass, ICLIENT__SEND_REQUEST);
		createEReference(iClientEClass, ICLIENT__RECEVE_REQUEST);

		iConnecteurRPCEClass = createEClass(ICONNECTEUR_RPC);
		createEReference(iConnecteurRPCEClass, ICONNECTEUR_RPC__ROLE_REQUIE_CR);
		createEReference(iConnecteurRPCEClass, ICONNECTEUR_RPC__ROLE_FOURNIE_RC);
		createEReference(iConnecteurRPCEClass, ICONNECTEUR_RPC__ROLE_REQUIE_RS);
		createEReference(iConnecteurRPCEClass, ICONNECTEUR_RPC__ROLE_FOURNIE_SR);
		createEReference(iConnecteurRPCEClass, ICONNECTEUR_RPC__PORT_RPC_S);

		iServeurEClass = createEClass(ISERVEUR);
		createEReference(iServeurEClass, ISERVEUR__PORT_FOURNIE_RS);
		createEReference(iServeurEClass, ISERVEUR__PORT_REQUIE_SR);
		createEReference(iServeurEClass, ISERVEUR__PERT_SCM);
		createEReference(iServeurEClass, ISERVEUR__PORT_SRPC);

		sendRequestEClass = createEClass(SEND_REQUEST);
		createEReference(sendRequestEClass, SEND_REQUEST__ATTACHEMENT_CR);

		attachementClientRPCEClass = createEClass(ATTACHEMENT_CLIENT_RPC);
		createEReference(attachementClientRPCEClass, ATTACHEMENT_CLIENT_RPC__SEND_REQUEST);
		createEReference(attachementClientRPCEClass, ATTACHEMENT_CLIENT_RPC__ROLE_REQUIE_CR);

		roleRequie_CREClass = createEClass(ROLE_REQUIE_CR);
		createEReference(roleRequie_CREClass, ROLE_REQUIE_CR__ATTACHEMENT_CR);

		attachementRPCClientEClass = createEClass(ATTACHEMENT_RPC_CLIENT);
		createEReference(attachementRPCClientEClass, ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE);
		createEReference(attachementRPCClientEClass, ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC);

		roleFournie_RCEClass = createEClass(ROLE_FOURNIE_RC);
		createEReference(roleFournie_RCEClass, ROLE_FOURNIE_RC__ATTACHEMENT_RC);

		receveResponseEClass = createEClass(RECEVE_RESPONSE);
		createEReference(receveResponseEClass, RECEVE_RESPONSE__ATTACHEMENT_RC);

		attachementRPCServeurEClass = createEClass(ATTACHEMENT_RPC_SERVEUR);
		createEReference(attachementRPCServeurEClass, ATTACHEMENT_RPC_SERVEUR__EREFERENCE0);
		createEReference(attachementRPCServeurEClass, ATTACHEMENT_RPC_SERVEUR__EREFERENCE1);

		roleRequie_RSEClass = createEClass(ROLE_REQUIE_RS);
		createEReference(roleRequie_RSEClass, ROLE_REQUIE_RS__ATTACHEMENT_RS);

		portFournie_RSEClass = createEClass(PORT_FOURNIE_RS);
		createEReference(portFournie_RSEClass, PORT_FOURNIE_RS__ATTACHEMENT_RS);

		attachementServeurRPCEClass = createEClass(ATTACHEMENT_SERVEUR_RPC);
		createEReference(attachementServeurRPCEClass, ATTACHEMENT_SERVEUR_RPC__EREFERENCE0);
		createEReference(attachementServeurRPCEClass, ATTACHEMENT_SERVEUR_RPC__EREFERENCE1);

		roleFournie_SREClass = createEClass(ROLE_FOURNIE_SR);
		createEReference(roleFournie_SREClass, ROLE_FOURNIE_SR__ATTACHEMENT_SR);

		portRequie_SREClass = createEClass(PORT_REQUIE_SR);
		createEReference(portRequie_SREClass, PORT_REQUIE_SR__ATTACHEMENT_SR);

		binding_S_RPCEClass = createEClass(BINDING_SRPC);
		createEReference(binding_S_RPCEClass, BINDING_SRPC__EREFERENCE0);
		createEReference(binding_S_RPCEClass, BINDING_SRPC__EREFERENCE1);

		port_RPC_SEClass = createEClass(PORT_RPC_S);
		createEReference(port_RPC_SEClass, PORT_RPC_S__EREFERENCE0);

		port_S_RPCEClass = createEClass(PORT_SRPC);
		createEReference(port_S_RPCEClass, PORT_SRPC__EREFERENCE0);

		binding_CM_SEClass = createEClass(BINDING_CM_S);
		createEReference(binding_CM_SEClass, BINDING_CM_S__EREFERENCE0);
		createEReference(binding_CM_SEClass, BINDING_CM_S__EREFERENCE1);

		port_S_CMEClass = createEClass(PORT_SCM);
		createEReference(port_S_CMEClass, PORT_SCM__EREFERENCE0);

		port_CM_SEClass = createEClass(PORT_CM_S);
		createEReference(port_CM_SEClass, PORT_CM_S__EREFERENCE0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Add supertypes to classes
		role_Requie_DB_4EClass.getESuperTypes().add(this.getAttachement_DB_4());
		port_fornie_DB_4EClass.getESuperTypes().add(this.getAttachement_DB_4());

		// Initialize classes and features; add operations and parameters
		initEClass(server_ConfigurationEClass, Server_Configuration.class, "Server_Configuration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getServer_Configuration_EReference0(), this.getDataBase(), null, "EReference0", null, 0, 1, Server_Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServer_Configuration_DataBase(), this.getDataBase(), null, "dataBase", null, 0, -1, Server_Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServer_Configuration_Gestionnaire_Connexion(), this.getGestionnaire_Connexion(), null, "Gestionnaire_Connexion", null, 0, -1, Server_Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServer_Configuration_Connecteur_GC_DB(), this.getConnecteur_GC_DB(), null, "connecteur_GC_DB", null, 0, -1, Server_Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServer_Configuration_Gestion_securite(), this.getGestion_Securiterity(), null, "gestion_securite", null, 0, -1, Server_Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServer_Configuration_Connecteur_GC_GS(), this.getConnecteur_GC_GS(), null, "connecteur_GC_GS", null, 0, -1, Server_Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServer_Configuration_Connecteur_GS_DB(), this.getConnecteur_GS_DB(), null, "Connecteur_GS_DB", null, 0, -1, Server_Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataBaseEClass, DataBase.class, "DataBase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataBase_IdataBase(), this.getIDataBase(), null, "IdataBase", null, 0, -1, DataBase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iDataBaseEClass, IDataBase.class, "IDataBase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIDataBase_Port_requie_DB_1(), this.getPort_Requie_DB_1(), null, "port_requie_DB_1", null, 0, 1, IDataBase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIDataBase_Port_fournie_DB_2(), this.getPort_fournie_DB_2(), null, "port_fournie_DB_2", null, 0, 1, IDataBase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIDataBase_Port_requie_DB_3(), this.getPort_requie_DB_3(), null, "port_requie_DB_3", null, 0, 1, IDataBase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIDataBase_Port_fournie_DB_4(), this.getPort_fornie_DB_4(), null, "port_fournie_DB_4", null, 0, 1, IDataBase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(atachement_DB_1EClass, Atachement_DB_1.class, "Atachement_DB_1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAtachement_DB_1_Role_fournie_DB_1(), this.getRole_fournie_DB_1(), this.getRole_fournie_DB_1_Attachement_DB_1(), "role_fournie_DB_1", null, 0, 1, Atachement_DB_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAtachement_DB_1_Port_requie_DB_1(), this.getPort_Requie_DB_1(), this.getPort_Requie_DB_1_Attachement_DB_1(), "port_requie_DB_1", null, 0, 1, Atachement_DB_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachement_DB_2EClass, Attachement_DB_2.class, "Attachement_DB_2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachement_DB_2_Role_requie_2(), this.getRole_Requie_DB_2(), this.getRole_Requie_DB_2_Attachement_DB_2(), "role_requie_2", null, 0, 1, Attachement_DB_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachement_DB_2_Port_fournie_DB_2(), this.getPort_fournie_DB_2(), this.getPort_fournie_DB_2_Attachement_DB_2(), "port_fournie_DB_2", null, 0, 1, Attachement_DB_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachement_DB_3EClass, Attachement_DB_3.class, "Attachement_DB_3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachement_DB_3_Role_fourni_DB_3(), this.getRole_fournie_DB_3(), this.getRole_fournie_DB_3_Attachement_DB_3(), "role_fourni_DB_3", null, 0, 1, Attachement_DB_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachement_DB_3_Port_requie_DB_3(), this.getPort_requie_DB_3(), this.getPort_requie_DB_3_Attachement_DB_3(), "port_requie_DB_3", null, 0, 1, Attachement_DB_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachement_DB_4EClass, Attachement_DB_4.class, "Attachement_DB_4", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachement_DB_4_Role_Requie_4(), this.getRole_Requie_DB_4(), this.getRole_Requie_DB_4_Attachement_DB_4(), "Role_Requie_4", null, 0, 1, Attachement_DB_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachement_DB_4_Port_fourne_4(), this.getPort_fornie_DB_4(), this.getPort_fornie_DB_4_Attachement_DB_4(), "port_fourne_4", null, 0, 1, Attachement_DB_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_Requie_DB_4EClass, Role_Requie_DB_4.class, "Role_Requie_DB_4", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Requie_DB_4_Attachement_DB_4(), this.getAttachement_DB_4(), this.getAttachement_DB_4_Role_Requie_4(), "attachement_DB_4", null, 0, 1, Role_Requie_DB_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_fornie_DB_4EClass, Port_fornie_DB_4.class, "Port_fornie_DB_4", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_fornie_DB_4_Attachement_DB_4(), this.getAttachement_DB_4(), this.getAttachement_DB_4_Port_fourne_4(), "attachement_DB_4", null, 0, 1, Port_fornie_DB_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_fournie_DB_3EClass, Role_fournie_DB_3.class, "Role_fournie_DB_3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_fournie_DB_3_Attachement_DB_3(), this.getAttachement_DB_3(), this.getAttachement_DB_3_Role_fourni_DB_3(), "attachement_DB_3", null, 0, 1, Role_fournie_DB_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_requie_DB_3EClass, Port_requie_DB_3.class, "Port_requie_DB_3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_requie_DB_3_Attachement_DB_3(), this.getAttachement_DB_3(), this.getAttachement_DB_3_Port_requie_DB_3(), "attachement_DB_3", null, 0, 1, Port_requie_DB_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_Requie_DB_2EClass, Role_Requie_DB_2.class, "Role_Requie_DB_2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Requie_DB_2_Attachement_DB_2(), this.getAttachement_DB_2(), this.getAttachement_DB_2_Role_requie_2(), "attachement_DB_2", null, 0, 1, Role_Requie_DB_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_fournie_DB_1EClass, Role_fournie_DB_1.class, "Role_fournie_DB_1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_fournie_DB_1_Attachement_DB_1(), this.getAtachement_DB_1(), this.getAtachement_DB_1_Role_fournie_DB_1(), "attachement_DB_1", null, 0, 1, Role_fournie_DB_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_Requie_DB_1EClass, Port_Requie_DB_1.class, "Port_Requie_DB_1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_Requie_DB_1_Attachement_DB_1(), this.getAtachement_DB_1(), this.getAtachement_DB_1_Port_requie_DB_1(), "attachement_DB_1", null, 0, 1, Port_Requie_DB_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_fournie_DB_2EClass, Port_fournie_DB_2.class, "Port_fournie_DB_2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_fournie_DB_2_Attachement_DB_2(), this.getAttachement_DB_2(), this.getAttachement_DB_2_Port_fournie_DB_2(), "attachement_DB_2", null, 0, 1, Port_fournie_DB_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gestionnaire_ConnexionEClass, Gestionnaire_Connexion.class, "Gestionnaire_Connexion", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGestionnaire_Connexion_IgestionnaireConnexion(), this.getIGestionnaire_Connexion(), null, "IgestionnaireConnexion", null, 0, -1, Gestionnaire_Connexion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iGestionnaire_ConnexionEClass, IGestionnaire_Connexion.class, "IGestionnaire_Connexion", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIGestionnaire_Connexion_Port_Fournie_Gc_1(), this.getPort_Fournie_GC_1(), null, "Port_Fournie_Gc_1", null, 0, 1, IGestionnaire_Connexion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIGestionnaire_Connexion_Port_Requie_Gc_2(), this.getPort_Requie_GC_2(), null, "Port_Requie_Gc_2", null, 0, 1, IGestionnaire_Connexion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIGestionnaire_Connexion_Port_Fournie_Gc_3(), this.getPort_Fournie_GC_3(), null, "Port_Fournie_Gc_3", null, 0, 1, IGestionnaire_Connexion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIGestionnaire_Connexion_Port_Requie_Gc_4(), this.getPort_Requie_GC_4(), null, "Port_Requie_Gc_4", null, 0, 1, IGestionnaire_Connexion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIGestionnaire_Connexion_Port_CM_S(), this.getPort_CM_S(), null, "port_CM_S", null, 0, -1, IGestionnaire_Connexion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachement_GC_2EClass, Attachement_GC_2.class, "Attachement_GC_2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachement_GC_2_EReference0(), this.getPort_Requie_GC_2(), this.getPort_Requie_GC_2_EReference0(), "EReference0", null, 0, 1, Attachement_GC_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachement_GC_2_EReference1(), this.getRole_Fournie_GC_2(), this.getRole_Fournie_GC_2_EReference0(), "EReference1", null, 0, 1, Attachement_GC_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachement_GC_3EClass, Attachement_GC_3.class, "Attachement_GC_3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachement_GC_3_EReference0(), this.getRole_Requie_GC_3(), this.getRole_Requie_GC_3_EReference0(), "EReference0", null, 0, 1, Attachement_GC_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachement_GC_3_EReference1(), this.getPort_Fournie_GC_3(), this.getPort_Fournie_GC_3_EReference0(), "EReference1", null, 0, 1, Attachement_GC_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachement_GC_4EClass, Attachement_GC_4.class, "Attachement_GC_4", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachement_GC_4_EReference0(), this.getRole_Fournie_GC_4(), this.getRole_Fournie_GC_4_EReference0(), "EReference0", null, 0, 1, Attachement_GC_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachement_GC_4_EReference1(), this.getPort_Requie_GC_4(), this.getPort_Requie_GC_4_EReference0(), "EReference1", null, 0, 1, Attachement_GC_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachement_GC_1EClass, Attachement_GC_1.class, "Attachement_GC_1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachement_GC_1_EReference0(), this.getRole_Requie_GC_1(), this.getRole_Requie_GC_1_EReference0(), "EReference0", null, 0, 1, Attachement_GC_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachement_GC_1_EReference1(), this.getPort_Fournie_GC_1(), this.getPort_Fournie_GC_1_EReference0(), "EReference1", null, 0, 1, Attachement_GC_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_Requie_GC_3EClass, Role_Requie_GC_3.class, "Role_Requie_GC_3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Requie_GC_3_EReference0(), this.getAttachement_GC_3(), this.getAttachement_GC_3_EReference0(), "EReference0", null, 0, 1, Role_Requie_GC_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_Requie_GC_4EClass, Port_Requie_GC_4.class, "Port_Requie_GC_4", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_Requie_GC_4_EReference0(), this.getAttachement_GC_4(), this.getAttachement_GC_4_EReference1(), "EReference0", null, 0, 1, Port_Requie_GC_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_Fournie_GC_3EClass, Port_Fournie_GC_3.class, "Port_Fournie_GC_3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_Fournie_GC_3_EReference0(), this.getAttachement_GC_3(), this.getAttachement_GC_3_EReference1(), "EReference0", null, 0, 1, Port_Fournie_GC_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_Fournie_GC_4EClass, Role_Fournie_GC_4.class, "Role_Fournie_GC_4", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Fournie_GC_4_EReference0(), this.getAttachement_GC_4(), this.getAttachement_GC_4_EReference0(), "EReference0", null, 0, 1, Role_Fournie_GC_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_Fournie_GC_2EClass, Role_Fournie_GC_2.class, "Role_Fournie_GC_2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Fournie_GC_2_EReference0(), this.getAttachement_GC_2(), this.getAttachement_GC_2_EReference1(), "EReference0", null, 0, 1, Role_Fournie_GC_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_Requie_GC_2EClass, Port_Requie_GC_2.class, "Port_Requie_GC_2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_Requie_GC_2_EReference0(), this.getAttachement_GC_2(), this.getAttachement_GC_2_EReference0(), "EReference0", null, 0, 1, Port_Requie_GC_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_Requie_GC_1EClass, Role_Requie_GC_1.class, "Role_Requie_GC_1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Requie_GC_1_EReference0(), this.getAttachement_GC_1(), this.getAttachement_GC_1_EReference0(), "EReference0", null, 0, 1, Role_Requie_GC_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_Fournie_GC_1EClass, Port_Fournie_GC_1.class, "Port_Fournie_GC_1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_Fournie_GC_1_EReference0(), this.getAttachement_GC_1(), this.getAttachement_GC_1_EReference1(), "EReference0", null, 0, 1, Port_Fournie_GC_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iConnecteur_GC_DBEClass, IConnecteur_GC_DB.class, "IConnecteur_GC_DB", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIConnecteur_GC_DB_EReference0(), this.getRole_fournie_DB_1(), null, "EReference0", null, 0, 1, IConnecteur_GC_DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteur_GC_DB_EReference1(), this.getRole_Requie_DB_2(), null, "EReference1", null, 0, 1, IConnecteur_GC_DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteur_GC_DB_EReference2(), this.getRole_Requie_GC_1(), null, "EReference2", null, 0, 1, IConnecteur_GC_DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteur_GC_DB_EReference3(), this.getRole_Fournie_GC_2(), null, "EReference3", null, 0, 1, IConnecteur_GC_DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connecteur_GC_DBEClass, Connecteur_GC_DB.class, "Connecteur_GC_DB", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnecteur_GC_DB_IconnecteurBCDB(), this.getIConnecteur_GC_DB(), null, "IconnecteurBCDB", null, 0, -1, Connecteur_GC_DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gestion_SecuriterityEClass, Gestion_Securiterity.class, "Gestion_Securiterity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGestion_Securiterity_IgestionSecurite(), this.getIGestion_Securite(), null, "IgestionSecurite", null, 0, -1, Gestion_Securiterity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachement_GS_1EClass, Attachement_GS_1.class, "Attachement_GS_1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachement_GS_1_Port_fournie_GS_1(), this.getPort_Fournie_GS_1(), this.getPort_Fournie_GS_1_Attachement_GS_1(), "port_fournie_GS_1", null, 0, 1, Attachement_GS_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachement_GS_1_Role_requie_GS_1(), this.getRole_Requie_GS_1(), this.getRole_Requie_GS_1_Attachement_GS_1(), "role_requie_GS_1", null, 0, 1, Attachement_GS_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_Requie_GS_2EClass, Port_Requie_GS_2.class, "Port_Requie_GS_2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_Requie_GS_2_Attachement_GS_2(), this.getAttachement_GS_2(), this.getAttachement_GS_2_Port_requie_GS_2(), "attachement_GS_2", null, 0, 1, Port_Requie_GS_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachement_GS_2EClass, Attachement_GS_2.class, "Attachement_GS_2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachement_GS_2_Port_requie_GS_2(), this.getPort_Requie_GS_2(), this.getPort_Requie_GS_2_Attachement_GS_2(), "port_requie_GS_2", null, 0, 1, Attachement_GS_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachement_GS_2_Role_fournie_GS_2(), this.getRole_Fournie_GS_2(), this.getRole_Fournie_GS_2_Attachement_GS_2(), "role_fournie_GS_2", null, 0, 1, Attachement_GS_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachement_GS_3EClass, Attachement_GS_3.class, "Attachement_GS_3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachement_GS_3_Port_fournie_GS_3(), this.getPort_Fournie_GS_3(), this.getPort_Fournie_GS_3_Attachement_GS_3(), "port_fournie_GS_3", null, 0, 1, Attachement_GS_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachement_GS_3_Role_requie_GS_3(), this.getRole_Requie_GS_3(), this.getRole_Requie_GS_3_Attachement_GS_3(), "role_requie_GS_3", null, 0, 1, Attachement_GS_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_Fournie_GS_1EClass, Port_Fournie_GS_1.class, "Port_Fournie_GS_1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_Fournie_GS_1_Attachement_GS_1(), this.getAttachement_GS_1(), this.getAttachement_GS_1_Port_fournie_GS_1(), "attachement_GS_1", null, 0, 1, Port_Fournie_GS_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_Requie_GS_1EClass, Role_Requie_GS_1.class, "Role_Requie_GS_1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Requie_GS_1_Attachement_GS_1(), this.getAttachement_GS_1(), this.getAttachement_GS_1_Role_requie_GS_1(), "attachement_GS_1", null, 0, 1, Role_Requie_GS_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_Fournie_GS_2EClass, Role_Fournie_GS_2.class, "Role_Fournie_GS_2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Fournie_GS_2_Attachement_GS_2(), this.getAttachement_GS_2(), this.getAttachement_GS_2_Role_fournie_GS_2(), "attachement_GS_2", null, 0, 1, Role_Fournie_GS_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_Fournie_GS_3EClass, Port_Fournie_GS_3.class, "Port_Fournie_GS_3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_Fournie_GS_3_Attachement_GS_3(), this.getAttachement_GS_3(), this.getAttachement_GS_3_Port_fournie_GS_3(), "attachement_GS_3", null, 0, 1, Port_Fournie_GS_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_Requie_GS_3EClass, Role_Requie_GS_3.class, "Role_Requie_GS_3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Requie_GS_3_Attachement_GS_3(), this.getAttachement_GS_3(), this.getAttachement_GS_3_Role_requie_GS_3(), "attachement_GS_3", null, 0, 1, Role_Requie_GS_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachement_GS_4EClass, Attachement_GS_4.class, "Attachement_GS_4", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachement_GS_4_Port_requie_GS_4(), this.getPort_Requie_GS_4(), this.getPort_Requie_GS_4_Attachement_GS_4(), "port_requie_GS_4", null, 0, 1, Attachement_GS_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachement_GS_4_Role_fournie_GS_4(), this.getRole_Fournie_GS_4(), this.getRole_Fournie_GS_4_Attachement_GS_4(), "role_fournie_GS_4", null, 0, 1, Attachement_GS_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_Requie_GS_4EClass, Port_Requie_GS_4.class, "Port_Requie_GS_4", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_Requie_GS_4_Attachement_GS_4(), this.getAttachement_GS_4(), this.getAttachement_GS_4_Port_requie_GS_4(), "attachement_GS_4", null, 0, 1, Port_Requie_GS_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(role_Fournie_GS_4EClass, Role_Fournie_GS_4.class, "Role_Fournie_GS_4", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Fournie_GS_4_Attachement_GS_4(), this.getAttachement_GS_4(), this.getAttachement_GS_4_Role_fournie_GS_4(), "attachement_GS_4", null, 0, 1, Role_Fournie_GS_4.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iGestion_SecuriteEClass, IGestion_Securite.class, "IGestion_Securite", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIGestion_Securite_Port_fournie_GS_1(), this.getPort_Fournie_GS_1(), null, "port_fournie_GS_1", null, 0, -1, IGestion_Securite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIGestion_Securite_Port_requie_GS_2(), this.getPort_Requie_GS_2(), null, "port_requie_GS_2", null, 0, -1, IGestion_Securite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIGestion_Securite_Port_fournie_GS_3(), this.getPort_Fournie_GS_3(), null, "port_fournie_GS_3", null, 0, -1, IGestion_Securite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIGestion_Securite_Port_requie_GS_4(), this.getPort_Requie_GS_4(), null, "port_requie_GS_4", null, 0, -1, IGestion_Securite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connecteur_GC_GSEClass, Connecteur_GC_GS.class, "Connecteur_GC_GS", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnecteur_GC_GS_Iconnecteur_GC_GS(), this.getIConnecteur_GC_GS(), null, "Iconnecteur_GC_GS", null, 0, -1, Connecteur_GC_GS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iConnecteur_GC_GSEClass, IConnecteur_GC_GS.class, "IConnecteur_GC_GS", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIConnecteur_GC_GS_Role_requie_GC_3(), this.getRole_Requie_GC_3(), null, "role_requie_GC_3", null, 0, -1, IConnecteur_GC_GS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteur_GC_GS_Role_fournie_GC_4(), this.getRole_Fournie_GC_4(), null, "role_fournie_GC_4", null, 0, -1, IConnecteur_GC_GS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteur_GC_GS_Role_requie_GS_1(), this.getRole_Requie_GS_1(), null, "role_requie_GS_1", null, 0, -1, IConnecteur_GC_GS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteur_GC_GS_Role_fournie_GS_2(), this.getRole_Fournie_GS_2(), null, "role_fournie_GS_2", null, 0, -1, IConnecteur_GC_GS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iConnecteur_GS_DBEClass, IConnecteur_GS_DB.class, "IConnecteur_GS_DB", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIConnecteur_GS_DB_Role_fournie_DB_3(), this.getRole_fournie_DB_3(), null, "role_fournie_DB_3", null, 0, -1, IConnecteur_GS_DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteur_GS_DB_Role_requie_DB_4(), this.getRole_Requie_DB_4(), null, "role_requie_DB_4", null, 0, -1, IConnecteur_GS_DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteur_GS_DB_Role_fournie_GS_4(), this.getRole_Fournie_GS_4(), null, "role_fournie_GS_4", null, 0, -1, IConnecteur_GS_DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteur_GS_DB_Role_requie_GS_3(), this.getRole_Requie_GS_3(), null, "role_requie_GS_3", null, 0, -1, IConnecteur_GS_DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connecteur_GS_DBEClass, Connecteur_GS_DB.class, "Connecteur_GS_DB", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnecteur_GS_DB_Iconnecteur_GS_DB(), this.getIConnecteur_GS_DB(), null, "Iconnecteur_GS_DB", null, 0, -1, Connecteur_GS_DB.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(clientServeurEClass, ClientServeur.class, "ClientServeur", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClientServeur_Client(), this.getClient(), null, "client", null, 0, -1, ClientServeur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClientServeur_ConnecteurRPC(), this.getConnecteurRPC(), null, "connecteurRPC", null, 0, -1, ClientServeur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClientServeur_Serveur(), this.getServeur(), null, "serveur", null, 0, -1, ClientServeur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(clientEClass, Client.class, "Client", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClient_InterfaceClient(), this.getIClient(), null, "interfaceClient", null, 0, -1, Client.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connecteurRPCEClass, ConnecteurRPC.class, "ConnecteurRPC", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnecteurRPC_InterfaceConnecteur(), this.getIConnecteurRPC(), null, "interfaceConnecteur", null, 0, -1, ConnecteurRPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serveurEClass, Serveur.class, "Serveur", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getServeur_InterfaceServeur(), this.getIServeur(), null, "interfaceServeur", null, 0, -1, Serveur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServeur_Serveur_Config(), this.getServer_Configuration(), null, "serveur_Config", null, 0, -1, Serveur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iClientEClass, IClient.class, "IClient", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIClient_SendRequest(), this.getSendRequest(), null, "sendRequest", null, 0, -1, IClient.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIClient_ReceveRequest(), this.getReceveResponse(), null, "receveRequest", null, 0, -1, IClient.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iConnecteurRPCEClass, IConnecteurRPC.class, "IConnecteurRPC", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIConnecteurRPC_RoleRequie_CR(), this.getRoleRequie_CR(), null, "roleRequie_CR", null, 0, -1, IConnecteurRPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteurRPC_RoleFournie_RC(), this.getRoleFournie_RC(), null, "roleFournie_RC", null, 0, -1, IConnecteurRPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteurRPC_RoleRequie_RS(), this.getRoleRequie_RS(), null, "roleRequie_RS", null, 0, -1, IConnecteurRPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteurRPC_RoleFournie_SR(), this.getRoleFournie_SR(), null, "roleFournie_SR", null, 0, -1, IConnecteurRPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIConnecteurRPC_Port_RPC_S(), this.getPort_RPC_S(), null, "port_RPC_S", null, 0, -1, IConnecteurRPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iServeurEClass, IServeur.class, "IServeur", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIServeur_PortFournie_RS(), this.getPortFournie_RS(), null, "portFournie_RS", null, 0, -1, IServeur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIServeur_PortRequie_SR(), this.getPortRequie_SR(), null, "portRequie_SR", null, 0, -1, IServeur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIServeur_Pert_S_CM(), this.getPort_S_CM(), null, "pert_S_CM", null, 0, -1, IServeur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIServeur_Port_S_RPC(), this.getPort_S_RPC(), null, "port_S_RPC", null, 0, -1, IServeur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sendRequestEClass, SendRequest.class, "SendRequest", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSendRequest_Attachement_CR(), this.getAttachementClientRPC(), null, "attachement_CR", null, 0, 1, SendRequest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachementClientRPCEClass, AttachementClientRPC.class, "AttachementClientRPC", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachementClientRPC_SendRequest(), this.getSendRequest(), null, "sendRequest", null, 0, 1, AttachementClientRPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachementClientRPC_RoleRequie_CR(), this.getRoleRequie_CR(), null, "roleRequie_CR", null, 0, 1, AttachementClientRPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(roleRequie_CREClass, RoleRequie_CR.class, "RoleRequie_CR", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoleRequie_CR_Attachement_CR(), this.getAttachementClientRPC(), null, "attachement_CR", null, 0, 1, RoleRequie_CR.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachementRPCClientEClass, AttachementRPCClient.class, "AttachementRPCClient", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachementRPCClient_ReceveResponce(), this.getReceveResponse(), this.getReceveResponse_AttachementRC(), "receveResponce", null, 0, 1, AttachementRPCClient.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachementRPCClient_RoleFourie_RC(), this.getRoleFournie_RC(), this.getRoleFournie_RC_AttachementRC(), "roleFourie_RC", null, 0, 1, AttachementRPCClient.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(roleFournie_RCEClass, RoleFournie_RC.class, "RoleFournie_RC", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoleFournie_RC_AttachementRC(), this.getAttachementRPCClient(), this.getAttachementRPCClient_RoleFourie_RC(), "attachementRC", null, 0, 1, RoleFournie_RC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(receveResponseEClass, ReceveResponse.class, "ReceveResponse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReceveResponse_AttachementRC(), this.getAttachementRPCClient(), this.getAttachementRPCClient_ReceveResponce(), "attachementRC", null, 0, 1, ReceveResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachementRPCServeurEClass, AttachementRPCServeur.class, "AttachementRPCServeur", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachementRPCServeur_EReference0(), this.getRoleRequie_RS(), this.getRoleRequie_RS_Attachement_RS(), "EReference0", null, 0, 1, AttachementRPCServeur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachementRPCServeur_EReference1(), this.getPortFournie_RS(), this.getPortFournie_RS_Attachement_RS(), "EReference1", null, 0, 1, AttachementRPCServeur.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(roleRequie_RSEClass, RoleRequie_RS.class, "RoleRequie_RS", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoleRequie_RS_Attachement_RS(), this.getAttachementRPCServeur(), this.getAttachementRPCServeur_EReference0(), "attachement_RS", null, 0, 1, RoleRequie_RS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portFournie_RSEClass, PortFournie_RS.class, "PortFournie_RS", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPortFournie_RS_Attachement_RS(), this.getAttachementRPCServeur(), this.getAttachementRPCServeur_EReference1(), "attachement_RS", null, 0, 1, PortFournie_RS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attachementServeurRPCEClass, AttachementServeurRPC.class, "AttachementServeurRPC", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttachementServeurRPC_EReference0(), this.getRoleFournie_SR(), this.getRoleFournie_SR_Attachement_SR(), "EReference0", null, 0, 1, AttachementServeurRPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachementServeurRPC_EReference1(), this.getPortRequie_SR(), this.getPortRequie_SR_Attachement_SR(), "EReference1", null, 0, 1, AttachementServeurRPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(roleFournie_SREClass, RoleFournie_SR.class, "RoleFournie_SR", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoleFournie_SR_Attachement_SR(), this.getAttachementServeurRPC(), this.getAttachementServeurRPC_EReference0(), "attachement_SR", null, 0, 1, RoleFournie_SR.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portRequie_SREClass, PortRequie_SR.class, "PortRequie_SR", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPortRequie_SR_Attachement_SR(), this.getAttachementServeurRPC(), this.getAttachementServeurRPC_EReference1(), "attachement_SR", null, 0, 1, PortRequie_SR.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(binding_S_RPCEClass, Binding_S_RPC.class, "Binding_S_RPC", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinding_S_RPC_EReference0(), this.getPort_RPC_S(), this.getPort_RPC_S_EReference0(), "EReference0", null, 0, 1, Binding_S_RPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinding_S_RPC_EReference1(), this.getPort_S_RPC(), this.getPort_S_RPC_EReference0(), "EReference1", null, 0, 1, Binding_S_RPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_RPC_SEClass, Port_RPC_S.class, "Port_RPC_S", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_RPC_S_EReference0(), this.getBinding_S_RPC(), this.getBinding_S_RPC_EReference0(), "EReference0", null, 0, 1, Port_RPC_S.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_S_RPCEClass, Port_S_RPC.class, "Port_S_RPC", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_S_RPC_EReference0(), this.getBinding_S_RPC(), this.getBinding_S_RPC_EReference1(), "EReference0", null, 0, 1, Port_S_RPC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(binding_CM_SEClass, Binding_CM_S.class, "Binding_CM_S", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinding_CM_S_EReference0(), this.getPort_S_CM(), this.getPort_S_CM_EReference0(), "EReference0", null, 0, 1, Binding_CM_S.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinding_CM_S_EReference1(), this.getPort_CM_S(), this.getPort_CM_S_EReference0(), "EReference1", null, 0, 1, Binding_CM_S.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_S_CMEClass, Port_S_CM.class, "Port_S_CM", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_S_CM_EReference0(), this.getBinding_CM_S(), this.getBinding_CM_S_EReference0(), "EReference0", null, 0, 1, Port_S_CM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(port_CM_SEClass, Port_CM_S.class, "Port_CM_S", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPort_CM_S_EReference0(), this.getBinding_CM_S(), this.getBinding_CM_S_EReference1(), "EReference0", null, 0, 1, Port_CM_S.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //M1PackageImpl
