/**
 */
package M1.impl;

import M1.IConnecteurRPC;
import M1.M1Package;
import M1.Port_RPC_S;
import M1.RoleFournie_RC;
import M1.RoleFournie_SR;
import M1.RoleRequie_CR;
import M1.RoleRequie_RS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IConnecteur RPC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.IConnecteurRPCImpl#getRoleRequie_CR <em>Role Requie CR</em>}</li>
 *   <li>{@link M1.impl.IConnecteurRPCImpl#getRoleFournie_RC <em>Role Fournie RC</em>}</li>
 *   <li>{@link M1.impl.IConnecteurRPCImpl#getRoleRequie_RS <em>Role Requie RS</em>}</li>
 *   <li>{@link M1.impl.IConnecteurRPCImpl#getRoleFournie_SR <em>Role Fournie SR</em>}</li>
 *   <li>{@link M1.impl.IConnecteurRPCImpl#getPort_RPC_S <em>Port RPC S</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IConnecteurRPCImpl extends EObjectImpl implements IConnecteurRPC {
	/**
	 * The cached value of the '{@link #getRoleRequie_CR() <em>Role Requie CR</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleRequie_CR()
	 * @generated
	 * @ordered
	 */
	protected EList roleRequie_CR;

	/**
	 * The cached value of the '{@link #getRoleFournie_RC() <em>Role Fournie RC</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleFournie_RC()
	 * @generated
	 * @ordered
	 */
	protected EList roleFournie_RC;

	/**
	 * The cached value of the '{@link #getRoleRequie_RS() <em>Role Requie RS</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleRequie_RS()
	 * @generated
	 * @ordered
	 */
	protected EList roleRequie_RS;

	/**
	 * The cached value of the '{@link #getRoleFournie_SR() <em>Role Fournie SR</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleFournie_SR()
	 * @generated
	 * @ordered
	 */
	protected EList roleFournie_SR;

	/**
	 * The cached value of the '{@link #getPort_RPC_S() <em>Port RPC S</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_RPC_S()
	 * @generated
	 * @ordered
	 */
	protected EList port_RPC_S;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IConnecteurRPCImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ICONNECTEUR_RPC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRoleRequie_CR() {
		if (roleRequie_CR == null) {
			roleRequie_CR = new EObjectContainmentEList(RoleRequie_CR.class, this, M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_CR);
		}
		return roleRequie_CR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRoleFournie_RC() {
		if (roleFournie_RC == null) {
			roleFournie_RC = new EObjectContainmentEList(RoleFournie_RC.class, this, M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_RC);
		}
		return roleFournie_RC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRoleRequie_RS() {
		if (roleRequie_RS == null) {
			roleRequie_RS = new EObjectContainmentEList(RoleRequie_RS.class, this, M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_RS);
		}
		return roleRequie_RS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRoleFournie_SR() {
		if (roleFournie_SR == null) {
			roleFournie_SR = new EObjectContainmentEList(RoleFournie_SR.class, this, M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_SR);
		}
		return roleFournie_SR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPort_RPC_S() {
		if (port_RPC_S == null) {
			port_RPC_S = new EObjectContainmentEList(Port_RPC_S.class, this, M1Package.ICONNECTEUR_RPC__PORT_RPC_S);
		}
		return port_RPC_S;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_CR:
				return ((InternalEList)getRoleRequie_CR()).basicRemove(otherEnd, msgs);
			case M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_RC:
				return ((InternalEList)getRoleFournie_RC()).basicRemove(otherEnd, msgs);
			case M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_RS:
				return ((InternalEList)getRoleRequie_RS()).basicRemove(otherEnd, msgs);
			case M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_SR:
				return ((InternalEList)getRoleFournie_SR()).basicRemove(otherEnd, msgs);
			case M1Package.ICONNECTEUR_RPC__PORT_RPC_S:
				return ((InternalEList)getPort_RPC_S()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_CR:
				return getRoleRequie_CR();
			case M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_RC:
				return getRoleFournie_RC();
			case M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_RS:
				return getRoleRequie_RS();
			case M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_SR:
				return getRoleFournie_SR();
			case M1Package.ICONNECTEUR_RPC__PORT_RPC_S:
				return getPort_RPC_S();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_CR:
				getRoleRequie_CR().clear();
				getRoleRequie_CR().addAll((Collection)newValue);
				return;
			case M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_RC:
				getRoleFournie_RC().clear();
				getRoleFournie_RC().addAll((Collection)newValue);
				return;
			case M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_RS:
				getRoleRequie_RS().clear();
				getRoleRequie_RS().addAll((Collection)newValue);
				return;
			case M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_SR:
				getRoleFournie_SR().clear();
				getRoleFournie_SR().addAll((Collection)newValue);
				return;
			case M1Package.ICONNECTEUR_RPC__PORT_RPC_S:
				getPort_RPC_S().clear();
				getPort_RPC_S().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_CR:
				getRoleRequie_CR().clear();
				return;
			case M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_RC:
				getRoleFournie_RC().clear();
				return;
			case M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_RS:
				getRoleRequie_RS().clear();
				return;
			case M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_SR:
				getRoleFournie_SR().clear();
				return;
			case M1Package.ICONNECTEUR_RPC__PORT_RPC_S:
				getPort_RPC_S().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_CR:
				return roleRequie_CR != null && !roleRequie_CR.isEmpty();
			case M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_RC:
				return roleFournie_RC != null && !roleFournie_RC.isEmpty();
			case M1Package.ICONNECTEUR_RPC__ROLE_REQUIE_RS:
				return roleRequie_RS != null && !roleRequie_RS.isEmpty();
			case M1Package.ICONNECTEUR_RPC__ROLE_FOURNIE_SR:
				return roleFournie_SR != null && !roleFournie_SR.isEmpty();
			case M1Package.ICONNECTEUR_RPC__PORT_RPC_S:
				return port_RPC_S != null && !port_RPC_S.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //IConnecteurRPCImpl
