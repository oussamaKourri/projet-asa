/**
 */
package M1.impl;

import M1.Connecteur_GS_DB;
import M1.IConnecteur_GS_DB;
import M1.M1Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connecteur GS DB</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Connecteur_GS_DBImpl#getIconnecteur_GS_DB <em>Iconnecteur GS DB</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Connecteur_GS_DBImpl extends EObjectImpl implements Connecteur_GS_DB {
	/**
	 * The cached value of the '{@link #getIconnecteur_GS_DB() <em>Iconnecteur GS DB</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIconnecteur_GS_DB()
	 * @generated
	 * @ordered
	 */
	protected EList iconnecteur_GS_DB;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Connecteur_GS_DBImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.CONNECTEUR_GS_DB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getIconnecteur_GS_DB() {
		if (iconnecteur_GS_DB == null) {
			iconnecteur_GS_DB = new EObjectContainmentEList(IConnecteur_GS_DB.class, this, M1Package.CONNECTEUR_GS_DB__ICONNECTEUR_GS_DB);
		}
		return iconnecteur_GS_DB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GS_DB__ICONNECTEUR_GS_DB:
				return ((InternalEList)getIconnecteur_GS_DB()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GS_DB__ICONNECTEUR_GS_DB:
				return getIconnecteur_GS_DB();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GS_DB__ICONNECTEUR_GS_DB:
				getIconnecteur_GS_DB().clear();
				getIconnecteur_GS_DB().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GS_DB__ICONNECTEUR_GS_DB:
				getIconnecteur_GS_DB().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.CONNECTEUR_GS_DB__ICONNECTEUR_GS_DB:
				return iconnecteur_GS_DB != null && !iconnecteur_GS_DB.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //Connecteur_GS_DBImpl
