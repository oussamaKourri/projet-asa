/**
 */
package M1.impl;

import M1.Atachement_DB_1;
import M1.M1Package;
import M1.Port_Requie_DB_1;
import M1.Role_fournie_DB_1;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Atachement DB 1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Atachement_DB_1Impl#getRole_fournie_DB_1 <em>Role fournie DB 1</em>}</li>
 *   <li>{@link M1.impl.Atachement_DB_1Impl#getPort_requie_DB_1 <em>Port requie DB 1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Atachement_DB_1Impl extends EObjectImpl implements Atachement_DB_1 {
	/**
	 * The cached value of the '{@link #getRole_fournie_DB_1() <em>Role fournie DB 1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_fournie_DB_1()
	 * @generated
	 * @ordered
	 */
	protected Role_fournie_DB_1 role_fournie_DB_1;

	/**
	 * The cached value of the '{@link #getPort_requie_DB_1() <em>Port requie DB 1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_requie_DB_1()
	 * @generated
	 * @ordered
	 */
	protected Port_Requie_DB_1 port_requie_DB_1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Atachement_DB_1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ATACHEMENT_DB_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_fournie_DB_1 getRole_fournie_DB_1() {
		if (role_fournie_DB_1 != null && role_fournie_DB_1.eIsProxy()) {
			InternalEObject oldRole_fournie_DB_1 = (InternalEObject)role_fournie_DB_1;
			role_fournie_DB_1 = (Role_fournie_DB_1)eResolveProxy(oldRole_fournie_DB_1);
			if (role_fournie_DB_1 != oldRole_fournie_DB_1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1, oldRole_fournie_DB_1, role_fournie_DB_1));
			}
		}
		return role_fournie_DB_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_fournie_DB_1 basicGetRole_fournie_DB_1() {
		return role_fournie_DB_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRole_fournie_DB_1(Role_fournie_DB_1 newRole_fournie_DB_1, NotificationChain msgs) {
		Role_fournie_DB_1 oldRole_fournie_DB_1 = role_fournie_DB_1;
		role_fournie_DB_1 = newRole_fournie_DB_1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1, oldRole_fournie_DB_1, newRole_fournie_DB_1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole_fournie_DB_1(Role_fournie_DB_1 newRole_fournie_DB_1) {
		if (newRole_fournie_DB_1 != role_fournie_DB_1) {
			NotificationChain msgs = null;
			if (role_fournie_DB_1 != null)
				msgs = ((InternalEObject)role_fournie_DB_1).eInverseRemove(this, M1Package.ROLE_FOURNIE_DB_1__ATTACHEMENT_DB_1, Role_fournie_DB_1.class, msgs);
			if (newRole_fournie_DB_1 != null)
				msgs = ((InternalEObject)newRole_fournie_DB_1).eInverseAdd(this, M1Package.ROLE_FOURNIE_DB_1__ATTACHEMENT_DB_1, Role_fournie_DB_1.class, msgs);
			msgs = basicSetRole_fournie_DB_1(newRole_fournie_DB_1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1, newRole_fournie_DB_1, newRole_fournie_DB_1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_DB_1 getPort_requie_DB_1() {
		if (port_requie_DB_1 != null && port_requie_DB_1.eIsProxy()) {
			InternalEObject oldPort_requie_DB_1 = (InternalEObject)port_requie_DB_1;
			port_requie_DB_1 = (Port_Requie_DB_1)eResolveProxy(oldPort_requie_DB_1);
			if (port_requie_DB_1 != oldPort_requie_DB_1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATACHEMENT_DB_1__PORT_REQUIE_DB_1, oldPort_requie_DB_1, port_requie_DB_1));
			}
		}
		return port_requie_DB_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_DB_1 basicGetPort_requie_DB_1() {
		return port_requie_DB_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_requie_DB_1(Port_Requie_DB_1 newPort_requie_DB_1, NotificationChain msgs) {
		Port_Requie_DB_1 oldPort_requie_DB_1 = port_requie_DB_1;
		port_requie_DB_1 = newPort_requie_DB_1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATACHEMENT_DB_1__PORT_REQUIE_DB_1, oldPort_requie_DB_1, newPort_requie_DB_1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_requie_DB_1(Port_Requie_DB_1 newPort_requie_DB_1) {
		if (newPort_requie_DB_1 != port_requie_DB_1) {
			NotificationChain msgs = null;
			if (port_requie_DB_1 != null)
				msgs = ((InternalEObject)port_requie_DB_1).eInverseRemove(this, M1Package.PORT_REQUIE_DB_1__ATTACHEMENT_DB_1, Port_Requie_DB_1.class, msgs);
			if (newPort_requie_DB_1 != null)
				msgs = ((InternalEObject)newPort_requie_DB_1).eInverseAdd(this, M1Package.PORT_REQUIE_DB_1__ATTACHEMENT_DB_1, Port_Requie_DB_1.class, msgs);
			msgs = basicSetPort_requie_DB_1(newPort_requie_DB_1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATACHEMENT_DB_1__PORT_REQUIE_DB_1, newPort_requie_DB_1, newPort_requie_DB_1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1:
				if (role_fournie_DB_1 != null)
					msgs = ((InternalEObject)role_fournie_DB_1).eInverseRemove(this, M1Package.ROLE_FOURNIE_DB_1__ATTACHEMENT_DB_1, Role_fournie_DB_1.class, msgs);
				return basicSetRole_fournie_DB_1((Role_fournie_DB_1)otherEnd, msgs);
			case M1Package.ATACHEMENT_DB_1__PORT_REQUIE_DB_1:
				if (port_requie_DB_1 != null)
					msgs = ((InternalEObject)port_requie_DB_1).eInverseRemove(this, M1Package.PORT_REQUIE_DB_1__ATTACHEMENT_DB_1, Port_Requie_DB_1.class, msgs);
				return basicSetPort_requie_DB_1((Port_Requie_DB_1)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1:
				return basicSetRole_fournie_DB_1(null, msgs);
			case M1Package.ATACHEMENT_DB_1__PORT_REQUIE_DB_1:
				return basicSetPort_requie_DB_1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1:
				if (resolve) return getRole_fournie_DB_1();
				return basicGetRole_fournie_DB_1();
			case M1Package.ATACHEMENT_DB_1__PORT_REQUIE_DB_1:
				if (resolve) return getPort_requie_DB_1();
				return basicGetPort_requie_DB_1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1:
				setRole_fournie_DB_1((Role_fournie_DB_1)newValue);
				return;
			case M1Package.ATACHEMENT_DB_1__PORT_REQUIE_DB_1:
				setPort_requie_DB_1((Port_Requie_DB_1)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1:
				setRole_fournie_DB_1((Role_fournie_DB_1)null);
				return;
			case M1Package.ATACHEMENT_DB_1__PORT_REQUIE_DB_1:
				setPort_requie_DB_1((Port_Requie_DB_1)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ATACHEMENT_DB_1__ROLE_FOURNIE_DB_1:
				return role_fournie_DB_1 != null;
			case M1Package.ATACHEMENT_DB_1__PORT_REQUIE_DB_1:
				return port_requie_DB_1 != null;
		}
		return super.eIsSet(featureID);
	}

} //Atachement_DB_1Impl
