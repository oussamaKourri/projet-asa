/**
 */
package M1.impl;

import M1.Attachement_GS_4;
import M1.M1Package;
import M1.Port_Requie_GS_4;
import M1.Role_Fournie_GS_4;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attachement GS 4</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.Attachement_GS_4Impl#getPort_requie_GS_4 <em>Port requie GS 4</em>}</li>
 *   <li>{@link M1.impl.Attachement_GS_4Impl#getRole_fournie_GS_4 <em>Role fournie GS 4</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Attachement_GS_4Impl extends EObjectImpl implements Attachement_GS_4 {
	/**
	 * The cached value of the '{@link #getPort_requie_GS_4() <em>Port requie GS 4</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort_requie_GS_4()
	 * @generated
	 * @ordered
	 */
	protected Port_Requie_GS_4 port_requie_GS_4;

	/**
	 * The cached value of the '{@link #getRole_fournie_GS_4() <em>Role fournie GS 4</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_fournie_GS_4()
	 * @generated
	 * @ordered
	 */
	protected Role_Fournie_GS_4 role_fournie_GS_4;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Attachement_GS_4Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ATTACHEMENT_GS_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_GS_4 getPort_requie_GS_4() {
		if (port_requie_GS_4 != null && port_requie_GS_4.eIsProxy()) {
			InternalEObject oldPort_requie_GS_4 = (InternalEObject)port_requie_GS_4;
			port_requie_GS_4 = (Port_Requie_GS_4)eResolveProxy(oldPort_requie_GS_4);
			if (port_requie_GS_4 != oldPort_requie_GS_4) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_GS_4__PORT_REQUIE_GS_4, oldPort_requie_GS_4, port_requie_GS_4));
			}
		}
		return port_requie_GS_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port_Requie_GS_4 basicGetPort_requie_GS_4() {
		return port_requie_GS_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort_requie_GS_4(Port_Requie_GS_4 newPort_requie_GS_4, NotificationChain msgs) {
		Port_Requie_GS_4 oldPort_requie_GS_4 = port_requie_GS_4;
		port_requie_GS_4 = newPort_requie_GS_4;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_4__PORT_REQUIE_GS_4, oldPort_requie_GS_4, newPort_requie_GS_4);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort_requie_GS_4(Port_Requie_GS_4 newPort_requie_GS_4) {
		if (newPort_requie_GS_4 != port_requie_GS_4) {
			NotificationChain msgs = null;
			if (port_requie_GS_4 != null)
				msgs = ((InternalEObject)port_requie_GS_4).eInverseRemove(this, M1Package.PORT_REQUIE_GS_4__ATTACHEMENT_GS_4, Port_Requie_GS_4.class, msgs);
			if (newPort_requie_GS_4 != null)
				msgs = ((InternalEObject)newPort_requie_GS_4).eInverseAdd(this, M1Package.PORT_REQUIE_GS_4__ATTACHEMENT_GS_4, Port_Requie_GS_4.class, msgs);
			msgs = basicSetPort_requie_GS_4(newPort_requie_GS_4, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_4__PORT_REQUIE_GS_4, newPort_requie_GS_4, newPort_requie_GS_4));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Fournie_GS_4 getRole_fournie_GS_4() {
		if (role_fournie_GS_4 != null && role_fournie_GS_4.eIsProxy()) {
			InternalEObject oldRole_fournie_GS_4 = (InternalEObject)role_fournie_GS_4;
			role_fournie_GS_4 = (Role_Fournie_GS_4)eResolveProxy(oldRole_fournie_GS_4);
			if (role_fournie_GS_4 != oldRole_fournie_GS_4) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4, oldRole_fournie_GS_4, role_fournie_GS_4));
			}
		}
		return role_fournie_GS_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role_Fournie_GS_4 basicGetRole_fournie_GS_4() {
		return role_fournie_GS_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRole_fournie_GS_4(Role_Fournie_GS_4 newRole_fournie_GS_4, NotificationChain msgs) {
		Role_Fournie_GS_4 oldRole_fournie_GS_4 = role_fournie_GS_4;
		role_fournie_GS_4 = newRole_fournie_GS_4;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4, oldRole_fournie_GS_4, newRole_fournie_GS_4);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole_fournie_GS_4(Role_Fournie_GS_4 newRole_fournie_GS_4) {
		if (newRole_fournie_GS_4 != role_fournie_GS_4) {
			NotificationChain msgs = null;
			if (role_fournie_GS_4 != null)
				msgs = ((InternalEObject)role_fournie_GS_4).eInverseRemove(this, M1Package.ROLE_FOURNIE_GS_4__ATTACHEMENT_GS_4, Role_Fournie_GS_4.class, msgs);
			if (newRole_fournie_GS_4 != null)
				msgs = ((InternalEObject)newRole_fournie_GS_4).eInverseAdd(this, M1Package.ROLE_FOURNIE_GS_4__ATTACHEMENT_GS_4, Role_Fournie_GS_4.class, msgs);
			msgs = basicSetRole_fournie_GS_4(newRole_fournie_GS_4, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4, newRole_fournie_GS_4, newRole_fournie_GS_4));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_4__PORT_REQUIE_GS_4:
				if (port_requie_GS_4 != null)
					msgs = ((InternalEObject)port_requie_GS_4).eInverseRemove(this, M1Package.PORT_REQUIE_GS_4__ATTACHEMENT_GS_4, Port_Requie_GS_4.class, msgs);
				return basicSetPort_requie_GS_4((Port_Requie_GS_4)otherEnd, msgs);
			case M1Package.ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4:
				if (role_fournie_GS_4 != null)
					msgs = ((InternalEObject)role_fournie_GS_4).eInverseRemove(this, M1Package.ROLE_FOURNIE_GS_4__ATTACHEMENT_GS_4, Role_Fournie_GS_4.class, msgs);
				return basicSetRole_fournie_GS_4((Role_Fournie_GS_4)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_4__PORT_REQUIE_GS_4:
				return basicSetPort_requie_GS_4(null, msgs);
			case M1Package.ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4:
				return basicSetRole_fournie_GS_4(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_4__PORT_REQUIE_GS_4:
				if (resolve) return getPort_requie_GS_4();
				return basicGetPort_requie_GS_4();
			case M1Package.ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4:
				if (resolve) return getRole_fournie_GS_4();
				return basicGetRole_fournie_GS_4();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_4__PORT_REQUIE_GS_4:
				setPort_requie_GS_4((Port_Requie_GS_4)newValue);
				return;
			case M1Package.ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4:
				setRole_fournie_GS_4((Role_Fournie_GS_4)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_4__PORT_REQUIE_GS_4:
				setPort_requie_GS_4((Port_Requie_GS_4)null);
				return;
			case M1Package.ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4:
				setRole_fournie_GS_4((Role_Fournie_GS_4)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_GS_4__PORT_REQUIE_GS_4:
				return port_requie_GS_4 != null;
			case M1Package.ATTACHEMENT_GS_4__ROLE_FOURNIE_GS_4:
				return role_fournie_GS_4 != null;
		}
		return super.eIsSet(featureID);
	}

} //Attachement_GS_4Impl
