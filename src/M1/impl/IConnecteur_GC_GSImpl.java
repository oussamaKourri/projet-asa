/**
 */
package M1.impl;

import M1.IConnecteur_GC_GS;
import M1.M1Package;
import M1.Role_Fournie_GC_4;
import M1.Role_Fournie_GS_2;
import M1.Role_Requie_GC_3;
import M1.Role_Requie_GS_1;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IConnecteur GC GS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.IConnecteur_GC_GSImpl#getRole_requie_GC_3 <em>Role requie GC 3</em>}</li>
 *   <li>{@link M1.impl.IConnecteur_GC_GSImpl#getRole_fournie_GC_4 <em>Role fournie GC 4</em>}</li>
 *   <li>{@link M1.impl.IConnecteur_GC_GSImpl#getRole_requie_GS_1 <em>Role requie GS 1</em>}</li>
 *   <li>{@link M1.impl.IConnecteur_GC_GSImpl#getRole_fournie_GS_2 <em>Role fournie GS 2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IConnecteur_GC_GSImpl extends EObjectImpl implements IConnecteur_GC_GS {
	/**
	 * The cached value of the '{@link #getRole_requie_GC_3() <em>Role requie GC 3</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_requie_GC_3()
	 * @generated
	 * @ordered
	 */
	protected EList role_requie_GC_3;

	/**
	 * The cached value of the '{@link #getRole_fournie_GC_4() <em>Role fournie GC 4</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_fournie_GC_4()
	 * @generated
	 * @ordered
	 */
	protected EList role_fournie_GC_4;

	/**
	 * The cached value of the '{@link #getRole_requie_GS_1() <em>Role requie GS 1</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_requie_GS_1()
	 * @generated
	 * @ordered
	 */
	protected EList role_requie_GS_1;

	/**
	 * The cached value of the '{@link #getRole_fournie_GS_2() <em>Role fournie GS 2</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole_fournie_GS_2()
	 * @generated
	 * @ordered
	 */
	protected EList role_fournie_GS_2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IConnecteur_GC_GSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ICONNECTEUR_GC_GS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRole_requie_GC_3() {
		if (role_requie_GC_3 == null) {
			role_requie_GC_3 = new EObjectContainmentEList(Role_Requie_GC_3.class, this, M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GC_3);
		}
		return role_requie_GC_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRole_fournie_GC_4() {
		if (role_fournie_GC_4 == null) {
			role_fournie_GC_4 = new EObjectContainmentEList(Role_Fournie_GC_4.class, this, M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GC_4);
		}
		return role_fournie_GC_4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRole_requie_GS_1() {
		if (role_requie_GS_1 == null) {
			role_requie_GS_1 = new EObjectContainmentEList(Role_Requie_GS_1.class, this, M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GS_1);
		}
		return role_requie_GS_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRole_fournie_GS_2() {
		if (role_fournie_GS_2 == null) {
			role_fournie_GS_2 = new EObjectContainmentEList(Role_Fournie_GS_2.class, this, M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GS_2);
		}
		return role_fournie_GS_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GC_3:
				return ((InternalEList)getRole_requie_GC_3()).basicRemove(otherEnd, msgs);
			case M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GC_4:
				return ((InternalEList)getRole_fournie_GC_4()).basicRemove(otherEnd, msgs);
			case M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GS_1:
				return ((InternalEList)getRole_requie_GS_1()).basicRemove(otherEnd, msgs);
			case M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GS_2:
				return ((InternalEList)getRole_fournie_GS_2()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GC_3:
				return getRole_requie_GC_3();
			case M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GC_4:
				return getRole_fournie_GC_4();
			case M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GS_1:
				return getRole_requie_GS_1();
			case M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GS_2:
				return getRole_fournie_GS_2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GC_3:
				getRole_requie_GC_3().clear();
				getRole_requie_GC_3().addAll((Collection)newValue);
				return;
			case M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GC_4:
				getRole_fournie_GC_4().clear();
				getRole_fournie_GC_4().addAll((Collection)newValue);
				return;
			case M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GS_1:
				getRole_requie_GS_1().clear();
				getRole_requie_GS_1().addAll((Collection)newValue);
				return;
			case M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GS_2:
				getRole_fournie_GS_2().clear();
				getRole_fournie_GS_2().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GC_3:
				getRole_requie_GC_3().clear();
				return;
			case M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GC_4:
				getRole_fournie_GC_4().clear();
				return;
			case M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GS_1:
				getRole_requie_GS_1().clear();
				return;
			case M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GS_2:
				getRole_fournie_GS_2().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GC_3:
				return role_requie_GC_3 != null && !role_requie_GC_3.isEmpty();
			case M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GC_4:
				return role_fournie_GC_4 != null && !role_fournie_GC_4.isEmpty();
			case M1Package.ICONNECTEUR_GC_GS__ROLE_REQUIE_GS_1:
				return role_requie_GS_1 != null && !role_requie_GS_1.isEmpty();
			case M1Package.ICONNECTEUR_GC_GS__ROLE_FOURNIE_GS_2:
				return role_fournie_GS_2 != null && !role_fournie_GS_2.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //IConnecteur_GC_GSImpl
