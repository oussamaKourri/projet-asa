/**
 */
package M1.impl;

import M1.IServeur;
import M1.M1Package;
import M1.Server_Configuration;
import M1.Serveur;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Serveur</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.ServeurImpl#getInterfaceServeur <em>Interface Serveur</em>}</li>
 *   <li>{@link M1.impl.ServeurImpl#getServeur_Config <em>Serveur Config</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ServeurImpl extends EObjectImpl implements Serveur {
	/**
	 * The cached value of the '{@link #getInterfaceServeur() <em>Interface Serveur</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceServeur()
	 * @generated
	 * @ordered
	 */
	protected EList interfaceServeur;

	/**
	 * The cached value of the '{@link #getServeur_Config() <em>Serveur Config</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServeur_Config()
	 * @generated
	 * @ordered
	 */
	protected EList serveur_Config;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServeurImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.SERVEUR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getInterfaceServeur() {
		if (interfaceServeur == null) {
			interfaceServeur = new EObjectContainmentEList(IServeur.class, this, M1Package.SERVEUR__INTERFACE_SERVEUR);
		}
		return interfaceServeur;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getServeur_Config() {
		if (serveur_Config == null) {
			serveur_Config = new EObjectContainmentEList(Server_Configuration.class, this, M1Package.SERVEUR__SERVEUR_CONFIG);
		}
		return serveur_Config;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.SERVEUR__INTERFACE_SERVEUR:
				return ((InternalEList)getInterfaceServeur()).basicRemove(otherEnd, msgs);
			case M1Package.SERVEUR__SERVEUR_CONFIG:
				return ((InternalEList)getServeur_Config()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.SERVEUR__INTERFACE_SERVEUR:
				return getInterfaceServeur();
			case M1Package.SERVEUR__SERVEUR_CONFIG:
				return getServeur_Config();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.SERVEUR__INTERFACE_SERVEUR:
				getInterfaceServeur().clear();
				getInterfaceServeur().addAll((Collection)newValue);
				return;
			case M1Package.SERVEUR__SERVEUR_CONFIG:
				getServeur_Config().clear();
				getServeur_Config().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.SERVEUR__INTERFACE_SERVEUR:
				getInterfaceServeur().clear();
				return;
			case M1Package.SERVEUR__SERVEUR_CONFIG:
				getServeur_Config().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.SERVEUR__INTERFACE_SERVEUR:
				return interfaceServeur != null && !interfaceServeur.isEmpty();
			case M1Package.SERVEUR__SERVEUR_CONFIG:
				return serveur_Config != null && !serveur_Config.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ServeurImpl
