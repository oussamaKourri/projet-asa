/**
 */
package M1.impl;

import M1.AttachementRPCClient;
import M1.M1Package;
import M1.ReceveResponse;
import M1.RoleFournie_RC;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attachement RPC Client</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link M1.impl.AttachementRPCClientImpl#getReceveResponce <em>Receve Responce</em>}</li>
 *   <li>{@link M1.impl.AttachementRPCClientImpl#getRoleFourie_RC <em>Role Fourie RC</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttachementRPCClientImpl extends EObjectImpl implements AttachementRPCClient {
	/**
	 * The cached value of the '{@link #getReceveResponce() <em>Receve Responce</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceveResponce()
	 * @generated
	 * @ordered
	 */
	protected ReceveResponse receveResponce;

	/**
	 * The cached value of the '{@link #getRoleFourie_RC() <em>Role Fourie RC</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleFourie_RC()
	 * @generated
	 * @ordered
	 */
	protected RoleFournie_RC roleFourie_RC;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttachementRPCClientImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return M1Package.Literals.ATTACHEMENT_RPC_CLIENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReceveResponse getReceveResponce() {
		if (receveResponce != null && receveResponce.eIsProxy()) {
			InternalEObject oldReceveResponce = (InternalEObject)receveResponce;
			receveResponce = (ReceveResponse)eResolveProxy(oldReceveResponce);
			if (receveResponce != oldReceveResponce) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE, oldReceveResponce, receveResponce));
			}
		}
		return receveResponce;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReceveResponse basicGetReceveResponce() {
		return receveResponce;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReceveResponce(ReceveResponse newReceveResponce, NotificationChain msgs) {
		ReceveResponse oldReceveResponce = receveResponce;
		receveResponce = newReceveResponce;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE, oldReceveResponce, newReceveResponce);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceveResponce(ReceveResponse newReceveResponce) {
		if (newReceveResponce != receveResponce) {
			NotificationChain msgs = null;
			if (receveResponce != null)
				msgs = ((InternalEObject)receveResponce).eInverseRemove(this, M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC, ReceveResponse.class, msgs);
			if (newReceveResponce != null)
				msgs = ((InternalEObject)newReceveResponce).eInverseAdd(this, M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC, ReceveResponse.class, msgs);
			msgs = basicSetReceveResponce(newReceveResponce, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE, newReceveResponce, newReceveResponce));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleFournie_RC getRoleFourie_RC() {
		if (roleFourie_RC != null && roleFourie_RC.eIsProxy()) {
			InternalEObject oldRoleFourie_RC = (InternalEObject)roleFourie_RC;
			roleFourie_RC = (RoleFournie_RC)eResolveProxy(oldRoleFourie_RC);
			if (roleFourie_RC != oldRoleFourie_RC) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, M1Package.ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC, oldRoleFourie_RC, roleFourie_RC));
			}
		}
		return roleFourie_RC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleFournie_RC basicGetRoleFourie_RC() {
		return roleFourie_RC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRoleFourie_RC(RoleFournie_RC newRoleFourie_RC, NotificationChain msgs) {
		RoleFournie_RC oldRoleFourie_RC = roleFourie_RC;
		roleFourie_RC = newRoleFourie_RC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC, oldRoleFourie_RC, newRoleFourie_RC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoleFourie_RC(RoleFournie_RC newRoleFourie_RC) {
		if (newRoleFourie_RC != roleFourie_RC) {
			NotificationChain msgs = null;
			if (roleFourie_RC != null)
				msgs = ((InternalEObject)roleFourie_RC).eInverseRemove(this, M1Package.ROLE_FOURNIE_RC__ATTACHEMENT_RC, RoleFournie_RC.class, msgs);
			if (newRoleFourie_RC != null)
				msgs = ((InternalEObject)newRoleFourie_RC).eInverseAdd(this, M1Package.ROLE_FOURNIE_RC__ATTACHEMENT_RC, RoleFournie_RC.class, msgs);
			msgs = basicSetRoleFourie_RC(newRoleFourie_RC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, M1Package.ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC, newRoleFourie_RC, newRoleFourie_RC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE:
				if (receveResponce != null)
					msgs = ((InternalEObject)receveResponce).eInverseRemove(this, M1Package.RECEVE_RESPONSE__ATTACHEMENT_RC, ReceveResponse.class, msgs);
				return basicSetReceveResponce((ReceveResponse)otherEnd, msgs);
			case M1Package.ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC:
				if (roleFourie_RC != null)
					msgs = ((InternalEObject)roleFourie_RC).eInverseRemove(this, M1Package.ROLE_FOURNIE_RC__ATTACHEMENT_RC, RoleFournie_RC.class, msgs);
				return basicSetRoleFourie_RC((RoleFournie_RC)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE:
				return basicSetReceveResponce(null, msgs);
			case M1Package.ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC:
				return basicSetRoleFourie_RC(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE:
				if (resolve) return getReceveResponce();
				return basicGetReceveResponce();
			case M1Package.ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC:
				if (resolve) return getRoleFourie_RC();
				return basicGetRoleFourie_RC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE:
				setReceveResponce((ReceveResponse)newValue);
				return;
			case M1Package.ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC:
				setRoleFourie_RC((RoleFournie_RC)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE:
				setReceveResponce((ReceveResponse)null);
				return;
			case M1Package.ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC:
				setRoleFourie_RC((RoleFournie_RC)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case M1Package.ATTACHEMENT_RPC_CLIENT__RECEVE_RESPONCE:
				return receveResponce != null;
			case M1Package.ATTACHEMENT_RPC_CLIENT__ROLE_FOURIE_RC:
				return roleFourie_RC != null;
		}
		return super.eIsSet(featureID);
	}

} //AttachementRPCClientImpl
