/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Requie DB 1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Port_Requie_DB_1#getAttachement_DB_1 <em>Attachement DB 1</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getPort_Requie_DB_1()
 * @model
 * @generated
 */
public interface Port_Requie_DB_1 extends EObject {
	/**
	 * Returns the value of the '<em><b>Attachement DB 1</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Atachement_DB_1#getPort_requie_DB_1 <em>Port requie DB 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attachement DB 1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attachement DB 1</em>' reference.
	 * @see #setAttachement_DB_1(Atachement_DB_1)
	 * @see M1.M1Package#getPort_Requie_DB_1_Attachement_DB_1()
	 * @see M1.Atachement_DB_1#getPort_requie_DB_1
	 * @model opposite="port_requie_DB_1"
	 * @generated
	 */
	Atachement_DB_1 getAttachement_DB_1();

	/**
	 * Sets the value of the '{@link M1.Port_Requie_DB_1#getAttachement_DB_1 <em>Attachement DB 1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attachement DB 1</em>' reference.
	 * @see #getAttachement_DB_1()
	 * @generated
	 */
	void setAttachement_DB_1(Atachement_DB_1 value);

} // Port_Requie_DB_1
