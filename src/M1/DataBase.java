/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Base</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.DataBase#getIdataBase <em>Idata Base</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getDataBase()
 * @model
 * @generated
 */
public interface DataBase extends EObject {
	/**
	 * Returns the value of the '<em><b>Idata Base</b></em>' containment reference list.
	 * The list contents are of type {@link M1.IDataBase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Idata Base</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Idata Base</em>' containment reference list.
	 * @see M1.M1Package#getDataBase_IdataBase()
	 * @model type="M1.IDataBase" containment="true"
	 * @generated
	 */
	EList getIdataBase();

} // DataBase
