/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Requie SR</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.PortRequie_SR#getAttachement_SR <em>Attachement SR</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getPortRequie_SR()
 * @model
 * @generated
 */
public interface PortRequie_SR extends EObject {
	/**
	 * Returns the value of the '<em><b>Attachement SR</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.AttachementServeurRPC#getEReference1 <em>EReference1</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attachement SR</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attachement SR</em>' reference.
	 * @see #setAttachement_SR(AttachementServeurRPC)
	 * @see M1.M1Package#getPortRequie_SR_Attachement_SR()
	 * @see M1.AttachementServeurRPC#getEReference1
	 * @model opposite="EReference1"
	 * @generated
	 */
	AttachementServeurRPC getAttachement_SR();

	/**
	 * Sets the value of the '{@link M1.PortRequie_SR#getAttachement_SR <em>Attachement SR</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attachement SR</em>' reference.
	 * @see #getAttachement_SR()
	 * @generated
	 */
	void setAttachement_SR(AttachementServeurRPC value);

} // PortRequie_SR
