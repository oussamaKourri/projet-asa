/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gestion Securiterity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Gestion_Securiterity#getIgestionSecurite <em>Igestion Securite</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getGestion_Securiterity()
 * @model
 * @generated
 */
public interface Gestion_Securiterity extends EObject {
	/**
	 * Returns the value of the '<em><b>Igestion Securite</b></em>' containment reference list.
	 * The list contents are of type {@link M1.IGestion_Securite}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Igestion Securite</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Igestion Securite</em>' containment reference list.
	 * @see M1.M1Package#getGestion_Securiterity_IgestionSecurite()
	 * @model type="M1.IGestion_Securite" containment="true"
	 * @generated
	 */
	EList getIgestionSecurite();

} // Gestion_Securiterity
