/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IConnecteur GS DB</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.IConnecteur_GS_DB#getRole_fournie_DB_3 <em>Role fournie DB 3</em>}</li>
 *   <li>{@link M1.IConnecteur_GS_DB#getRole_requie_DB_4 <em>Role requie DB 4</em>}</li>
 *   <li>{@link M1.IConnecteur_GS_DB#getRole_fournie_GS_4 <em>Role fournie GS 4</em>}</li>
 *   <li>{@link M1.IConnecteur_GS_DB#getRole_requie_GS_3 <em>Role requie GS 3</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getIConnecteur_GS_DB()
 * @model
 * @generated
 */
public interface IConnecteur_GS_DB extends EObject {
	/**
	 * Returns the value of the '<em><b>Role fournie DB 3</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Role_fournie_DB_3}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role fournie DB 3</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role fournie DB 3</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteur_GS_DB_Role_fournie_DB_3()
	 * @model type="M1.Role_fournie_DB_3" containment="true"
	 * @generated
	 */
	EList getRole_fournie_DB_3();

	/**
	 * Returns the value of the '<em><b>Role requie DB 4</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Role_Requie_DB_4}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role requie DB 4</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role requie DB 4</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteur_GS_DB_Role_requie_DB_4()
	 * @model type="M1.Role_Requie_DB_4" containment="true"
	 * @generated
	 */
	EList getRole_requie_DB_4();

	/**
	 * Returns the value of the '<em><b>Role fournie GS 4</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Role_Fournie_GS_4}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role fournie GS 4</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role fournie GS 4</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteur_GS_DB_Role_fournie_GS_4()
	 * @model type="M1.Role_Fournie_GS_4" containment="true"
	 * @generated
	 */
	EList getRole_fournie_GS_4();

	/**
	 * Returns the value of the '<em><b>Role requie GS 3</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Role_Requie_GS_3}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role requie GS 3</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role requie GS 3</em>' containment reference list.
	 * @see M1.M1Package#getIConnecteur_GS_DB_Role_requie_GS_3()
	 * @model type="M1.Role_Requie_GS_3" containment="true"
	 * @generated
	 */
	EList getRole_requie_GS_3();

} // IConnecteur_GS_DB
