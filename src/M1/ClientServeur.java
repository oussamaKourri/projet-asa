/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Client Serveur</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.ClientServeur#getClient <em>Client</em>}</li>
 *   <li>{@link M1.ClientServeur#getConnecteurRPC <em>Connecteur RPC</em>}</li>
 *   <li>{@link M1.ClientServeur#getServeur <em>Serveur</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getClientServeur()
 * @model
 * @generated
 */
public interface ClientServeur extends EObject {
	/**
	 * Returns the value of the '<em><b>Client</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Client}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Client</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Client</em>' containment reference list.
	 * @see M1.M1Package#getClientServeur_Client()
	 * @model type="M1.Client" containment="true"
	 * @generated
	 */
	EList getClient();

	/**
	 * Returns the value of the '<em><b>Connecteur RPC</b></em>' containment reference list.
	 * The list contents are of type {@link M1.ConnecteurRPC}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connecteur RPC</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connecteur RPC</em>' containment reference list.
	 * @see M1.M1Package#getClientServeur_ConnecteurRPC()
	 * @model type="M1.ConnecteurRPC" containment="true"
	 * @generated
	 */
	EList getConnecteurRPC();

	/**
	 * Returns the value of the '<em><b>Serveur</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Serveur}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Serveur</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Serveur</em>' containment reference list.
	 * @see M1.M1Package#getClientServeur_Serveur()
	 * @model type="M1.Serveur" containment="true"
	 * @generated
	 */
	EList getServeur();

} // ClientServeur
