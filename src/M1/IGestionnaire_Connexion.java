/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IGestionnaire Connexion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.IGestionnaire_Connexion#getPort_Fournie_Gc_1 <em>Port Fournie Gc 1</em>}</li>
 *   <li>{@link M1.IGestionnaire_Connexion#getPort_Requie_Gc_2 <em>Port Requie Gc 2</em>}</li>
 *   <li>{@link M1.IGestionnaire_Connexion#getPort_Fournie_Gc_3 <em>Port Fournie Gc 3</em>}</li>
 *   <li>{@link M1.IGestionnaire_Connexion#getPort_Requie_Gc_4 <em>Port Requie Gc 4</em>}</li>
 *   <li>{@link M1.IGestionnaire_Connexion#getPort_CM_S <em>Port CM S</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getIGestionnaire_Connexion()
 * @model
 * @generated
 */
public interface IGestionnaire_Connexion extends EObject {
	/**
	 * Returns the value of the '<em><b>Port Fournie Gc 1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Fournie Gc 1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Fournie Gc 1</em>' containment reference.
	 * @see #setPort_Fournie_Gc_1(Port_Fournie_GC_1)
	 * @see M1.M1Package#getIGestionnaire_Connexion_Port_Fournie_Gc_1()
	 * @model containment="true"
	 * @generated
	 */
	Port_Fournie_GC_1 getPort_Fournie_Gc_1();

	/**
	 * Sets the value of the '{@link M1.IGestionnaire_Connexion#getPort_Fournie_Gc_1 <em>Port Fournie Gc 1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Fournie Gc 1</em>' containment reference.
	 * @see #getPort_Fournie_Gc_1()
	 * @generated
	 */
	void setPort_Fournie_Gc_1(Port_Fournie_GC_1 value);

	/**
	 * Returns the value of the '<em><b>Port Requie Gc 2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Requie Gc 2</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Requie Gc 2</em>' containment reference.
	 * @see #setPort_Requie_Gc_2(Port_Requie_GC_2)
	 * @see M1.M1Package#getIGestionnaire_Connexion_Port_Requie_Gc_2()
	 * @model containment="true"
	 * @generated
	 */
	Port_Requie_GC_2 getPort_Requie_Gc_2();

	/**
	 * Sets the value of the '{@link M1.IGestionnaire_Connexion#getPort_Requie_Gc_2 <em>Port Requie Gc 2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Requie Gc 2</em>' containment reference.
	 * @see #getPort_Requie_Gc_2()
	 * @generated
	 */
	void setPort_Requie_Gc_2(Port_Requie_GC_2 value);

	/**
	 * Returns the value of the '<em><b>Port Fournie Gc 3</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Fournie Gc 3</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Fournie Gc 3</em>' containment reference.
	 * @see #setPort_Fournie_Gc_3(Port_Fournie_GC_3)
	 * @see M1.M1Package#getIGestionnaire_Connexion_Port_Fournie_Gc_3()
	 * @model containment="true"
	 * @generated
	 */
	Port_Fournie_GC_3 getPort_Fournie_Gc_3();

	/**
	 * Sets the value of the '{@link M1.IGestionnaire_Connexion#getPort_Fournie_Gc_3 <em>Port Fournie Gc 3</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Fournie Gc 3</em>' containment reference.
	 * @see #getPort_Fournie_Gc_3()
	 * @generated
	 */
	void setPort_Fournie_Gc_3(Port_Fournie_GC_3 value);

	/**
	 * Returns the value of the '<em><b>Port Requie Gc 4</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Requie Gc 4</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Requie Gc 4</em>' containment reference.
	 * @see #setPort_Requie_Gc_4(Port_Requie_GC_4)
	 * @see M1.M1Package#getIGestionnaire_Connexion_Port_Requie_Gc_4()
	 * @model containment="true"
	 * @generated
	 */
	Port_Requie_GC_4 getPort_Requie_Gc_4();

	/**
	 * Sets the value of the '{@link M1.IGestionnaire_Connexion#getPort_Requie_Gc_4 <em>Port Requie Gc 4</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Requie Gc 4</em>' containment reference.
	 * @see #getPort_Requie_Gc_4()
	 * @generated
	 */
	void setPort_Requie_Gc_4(Port_Requie_GC_4 value);

	/**
	 * Returns the value of the '<em><b>Port CM S</b></em>' containment reference list.
	 * The list contents are of type {@link M1.Port_CM_S}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port CM S</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port CM S</em>' containment reference list.
	 * @see M1.M1Package#getIGestionnaire_Connexion_Port_CM_S()
	 * @model type="M1.Port_CM_S" containment="true"
	 * @generated
	 */
	EList getPort_CM_S();

} // IGestionnaire_Connexion
