/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port RPC S</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Port_RPC_S#getEReference0 <em>EReference0</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getPort_RPC_S()
 * @model
 * @generated
 */
public interface Port_RPC_S extends EObject {
	/**
	 * Returns the value of the '<em><b>EReference0</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Binding_S_RPC#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference0</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference0</em>' reference.
	 * @see #setEReference0(Binding_S_RPC)
	 * @see M1.M1Package#getPort_RPC_S_EReference0()
	 * @see M1.Binding_S_RPC#getEReference0
	 * @model opposite="EReference0"
	 * @generated
	 */
	Binding_S_RPC getEReference0();

	/**
	 * Sets the value of the '{@link M1.Port_RPC_S#getEReference0 <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference0</em>' reference.
	 * @see #getEReference0()
	 * @generated
	 */
	void setEReference0(Binding_S_RPC value);

} // Port_RPC_S
