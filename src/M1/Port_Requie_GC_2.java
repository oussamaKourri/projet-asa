/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Requie GC 2</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Port_Requie_GC_2#getEReference0 <em>EReference0</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getPort_Requie_GC_2()
 * @model
 * @generated
 */
public interface Port_Requie_GC_2 extends EObject {
	/**
	 * Returns the value of the '<em><b>EReference0</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Attachement_GC_2#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference0</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference0</em>' reference.
	 * @see #setEReference0(Attachement_GC_2)
	 * @see M1.M1Package#getPort_Requie_GC_2_EReference0()
	 * @see M1.Attachement_GC_2#getEReference0
	 * @model opposite="EReference0"
	 * @generated
	 */
	Attachement_GC_2 getEReference0();

	/**
	 * Sets the value of the '{@link M1.Port_Requie_GC_2#getEReference0 <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference0</em>' reference.
	 * @see #getEReference0()
	 * @generated
	 */
	void setEReference0(Attachement_GC_2 value);

} // Port_Requie_GC_2
