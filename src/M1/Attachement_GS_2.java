/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attachement GS 2</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Attachement_GS_2#getPort_requie_GS_2 <em>Port requie GS 2</em>}</li>
 *   <li>{@link M1.Attachement_GS_2#getRole_fournie_GS_2 <em>Role fournie GS 2</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getAttachement_GS_2()
 * @model
 * @generated
 */
public interface Attachement_GS_2 extends EObject {
	/**
	 * Returns the value of the '<em><b>Port requie GS 2</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Port_Requie_GS_2#getAttachement_GS_2 <em>Attachement GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port requie GS 2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port requie GS 2</em>' reference.
	 * @see #setPort_requie_GS_2(Port_Requie_GS_2)
	 * @see M1.M1Package#getAttachement_GS_2_Port_requie_GS_2()
	 * @see M1.Port_Requie_GS_2#getAttachement_GS_2
	 * @model opposite="attachement_GS_2"
	 * @generated
	 */
	Port_Requie_GS_2 getPort_requie_GS_2();

	/**
	 * Sets the value of the '{@link M1.Attachement_GS_2#getPort_requie_GS_2 <em>Port requie GS 2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port requie GS 2</em>' reference.
	 * @see #getPort_requie_GS_2()
	 * @generated
	 */
	void setPort_requie_GS_2(Port_Requie_GS_2 value);

	/**
	 * Returns the value of the '<em><b>Role fournie GS 2</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Role_Fournie_GS_2#getAttachement_GS_2 <em>Attachement GS 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role fournie GS 2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role fournie GS 2</em>' reference.
	 * @see #setRole_fournie_GS_2(Role_Fournie_GS_2)
	 * @see M1.M1Package#getAttachement_GS_2_Role_fournie_GS_2()
	 * @see M1.Role_Fournie_GS_2#getAttachement_GS_2
	 * @model opposite="attachement_GS_2"
	 * @generated
	 */
	Role_Fournie_GS_2 getRole_fournie_GS_2();

	/**
	 * Sets the value of the '{@link M1.Attachement_GS_2#getRole_fournie_GS_2 <em>Role fournie GS 2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role fournie GS 2</em>' reference.
	 * @see #getRole_fournie_GS_2()
	 * @generated
	 */
	void setRole_fournie_GS_2(Role_Fournie_GS_2 value);

} // Attachement_GS_2
