/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IConnecteur GC DB</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.IConnecteur_GC_DB#getEReference0 <em>EReference0</em>}</li>
 *   <li>{@link M1.IConnecteur_GC_DB#getEReference1 <em>EReference1</em>}</li>
 *   <li>{@link M1.IConnecteur_GC_DB#getEReference2 <em>EReference2</em>}</li>
 *   <li>{@link M1.IConnecteur_GC_DB#getEReference3 <em>EReference3</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getIConnecteur_GC_DB()
 * @model
 * @generated
 */
public interface IConnecteur_GC_DB extends EObject {
	/**
	 * Returns the value of the '<em><b>EReference0</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference0</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference0</em>' containment reference.
	 * @see #setEReference0(Role_fournie_DB_1)
	 * @see M1.M1Package#getIConnecteur_GC_DB_EReference0()
	 * @model containment="true"
	 * @generated
	 */
	Role_fournie_DB_1 getEReference0();

	/**
	 * Sets the value of the '{@link M1.IConnecteur_GC_DB#getEReference0 <em>EReference0</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference0</em>' containment reference.
	 * @see #getEReference0()
	 * @generated
	 */
	void setEReference0(Role_fournie_DB_1 value);

	/**
	 * Returns the value of the '<em><b>EReference1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference1</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference1</em>' containment reference.
	 * @see #setEReference1(Role_Requie_DB_2)
	 * @see M1.M1Package#getIConnecteur_GC_DB_EReference1()
	 * @model containment="true"
	 * @generated
	 */
	Role_Requie_DB_2 getEReference1();

	/**
	 * Sets the value of the '{@link M1.IConnecteur_GC_DB#getEReference1 <em>EReference1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference1</em>' containment reference.
	 * @see #getEReference1()
	 * @generated
	 */
	void setEReference1(Role_Requie_DB_2 value);

	/**
	 * Returns the value of the '<em><b>EReference2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference2</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference2</em>' containment reference.
	 * @see #setEReference2(Role_Requie_GC_1)
	 * @see M1.M1Package#getIConnecteur_GC_DB_EReference2()
	 * @model containment="true"
	 * @generated
	 */
	Role_Requie_GC_1 getEReference2();

	/**
	 * Sets the value of the '{@link M1.IConnecteur_GC_DB#getEReference2 <em>EReference2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference2</em>' containment reference.
	 * @see #getEReference2()
	 * @generated
	 */
	void setEReference2(Role_Requie_GC_1 value);

	/**
	 * Returns the value of the '<em><b>EReference3</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference3</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference3</em>' containment reference.
	 * @see #setEReference3(Role_Fournie_GC_2)
	 * @see M1.M1Package#getIConnecteur_GC_DB_EReference3()
	 * @model containment="true"
	 * @generated
	 */
	Role_Fournie_GC_2 getEReference3();

	/**
	 * Sets the value of the '{@link M1.IConnecteur_GC_DB#getEReference3 <em>EReference3</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference3</em>' containment reference.
	 * @see #getEReference3()
	 * @generated
	 */
	void setEReference3(Role_Fournie_GC_2 value);

} // IConnecteur_GC_DB
