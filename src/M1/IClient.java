/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IClient</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.IClient#getSendRequest <em>Send Request</em>}</li>
 *   <li>{@link M1.IClient#getReceveRequest <em>Receve Request</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getIClient()
 * @model
 * @generated
 */
public interface IClient extends EObject {
	/**
	 * Returns the value of the '<em><b>Send Request</b></em>' containment reference list.
	 * The list contents are of type {@link M1.SendRequest}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Send Request</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Send Request</em>' containment reference list.
	 * @see M1.M1Package#getIClient_SendRequest()
	 * @model type="M1.SendRequest" containment="true"
	 * @generated
	 */
	EList getSendRequest();

	/**
	 * Returns the value of the '<em><b>Receve Request</b></em>' containment reference list.
	 * The list contents are of type {@link M1.ReceveResponse}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receve Request</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receve Request</em>' containment reference list.
	 * @see M1.M1Package#getIClient_ReceveRequest()
	 * @model type="M1.ReceveResponse" containment="true"
	 * @generated
	 */
	EList getReceveRequest();

} // IClient
