/**
 */
package M1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connecteur GS DB</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Connecteur_GS_DB#getIconnecteur_GS_DB <em>Iconnecteur GS DB</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getConnecteur_GS_DB()
 * @model
 * @generated
 */
public interface Connecteur_GS_DB extends EObject {
	/**
	 * Returns the value of the '<em><b>Iconnecteur GS DB</b></em>' containment reference list.
	 * The list contents are of type {@link M1.IConnecteur_GS_DB}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iconnecteur GS DB</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iconnecteur GS DB</em>' containment reference list.
	 * @see M1.M1Package#getConnecteur_GS_DB_Iconnecteur_GS_DB()
	 * @model type="M1.IConnecteur_GS_DB" containment="true"
	 * @generated
	 */
	EList getIconnecteur_GS_DB();

} // Connecteur_GS_DB
