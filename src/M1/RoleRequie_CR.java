/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role Requie CR</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.RoleRequie_CR#getAttachement_CR <em>Attachement CR</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getRoleRequie_CR()
 * @model
 * @generated
 */
public interface RoleRequie_CR extends EObject {
	/**
	 * Returns the value of the '<em><b>Attachement CR</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attachement CR</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attachement CR</em>' reference.
	 * @see #setAttachement_CR(AttachementClientRPC)
	 * @see M1.M1Package#getRoleRequie_CR_Attachement_CR()
	 * @model
	 * @generated
	 */
	AttachementClientRPC getAttachement_CR();

	/**
	 * Sets the value of the '{@link M1.RoleRequie_CR#getAttachement_CR <em>Attachement CR</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attachement CR</em>' reference.
	 * @see #getAttachement_CR()
	 * @generated
	 */
	void setAttachement_CR(AttachementClientRPC value);

} // RoleRequie_CR
