/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attachement DB 4</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Attachement_DB_4#getRole_Requie_4 <em>Role Requie 4</em>}</li>
 *   <li>{@link M1.Attachement_DB_4#getPort_fourne_4 <em>Port fourne 4</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getAttachement_DB_4()
 * @model
 * @generated
 */
public interface Attachement_DB_4 extends EObject {
	/**
	 * Returns the value of the '<em><b>Role Requie 4</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Role_Requie_DB_4#getAttachement_DB_4 <em>Attachement DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Requie 4</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Requie 4</em>' reference.
	 * @see #setRole_Requie_4(Role_Requie_DB_4)
	 * @see M1.M1Package#getAttachement_DB_4_Role_Requie_4()
	 * @see M1.Role_Requie_DB_4#getAttachement_DB_4
	 * @model opposite="attachement_DB_4"
	 * @generated
	 */
	Role_Requie_DB_4 getRole_Requie_4();

	/**
	 * Sets the value of the '{@link M1.Attachement_DB_4#getRole_Requie_4 <em>Role Requie 4</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role Requie 4</em>' reference.
	 * @see #getRole_Requie_4()
	 * @generated
	 */
	void setRole_Requie_4(Role_Requie_DB_4 value);

	/**
	 * Returns the value of the '<em><b>Port fourne 4</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Port_fornie_DB_4#getAttachement_DB_4 <em>Attachement DB 4</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port fourne 4</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port fourne 4</em>' reference.
	 * @see #setPort_fourne_4(Port_fornie_DB_4)
	 * @see M1.M1Package#getAttachement_DB_4_Port_fourne_4()
	 * @see M1.Port_fornie_DB_4#getAttachement_DB_4
	 * @model opposite="attachement_DB_4"
	 * @generated
	 */
	Port_fornie_DB_4 getPort_fourne_4();

	/**
	 * Sets the value of the '{@link M1.Attachement_DB_4#getPort_fourne_4 <em>Port fourne 4</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port fourne 4</em>' reference.
	 * @see #getPort_fourne_4()
	 * @generated
	 */
	void setPort_fourne_4(Port_fornie_DB_4 value);

} // Attachement_DB_4
