/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binding SRPC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Binding_S_RPC#getEReference0 <em>EReference0</em>}</li>
 *   <li>{@link M1.Binding_S_RPC#getEReference1 <em>EReference1</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getBinding_S_RPC()
 * @model
 * @generated
 */
public interface Binding_S_RPC extends EObject {
	/**
	 * Returns the value of the '<em><b>EReference0</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Port_RPC_S#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference0</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference0</em>' reference.
	 * @see #setEReference0(Port_RPC_S)
	 * @see M1.M1Package#getBinding_S_RPC_EReference0()
	 * @see M1.Port_RPC_S#getEReference0
	 * @model opposite="EReference0"
	 * @generated
	 */
	Port_RPC_S getEReference0();

	/**
	 * Sets the value of the '{@link M1.Binding_S_RPC#getEReference0 <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference0</em>' reference.
	 * @see #getEReference0()
	 * @generated
	 */
	void setEReference0(Port_RPC_S value);

	/**
	 * Returns the value of the '<em><b>EReference1</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Port_S_RPC#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference1</em>' reference.
	 * @see #setEReference1(Port_S_RPC)
	 * @see M1.M1Package#getBinding_S_RPC_EReference1()
	 * @see M1.Port_S_RPC#getEReference0
	 * @model opposite="EReference0"
	 * @generated
	 */
	Port_S_RPC getEReference1();

	/**
	 * Sets the value of the '{@link M1.Binding_S_RPC#getEReference1 <em>EReference1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference1</em>' reference.
	 * @see #getEReference1()
	 * @generated
	 */
	void setEReference1(Port_S_RPC value);

} // Binding_S_RPC
