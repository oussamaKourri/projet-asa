/**
 */
package M1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attachement GC 4</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link M1.Attachement_GC_4#getEReference0 <em>EReference0</em>}</li>
 *   <li>{@link M1.Attachement_GC_4#getEReference1 <em>EReference1</em>}</li>
 * </ul>
 * </p>
 *
 * @see M1.M1Package#getAttachement_GC_4()
 * @model
 * @generated
 */
public interface Attachement_GC_4 extends EObject {
	/**
	 * Returns the value of the '<em><b>EReference0</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Role_Fournie_GC_4#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference0</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference0</em>' reference.
	 * @see #setEReference0(Role_Fournie_GC_4)
	 * @see M1.M1Package#getAttachement_GC_4_EReference0()
	 * @see M1.Role_Fournie_GC_4#getEReference0
	 * @model opposite="EReference0"
	 * @generated
	 */
	Role_Fournie_GC_4 getEReference0();

	/**
	 * Sets the value of the '{@link M1.Attachement_GC_4#getEReference0 <em>EReference0</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference0</em>' reference.
	 * @see #getEReference0()
	 * @generated
	 */
	void setEReference0(Role_Fournie_GC_4 value);

	/**
	 * Returns the value of the '<em><b>EReference1</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link M1.Port_Requie_GC_4#getEReference0 <em>EReference0</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EReference1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EReference1</em>' reference.
	 * @see #setEReference1(Port_Requie_GC_4)
	 * @see M1.M1Package#getAttachement_GC_4_EReference1()
	 * @see M1.Port_Requie_GC_4#getEReference0
	 * @model opposite="EReference0"
	 * @generated
	 */
	Port_Requie_GC_4 getEReference1();

	/**
	 * Sets the value of the '{@link M1.Attachement_GC_4#getEReference1 <em>EReference1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EReference1</em>' reference.
	 * @see #getEReference1()
	 * @generated
	 */
	void setEReference1(Port_Requie_GC_4 value);

} // Attachement_GC_4
