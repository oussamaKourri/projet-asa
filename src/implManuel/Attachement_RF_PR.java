package implManuel;

public class Attachement_RF_PR {
	private RoleFourni roleFourni;
	private PortRequis portRequis;
	

	public RoleFourni getRoleFourni() {
		return roleFourni;
	}

	public void setRoleFourni(RoleFourni roleFourni) {
		this.roleFourni = roleFourni;
	}

	public PortRequis getPortRequis() {
		return portRequis;
	}

	public void setPortRequis(PortRequis portRequis) {
		this.portRequis = portRequis;
	}

	public Attachement_RF_PR(RoleFourni portFourni, PortRequis portRequis) {
		super();
		this.roleFourni = portFourni;
		this.portRequis = portRequis;
	}
	
	public Attachement_RF_PR() {
		super();
		// TODO Auto-generated constructor stub
		this.roleFourni = new RoleFourni();
		this.portRequis = new PortRequis();
	}
	
	public void transmit() {
		// Todo
	}
}
