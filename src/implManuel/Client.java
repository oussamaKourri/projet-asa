package implManuel;


public class Client implements Observable{
	private PortFourni portFourni;
	private PortRequis portRequis;
	
	private Observateur obs;
	
	public PortFourni getPortFourni() {
		return portFourni;
	}
	public void setPortFourni(PortFourni portFourni) {
		this.portFourni = portFourni;
	}
	public PortRequis getPortRequis() {
		return portRequis;
	}
	public void setPortRequis(PortRequis portRequis) {
		this.portRequis = portRequis;
	}
	public Client(PortFourni portFourni, PortRequis portRequis) {
		super();
		this.portFourni = portFourni;
		this.portRequis = portRequis;
	}
	
	public Client() {
		this.portFourni = new PortFourni();
		this.portRequis = new PortRequis();
	}
	
	public void sendRequest()
	{
		
	}
	
	public void receiveRequest()
	{
			
	}
	
	
	public void addObservateur(Observateur o) {
		
	}
	
	public void removeObservateur(Observateur o) {
				
	}
	
	public void notifyObservateurs() {

	}
	
}
