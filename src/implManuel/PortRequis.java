package implManuel;

public class PortRequis{
	private String messageAtransmettre;
		
	public PortRequis() {
		super();
		this.messageAtransmettre = "";
	}

	public String getMessageAtransmettre() {
		return messageAtransmettre;
	}

	public void setMessageAtransmettre(String messageAtransmettre) {
		this.messageAtransmettre = messageAtransmettre;
	}
	
}
