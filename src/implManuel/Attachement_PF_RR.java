package implManuel;


public class Attachement_PF_RR{
	private PortFourni portFourni;
	private RoleRequis roleRequis;
	

	public PortFourni getPortFourni() {
		return portFourni;
	}

	public void setPortFourni(PortFourni portFourni) {
		this.portFourni = portFourni;
	}

	public RoleRequis getRoleRequis() {
		return roleRequis;
	}

	public void setRoleRequis(RoleRequis roleRequis) {
		this.roleRequis = roleRequis;
	}

	public Attachement_PF_RR(PortFourni portFourni, RoleRequis roleRequis) {
		super();
		this.portFourni = portFourni;
		this.roleRequis = roleRequis;
	}
	
	public Attachement_PF_RR(){
		this.portFourni = new PortFourni();
		this.roleRequis = new RoleRequis();
	}
	
	public void transmit() {
		// Todo comment transmettre un message
	}
}
