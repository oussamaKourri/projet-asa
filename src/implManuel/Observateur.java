package implManuel;

public interface Observateur
{
     
     public void notifier();

}