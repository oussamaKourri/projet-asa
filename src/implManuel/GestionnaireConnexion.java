package implManuel;

public class GestionnaireConnexion {
	

	private PortRequis portRequis1;
	private PortFourni porFournie1;
	
	private PortRequis portRequis2;
	private PortFourni porFournie2;
		

	public GestionnaireConnexion() {
		super();
		this.portRequis1 = new PortRequis();
		this.porFournie1 = new PortFourni();
		this.portRequis2 = new PortRequis();
		this.porFournie2 = new PortFourni();
	}
	

	// lieaison avec gestionnaire de securite
	public void transmitToConnectorGestionnaireSecurite()
	{
		
	}
	
	// liaison avec conneteur avec la database
	public void transmiteToConnectorDataBase()
	{
		
	}
	
	// relie a RPC
	public void transmitToExternalSocket()
	{
		
	}
	
}
