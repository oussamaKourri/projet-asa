package implManuel;
public class PortFourni {
	private String messageAtransmettre;

	public PortFourni() {
		super();
		this.messageAtransmettre = "";
	}

	public String getMessageAtransmettre() {
		return messageAtransmettre;
	}

	public void setMessageAtransmettre(String messageAtransmettre) {
		this.messageAtransmettre = messageAtransmettre;
	}

}
