package implManuel;

public class ConnectorDBGS {

	private RoleRequis roleRequisSecurity;
	private RoleFourni roleFourniSecurity;
	
	private RoleRequis roleRequisDataBase;
	private RoleFourni roleFourniDataBase;
	

	public RoleRequis getRoleRequisSecurity() {
		return roleRequisSecurity;
	}

	public void setRoleRequisSecurity(RoleRequis roleRequisSecurity) {
		this.roleRequisSecurity = roleRequisSecurity;
	}

	public RoleFourni getRoleFourniSecurity() {
		return roleFourniSecurity;
	}

	public void setRoleFourniSecurity(RoleFourni roleFourniSecurity) {
		this.roleFourniSecurity = roleFourniSecurity;
	}

	public RoleRequis getRoleRequisDataBase() {
		return roleRequisDataBase;
	}

	public void setRoleRequisDataBase(RoleRequis roleRequisDataBase) {
		this.roleRequisDataBase = roleRequisDataBase;
	}

	public RoleFourni getRoleFourniDataBase() {
		return roleFourniDataBase;
	}

	public void setRoleFourniDataBase(RoleFourni roleFourniDataBase) {
		this.roleFourniDataBase = roleFourniDataBase;
	}

	public void transmettreToDataBase()
	{
		
	}
	
	public void transmettreToSecurity()
	{
		
	}

	public ConnectorDBGS() {
		super();
		this.roleRequisSecurity = new RoleRequis();
		this.roleFourniSecurity = new RoleFourni();
		this.roleRequisDataBase = new RoleRequis();
		this.roleFourniDataBase = new RoleFourni();
	}
}
