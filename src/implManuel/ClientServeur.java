package implManuel;
public class ClientServeur implements Observateur {
	private Server server;
	private RPCConnecteur RPCConnector;
	private Client client;
	private Attachement_PF_RR attachementClientRPC;
	private Attachement_RF_PR attachementRPCServeur;
	private Attachement_PF_RR attachementServeurRPC;
	private Attachement_RF_PR attachementRPCClient;
	
	private Binding bindToServConf;
	private Binding bindFromServConfToServ;
	
	public ClientServeur() {
		super();
		//Creation du modèle Client-Server

		//Client
		client = new Client();
		
		//RPCConnecteur
		RPCConnector = new RPCConnecteur();		
		
		//Serveur
		server = new Server();
		
		
			//Client -> RPCConnector
		attachementClientRPC = new Attachement_PF_RR();	
		attachementClientRPC.setPortFourni(client.getPortFourni());
		attachementClientRPC.setRoleRequis(RPCConnector.getroleRequis1());
		
		//RPCCOnnector -> Client
		attachementRPCClient = new Attachement_RF_PR();
		attachementRPCClient.setRoleFourni(RPCConnector.getroleFourie1());
		attachementRPCClient.setPortRequis(client.getPortRequis());
			
		//RPCConnector -> Serveur
		attachementRPCServeur = new Attachement_RF_PR();	
		attachementRPCServeur.setRoleFourni(RPCConnector.getRoleFournie2());
		attachementRPCServeur.setPortRequis(server.getPortRequis());
		
		//Serveur -> RPCConnector
		attachementServeurRPC = new Attachement_PF_RR();
		attachementServeurRPC.setPortFourni(server.getPortFourniToRPC());
		attachementServeurRPC.setRoleRequis(RPCConnector.getroleRequis2());
		
		
		
	}
	

	public void notifier() {
	
	}
	
	public void bind()
	{
		//
	}
	public void run(){
		
	}

	
}
